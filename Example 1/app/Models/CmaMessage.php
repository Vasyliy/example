<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @OA\Schema(
 *      required={"uuid", "lid", "player_id", "token", "content"},
 *      @OA\Xml(
 *          name="CmaMessage"
 *      ),
 *      @OA\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @OA\Property(
 *          property="lid",
 *          description="lid",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @OA\Property(
 *          property="player_id",
 *          description="player_id",
 *          type="string",
 *          example="player"
 *      ),
 *      @OA\Property(
 *          property="uuid",
 *          description="uuid",
 *          type="string",
 *          example="a407b0ce-f306-11e8-8eb2-f2801f1b9fd1"
 *      ),
 *      @OA\Property(
 *          property="token",
 *          description="token",
 *          type="string",
 *          example="d75d26a662397ed4ef5a8100778ada99"
 *      ),
 *      @OA\Property(
 *          property="content",
 *          description="content",
 *          type="string",
 *          example="Session limit time expired."
 *      ),
 *      @OA\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @OA\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 *
 * @OA\Schema(
 *   schema="tokenRequired",
 *   type="string",
 *   example="The token field is required."
 * )
 * @OA\Schema(
 *   schema="tokenString",
 *   type="string",
 *   example="The token must be a string."
 * )
 * @OA\Schema(
 *   schema="tokenMax",
 *   type="string",
 *   example="The token may not be greater than 128 characters."
 * )
 * @OA\Schema(
 *   schema="uuidRequired",
 *   type="string",
 *   example="The uuid field is required."
 * )
 * @OA\Schema(
 *   schema="uuidString",
 *   type="string",
 *   example="The uuid must be a string."
 * )
 * @OA\Schema(
 *   schema="uuidMax",
 *   type="string",
 *   example="The uuid may not be greater than 36 characters."
 * )
 * @OA\Schema(
 *   schema="playerIdRequired",
 *   type="string",
 *   example="The player id field is required."
 * )
 * @OA\Schema(
 *   schema="playerIdString",
 *   type="string",
 *   example="The player id must be a string."
 * )
 * @OA\Schema(
 *   schema="playerIdMax",
 *   type="string",
 *   example="The player id may not be greater than 32 characters."
 * )
 * @OA\Schema(
 *   schema="contentRequired",
 *   type="string",
 *   example="The content field is required."
 * )
 * @OA\Schema(
 *   schema="contentString",
 *   type="string",
 *   example="The content must be a string."
 * )
 */
class CmaMessage extends Model
{
    use SoftDeletes;

    public $table = 'cma_messages';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'uuid',
        'lid',
        'player_id',
        'token',
        'content'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'uuid' => 'string',
        'lid' => 'integer',
        'player_id' => 'string',
        'token' => 'string',
        'content' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'uuid' => 'required|string|max:36',
        'lid' => 'required|numeric|min:1|max:4294967295',
        'player_id' => 'required|string|max:32',
        'token' => 'required|string|max:128',
        'content' => 'required|string'
    ];

    
}
