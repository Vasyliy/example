<?php

namespace App\Repositories;

use App\Models\CmaMessage;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class CmaMessageRepository
 * @package App\Repositories
 * @version November 21, 2018, 2:17 pm UTC
 *
 * @method CmaMessage findWithoutFail($id, $columns = ['*'])
 * @method CmaMessage find($id, $columns = ['*'])
 * @method CmaMessage first($columns = ['*'])
*/
class CmaMessageRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'uuid',
        'lid',
        'player_id',
        'token',
        'content'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CmaMessage::class;
    }
}
