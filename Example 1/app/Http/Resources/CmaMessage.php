<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CmaMessage extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'lid' => $this->lid,
            'player_id' => $this->player_id,
            'uuid' => $this->uuid,
            'token' => $this->token,
            'content' => $this->content,
            'created_at' => $this->created_at->format('d.m.Y h:i'),
            'updated_at' => $this->updated_at->format('d.m.Y h:i')
        ];
    }
}
