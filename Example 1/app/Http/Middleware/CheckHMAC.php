<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Validator;

/**
 * @OA\Schema(
 *   schema="hashRequired",
 *   type="string",
 *   example="The hash is required."
 * )
 * @OA\Schema(
 *   schema="hashString",
 *   type="string",
 *   example="The hash must be a string."
 * )
 * @OA\Schema(
 *   schema="hashSize",
 *   type="string",
 *   example="The hash must be 64 characters."
 * )
 * @OA\Schema(
 *   schema="hashCorrect",
 *   type="string",
 *   example="The hash is incorrect."
 * )
 */
class CheckHMAC
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     *
     */
    public function handle($request, Closure $next)
    {
        $validator = Validator::make(
            [
                'hash' => $request->input('hash')
            ],
            [
                'hash' => 'required|string|size:64'
            ],
            [
                'hash.required' => 'The hash is required.',
                'hash.string' => 'The hash must be a string.',
                'hash.size' => 'The hash must be 64 characters.'
            ]
        );

        if ($validator->fails())
            return response()->json(['errors' => $validator->errors()], 401);

        $hmac = self::generateHash($request->post());

        if ($hmac === $request->input('hash'))
            return $next($request);
        else
            return response()->json(['errors' => ['hash' => ['The hash is incorrect.']]], 401);
    }

    public function generateHash($message)
    {
        return hash_hmac('SHA256', json_encode($message), env('API_KEY'));
    }
}
