<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Validator;

class CheckLicenseeId
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     *
     * @OA\Schema(
     *   schema="lidRequired",
     *   type="string",
     *   example="The X-LICENSEE-ID header is required."
     * )
     * @OA\Schema(
     *   schema="lidNumeric",
     *   type="string",
     *   example="The X-LICENSEE-ID header must be a number."
     * )
     * @OA\Schema(
     *   schema="lidMin",
     *   type="string",
     *   example="The X-LICENSEE-ID header must be at least 1."
     * )
     * @OA\Schema(
     *   schema="lidMax",
     *   type="string",
     *   example="The X-LICENSEE-ID header may not be greater than 4294967295."
     * )
     */
    public function handle($request, Closure $next)
    {
        $validator = Validator::make(
            [
                'x-licensee-id' => $request->header('x-licensee-id')
            ],
            [
                'x-licensee-id' => 'required|numeric|min:1|max:4294967295'
            ],
            [
                'x-licensee-id.required' => 'The X-LICENSEE-ID header is required.',
                'x-licensee-id.numeric' => 'The X-LICENSEE-ID header must be a number.',
                'x-licensee-id.min' => 'The X-LICENSEE-ID header must be at least 1.',
                'x-licensee-id.max' => 'The X-LICENSEE-ID header may not be greater than 4294967295.'
            ]
        );

        if ($validator->fails())
            return response()->json(['errors' => $validator->errors()], 401);

        return $next($request);
    }
}
