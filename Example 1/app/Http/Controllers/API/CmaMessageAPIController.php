<?php

namespace App\Http\Controllers\API;

use App\Criteria\CmaMessageFiltersCriteria;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\CreateCmaMessageAPIRequest;
use App\Http\Requests\API\DeleteCmaMessageAPIRequest;
use App\Http\Requests\API\GetCmaMessageAPIRequest;
use App\Http\Resources\CmaMessage as CmaMessageResource;
use App\Http\Resources\CmaMessageCollection;
use App\Models\CmaMessage;
use App\Repositories\CmaMessageRepository;
use Illuminate\Http\Request;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Class CmaMessageController
 * @package App\Http\Controllers\API
 */

class CmaMessageAPIController extends Controller
{
    /** @var  CmaMessageRepository */
    private $cmaMessageRepository;

    public function __construct(CmaMessageRepository $cmaMessageRepo)
    {
        $this->cmaMessageRepository = $cmaMessageRepo;
    }


    /**
     * @param Request $request
     * @return Response
     *
     * @OA\Get(
     *      path="/message",
     *      summary="Get a listing of the CmaMessages",
     *      tags={"CmaMessage"},
     *      description="Get CmaMessage",
     *      @OA\Parameter(
     *          name="token",
     *          in="query",
     *          description="token of CmaMessage",
     *          required=false,
     *          @OA\Schema(
     *              type="string",
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(
     *              type="array",
     *              @OA\Items(
     *                  ref="#/components/schemas/CmaMessage"
     *              )
     *          ),
     *      ),
     *      @OA\Response(
     *          response=422,
     *          description="The given data was invalid",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  @OA\Property(
     *                      property="message",
     *                      type="string",
     *                      example="The given data was invalid."
     *                  ),
     *                  @OA\Property(
     *                      property="errors",
     *                      type="object",
     *                      @OA\Property(
     *                          property="token",
     *                          type="array",
     *                          @OA\Items(
     *                              anyOf={
     *                                  @OA\Property(ref="#/components/schemas/tokenString"),
     *                                  @OA\Property(ref="#/components/schemas/tokenMax"),
     *                              },
     *                          ),
     *                      ),
     *                  ),
     *              ),
     *          )
     *      ),
     *     @OA\Response(response=400, description="Bad Request"),
     *     @OA\Response(response=404, description="Cma Message not found")
     * )
     */
    public function index(GetCmaMessageAPIRequest $request)
    {
        $this->cmaMessageRepository->pushCriteria(new RequestCriteria($request));
        $this->cmaMessageRepository->pushCriteria(new LimitOffsetCriteria($request));
        $this->cmaMessageRepository->pushCriteria(new CmaMessageFiltersCriteria($request));
        $cmaMessages = $this->cmaMessageRepository->all();

        if (!$cmaMessages->count()) {
            return response('Cma Message not found', 404);
        }

        return new CmaMessageCollection(
            $cmaMessages
        );
    }

    /**
     * @param CreateCmaMessageAPIRequest $request
     * @return Response
     *
     * @OA\Post(
     *      path="/message",
     *      summary="Store a newly created CmaMessage in storage",
     *      tags={"CmaMessage"},
     *      description="Store CmaMessage",
     *      @OA\RequestBody(
     *          description="Transaction that should be stored",
     *          required=false,
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="uuid", type="string", example="a407b0ce-f306-11e8-8eb2-f2801f1b9fd1"),
     *                  @OA\Property(property="player_id", type="string", example="player"),
     *                  @OA\Property(property="token", type="string", example="d75d26a662397ed4ef5a8100778ada99"),
     *                  @OA\Property(property="content", type="string", example="Session limit time expired.")
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Created",
     *          @OA\JsonContent(ref="#/components/schemas/CmaMessage"),
     *      ),
     *      @OA\Response(response=400, description="Bad Request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *      @OA\Response(
     *          response=422,
     *          description="The given data was invalid",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  @OA\Property(
     *                      property="message",
     *                      type="string",
     *                      example="The given data was invalid."
     *                  ),
     *                  @OA\Property(
     *                      property="errors",
     *                      type="object",
     *                      @OA\Property(
     *                          property="uuid",
     *                          type="array",
     *                          @OA\Items(
     *                              anyOf={
     *                                  @OA\Property(ref="#/components/schemas/uuidRequired"),
     *                                  @OA\Property(ref="#/components/schemas/uuidString"),
     *                                  @OA\Property(ref="#/components/schemas/uuidMax"),
     *                              },
     *                          ),
     *                      ),
     *                      @OA\Property(
     *                          property="player_id",
     *                          type="array",
     *                          @OA\Items(
     *                              anyOf={
     *                                  @OA\Property(ref="#/components/schemas/playerIdRequired"),
     *                                  @OA\Property(ref="#/components/schemas/playerIdString"),
     *                                  @OA\Property(ref="#/components/schemas/playerIdMax"),
     *                              },
     *                          ),
     *                      ),
     *                      @OA\Property(
     *                          property="token",
     *                          type="array",
     *                          @OA\Items(
     *                              anyOf={
     *                                  @OA\Property(ref="#/components/schemas/tokenRequired"),
     *                                  @OA\Property(ref="#/components/schemas/tokenString"),
     *                                  @OA\Property(ref="#/components/schemas/tokenMax"),
     *                              },
     *                          ),
     *                      ),
     *                      @OA\Property(
     *                          property="content",
     *                          type="array",
     *                          @OA\Items(
     *                              anyOf={
     *                                  @OA\Property(ref="#/components/schemas/contentRequired"),
     *                                  @OA\Property(ref="#/components/schemas/contentString"),
     *                              },
     *                          ),
     *                      ),
     *                      @OA\Property(
     *                          property="hash",
     *                          type="array",
     *                          @OA\Items(
     *                              anyOf={
     *                                  @OA\Property(ref="#/components/schemas/hashRequired"),
     *                                  @OA\Property(ref="#/components/schemas/hashString"),
     *                                  @OA\Property(ref="#/components/schemas/hashSize"),
     *                                  @OA\Property(ref="#/components/schemas/hashCorrect"),
     *                              },
     *                          ),
     *                      ),
     *                      @OA\Property(
     *                          property="x-licensee-id",
     *                          type="array",
     *                          @OA\Items(
     *                              anyOf={
     *                                  @OA\Property(ref="#/components/schemas/lidRequired"),
     *                                  @OA\Property(ref="#/components/schemas/lidNumeric"),
     *                                  @OA\Property(ref="#/components/schemas/lidMin"),
     *                                  @OA\Property(ref="#/components/schemas/lidMax"),
     *                              },
     *                          ),
     *                      ),
     *                  ),
     *              ),
     *          )
     *      ),
     *      security={
     *          {"lid": {}},
     *          {"SecretKey": {}}
     *      }
     * )
     */
    public function store(CreateCmaMessageAPIRequest $request)
    {
        $input = $request->all();
        $input['lid'] = $request->header('x-licensee-id');

        $cmaMessages = $this->cmaMessageRepository->create($input);

        return new CmaMessageResource(
            $cmaMessages
        );
    }

    /**
     * @param DeleteCmaMessageAPIRequest $request
     * @return Response
     *
     * @OA\Delete(
     *     path="/message",
     *     summary="Remove the specified CmaMessage from storage",
     *     tags={"CmaMessage"},
     *     description="Delete CmaMessage",
     *     @OA\Parameter(
     *         name="token",
     *         in="query",
     *         description="token of CmaMessage",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="d75d26a662397ed4ef5a8100778ada99"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="uuid",
     *         in="query",
     *         description="uuid of CmaMessage",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="a407b0ce-f306-11e8-8eb2-f2801f1b9fd1"
     *         )
     *     ),
     *     @OA\Response(response=204, description="Successful operation"),
     *     @OA\Response(response=400, description="Bad Request"),
     *     @OA\Response(response=404, description="Cma Message not found"),
     *     @OA\Response(
     *          response=422,
     *          description="The given data was invalid",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  @OA\Property(
     *                      property="message",
     *                      type="string",
     *                      example="The given data was invalid."
     *                  ),
     *                  @OA\Property(
     *                      property="errors",
     *                      type="object",
     *                      @OA\Property(
     *                          property="uuid",
     *                          type="array",
     *                          @OA\Items(
     *                              anyOf={
     *                                  @OA\Property(ref="#/components/schemas/uuidString"),
     *                                  @OA\Property(ref="#/components/schemas/uuidMax"),
     *                              },
     *                          ),
     *                      ),
     *                      @OA\Property(
     *                          property="token",
     *                          type="array",
     *                          @OA\Items(
     *                              anyOf={
     *                                  @OA\Property(ref="#/components/schemas/tokenString"),
     *                                  @OA\Property(ref="#/components/schemas/tokenMax"),
     *                              },
     *                          ),
     *                      ),
     *                  ),
     *              ),
     *          )
     *     )
     * )
     */
    public function destroy(DeleteCmaMessageAPIRequest $request)
    {
        $cmaMessage = false;

        if (!($request->has('token') || $request->has('uuid')))
            return response('Bad Request', 400);

        if ($request->has('token') && $request->has('uuid')) {
            $cmaMessage = CmaMessage::where('token',  $request->input('token'))
                ->where('uuid',  $request->input('uuid'))
                ->delete();
        } elseif ($request->has('token')) {
            $cmaMessage = CmaMessage::where('token',  $request->input('token'))
                ->delete();
        } elseif ($request->has('uuid')) {
            $cmaMessage = CmaMessage::where('uuid',  $request->input('uuid'))
                ->delete();
        }

        if ($cmaMessage)
            return response('Successful operation', 204);
        else
            return response('Cma Message not found', 404);
    }
}
