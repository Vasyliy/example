<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

/**
 * @OA\SecurityScheme(
 *   securityScheme="SecretKey",
 *   type="apiKey",
 *   in="query",
 *   name="hash",
 *   description="SHA256 HMAC

64 char long hexadecimal string

__Note:__ Every implementation will have an individual secret key
        provided by iSoftBet.",
 * )
 */

/**
 * @OA\SecurityScheme(
 *   securityScheme="lid",
 *   type="apiKey",
 *   in="header",
 *   name="X-LICENSEE-ID",
 *   description="Unique licensee ID",
 * )
 */

/**
 * @OA\Info(title="cma.isoftbet", version="0.1")
 */

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
