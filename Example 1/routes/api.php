<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('message', 'API\CmaMessageAPIController@index');
Route::delete('message', 'API\CmaMessageAPIController@destroy');
Route::group(['middleware' => ['licensee']], function() {
    Route::post('message', 'API\CmaMessageAPIController@store');
});