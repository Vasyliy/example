<?php

namespace Tests\Traits;

use Faker\Factory as Faker;
use App\Models\CmaMessage;
use App\Repositories\CmaMessageRepository;
use Illuminate\Support\Facades\App;

trait MakeCmaMessageTrait
{
    /**
     * Create fake instance of CmaMessage and save it in database
     *
     * @param array $cmaMessageFields
     * @return CmaMessage
     */
    public function makeCmaMessage($cmaMessageFields = [])
    {
        /** @var CmaMessageRepository $cmaMessageRepo */
        $cmaMessageRepo = App::make(CmaMessageRepository::class);
        $theme = $this->fakeCmaMessageData($cmaMessageFields);
        return $cmaMessageRepo->create($theme);
    }

    /**
     * Get fake instance of CmaMessage
     *
     * @param array $cmaMessageFields
     * @return CmaMessage
     */
    public function fakeCmaMessage($cmaMessageFields = [])
    {
        return new CmaMessage($this->fakeCmaMessageData($cmaMessageFields));
    }

    /**
     * Get fake data of CmaMessage
     *
     * @param array $postFields
     * @return array
     */
    public function fakeCmaMessageData($cmaMessageFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'uuid' => $fake->word,
            'lid' => $fake->randomDigitNotNull,
            'player_id' => $fake->word,
            'token' => $fake->word,
            'content' => $fake->text,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $cmaMessageFields);
    }
}
