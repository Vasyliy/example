<?php

namespace Tests\Unit;

use App\Http\Middleware\CheckHMAC;
use Tests\TestCase;
use Tests\Traits\MakeCmaMessageTrait;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AuthTest extends TestCase
{
    use MakeCmaMessageTrait;

    /**
     * @test
     */
    public function testAuthLidRequired()
    {
        $lid = 1;
        $cmaMessage = $this->fakeCmaMessageData(['lid' => $lid]);
        $checkHmac = new CheckHMAC();
        $hash = $checkHmac->generateHash($cmaMessage);

        $response = $this->json('POST', '/message?hash=' . $hash, $cmaMessage);

        $response
            ->assertStatus(401)
            ->assertJson([
                'errors' => [
                    'x-licensee-id' => [
                        'The X-LICENSEE-ID header is required.'
                    ]
                ]
            ]);
    }

    /**
     * @test
     */
    public function testAuthLidNumeric()
    {
        $lid = 'test';
        $cmaMessage = $this->fakeCmaMessageData(['lid' => $lid]);
        $checkHmac = new CheckHMAC();
        $hash = $checkHmac->generateHash($cmaMessage);

        $response = $this->withHeaders([
            'x-licensee-id' => $lid,
        ])->json('POST', '/message?hash=' . $hash, $cmaMessage);

        $response
            ->assertStatus(401)
            ->assertJson([
                'errors' => [
                    'x-licensee-id' => [
                        'The X-LICENSEE-ID header must be a number.'
                    ]
                ]
            ]);
    }

    /**
     * @test
     */
    public function testAuthLidMin()
    {
        $lid = 0;
        $cmaMessage = $this->fakeCmaMessageData(['lid' => $lid]);
        $checkHmac = new CheckHMAC();
        $hash = $checkHmac->generateHash($cmaMessage);

        $response = $this->withHeaders([
            'x-licensee-id' => $lid,
        ])->json('POST', '/message?hash=' . $hash, $cmaMessage);

        $response
            ->assertStatus(401)
            ->assertJson([
                'errors' => [
                    'x-licensee-id' => [
                        'The X-LICENSEE-ID header must be at least 1.'
                    ]
                ]
            ]);
    }

    /**
     * @test
     */
    public function testAuthLidMax()
    {
        $lid = 4294967296;
        $cmaMessage = $this->fakeCmaMessageData(['lid' => $lid]);
        $checkHmac = new CheckHMAC();
        $hash = $checkHmac->generateHash($cmaMessage);

        $response = $this->withHeaders([
            'x-licensee-id' => $lid,
        ])->json('POST', '/message?hash=' . $hash, $cmaMessage);

        $response
            ->assertStatus(401)
            ->assertJson([
                'errors' => [
                    'x-licensee-id' => [
                        'The X-LICENSEE-ID header may not be greater than 4294967295.'
                    ]
                ]
            ]);
    }

    /**
     * @test
     */
    public function testAuthHashRequired()
    {
        $lid = 1;
        $cmaMessage = $this->fakeCmaMessageData(['lid' => $lid]);

        $response = $this->withHeaders([
            'x-licensee-id' => $lid,
        ])->json('POST', '/message', $cmaMessage);

        $response
            ->assertStatus(401)
            ->assertJson([
                'errors' => [
                    'hash' => [
                        'The hash is required.'
                    ]
                ]
            ]);
    }

    /**
     * @test
     */
    public function testAuthHashSize()
    {
        $lid = 1;
        $cmaMessage = $this->fakeCmaMessageData(['lid' => $lid]);
        $hash = '9003a2391b06a2e4e8d7fa8a25a5c5485065f080c9f7a442469b77bbbf410a4';

        $response = $this->withHeaders([
            'x-licensee-id' => $lid,
        ])->json('POST', '/message?hash=' . $hash, $cmaMessage);

        $response
            ->assertStatus(401)
            ->assertJson([
                'errors' => [
                    'hash' => [
                        'The hash must be 64 characters.'
                    ]
                ]
            ]);
    }

    /**
     * @test
     */
    public function testAuthHashCorrect()
    {
        $lid = 1;
        $cmaMessage = $this->fakeCmaMessageData(['lid' => $lid]);
        $hash = '9003a2391b06a2e4e8d7fa8a25a5c5485065f080c9f7a442469b77bbbf410a42';

        $response = $this->withHeaders([
            'x-licensee-id' => $lid,
        ])->json('POST', '/message?hash=' . $hash, $cmaMessage);

        $response
            ->assertStatus(401)
            ->assertJson([
                'errors' => [
                    'hash' => [
                        'The hash is incorrect.'
                    ]
                ]
            ]);
    }
}
