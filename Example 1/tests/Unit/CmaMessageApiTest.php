<?php

namespace Tests\Unit;

use App\Http\Middleware\CheckHMAC;
use Tests\TestCase;
use Tests\Traits\MakeCmaMessageTrait;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CmaMessageApiTest extends TestCase
{
    use MakeCmaMessageTrait, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateCmaMessage()
    {
        $lid = 1;
        $cmaMessage = $this->fakeCmaMessageData(['lid' => $lid]);
        $checkHmac = new CheckHMAC();
        $hash = $checkHmac->generateHash($cmaMessage);

        $response = $this->withHeaders([
            'x-licensee-id' => $lid,
        ])->json('POST', '/message?hash=' . $hash, $cmaMessage);

        $response
            ->assertStatus(201)
            ->assertJson([
                'lid' => $lid,
                'uuid' => $cmaMessage['uuid'],
                'player_id' => $cmaMessage['player_id'],
                'token' => $cmaMessage['token'],
                'content' => $cmaMessage['content']
            ]);
    }

    /**
     * @test
     */
    public function testCreateCmaMessageMaxLength()
    {
        $lid = 1;
        $cmaMessage = $this->fakeCmaMessageData([
            'lid' => $lid,
            'uuid' => '9003a2391b06a2e4e8d7fa8a25a5c5485065f',
            'token' => '19003a2391b06a2e4e8d7fa8a25a5c5485065f9003a2391b06a2e4e8d7fa8a25a5c5485065f9003a2391b06a2e4e8d7fa8a25a5c5485065f9003a2391b06a2e4e',
            'player_id' => '9003a2391b06a2e4e8d7fa8a25a5c5485'
        ]);
        $checkHmac = new CheckHMAC();
        $hash = $checkHmac->generateHash($cmaMessage);

        $response = $this->withHeaders([
            'x-licensee-id' => $lid,
        ])->json('POST', '/message?hash=' . $hash, $cmaMessage);

        $response
            ->assertStatus(422)
            ->assertJson([
                'errors' => [
                    'uuid' => [
                        'The uuid may not be greater than 36 characters.'
                    ],
                    'token' => [
                        'The token may not be greater than 128 characters.'
                    ],
                    'player_id' => [
                        'The player id may not be greater than 32 characters.'
                    ]
                ]
            ]);
    }

    /**
     * @test
     */
    public function testCreateCmaMessageRequiredFields()
    {
        $lid = 1;
        $cmaMessage = $this->fakeCmaMessageData([
            'lid' => $lid,
            'player_id' => null,
            'token' => null,
            'content' => null,
            'uuid' => null
        ]);
        $checkHmac = new CheckHMAC();
        $hash = $checkHmac->generateHash($cmaMessage);

        $response = $this->withHeaders([
            'x-licensee-id' => $lid,
        ])->json('POST', '/message?hash=' . $hash, $cmaMessage);

        $response
            ->assertStatus(422)
            ->assertJson([
                'errors' => [
                    'player_id' => [
                        'The player id field is required.'
                    ],
                    'content' => [
                        'The content field is required.'
                    ],
                    'token' => [
                        'The token field is required.'
                    ],
                    'uuid' => [
                        'The uuid field is required.'
                    ]
                ]
            ]);
    }

    /**
     * @test
     */
    public function testReadCmaMessage()
    {
        $cmaMessage = $this->makeCmaMessage();
        $response = $this->json('GET', '/message', ['token' => $cmaMessage['token']]);

        $response
            ->assertStatus(200)
            ->assertJson([[
                'id' => $cmaMessage['id'],
                'lid' => $cmaMessage['lid'],
                'uuid' => $cmaMessage['uuid'],
                'player_id' => $cmaMessage['player_id'],
                'token' => $cmaMessage['token'],
                'content' => $cmaMessage['content']
            ]]);
    }

    /**
     * @test
     */
    public function testReadCmaMessageMaxLength()
    {
        $response = $this->json('DELETE', '/message', [
            'token' => '19003a2391b06a2e4e8d7fa8a25a5c5485065f9003a2391b06a2e4e8d7fa8a25a5c5485065f9003a2391b06a2e4e8d7fa8a25a5c5485065f9003a2391b06a2e4e'
        ]);

        $response
            ->assertStatus(422)
            ->assertJson([
                'errors' => [
                    'token' => [
                        'The token may not be greater than 128 characters.'
                    ]
                ]
            ]);
    }

    /**
     * @test
     */
    public function testDeleteByTokenCmaMessage()
    {
        $cmaMessage = $this->makeCmaMessage();
        $response = $this->json('DELETE', '/message', ['token' => $cmaMessage['token']]);

        $response->assertStatus(204);
    }

    /**
     * @test
     */
    public function testDeleteByUuidCmaMessage()
    {
        $cmaMessage = $this->makeCmaMessage();
        $response = $this->json('DELETE', '/message', ['uuid' => $cmaMessage['uuid']]);

        $response->assertStatus(204);
    }

    /**
     * @test
     */
    public function testDeleteByTokenAndUuidCmaMessage()
    {
        $cmaMessage = $this->makeCmaMessage();
        $response = $this->json('DELETE', '/message', ['token' => $cmaMessage['token'], 'uuid' => $cmaMessage['uuid']]);

        $response->assertStatus(204);
    }

    /**
     * @test
     */
    public function testDeleteByTokenAndUuidCmaMessageMaxLength()
    {
        $cmaMessage = $this->makeCmaMessage();
        $response = $this->json('DELETE', '/message', [
            'token' => '19003a2391b06a2e4e8d7fa8a25a5c5485065f9003a2391b06a2e4e8d7fa8a25a5c5485065f9003a2391b06a2e4e8d7fa8a25a5c5485065f9003a2391b06a2e4e',
            'uuid' => '9003a2391b06a2e4e8d7fa8a25a5c5485065f'
        ]);

        $response
            ->assertStatus(422)
            ->assertJson([
                'errors' => [
                    'token' => [
                        'The token may not be greater than 128 characters.'
                    ],
                    'uuid' => [
                        'The uuid may not be greater than 36 characters.'
                    ]
                ]
            ]);
    }
}