<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCmaMessagesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cma_messages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uuid', 36);
            $table->unsignedInteger('lid');
            $table->string('player_id', 32);
            $table->string('token', 128);
            $table->text('content');
            $table->timestamps();
            $table->softDeletes();
            $table->index(['uuid', 'lid', 'player_id', 'token']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cma_messages');
    }
}
