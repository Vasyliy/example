<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFeedbackTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feedback', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('url_id')->nullable();
            $table->string('name')->nullable();
            $table->string('city')->nullable();
            $table->text('message')->nullable();
            $table->string('img')->nullable();
            $table->integer('parent_id')->nullable();
            $table->integer('type_id')->nullable();
            $table->boolean('is_viewed')->nullable();
            $table->boolean('is_active')->nullable();
            $table->integer('status_id')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->index(['url_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('feedback');
    }
}
