<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ContentOptimisation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contents', function (Blueprint $table) {
            $table->unsignedInteger('views')->default(0)->change();
            $table->unsignedInteger('likes')->default(0)->change();
            $table->string('text', 70000)->nullable()->change();
            $table->unsignedInteger('ord')->nullable()->change();
            $table->unsignedInteger('parent_id')->nullable()->change();
            $table->unsignedInteger('author_id')->nullable()->change();
            $table->boolean('is_active')->default(0)->change();
            $table->boolean('show_author')->default(0)->change();
            $table->boolean('show_date')->default(0)->change();

            $table->foreign('parent_id')->references('id')->on('contents')->onDelete('cascade');
            $table->foreign('author_id')->references('id')->on('contents')->onDelete('cascade');

            $table->index(['content_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
