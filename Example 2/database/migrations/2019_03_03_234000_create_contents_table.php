<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContentsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contents', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->string('url')->nullable();
            $table->integer('parent_id')->nullable();
            $table->string('img')->nullable();
            $table->string('file')->nullable();
            $table->integer('author_id')->nullable();
            $table->boolean('is_active')->nullable();
            $table->boolean('show_author')->nullable();
            $table->boolean('show_date')->nullable();
            $table->string('seo_img')->nullable();
            $table->string('seo_alt')->nullable();
            $table->string('seo_title')->nullable();
            $table->text('seo_desc')->nullable();
            $table->text('seo_key')->nullable();
            $table->text('subtext')->nullable();
            $table->text('text')->nullable();
            $table->text('object')->nullable();
            $table->date('date_start')->nullable();
            $table->date('date_end')->nullable();
            $table->integer('ord')->nullable();
            $table->unsignedTinyInteger('views')->default(0);
            $table->unsignedTinyInteger('likes')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contents');
    }
}
