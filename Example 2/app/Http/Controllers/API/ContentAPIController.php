<?php

namespace App\Http\Controllers\API;

use App\Criteria\DateCriteria;
use App\Http\Requests\API\CreateContentAPIRequest;
use App\Http\Requests\API\UpdateContentAPIRequest;
use App\Models\Content;
use App\Repositories\ContentRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class ContentController
 * @package App\Http\Controllers\API
 */

class ContentAPIController extends AppBaseController
{
    /** @var  ContentRepository */
    private $contentRepository;

    public function __construct(ContentRepository $contentRepo)
    {
        $this->contentRepository = $contentRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/contents",
     *      summary="Get a listing of the Contents.",
     *      tags={"Content"},
     *      description="Get all Contents",
     *      produces={"application/json"},
     *     @SWG\Parameter(
     *          name="offset",
     *          type="integer",
     *          in="query"
     *     ),
     *     @SWG\Parameter(
     *          name="limit",
     *          type="integer",
     *          in="query"
     *     ),
     *     @SWG\Parameter(
     *          name="search",
     *          type="string",
     *          in="query"
     *     ),
     *     @SWG\Parameter(
     *          name="searchFields",
     *          type="string",
     *          in="query"
     *     ),
     *     @SWG\Parameter(
     *          name="feature",
     *          type="boolean",
     *          in="query"
     *     ),
     *     @SWG\Parameter(
     *          name="filter",
     *          type="string",
     *          in="query"
     *     ),
     *     @SWG\Parameter(
     *          name="orderBy",
     *          type="string",
     *          in="query"
     *     ),
     *     @SWG\Parameter(
     *          name="sortedBy",
     *          type="string",
     *          in="query"
     *     ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="count",
     *                  type="integer"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->contentRepository->pushCriteria(new RequestCriteria($request));
        $this->contentRepository->pushCriteria(new DateCriteria($request));
        $this->contentRepository->pushCriteria(new LimitOffsetCriteria($request));
        $contents = $this->contentRepository->all();

        return $this->sendResponse($contents->toArray(), 'Contents retrieved successfully');
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/contents/count",
     *      summary="Get count of the Contents.",
     *      tags={"Content"},
     *      description="Get Contents count",
     *      produces={"application/json"},
     *     @SWG\Parameter(
     *          name="search",
     *          type="string",
     *          in="query"
     *     ),
     *     @SWG\Parameter(
     *          name="searchFields",
     *          type="string",
     *          in="query"
     *     ),
     *     @SWG\Parameter(
     *          name="feature",
     *          type="boolean",
     *          in="query"
     *     ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="integer"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function count(Request $request)
    {
        $this->contentRepository->pushCriteria(new RequestCriteria($request));
        $this->contentRepository->pushCriteria(new DateCriteria($request));
        $contents = $this->contentRepository->all()->count();

        return $this->sendResponse($contents, 'Contents retrieved successfully');
    }


    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Post(
     *      path="/contents/like/{id}",
     *      summary="Content like",
     *      tags={"Content"},
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Content",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="liked",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="likes",
     *                  type="integer"
     *              )
     *          )
     *      )
     * )
     */
    public function like($id)
    {
        $model = Content::find($id);

        return response([
            'liked' => AppBaseController::like($model),
            'likes' => $model->likes
        ]);
    }


    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Post(
     *      path="/contents/increment/{id}",
     *      summary="Increment content field in object",
     *      tags={"Content"},
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Content",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *     @SWG\Parameter(
     *          name="field",
     *          type="string",
     *          required=true,
     *          in="query"
     *     ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="amount",
     *                  type="integer"
     *              )
     *          )
     *      )
     * )
     */
    public function increment($id, Request $request)
    {
        if (!$request->has('field'))
            return $this->sendError('Field name is necessary');

        $model = Content::find($id);

        if (empty($model))
            return $this->sendError('Content not found');
        if (!$model->object)
            return $this->sendError('Object is empty');

        $object = json_decode($model->object);
        $field = $request->input('field');

        if (!isset($object->{$field}))
            return $this->sendError('Filed {'. $field .'} does not exist in object.');

        $amount = $object->{$field};

        if (!is_int($amount))
            return $this->sendError('Field should be integer');

        if (!session()->has($field . '.' . $id)) {
            $object->{$field} = ++$amount;
            $model->object = json_encode($object);
            $model->save();
            session([$field . '.' . $id => true]);
        }

        return response([
            'amount' => $amount
        ]);
    }


    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Put(
     *      path="/contents/ord/{id}",
     *      summary="Change content order",
     *      tags={"Content"},
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Content",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *     @SWG\Parameter(
     *          name="ord",
     *          type="integer",
     *          required=true,
     *          in="query"
     *     ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="amount",
     *                  type="integer"
     *              )
     *          )
     *      ),
     *     @SWG\Response(response=400, description="Bad request"),
     *     @SWG\Response(response=401, description="Unauthenticated"),
     *     @SWG\Response(response=404, description="Resource Not Found"),
     *     security={
     *       {"Bearer": {}}
     *     }
     * )
     */
    public function ord($id, Request $request)
    {
        if (!$request->has('ord'))
            return $this->sendError('Ord parameter is necessary');

        $model = Content::find($id);

        if (empty($model))
            return $this->sendError('Content not found');

        $newOrder = $request->input('ord');
        $oldOrder = $model->ord;

        if ($oldOrder != $newOrder) {
            if ($oldOrder > $newOrder)
                Content::where('content_id', $model->content_id)
                    ->where('ord', '<', $oldOrder)
                    ->where('ord', '>=', $newOrder)
                    ->increment('ord');
            else
                Content::where('content_id', $model->content_id)
                    ->where('ord', '>', $oldOrder)
                    ->where('ord', '<=', $newOrder)
                    ->decrement('ord');

            $model->update(['ord' => $newOrder]);
        }

        return $this->sendResponse($id, 'Content order updated');
    }

    /**
     * @param CreateContentAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/contents",
     *      summary="Store a newly created Content in storage",
     *      tags={"Content"},
     *      description="Store Content",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Content that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Content")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Content"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      ),
     *     @SWG\Response(response=400, description="Bad request"),
     *     @SWG\Response(response=401, description="Unauthenticated"),
     *     @SWG\Response(response=404, description="Resource Not Found"),
     *     security={
     *       {"Bearer": {}}
     *     }
     * )
     */
    public function store(CreateContentAPIRequest $request)
    {
        $input = $request->all();

        $contents = $this->contentRepository->create($input);

        return $this->sendResponse($contents->toArray(), 'Content saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/contents/{id}",
     *      summary="Display the specified Content",
     *      tags={"Content"},
     *      description="Get Content",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Content",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Content"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Content $content */
        $content = $this->contentRepository->findWithoutFail($id);

        if (empty($content)) {
            return $this->sendError('Content not found');
        }

        $content = $content->toArray();
        $content['liked'] = session()->has('contents.likes.' . $id);

        return $this->sendResponse($content, 'Content retrieved successfully');
    }

    /**
     * @param integer $content_id
     * @param string $url
     * @param string $use_case
     * @return Response
     *
     * @SWG\Get(
     *      path="/contents/url/{content_id}/{url}/{use_case}",
     *      summary="Display the specified Content by content id and URL",
     *      tags={"Content"},
     *      description="Get Content",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="content_id",
     *          description="content_id of Content",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="url",
     *          description="url of Content",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="use_case",
     *          type="string",
     *          in="path",
     *          enum={"view", "content", "view_content"}
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Content"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function url($content_id, $url, $use_case = null)
    {
        /** @var Content $content */
        $content = $this->contentRepository->findWhere(['content_id' => $content_id, 'url' => $url])->first();

        if (empty($content)) {
            return $this->sendError('Content not found');
        }

        $content = $content->toArray();
        $content['liked'] = session()->has('contents.likes.' . $content['id']);

        switch ($use_case) {
            case 'view':
                AppBaseController::view('contents', $this->contentRepository->model(), $content['id']);
                return response('view', 200);
            case 'view_content':
                AppBaseController::view('contents', $this->contentRepository->model(), $content['id']);
                break;
        }

        return $this->sendResponse($content, 'Content retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateContentAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/contents/{id}",
     *      summary="Update the specified Content in storage",
     *      tags={"Content"},
     *      description="Update Content",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Content",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Content that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Content")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Content"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      ),
     *     @SWG\Response(response=400, description="Bad request"),
     *     @SWG\Response(response=401, description="Unauthenticated"),
     *     @SWG\Response(response=404, description="Resource Not Found"),
     *     security={
     *       {"Bearer": {}}
     *     }
     * )
     */
    public function update($id, UpdateContentAPIRequest $request)
    {
        $input = $request->all();

        /** @var Content $content */
        $content = $this->contentRepository->findWithoutFail($id);

        if (empty($content)) {
            return $this->sendError('Content not found');
        }

        $content = $this->contentRepository->update($input, $id);

        return $this->sendResponse($content->toArray(), 'Content updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/contents/{id}",
     *      summary="Remove the specified Content from storage",
     *      tags={"Content"},
     *      description="Delete Content",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Content",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      ),
     *     @SWG\Response(response=400, description="Bad request"),
     *     @SWG\Response(response=401, description="Unauthenticated"),
     *     @SWG\Response(response=404, description="Resource Not Found"),
     *     security={
     *       {"Bearer": {}}
     *     }
     * )
     */
    public function destroy($id)
    {
        /** @var Content $content */
        $content = $this->contentRepository->findWithoutFail($id);

        if (empty($content)) {
            return $this->sendError('Content not found');
        }

        $content->delete();

        return $this->sendResponse($id, 'Content deleted successfully');
    }
}
