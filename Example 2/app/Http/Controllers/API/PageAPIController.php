<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePageAPIRequest;
use App\Http\Requests\API\UpdatePageAPIRequest;
use App\Models\Page;
use App\Repositories\PageRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class PageController
 * @package App\Http\Controllers\API
 */

class PageAPIController extends AppBaseController
{
    /** @var  PageRepository */
    private $pageRepository;

    public function __construct(PageRepository $pageRepo)
    {
        $this->pageRepository = $pageRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/pages",
     *      summary="Get a listing of the Pages.",
     *      tags={"Page"},
     *      description="Get all Pages",
     *      produces={"application/json"},
     *     @SWG\Parameter(
     *          name="offset",
     *          type="integer",
     *          in="query"
     *     ),
     *     @SWG\Parameter(
     *          name="limit",
     *          type="integer",
     *          in="query"
     *     ),
     *     @SWG\Parameter(
     *          name="search",
     *          type="string",
     *          in="query"
     *     ),
     *     @SWG\Parameter(
     *          name="searchFields",
     *          type="string",
     *          in="query"
     *     ),
     *     @SWG\Parameter(
     *          name="filter",
     *          type="string",
     *          in="query"
     *     ),
     *     @SWG\Parameter(
     *          name="orderBy",
     *          type="string",
     *          in="query"
     *     ),
     *     @SWG\Parameter(
     *          name="sortedBy",
     *          type="string",
     *          in="query"
     *     ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Page")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->pageRepository->pushCriteria(new RequestCriteria($request));
        $this->pageRepository->pushCriteria(new LimitOffsetCriteria($request));
        $pages = $this->pageRepository->all();

        return $this->sendResponse($pages->toArray(), 'Pages retrieved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Post(
     *      path="/pages/like/{id}",
     *      summary="Page like",
     *      tags={"Page"},
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Page",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="liked",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="likes",
     *                  type="integer"
     *              )
     *          )
     *      )
     * )
     */
    public function like($id)
    {
        $model = Page::find($id);

        return response([
            'liked' => AppBaseController::like($model),
            'likes' => $model->likes
        ]);
    }

    /**
     * @param CreatePageAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/pages",
     *      summary="Store a newly created Page in storage",
     *      tags={"Page"},
     *      description="Store Page",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Page that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Page")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Page"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      ),
     *     @SWG\Response(response=400, description="Bad request"),
     *     @SWG\Response(response=401, description="Unauthenticated"),
     *     @SWG\Response(response=404, description="Resource Not Found"),
     *     security={
     *       {"Bearer": {}}
     *     }
     * )
     */
    public function store(CreatePageAPIRequest $request)
    {
        $input = $request->all();

        $pages = $this->pageRepository->create($input);

        return $this->sendResponse($pages->toArray(), 'Page saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/pages/{id}",
     *      summary="Display the specified Page",
     *      tags={"Page"},
     *      description="Get Page",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Page",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Page"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Page $page */
        $page = $this->pageRepository->findWithoutFail($id);

        if (empty($page)) {
            return $this->sendError('Page not found');
        }

        $page = $page->toArray();
        $page['liked'] = session()->has('pages.likes.' . $id);

        return $this->sendResponse($page, 'Page retrieved successfully');
    }

    /**
     * @param string $url
     * @param string $use_case
     * @return Response
     *
     * @SWG\Get(
     *      path="/pages/url/{url}/{use_case}",
     *      summary="Display the specified Page by url",
     *      tags={"Page"},
     *      description="Get Page",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="url",
     *          description="url of Page",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="use_case",
     *          type="string",
     *          in="path",
     *          enum={"view", "content", "view_content"}
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Page"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function url($url, $use_case = null)
    {
        /** @var Page $page */
        $page = $this->pageRepository->findByField('url', $url)->first();

        if (empty($page)) {
            return $this->sendError('Page not found');
        }

        $page = $page->toArray();
        $page['liked'] = session()->has('pages.likes.' . $page['id']);

        switch ($use_case) {
            case 'view':
                AppBaseController::view('pages', $this->pageRepository->model(), $page['id']);
                return response('view', 200);
            case 'view_content':
                AppBaseController::view('pages', $this->pageRepository->model(), $page['id']);
                break;
        }

        return $this->sendResponse($page, 'Page retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdatePageAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/pages/{id}",
     *      summary="Update the specified Page in storage",
     *      tags={"Page"},
     *      description="Update Page",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Page",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Page that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Page")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Page"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      ),
     *     @SWG\Response(response=400, description="Bad request"),
     *     @SWG\Response(response=401, description="Unauthenticated"),
     *     @SWG\Response(response=404, description="Resource Not Found"),
     *     security={
     *       {"Bearer": {}}
     *     }
     * )
     */
    public function update($id, UpdatePageAPIRequest $request)
    {
        $input = $request->all();

        /** @var Page $page */
        $page = $this->pageRepository->findWithoutFail($id);

        if (empty($page)) {
            return $this->sendError('Page not found');
        }

        $page = $this->pageRepository->update($input, $id);

        return $this->sendResponse($page->toArray(), 'Page updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/pages/{id}",
     *      summary="Remove the specified Page from storage",
     *      tags={"Page"},
     *      description="Delete Page",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Page",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      ),
     *     @SWG\Response(response=400, description="Bad request"),
     *     @SWG\Response(response=401, description="Unauthenticated"),
     *     @SWG\Response(response=404, description="Resource Not Found"),
     *     security={
     *       {"Bearer": {}}
     *     }
     * )
     */
    public function destroy($id)
    {
        /** @var Page $page */
        $page = $this->pageRepository->findWithoutFail($id);

        if (empty($page)) {
            return $this->sendError('Page not found');
        }

        $page->delete();

        return $this->sendResponse($id, 'Page deleted successfully');
    }
}
