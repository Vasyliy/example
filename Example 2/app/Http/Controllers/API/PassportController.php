<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;


class PassportController extends AppBaseController
{

	public $successStatus = 200;

	/**
	 * @SWG\Post(
	 *     path="/oauth/token",
	 *     tags={"Authentication"},
	 *     summary="Get tokens",
	 *     produces= {"application/json"},
	 *     consumes={"application/x-www-form-urlencoded"},
	 *     @SWG\Parameter(
	 *          name="client_id",
	 *          description="oauth client id",
	 *          type="integer",
	 *          in="formData",
	 *          default="1",
	 *     ),
	 *     @SWG\Parameter(
	 *          name="client_secret",
	 *          description="oauth client secret",
	 *          type="string",
	 *          in="formData",
	 *          default="B16HsgJKBNZejxZI1VyBPdVo29OlV1vbbfYAIlza",
	 *     ),
	 *     @SWG\Parameter(
	 *          name="grant_type",
	 *          description="oauth client grant type",
	 *          type="string",
	 *          in="formData",
	 *          default="password",
	 *     ),
	 *     @SWG\Parameter(
	 *          name="username",
	 *          description="User email",
	 *          type="string",
	 *          in="formData",
	 *     ),
	 *     @SWG\Parameter(
	 *          name="password",
	 *          description="User password",
	 *          type="string",
	 *          in="formData",
	 *     ),
	 *     @SWG\Response(response=200, description="Successful operation"),
	 *     @SWG\Response(response=400, description="Bad request"),
	 *     @SWG\Response(response=404, description="Resource Not Found"),
	 * )
	 */

	/**
	 * @SWG\Post(
	 *     path="/register",
	 *     tags={"Authentication"},
	 *     summary="Register new account",
	 *     consumes={"application/x-www-form-urlencoded"},
	 *     produces={"application/json"},
	 *     @SWG\Parameter(
	 *          name="name",
	 *          in="formData",
	 *          description="User name",
	 *          type="string",
	 *          required=true
	 *     ),
	 *     @SWG\Parameter(
	 *          name="email",
	 *          in="formData",
	 *          description="User email",
	 *          type="string",
	 *          required=true
	 *     ),
	 *     @SWG\Parameter(
	 *          name="password",
	 *          in="formData",
	 *          description="User password",
	 *          type="string",
	 *          required=true
	 *     ),
	 *     @SWG\Parameter(
	 *          name="password_confirmation",
	 *          in="formData",
	 *          description="User password confirmation",
	 *          type="string",
	 *          required=true
	 *     ),
	 *     @SWG\Response(response=200, description="Successful operation"),
	 *     @SWG\Response(response=400, description="Bad request"),
	 *     @SWG\Response(response=404, description="Resource Not Found"),
	 * )
	 */


	public function register(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'name' => 'required|string|max:255',
			'email' => 'required|string|email|max:255|unique:users',
			'password' => 'required|string|min:6|confirmed'
		]);

		if ($validator->fails()) {
			return response()->json(['errors' => $validator->errors()], 401);
		}

		$input = $request->all();
		$input['password'] = bcrypt($input['password']);
		$user = User::create($input);
		$success['name'] = $user->name;

		return response()->json(['success' => $success], $this->successStatus);
	}

	public function test()
	{
		$user = Auth::user();
		return response()->json(['success' => $user], $this->successStatus);
	}
}
