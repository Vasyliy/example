<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateFeedbackAPIRequest;
use App\Http\Requests\API\UpdateFeedbackAPIRequest;
use App\Models\Feedback;
use App\Repositories\FeedbackRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class FeedbackController
 * @package App\Http\Controllers\API
 */

class FeedbackAPIController extends AppBaseController
{
    /** @var  FeedbackRepository */
    private $feedbackRepository;

    public function __construct(FeedbackRepository $feedbackRepo)
    {
        $this->feedbackRepository = $feedbackRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/feedback",
     *      summary="Get a listing of the Feedback.",
     *      tags={"Feedback"},
     *      description="Get all Feedback",
     *      produces={"application/json"},
     *     @SWG\Parameter(
     *          name="offset",
     *          type="integer",
     *          in="query"
     *     ),
     *     @SWG\Parameter(
     *          name="limit",
     *          type="integer",
     *          in="query"
     *     ),
     *     @SWG\Parameter(
     *          name="search",
     *          type="string",
     *          in="query"
     *     ),
     *     @SWG\Parameter(
     *          name="searchFields",
     *          type="string",
     *          in="query"
     *     ),
     *     @SWG\Parameter(
     *          name="filter",
     *          type="string",
     *          in="query"
     *     ),
     *     @SWG\Parameter(
     *          name="orderBy",
     *          type="string",
     *          in="query"
     *     ),
     *     @SWG\Parameter(
     *          name="sortedBy",
     *          type="string",
     *          in="query"
     *     ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Feedback")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      ),
     *     @SWG\Response(response=400, description="Bad request"),
     *     @SWG\Response(response=401, description="Unauthenticated"),
     *     @SWG\Response(response=404, description="Resource Not Found"),
     *     security={
     *       {"Bearer": {}}
     *     }
     * )
     */
    public function index(Request $request)
    {
        $this->feedbackRepository->pushCriteria(new RequestCriteria($request));
        $this->feedbackRepository->pushCriteria(new LimitOffsetCriteria($request));
        $feedback = $this->feedbackRepository->all();

        return $this->sendResponse($feedback->toArray(), 'Feedback retrieved successfully');
    }


    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/feedback/count",
     *      summary="Get count of the Feedback.",
     *      tags={"Feedback"},
     *      description="Get Feedback count",
     *      produces={"application/json"},
     *     @SWG\Parameter(
     *          name="search",
     *          type="string",
     *          in="query"
     *     ),
     *     @SWG\Parameter(
     *          name="searchFields",
     *          type="string",
     *          in="query"
     *     ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="integer"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function count(Request $request)
    {
        $this->feedbackRepository->pushCriteria(new RequestCriteria($request));
        $feedback = $this->feedbackRepository->all()->count();

        return $this->sendResponse($feedback, 'Feedback retrieved successfully');
    }

    /**
     * @param CreateFeedbackAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/feedback",
     *      summary="Store a newly created Feedback in storage",
     *      tags={"Feedback"},
     *      description="Store Feedback",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Feedback that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Feedback")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Feedback"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateFeedbackAPIRequest $request)
    {
        $input = $request->all();

        $feedback = $this->feedbackRepository->create($input);

        return $this->sendResponse($feedback->toArray(), 'Feedback saved successfully');
    }

    /**
     * @param int $id
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/feedback/{id}",
     *      summary="Display the specified Feedback",
     *      tags={"Feedback"},
     *      description="Get Feedback",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Feedback",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="admin",
     *          description="is request from admin",
     *          type="boolean",
     *          required=false,
     *          in="query"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Feedback"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      ),
     *     @SWG\Response(response=400, description="Bad request"),
     *     @SWG\Response(response=401, description="Unauthenticated"),
     *     @SWG\Response(response=404, description="Resource Not Found"),
     *     security={
     *       {"Bearer": {}}
     *     }
     * )
     */
    public function show($id, Request $request)
    {
        /** @var Feedback $feedback */
        $feedback = $this->feedbackRepository->findWithoutFail($id);

        if (empty($feedback)) {
            return $this->sendError('Feedback not found');
        }

        $feedback = $feedback->toArray();

        $feedback['liked'] = session()->has('feedback.likes.' . $id);

        if(!$request->has('admin'))
            AppBaseController::view('feedback', $this->feedbackRepository->model(), $id);

        return $this->sendResponse($feedback, 'Feedback retrieved successfully');
    }

    /**
     * @param integer $type_id
     * @param integer $url_id
     * @param string $use_case
     * @return Response
     *
     * @SWG\Get(
     *      path="/feedback/url/{type_id}/{url_id}/{use_case}",
     *      summary="Display the specified Feedback by type id and url id",
     *      tags={"Feedback"},
     *      description="Get Feedback",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="type_id",
     *          description="type_id of Feedback",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="url_id",
     *          description="url_id of Feedback",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="use_case",
     *          type="string",
     *          in="path",
     *          enum={"view", "content", "view_content"}
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Content"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function url($type_id, $url_id, $use_case = null)
    {
        /** @var Feedback $feedback */
        $feedback = $this->feedbackRepository->findWhere(['type_id' => $type_id, 'url_id' => $url_id])->first();

        if (empty($feedback)) {
            return $this->sendError('Feedback not found');
        }

        $feedback = $feedback->toArray();

        $feedback['liked'] = session()->has('feedback.likes.' . $feedback['id']);

        switch ($use_case) {
            case 'view':
                AppBaseController::view('feedback', $this->feedbackRepository->model(), $feedback['id']);
                return response('view', 200);
            case 'view_content':
                AppBaseController::view('feedback', $this->feedbackRepository->model(), $feedback['id']);
                break;
        }

        return $this->sendResponse($feedback, 'Feedback retrieved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Post(
     *      path="/feedback/like/{id}",
     *      summary="Feedback like",
     *      tags={"Feedback"},
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Feedback",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="liked",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="likes",
     *                  type="integer"
     *              )
     *          )
     *      )
     * )
     */
    public function like($id)
    {
        $model = Feedback::find($id);

        return response([
            'liked' => AppBaseController::like($model),
            'likes' => $model->likes
        ]);
    }

    /**
     * @param int $id
     * @param UpdateFeedbackAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/feedback/{id}",
     *      summary="Update the specified Feedback in storage",
     *      tags={"Feedback"},
     *      description="Update Feedback",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Feedback",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Feedback that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Feedback")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Feedback"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      ),
     *     @SWG\Response(response=400, description="Bad request"),
     *     @SWG\Response(response=401, description="Unauthenticated"),
     *     @SWG\Response(response=404, description="Resource Not Found"),
     *     security={
     *       {"Bearer": {}}
     *     }
     * )
     */
    public function update($id, UpdateFeedbackAPIRequest $request)
    {
        $input = $request->all();

        /** @var Feedback $feedback */
        $feedback = $this->feedbackRepository->findWithoutFail($id);

        if (empty($feedback)) {
            return $this->sendError('Feedback not found');
        }

        $feedback = $this->feedbackRepository->update($input, $id);

        return $this->sendResponse($feedback->toArray(), 'Feedback updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/feedback/{id}",
     *      summary="Remove the specified Feedback from storage",
     *      tags={"Feedback"},
     *      description="Delete Feedback",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Feedback",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      ),
     *     @SWG\Response(response=400, description="Bad request"),
     *     @SWG\Response(response=401, description="Unauthenticated"),
     *     @SWG\Response(response=404, description="Resource Not Found"),
     *     security={
     *       {"Bearer": {}}
     *     }
     * )
     */
    public function destroy($id)
    {
        /** @var Feedback $feedback */
        $feedback = $this->feedbackRepository->findWithoutFail($id);

        if (empty($feedback)) {
            return $this->sendError('Feedback not found');
        }

        $feedback->delete();

        return $this->sendResponse($id, 'Feedback deleted successfully');
    }
}
