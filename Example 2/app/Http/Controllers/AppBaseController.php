<?php

namespace App\Http\Controllers;

use App\Models\Content;
use InfyOm\Generator\Utils\ResponseUtil;
use Response;

/**
 * @SWG\SecurityScheme(
 *      securityDefinition="Bearer",
 *      type="apiKey",
 *      in="header",
 *      name="Authorization"
 * )
 */

/**
 * @SWG\Swagger(
 *   basePath="/api",
 *   @SWG\Info(
 *     title="Academy API",
 *     version="2.0.1",
 *   )
 * )
 * This class should be parent class for other API controllers
 * Class AppBaseController
 */
class AppBaseController extends Controller
{
    public function sendResponse($result, $message)
    {
        return Response::json(ResponseUtil::makeResponse($message, $result));
    }

    public function sendError($error, $code = 404)
    {
        return Response::json(ResponseUtil::makeError($error), $code);
    }

    public function like($model)
    {
        $liked = true;
        $table = $model->getTable();

        if (session()->has($table . '.likes.' . $model->id)) {
            $model->decrement('likes');
            session()->forget($table . '.likes.' . $model->id);
            $liked = false;
        } else {
            $model->increment('likes');
            session([$table . '.likes.' . $model->id => true]);
        }

        return $liked;
    }

    public function view($name, $model, $id)
    {
        if (!session()->has($name . '.views.' . $id)) {
            $model::find($id)->increment('views');
            Content::find(1636)->increment('views');
            session([$name . '.views.' . $id => true]);
        }
    }
}
