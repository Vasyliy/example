<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Feedback",
 *      required={""},
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="city",
 *          description="city",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="message",
 *          description="message",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="img",
 *          description="img",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="parent_id",
 *          description="parent_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="type_id",
 *          description="type_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="is_viewed",
 *          description="is_viewed",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="is_active",
 *          description="is_active",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="status_id",
 *          description="status_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Feedback extends Model
{
    use SoftDeletes;

    public $table = 'feedback';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'url_id',
        'name',
        'city',
        'message',
        'img',
        'parent_id',
        'type_id',
        'is_viewed',
        'is_active',
        'status_id',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'url_id' => 'integer',
        'name' => 'string',
        'city' => 'string',
        'message' => 'string',
        'img' => 'string',
        'parent_id' => 'integer',
        'type_id' => 'integer',
        'is_viewed' => 'boolean',
        'is_active' => 'boolean',
        'status_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'url_id' => 'nullable|numeric|min:1',
        'name' => 'nullable|string|max:191',
        'city' => 'nullable|string|max:191',
        'message' => 'nullable|string',
        'img' => 'nullable|string|max:191',
        'parent_id' => 'nullable|numeric|exists:contents,id',
        'type_id' => 'nullable|numeric',
        'status_id' => 'nullable|numeric'
    ];

    public static function boot()
    {
        parent::boot();

        self::creating(function($model){
            $model->url_id = self::max('url_id') + 1;
        });
    }
}
