<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Page",
 *      required={""},
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="url",
 *          description="url",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="title",
 *          description="title",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="text",
 *          description="text",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="description",
 *          description="description",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="keywords",
 *          description="keywords",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="seo_image",
 *          description="seo_image",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="seo_title",
 *          description="seo_title",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="views",
 *          description="views",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="likes",
 *          description="likes",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Page extends Model
{
    use SoftDeletes;

    public $table = 'pages';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'url',
        'title',
        'text',
        'description',
        'keywords',
        'seo_image',
        'seo_title',
        'views',
        'likes',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'url' => 'string',
        'title' => 'string',
        'text' => 'string',
        'description' => 'string',
        'keywords' => 'string',
        'seo_image' => 'string',
        'seo_title' => 'string',
        'views' => 'integer',
        'likes' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
    ];

    
}
