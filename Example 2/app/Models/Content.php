<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Content",
 *      required={""},
 *      @SWG\Property(
 *          property="title",
 *          description="title",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="content_id",
 *          description="content_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="url",
 *          description="url",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="parent_id",
 *          description="parent_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="img",
 *          description="img",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="file",
 *          description="file",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="author_id",
 *          description="author_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="is_active",
 *          description="is_active",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="bool",
 *          description="bool",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="show_author",
 *          description="show_author",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="show_date",
 *          description="show_date",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="seo_img",
 *          description="seo_img",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="seo_alt",
 *          description="seo_alt",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="seo_title",
 *          description="seo_title",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="seo_desc",
 *          description="seo_desc",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="seo_key",
 *          description="seo_key",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="subtext",
 *          description="subtext",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="text",
 *          description="text",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="object",
 *          description="object",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="date_start",
 *          description="date_start",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="date_end",
 *          description="date_end",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="ord",
 *          description="ord",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="views",
 *          description="views",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="likes",
 *          description="likes",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Content extends Model
{
    use SoftDeletes;

    public $table = 'contents';
    

    protected $dates = ['deleted_at', 'date_start', 'date_end'];


    public $fillable = [
        'content_id',
        'title',
        'url',
        'parent_id',
        'img',
        'file',
        'author_id',
        'is_active',
        'bool',
        'show_author',
        'show_date',
        'seo_img',
        'seo_alt',
        'seo_title',
        'seo_desc',
        'seo_key',
        'subtext',
        'text',
        'object',
        'date_start',
        'date_end',
        'ord',
        'views',
        'likes',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'content_id' => 'integer',
        'title' => 'string',
        'url' => 'string',
        'parent_id' => 'integer',
        'img' => 'string',
        'file' => 'string',
        'author_id' => 'integer',
        'is_active' => 'boolean',
        'bool' => 'boolean',
        'show_author' => 'boolean',
        'show_date' => 'boolean',
        'seo_img' => 'string',
        'seo_alt' => 'string',
        'seo_title' => 'string',
        'seo_desc' => 'string',
        'seo_key' => 'string',
        'subtext' => 'string',
        'text' => 'string',
        'object' => 'string',
        'ord' => 'integer',
        'views' => 'integer',
        'likes' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'content_id' => 'nullable|numeric',
        'title' => 'nullable|string|max:191',
        'url' => 'nullable|string|max:191',
        'parent_id' => 'nullable|numeric|exists:contents,id',
        'img' => 'nullable|string|max:191',
        'file' => 'nullable|string|max:191',
        'author_id' => 'nullable|numeric|exists:contents,id',
        'seo_img' => 'nullable|string|max:191',
        'seo_alt' => 'nullable|string|max:191',
        'seo_title' => 'nullable|string|max:191',
        'seo_desc' => 'nullable|string',
        'seo_key' => 'nullable|string',
        'subtext' => 'nullable|string',
        'text' => 'nullable|string',
        'object' => 'nullable|string'
    ];

    public static function boot()
    {
        parent::boot();

        self::creating(function($model){
            $ord = self::where('content_id', $model->content_id)
                ->max('ord');

            $model->ord = ++$ord;
        });

        self::updating(function($model){
            $oldModel = self::find($model->id);
            if ($oldModel->content_id != $model->content_id) {
                self::where('content_id', $oldModel->content_id)
                    ->where('ord', '>', $oldModel->ord)
                    ->decrement('ord');

                $ord = self::where('content_id', $model->content_id)
                    ->max('ord');

                $model->ord = ++$ord;
            }

            if ($oldModel->is_active != $model->is_active) {
                if ($model->is_active == 0)
                    self::where('parent_id', $model->id)
                        ->where('is_active', 1)
                        ->update(['is_active' => 0]);

                if ($model->is_active == 1)
                    self::where('parent_id', $model->id)
                        ->where('is_active', 0)
                        ->update(['is_active' => 1]);
            }
        });

        self::deleting(function($model){
            if ($model->parent_id && $model->content_id && $model->ord) {
                self::where('content_id', $model->content_id)
                    ->where('ord', '>', $model->ord)
                    ->decrement('ord');
            }
        });
    }
}
