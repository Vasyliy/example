<?php

namespace App\Criteria;

use Carbon\Carbon;
use Prettus\Repository\Contracts\CriteriaInterface;
use Illuminate\Http\Request;

/**
 * Class DateCriteria.
 *
 * @package namespace App\Criteria;
 */
class DateCriteria implements CriteriaInterface
{
    /**
     * @var \Illuminate\Http\Request
     */
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Apply criteria in query repository.
     *
     * @param $model
     * @param \Prettus\Repository\Contracts\RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, \Prettus\Repository\Contracts\RepositoryInterface $repository)
    {
        if ($this->request->has('feature')) {
            $feature = $this->request->get('feature');
            if ($feature)
                $model = $model->where('date_end', '>=', Carbon::now()->setTime(0,0,0));
            else
                $model = $model->where('date_end', '<', Carbon::now()->setTime(0,0,0));
        }

        return $model;
    }
}
