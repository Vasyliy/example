<?php

namespace App\Repositories;

use App\Models\Content;
use InfyOm\Generator\Common\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;

/**
 * Class ContentRepository
 * @package App\Repositories
 * @version March 3, 2019, 11:40 pm UTC
 *
 * @method Content findWithoutFail($id, $columns = ['*'])
 * @method Content find($id, $columns = ['*'])
 * @method Content first($columns = ['*'])
*/
class ContentRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'content_id',
        'title',
        'url',
        'parent_id',
        'img',
        'file',
        'author_id',
        'is_active',
        'bool',
        'show_author',
        'show_date',
        'seo_img',
        'seo_alt',
        'seo_title',
        'seo_desc',
        'seo_key',
        'subtext',
        'text',
        'object',
        'date_start',
        'date_end',
        'ord',
        'views',
        'likes'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Content::class;
    }
}
