<?php

namespace App\Repositories;

use App\Models\Page;
use InfyOm\Generator\Common\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;

/**
 * Class PageRepository
 * @package App\Repositories
 * @version February 28, 2019, 3:28 pm UTC
 *
 * @method Page findWithoutFail($id, $columns = ['*'])
 * @method Page find($id, $columns = ['*'])
 * @method Page first($columns = ['*'])
*/
class PageRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;
    
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'url',
        'title',
        'text',
        'description',
        'keywords',
        'seo_image',
        'seo_title',
        'views',
        'likes'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Page::class;
    }
}
