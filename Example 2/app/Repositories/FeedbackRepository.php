<?php

namespace App\Repositories;

use App\Models\Feedback;
use InfyOm\Generator\Common\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;

/**
 * Class FeedbackRepository
 * @package App\Repositories
 * @version March 5, 2019, 5:41 pm UTC
 *
 * @method Feedback findWithoutFail($id, $columns = ['*'])
 * @method Feedback find($id, $columns = ['*'])
 * @method Feedback first($columns = ['*'])
*/
class FeedbackRepository extends BaseRepository implements CacheableInterface
{
    use CacheableRepository;

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'city',
        'message',
        'img',
        'parent_id',
        'type_id',
        'is_viewed',
        'is_active',
        'status_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Feedback::class;
    }
}
