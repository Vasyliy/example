<?php
namespace App\Services\Cors;

use Spatie\Cors\CorsProfile\DefaultProfile;

class UserBasedCorsProfile extends DefaultProfile
{
    public function addCorsHeaders($response)
    {
        return $response
            ->header('Access-Control-Allow-Origin', $this->allowedOriginsToString())
            ->header('Access-Control-Allow-Credentials', 'true')
            ->header('Access-Control-Allow-Headers', $this->toString($this->allowHeaders()))
            ->header('Access-Control-Expose-Headers', $this->toString($this->exposeHeaders()));
    }

    public function addPreflightHeaders($response)
    {
        return $response
            ->header('Access-Control-Allow-Methods', $this->toString($this->allowMethods()))
            ->header('Access-Control-Allow-Headers', $this->toString($this->allowHeaders()))
            ->header('Access-Control-Allow-Origin', $this->allowedOriginsToString())
            ->header('Access-Control-Expose-Headers', $this->toString($this->exposeHeaders()))
            ->header('Access-Control-Max-Age', $this->maxAge());
    }
}