<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', 'API\PassportController@register');

Route::post('contents/like/{id}', 'API\ContentAPIController@like');
Route::post('contents/increment/{id}', 'API\ContentAPIController@increment');
Route::get('contents/url/{content_id}/{url}/{use_case?}', 'API\ContentAPIController@url');
Route::get('contents/count', 'API\ContentAPIController@count');
Route::get('feedback/count', 'API\FeedbackAPIController@count');
Route::resource('contents', 'API\ContentAPIController')->only(['index', 'show']);
Route::post('feedback/like/{id}', 'API\FeedbackAPIController@like');
Route::resource('feedback', 'API\FeedbackAPIController')->only(['index', 'show']);
Route::post('pages/like/{id}', 'API\PageAPIController@like');
Route::resource('pages', 'API\PageAPIController')->only(['index', 'show']);
Route::get('pages/url/{url}/{use_case?}', 'API\PageAPIController@url');
Route::resource('feedback', 'API\FeedbackAPIController')->only(['store']);
Route::get('feedback/url/{type_id}/{url_id}/{use_case?}', 'API\FeedbackAPIController@url');

// StorageController
Route::post('uploadfile', 'API\StorageController@uploadFile');

Route::group(['middleware' => ['auth:api', 'admin']], function() {
    Route::resource('contents', 'API\ContentAPIController')->only(['store', 'update', 'destroy']);
    Route::resource('pages', 'API\PageAPIController')->only(['store', 'update', 'destroy']);
    Route::resource('feedback', 'API\FeedbackAPIController')->only(['update', 'destroy']);
    Route::put('contents/ord/{id}', 'API\ContentAPIController@ord');
    Route::delete('destroyfile/front', 'API\StorageController@destroyfileOnFront');
    Route::delete('destroyfile', 'API\StorageController@destroyfile');
    Route::post('uploadfile/front', 'API\StorageController@uploadFileOnFront');
    Route::post('uploadCKEditorImage', 'API\StorageController@uploadCKEditorImage');
});