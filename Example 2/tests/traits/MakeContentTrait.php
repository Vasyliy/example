<?php

use Faker\Factory as Faker;
use App\Models\Content;
use App\Repositories\ContentRepository;

trait MakeContentTrait
{
    /**
     * Create fake instance of Content and save it in database
     *
     * @param array $contentFields
     * @return Content
     */
    public function makeContent($contentFields = [])
    {
        /** @var ContentRepository $contentRepo */
        $contentRepo = App::make(ContentRepository::class);
        $theme = $this->fakeContentData($contentFields);
        return $contentRepo->create($theme);
    }

    /**
     * Get fake instance of Content
     *
     * @param array $contentFields
     * @return Content
     */
    public function fakeContent($contentFields = [])
    {
        return new Content($this->fakeContentData($contentFields));
    }

    /**
     * Get fake data of Content
     *
     * @param array $postFields
     * @return array
     */
    public function fakeContentData($contentFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'title' => $fake->word,
            'url' => $fake->word,
            'parent_id' => $fake->randomDigitNotNull,
            'img' => $fake->word,
            'file' => $fake->word,
            'author_id' => $fake->randomDigitNotNull,
            'is_active' => $fake->word,
            'show_author' => $fake->word,
            'show_date' => $fake->word,
            'seo_img' => $fake->word,
            'seo_alt' => $fake->word,
            'seo_title' => $fake->word,
            'seo_desc' => $fake->word,
            'seo_key' => $fake->word,
            'subtext' => $fake->text,
            'text' => $fake->text,
            'object' => $fake->text,
            'date_start' => $fake->word,
            'date_end' => $fake->word,
            'ord' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $contentFields);
    }
}
