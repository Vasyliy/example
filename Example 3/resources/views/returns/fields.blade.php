<!-- Interval Field -->
<div class="form-group col-sm-12">
    {!! Form::label('interval', 'Интервал:') !!}
    {!! Form::text('interval', null, ['class' => 'form-control']) !!}
</div>

<!-- Factor Field -->
<div class="form-group col-sm-12">
    {!! Form::label('factor', 'Коэфициент:') !!}
    {!! Form::text('factor', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('returns.index') !!}" class="btn btn-default">Отменить</a>
</div>
