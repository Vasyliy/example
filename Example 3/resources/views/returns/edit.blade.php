@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>Возврат Билетов</h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($returns, ['route' => ['returns.update', $returns->id], 'method' => 'patch']) !!}

                        @include('returns.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection