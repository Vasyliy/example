<table class="table table-responsive" id="returns-table">
    <thead>
        <tr>
            <th>Интрвал</th>
        <th>Коэфициент</th>
            <th colspan="3">Действия</th>
        </tr>
    </thead>
    <tbody>
    @foreach($returns as $returns)
        <tr>
            <td>{!! $returns->interval !!}</td>
            <td>{!! $returns->factor !!}</td>
            <td>
                {!! Form::open(['route' => ['returns.destroy', $returns->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <!--<a href="{!! route('returns.show', [$returns->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>-->
                    <a href="{!! route('returns.edit', [$returns->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Вы уверены?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>