<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $returns->id !!}</p>
</div>

<!-- Interval Field -->
<div class="form-group">
    {!! Form::label('interval', 'Interval:') !!}
    <p>{!! $returns->interval !!}</p>
</div>

<!-- Factor Field -->
<div class="form-group">
    {!! Form::label('factor', 'Factor:') !!}
    <p>{!! $returns->factor !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $returns->deleted_at !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $returns->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $returns->updated_at !!}</p>
</div>

