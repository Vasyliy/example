<table class="table table-responsive" id="tradingPlatformRoutes-table">
    <thead>
        <tr>
            <th>Trading Platform Id</th>
        <th>Route Id</th>
        <th>Name</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($tradingPlatformRoutes as $tradingPlatformRoutes)
        <tr>
            <td>{!! $tradingPlatformRoutes->agent_trading_platform_id !!}</td>
            <td>{!! $tradingPlatformRoutes->route_id !!}</td>
            <td>{!! $tradingPlatformRoutes->name !!}</td>
            <td>
                {!! Form::open(['route' => ['tradingPlatformRoutes.destroy', $tradingPlatformRoutes->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('tradingPlatformRoutes.show', [$tradingPlatformRoutes->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('tradingPlatformRoutes.edit', [$tradingPlatformRoutes->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>