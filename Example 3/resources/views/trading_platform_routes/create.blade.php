@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Trading Platform Routes
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'tradingPlatformRoutes.store']) !!}

                        @include('trading_platform_routes.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
