<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $tradingPlatformRoutes->id !!}</p>
</div>

<!-- Trading Platform Id Field -->
<div class="form-group">
    {!! Form::label('trading_platform_id', 'Trading Platform Id:') !!}
    <p>{!! $tradingPlatformRoutes->agent_trading_platform_id !!}</p>
</div>

<!-- Route Id Field -->
<div class="form-group">
    {!! Form::label('route_id', 'Route Id:') !!}
    <p>{!! $tradingPlatformRoutes->route_id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $tradingPlatformRoutes->name !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $tradingPlatformRoutes->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $tradingPlatformRoutes->updated_at !!}</p>
</div>

