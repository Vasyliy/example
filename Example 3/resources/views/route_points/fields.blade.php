<!-- Route Id Field -->
<div class="form-group col-sm-12">
    {!! Form::label('route_id', 'Маршрут:') !!}
    {!! Form::select('route_id', \App\Models\Route::getAll(), null, ['class' => 'form-control']) !!}
</div>

<!-- From Point Id Field -->
<div class="form-group col-sm-12">
    {!! Form::label('from_point_id', 'Из:') !!}
    {!! Form::select('from_point_id', \App\Models\Point::getAll(), null, ['class' => 'form-control']) !!}
</div>

<!-- To Point Id Field -->
<div class="form-group col-sm-12">
    {!! Form::label('to_point_id', 'В:') !!}
    {!! Form::select('to_point_id', \App\Models\Point::getAll(), null, ['class' => 'form-control']) !!}
</div>

<!-- Number Field -->
<div class="form-group col-sm-12">
    {!! Form::label('number', 'Номер в маршруте:') !!}
    {!! Form::number('number', null, ['class' => 'form-control']) !!}
</div>

<!-- Price Field -->
<div class="form-group col-sm-12">
    {!! Form::label('price', 'Цена:') !!}
    {!! Form::number('price', null, ['class' => 'form-control']) !!}
</div>

<!-- Distance Field -->
<div class="form-group col-sm-12">
    {!! Form::label('distance', 'Растояние:') !!}
    {!! Form::number('distance', null, ['class' => 'form-control']) !!}
</div>

<!-- From Time Field -->
<div class="form-group col-sm-12">
    {!! Form::label('from_time', 'Время выезда:') !!}
    {!! Form::number('from_time', null, ['class' => 'form-control']) !!}
</div>

<!-- To Time Field -->
<div class="form-group col-sm-12">
    {!! Form::label('to_time', 'Время приезда:') !!}
    {!! Form::number('to_time', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('routePoints.index') !!}" class="btn btn-default">Отменить</a>
</div>
