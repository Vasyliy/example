<table class="table table-responsive" id="routePoints-table">
    <thead>
        <tr>
            <th>Маршрут</th>
            <th>Из точки</th>
            <th>В точку</th>
            <th>Номер</th>
            <th>Цена</th>
            <th>Расстояние</th>
            <th>Время выезда</th>
            <th>Время приезда</th>
            <th colspan="3">Действия</th>
        </tr>
    </thead>
    <tbody>
    @foreach($routePoints as $routePoint)
        <tr>
            <td>{!! $routePoint->route_id !!}</td>
            <td>{!! $routePoint->from_point_id !!}</td>
            <td>{!! $routePoint->to_point_id !!}</td>
            <td>{!! $routePoint->number !!}</td>
            <td>{!! $routePoint->price !!}</td>
            <td>{!! $routePoint->distance !!}</td>
            <td>{!! $routePoint->from_time !!}</td>
            <td>{!! $routePoint->to_time !!}</td>
            <td>
                {!! Form::open(['route' => ['routePoints.destroy', $routePoint->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('routePoints.show', [$routePoint->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('routePoints.edit', [$routePoint->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Вы уверены?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>