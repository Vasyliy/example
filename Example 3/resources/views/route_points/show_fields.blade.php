<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $routePoint->id !!}</p>
</div>

<!-- Route Id Field -->
<div class="form-group">
    {!! Form::label('route_id', 'Route Id:') !!}
    <p>{!! $routePoint->route_id !!}</p>
</div>

<!-- From Point Id Field -->
<div class="form-group">
    {!! Form::label('from_point_id', 'From Point Id:') !!}
    <p>{!! $routePoint->from_point_id !!}</p>
</div>

<!-- To Point Id Field -->
<div class="form-group">
    {!! Form::label('to_point_id', 'To Point Id:') !!}
    <p>{!! $routePoint->to_point_id !!}</p>
</div>

<!-- Number Field -->
<div class="form-group">
    {!! Form::label('number', 'Number:') !!}
    <p>{!! $routePoint->number !!}</p>
</div>

<!-- Price Field -->
<div class="form-group">
    {!! Form::label('price', 'Price:') !!}
    <p>{!! $routePoint->price !!}</p>
</div>

<!-- Distance Field -->
<div class="form-group">
    {!! Form::label('distance', 'Distance:') !!}
    <p>{!! $routePoint->distance !!}</p>
</div>

<!-- From Time Field -->
<div class="form-group">
    {!! Form::label('from_time', 'From Time:') !!}
    <p>{!! $routePoint->from_time !!}</p>
</div>

<!-- To Time Field -->
<div class="form-group">
    {!! Form::label('to_time', 'To Time:') !!}
    <p>{!! $routePoint->to_time !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $routePoint->deleted_at !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $routePoint->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $routePoint->updated_at !!}</p>
</div>

