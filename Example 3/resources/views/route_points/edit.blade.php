@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>Отрезок маршрута</h1>
        <ol class="breadcrumb">
            <li><a href="{!! url('/search') !!}"><i class="fa fa-dashboard"></i>Главная</a></li>
            <li><a href="{!! url('/routePoints') !!}">Отрезки маршрута</a></li>
            <li class="active">Редактировать Отрезок маршрута</li>
        </ol>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($routePoint, ['route' => ['routePoints.update', $routePoint->id], 'method' => 'patch']) !!}

                        @include('route_points.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection