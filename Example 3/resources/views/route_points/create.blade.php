@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>Отрезок Мршрута</h1>
        <ol class="breadcrumb">
            <li><a href="{!! url('/search') !!}"><i class="fa fa-dashboard"></i>Главная</a></li>
            <li><a href="{!! url('/routePoints') !!}">Отрезки Мршрута</a></li>
            <li class="active">Создать Отрезок Мршрута</li>
        </ol>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'routePoints.store']) !!}

                        @include('route_points.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
