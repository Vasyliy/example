<table class="table table-responsive" id="tradingPlatforms-table">
    <thead>
        <tr>
            <th>Name</th>
        <th>Refresh Time 1</th>
        <th>Refresh Time 2</th>
        <th>Refresh Time 3</th>
        <th>Options</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($tradingPlatforms as $tradingPlatform)
        <tr>
            <td>{!! $tradingPlatform->name !!}</td>
            <td>{!! $tradingPlatform->refresh_time_1 !!}</td>
            <td>{!! $tradingPlatform->refresh_time_2 !!}</td>
            <td>{!! $tradingPlatform->refresh_time_3 !!}</td>
            <td>{!! $tradingPlatform->options !!}</td>
            <td>
                {!! Form::open(['route' => ['tradingPlatforms.destroy', $tradingPlatform->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('tradingPlatforms.show', [$tradingPlatform->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('tradingPlatforms.edit', [$tradingPlatform->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>