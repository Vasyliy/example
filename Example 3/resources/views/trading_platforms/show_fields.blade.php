<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $tradingPlatform->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $tradingPlatform->name !!}</p>
</div>

<!-- Refresh Time 1 Field -->
<div class="form-group">
    {!! Form::label('refresh_time_1', 'Refresh Time 1:') !!}
    <p>{!! $tradingPlatform->refresh_time_1 !!}</p>
</div>

<!-- Refresh Time 2 Field -->
<div class="form-group">
    {!! Form::label('refresh_time_2', 'Refresh Time 2:') !!}
    <p>{!! $tradingPlatform->refresh_time_2 !!}</p>
</div>

<!-- Refresh Time 3 Field -->
<div class="form-group">
    {!! Form::label('refresh_time_3', 'Refresh Time 3:') !!}
    <p>{!! $tradingPlatform->refresh_time_3 !!}</p>
</div>

<!-- Options Field -->
<div class="form-group">
    {!! Form::label('options', 'Options:') !!}
    <p>{!! $tradingPlatform->options !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $tradingPlatform->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $tradingPlatform->updated_at !!}</p>
</div>

