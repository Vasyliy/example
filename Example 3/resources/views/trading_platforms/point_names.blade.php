    <table class="table table-striped">
        <thead>
        <tr><th>Станция</th><th>Название</th><th colspan="1">Действия</th></tr>
        </thead>
        <tbody>
        @foreach($tradingPlatform->points as $point)
            <tr>
                <td><b>{!! $point->point->city->name !!}</b>
                    {!! $point->point->name !!}
                </td>
                <td>{!! $point->name !!}</td>
                <td>
                    {!! Form::open(['route' => ['tradingPlatformPoints.destroy', $point->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                            {!! Form::button('Удалить', ['type' => 'submit', 'class' => 'btn btn-danger', 'style' => 'width:80px', 'onclick' => "return confirm('Вы уверены?')"]) !!}
                        </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        <tr>
            {!! Form::open(['route' => 'tradingPlatformPoints.store']) !!}
                {!! Form::hidden('trading_platform_id', $tradingPlatform->id) !!}
                <td>{!! Form::select('point_id', \App\Models\Point::getAll(), null, ['class' => 'form-control fine_select']) !!}</td>
                <td>
                    <div class="input-group">
                        {!! Form::text('name', null, ['class' => 'form-control to_time', 'style'=>'width:100%;']) !!}
                    </div>
                </td>
                <td>
                    <div class='btn-group'>
                        {!! Form::submit('Добавить', ['class' => 'btn btn-primary', 'style' => 'width:80px']) !!}
                    </div>
                </td>
            {!! Form::close() !!}
        </tr>
        </tbody>
    </table>

@section('scripts')
    <script>
        $(function () {
            $('.fine_select').select2();
        });
    </script>
@endsection