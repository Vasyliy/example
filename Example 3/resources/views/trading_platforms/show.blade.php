@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Trading Platform
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('trading_platforms.show_fields')
                    <a href="{!! route('tradingPlatforms.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
