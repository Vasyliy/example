<!-- Name Field -->
<div class="form-group col-sm-12">
    {!! Form::label('name', 'Название:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Refresh Time 1 Field -->
<div class="form-group col-sm-4">
    {!! Form::label('refresh_time_1', 'Время обновления (1-й день):') !!}
    {!! Form::number('refresh_time_1', null, ['class' => 'form-control']) !!}
</div>

<!-- Refresh Time 2 Field -->
<div class="form-group col-sm-4">
    {!! Form::label('refresh_time_2', 'Время обновления (2-3 дни):') !!}
    {!! Form::number('refresh_time_2', null, ['class' => 'form-control']) !!}
</div>

<!-- Refresh Time 3 Field -->
<div class="form-group col-sm-4">
    {!! Form::label('refresh_time_3', 'Время обновления (4-14 дни):') !!}
    {!! Form::number('refresh_time_3', null, ['class' => 'form-control']) !!}
</div>

<!-- Options Field -->
@if(isset($tradingPlatform->options))
    @foreach(json_decode($tradingPlatform->options, true) as $key => $option)
        <div class="form-group col-sm-12">
            {!! Form::label('options['.$key.']', $key . ':') !!}
            {!! Form::text('options['.$key.']', $option, ['class' => 'form-control']) !!}
        </div>
    @endforeach
@endif

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('tradingPlatforms.index') !!}" class="btn btn-default">Отменить</a>
</div>
