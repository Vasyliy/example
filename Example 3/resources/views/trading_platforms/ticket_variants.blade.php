@foreach(\App\Models\Ticket::VARIANT as $key => $variant)
    <div class="form-group col-sm-6">
        {!! Form::label('variants['.$key.']', $variant . ':') !!}
        {!! Form::text('variants['.$key.']', isset($ticketVariants[$key]) ? $ticketVariants[$key] : null, ['class' => 'form-control']) !!}
    </div>
@endforeach

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('tradingPlatforms.edit', [$tradingPlatform->id]) !!}" class="btn btn-default">Отменить</a>
</div>