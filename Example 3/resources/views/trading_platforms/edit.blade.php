@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            {{ $tradingPlatform->name }}
        </h1>
    </section>
    <div class="content">
        @include('flash::message')
        @include('adminlte-templates::common.errors')
        <div class="row">
            <div class="col-md-6">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Настройки</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            {!! Form::model($tradingPlatform, ['route' => ['tradingPlatforms.update', $tradingPlatform->id], 'method' => 'patch']) !!}

                                @include('trading_platforms.fields')

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Статусы билетов</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            {!! Form::model($ticketVariants, ['route' => ['tradingPlatformTicketVariants.update', $tradingPlatform->id], 'method' => 'patch']) !!}

                            @include('trading_platforms.ticket_variants')

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Станции</h3>
                    </div>
                    <div class="box-body no-padding">
                        @include('trading_platforms.point_names')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection