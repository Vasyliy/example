<!-- Phone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phone', 'Номер Телефона:') !!}
    <div class="input-group">
        <div class="input-group-addon">
            <i class="fa fa-phone"></i>
        </div>
        {!! Form::text('phone', null, ['class' => 'form-control', 'data-inputmask' => '"mask": "+99(999)999-9999"', 'data-mask' => ""]) !!}
    </div>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('dispatcherPhones.index') !!}" class="btn btn-default">Отменить</a>
</div>

@section('scripts')
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            $(function () {
                $('[data-mask]').inputmask();
            })
        }, false);
    </script>
@endsection