@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>Номер телефона</h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($dispatcherPhone, ['route' => ['dispatcherPhones.update', $dispatcherPhone->id], 'method' => 'patch']) !!}
                        @include('dispatcher_phones.fields')
                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection