@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>Место</h1>
        <ol class="breadcrumb">
            <li><a href="{!! url('/search') !!}"><i class="fa fa-dashboard"></i>Главная</a></li>
            <li><a href="{!! url('/places') !!}">Места</a></li>
            <li class="active">Редактировать Место</li>
        </ol>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($place, ['route' => ['places.update', $place->id], 'method' => 'patch']) !!}

                        @include('places.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection