@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>Место</h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('places.show_fields')
                    <a href="{!! route('places.index') !!}" class="btn btn-default">Назад</a>
                </div>
            </div>
        </div>
    </div>
@endsection
