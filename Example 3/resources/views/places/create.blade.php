@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>Место</h1>
        <ol class="breadcrumb">
            <li><a href="{!! url('/search') !!}"><i class="fa fa-dashboard"></i>Главная</a></li>
            <li><a href="{!! url('/places') !!}">Места</a></li>
            <li class="active">Создать Место</li>
        </ol>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'places.store']) !!}

                        @include('places.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
