<table class="table table-responsive" id="places-table">
    <thead>
        <tr>
            <th>Схема</th>
            <th>Название/Место</th>
            <th>Статус</th>
            <th colspan="3">Действия</th>
        </tr>
    </thead>
    <tbody>
    @foreach($places as $place)
        <tr>
            <td>{!! $place->scheme->name !!}</td>
            <td>{!! $place->name !!}</td>
            <td>{!! $place->status !!}</td>
            <td>
                {!! Form::open(['route' => ['places.destroy', $place->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <!--<a href="{!! route('places.show', [$place->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>-->
                    <a href="{!! route('places.edit', [$place->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Вы уверены?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>