@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>Общие настройки</h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($settings, ['route' => ['settings.general.update', $settings->id], 'method' => 'patch']) !!}

                        @include('settings.fields_general')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection