@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>Настройки Билета</h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'settings.store']) !!}

                        @include('settings.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
