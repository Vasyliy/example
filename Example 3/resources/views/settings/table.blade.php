<table class="table table-responsive" id="settings-table">
    <thead>
        <tr>
            <th>Baggage Comment</th>
        <th>Additional Comment</th>
        <th>Return Comment</th>
        <th>Agreement Comment</th>
        <th>Map Comment</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($settings as $settings)
        <tr>
            <td>{!! $settings->baggage_comment !!}</td>
            <td>{!! $settings->additional_comment !!}</td>
            <td>{!! $settings->return_comment !!}</td>
            <td>{!! $settings->agreement_comment !!}</td>
            <td>{!! $settings->map_comment !!}</td>
            <td>
                {!! Form::open(['route' => ['settings.destroy', $settings->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('settings.show', [$settings->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('settings.edit', [$settings->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>