<!-- Notification Email Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('notification_email', 'Email для уведомлений:') !!}
    {!! Form::text('notification_email', null, ['class' => 'form-control']) !!}
</div>

<!-- Critical Places Number Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('critical_places_number', 'Критическое количество мест:') !!}
    {!! Form::number('critical_places_number', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
            <!--<a href="{!! route('settings.index') !!}" class="btn btn-default">Cancel</a>-->
</div>