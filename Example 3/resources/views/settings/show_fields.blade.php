<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $settings->id !!}</p>
</div>

<!-- Baggage Comment Field -->
<div class="form-group">
    {!! Form::label('baggage_comment', 'Baggage Comment:') !!}
    <p>{!! $settings->baggage_comment !!}</p>
</div>

<!-- Additional Comment Field -->
<div class="form-group">
    {!! Form::label('additional_comment', 'Additional Comment:') !!}
    <p>{!! $settings->additional_comment !!}</p>
</div>

<!-- Return Comment Field -->
<div class="form-group">
    {!! Form::label('return_comment', 'Return Comment:') !!}
    <p>{!! $settings->return_comment !!}</p>
</div>

<!-- Agreement Comment Field -->
<div class="form-group">
    {!! Form::label('agreement_comment', 'Agreement Comment:') !!}
    <p>{!! $settings->agreement_comment !!}</p>
</div>

<!-- Map Comment Field -->
<div class="form-group">
    {!! Form::label('map_comment', 'Map Comment:') !!}
    <p>{!! $settings->map_comment !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $settings->deleted_at !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $settings->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $settings->updated_at !!}</p>
</div>

