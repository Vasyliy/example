<!-- Baggage Comment Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('baggage_comment', 'Багаж:') !!}
    {!! Form::textarea('baggage_comment', null, ['class' => 'form-control']) !!}
</div>

<!-- Additional Comment Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('additional_comment', 'Дополнительная информация:') !!}
    {!! Form::textarea('additional_comment', null, ['class' => 'form-control']) !!}
</div>

<!-- Return Comment Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('return_comment', 'Условия возврата билета:') !!}
    {!! Form::textarea('return_comment', null, ['class' => 'form-control']) !!}
</div>

<!-- Agreement Comment Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('agreement_comment', 'Соглашение о перевозке:') !!}
    {!! Form::textarea('agreement_comment', null, ['class' => 'form-control', 'id' => 'agreement_comment_editor']) !!}
</div>

<!-- Map Comment Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('map_comment', 'Уточнение к карте:') !!}
    {!! Form::textarea('map_comment', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
    <!--<a href="{!! route('settings.index') !!}" class="btn btn-default">Cancel</a>-->
</div>

<script>
    document.addEventListener('DOMContentLoaded', function() {
        $(function () {
            // Replace the <textarea id="editor1"> with a CKEditor
            // instance, using default configuration.
            // CKEDITOR.replace('note_editor');
            //bootstrap WYSIHTML5 - text editor
            $('#agreement_comment_editor').wysihtml5();
        })
    }, false);
</script>