@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Trip Point Blocked
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'tripPointBlockeds.store']) !!}

                        @include('trip_point_blockeds.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
