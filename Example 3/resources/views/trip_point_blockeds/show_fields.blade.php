<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $tripPointBlocked->id !!}</p>
</div>

<!-- Trip Id Field -->
<div class="form-group">
    {!! Form::label('trip_id', 'Trip Id:') !!}
    <p>{!! $tripPointBlocked->trip_id !!}</p>
</div>

<!-- Point Id Field -->
<div class="form-group">
    {!! Form::label('point_id', 'Point Id:') !!}
    <p>{!! $tripPointBlocked->point_id !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $tripPointBlocked->deleted_at !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $tripPointBlocked->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $tripPointBlocked->updated_at !!}</p>
</div>

