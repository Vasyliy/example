<table class="table table-responsive" id="tripPointBlockeds-table">
    <thead>
        <tr>
            <th>Trip Id</th>
        <th>Point Id</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($tripPointBlockeds as $tripPointBlocked)
        <tr>
            <td>{!! $tripPointBlocked->trip_id !!}</td>
            <td>{!! $tripPointBlocked->point_id !!}</td>
            <td>
                {!! Form::open(['route' => ['tripPointBlockeds.destroy', $tripPointBlocked->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('tripPointBlockeds.show', [$tripPointBlocked->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('tripPointBlockeds.edit', [$tripPointBlocked->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>