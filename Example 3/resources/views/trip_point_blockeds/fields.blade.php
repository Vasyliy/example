<!-- Trip Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('trip_id', 'Trip Id:') !!}
    {!! Form::number('trip_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Point Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('point_id', 'Point Id:') !!}
    {!! Form::number('point_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('tripPointBlockeds.index') !!}" class="btn btn-default">Cancel</a>
</div>
