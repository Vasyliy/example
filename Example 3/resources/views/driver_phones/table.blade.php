<table class="table table-responsive" id="driverPhones-table">
    <thead>
        <tr>
            <th>Driver Id</th>
        <th>Phone</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($driverPhones as $driverPhone)
        <tr>
            <td>{!! $driverPhone->driver_id !!}</td>
            <td>{!! $driverPhone->phone !!}</td>
            <td>
                {!! Form::open(['route' => ['driverPhones.destroy', $driverPhone->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('driverPhones.show', [$driverPhone->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('driverPhones.edit', [$driverPhone->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>