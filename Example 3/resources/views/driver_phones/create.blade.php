@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>Телефон</h1>
        <ol class="breadcrumb">
            <li><a href="{!! url('/search') !!}"><i class="fa fa-dashboard"></i>Главная</a></li>
            <li><a href="{!! url('/driverPhones') !!}">Телефоны</a></li>
            <li class="active">Создать Телефон</li>
        </ol>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'driverPhones.store']) !!}

                        @include('driver_phones.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
