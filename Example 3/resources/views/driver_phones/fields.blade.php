<!-- Driver Id Field -->
<div class="form-group col-sm-12">
    {!! Form::label('driver_id', 'Водитель:') !!}
    {!! Form::select('driver_id', [$driver->id=>$driver->name], $driver->id, ['class' => 'form-control']) !!}
</div>

<!-- Phone Field -->
<div class="form-group col-sm-12">
    {!! Form::label('phone', 'Телефон:') !!}
    <div class="input-group">
        <div class="input-group-addon">
            <i class="fa fa-phone"></i>
        </div>
        {!! Form::text('phone', null, ['class'=>'form-control', 'data-inputmask'=>"'mask': ['+99 (999) 999-99-99']", 'data-mask'=>"" ]) !!}
    </div>

</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('drivers.index') !!}" class="btn btn-default">Отменить</a>
</div>


<script>
    document.addEventListener('DOMContentLoaded', function() {
        $(function () {
            $('[data-mask]').inputmask();
        })
    }, false);

</script>