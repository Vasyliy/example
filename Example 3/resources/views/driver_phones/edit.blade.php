@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>Телефон</h1>
        <ol class="breadcrumb">
            <li><a href="{!! url('/search') !!}"><i class="fa fa-dashboard"></i>Главная</a></li>
            <li><a href="{!! url('/driverPhones') !!}">Телефоны</a></li>
            <li class="active">Редактировать Телефон</li>
        </ol>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($driverPhone, ['route' => ['driverPhones.update', $driverPhone->id], 'method' => 'patch']) !!}

                        @include('driver_phones.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection