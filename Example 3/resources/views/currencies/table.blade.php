<table class="table table-responsive" id="currencies-table">
    <thead>
        <tr>
            <th>Название</th>
            <th>Сокращение</th>
            <!--<th>Exchange</th>-->
            <th colspan="3">Действия</th>
        </tr>
    </thead>
    <tbody>
    @foreach($currencies as $currency)
        <tr>
            <td>{!! $currency->name !!}</td>
            <td>{!! $currency->short !!}</td>
            <!--<td>{!! $currency->exchange !!}</td>-->
            <td>
                {!! Form::open(['route' => ['currencies.destroy', $currency->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <!--<a href="{!! route('currencies.show', [$currency->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>-->
                    <a href="{!! route('currencies.edit', [$currency->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Вы уверены?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>