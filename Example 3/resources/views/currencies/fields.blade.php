<!-- Name Field -->
<div class="form-group col-sm-12">
    {!! Form::label('name', 'Название (по умолчанию):') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}

    @foreach(\App\Models\Lang::all() as $lang_item)
    <div class="form-group col-sm-12">
        <label>Название для {{ $lang_item->name }}</label>
        {!! Form::text('translate['.$lang_item->id.']', isset($currency) ? $currency->getTranslate($lang_item->id) : '', ['class' => 'form-control']) !!}
    </div>
    @endforeach
</div>

<!-- Short Field -->
<div class="form-group col-sm-12">
    {!! Form::label('short', 'Сокращение:') !!}
    {!! Form::text('short', null, ['class' => 'form-control']) !!}
</div>

<!-- Symbol Field -->
<div class="form-group col-sm-12">
    {!! Form::label('symbol', 'Символ:') !!}
    {!! Form::text('symbol', null, ['class' => 'form-control']) !!}
</div>

<!-- Exchange Field -->
<!--<div class="form-group col-sm-12">
    {!! Form::label('exchange', 'Курс:') !!}
    {!! Form::number('exchange', null, ['class' => 'form-control']) !!}
</div>-->

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('currencies.index') !!}" class="btn btn-default">Отменить</a>
</div>
