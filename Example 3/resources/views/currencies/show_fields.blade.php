<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $currency->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $currency->name !!}</p>
</div>

<!-- Short Field -->
<div class="form-group">
    {!! Form::label('short', 'Short:') !!}
    <p>{!! $currency->short !!}</p>
</div>

<!-- Symbol Field -->
<div class="form-group col-sm-12">
    {!! Form::label('symbol', 'Символ:') !!}
    <p>{!! $currency->symbol !!}</p>
</div>

<!-- Exchange Field -->
<div class="form-group">
    {!! Form::label('exchange', 'Exchange:') !!}
    <p>{!! $currency->exchange !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $currency->deleted_at !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $currency->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $currency->updated_at !!}</p>
</div>

