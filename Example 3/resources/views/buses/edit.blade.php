@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>Автобус</h1>
        <ol class="breadcrumb">
            <li><a href="{!! url('/search') !!}"><i class="fa fa-dashboard"></i>Главная</a></li>
            <li><a href="{!! url('/buses') !!}">Автобусы</a></li>
            <li class="active">Редактировать Автобус</li>
        </ol>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($bus, ['route' => ['buses.update', $bus->id], 'method' => 'patch']) !!}

                        @include('buses.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection