<table class="table table-responsive" id="buses-table">
    <thead>
        <tr>
            <th>Схема</th>
            <th>Название</th>
            <th>Модель</th>
            <th>Гос Номер</th>
            <th>Примечания</th>
            <th colspan="3">Действия</th>
        </tr>
    </thead>
    <tbody>
    @foreach($buses as $bus)
        <tr>
            <td>{!! $bus->scheme->name !!}</td>
            <td>{!! $bus->name !!}</td>
            <td>{!! $bus->model !!}</td>
            <td>{!! $bus->number !!}</td>
            <td>{!! $bus->note !!}</td>
            <td>
                {!! Form::open(['route' => ['buses.destroy', $bus->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <!--<a href="{!! route('buses.show', [$bus->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>-->
                    <a href="{!! route('buses.edit', [$bus->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>