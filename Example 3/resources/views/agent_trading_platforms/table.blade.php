<table class="table table-responsive" id="agentTradingPlatforms-table">
    <thead>
        <tr>
        <th>Агент</th>
        <th>Сервис</th>
            <th colspan="3">Действия</th>
        </tr>
    </thead>
    <tbody>
    @foreach($agentTradingPlatforms as $agentTradingPlatform)
        <tr>
            <td>{!! $agentTradingPlatform->agent->name !!}</td>
            <td>{!! $agentTradingPlatform->tradingPlatform->name !!}</td>
            <td>
                {!! Form::open(['route' => ['agentTradingPlatforms.destroy', $agentTradingPlatform->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('agentTradingPlatforms.edit', [$agentTradingPlatform->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Вы уверены?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>