@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Настройки Агенты/Сервисы
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'agentTradingPlatforms.store']) !!}

                        @include('agent_trading_platforms.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
