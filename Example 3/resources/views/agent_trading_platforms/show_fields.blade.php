<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $agentTradingPlatform->id !!}</p>
</div>

<!-- Agent Id Field -->
<div class="form-group">
    {!! Form::label('agent_id', 'Agent Id:') !!}
    <p>{!! $agentTradingPlatform->agent_id !!}</p>
</div>

<!-- Trading Platform Id Field -->
<div class="form-group">
    {!! Form::label('trading_platform_id', 'Trading Platform Id:') !!}
    <p>{!! $agentTradingPlatform->trading_platform_id !!}</p>
</div>

<!-- Options Field -->
<div class="form-group">
    {!! Form::label('options', 'Options:') !!}
    <p>{!! $agentTradingPlatform->options !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $agentTradingPlatform->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $agentTradingPlatform->updated_at !!}</p>
</div>

