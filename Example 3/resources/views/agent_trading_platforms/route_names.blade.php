    <table class="table table-striped">
        <thead>
        <tr><th>Маршрут</th><th>Название</th><th colspan="1">Действия</th></tr>
        </thead>
        <tbody>
        @foreach($agentTradingPlatform->routes as $route)
            <tr>
                <td>{!! $route->route->name !!}</td>
                <td>{!! $route->name !!}</td>
                <td>
                    {!! Form::open(['route' => ['tradingPlatformRoutes.destroy', $route->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        {!! Form::button('Удалить', ['type' => 'submit', 'class' => 'btn btn-danger', 'style' => 'width:80px', 'onclick' => "return confirm('Вы уверены?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        <tr>
            {!! Form::open(['route' => 'tradingPlatformRoutes.store']) !!}
                {!! Form::hidden('agent_trading_platform_id', $agentTradingPlatform->id) !!}
                <td>{!! Form::select('route_id', \App\Models\Route::getAll(), null, ['class' => 'form-control fine_select']) !!}</td>
                <td>
                    <div class="input-group">
                        {!! Form::text('name', null, ['class' => 'form-control to_time', 'style'=>'width:100%;']) !!}
                    </div>
                </td>
                <td>
                    <div class='btn-group'>
                        {!! Form::submit('Добавить', ['class' => 'btn btn-primary', 'style' => 'width:80px']) !!}
                    </div>
                </td>
            {!! Form::close() !!}
        </tr>
        </tbody>
    </table>

@section('scripts')
    <script>
        $(function () {
            $('.fine_select').select2();
        });
    </script>
@endsection