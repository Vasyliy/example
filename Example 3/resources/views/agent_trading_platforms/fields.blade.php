<!-- Agent Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('agent_id', 'Агент:') !!}
    {!! Form::select('agent_id', \App\Models\Agent::getAll(), null, ['class' => 'form-control fine_select']) !!}
</div>

<!-- Trading Platform Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('trading_platform_id', 'Торговая площадка:') !!}
    {!! Form::select('trading_platform_id', \App\Models\TradingPlatform::getAll(), null, ['class' => 'form-control fine_select']) !!}
</div>

<!-- Options Field -->
@if(isset($agentTradingPlatform->options))
    @foreach(json_decode($agentTradingPlatform->options, true) as $key => $option)
        <div class="form-group col-sm-12">
            {!! Form::label('options['.$key.']', $key . ':') !!}
            {!! Form::text('options['.$key.']', $option, ['class' => 'form-control']) !!}
        </div>
    @endforeach
@else
    <div class="form-group col-sm-12">
        {!! Form::label('options', 'Настройки(json):') !!}
        {!! Form::text('options', null, ['class' => 'form-control']) !!}
    </div>
@endif

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('agentTradingPlatforms.index') !!}" class="btn btn-default">Отменить</a>
</div>

@section('scripts')
    <script>
        $(function () {
            $('.fine_select').select2();
        });
    </script>
@endsection