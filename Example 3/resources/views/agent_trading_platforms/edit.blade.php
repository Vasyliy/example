@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Настройки Агенты/Сервисы
        </h1>
   </section>
   <div class="content">
       @include('flash::message')
       @include('adminlte-templates::common.errors')
       <div class="col-md-6">
           <div class="box box-primary">
               <div class="box-header with-border">
                   <h3 class="box-title">Новости</h3>
               </div>
               <div class="box-body">
                   <div class="row">
                       {!! Form::model($agentTradingPlatform, ['route' => ['agentTradingPlatforms.update', $agentTradingPlatform->id], 'method' => 'patch']) !!}

                       @include('agent_trading_platforms.fields')

                       {!! Form::close() !!}
                   </div>
               </div>
           </div>
       </div>
       <div class="col-md-6">
           <div class="box">
               <div class="box-header with-border">
                   <h3 class="box-title">Маршруты</h3>
               </div>
               <div class="box-body no-padding">
                   @include('agent_trading_platforms.route_names')
               </div>
           </div>
       </div>
   </div>
@endsection