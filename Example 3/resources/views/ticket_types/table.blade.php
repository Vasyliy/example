<table class="table table-responsive" id="ticketTypes-table">
    <thead>
        <tr>
            <th>Название</th>
            <th>Сокращение</th>
            <th>Скидка</th>
            <th>Примечания</th>
            <th colspan="3">Действия</th>
        </tr>
    </thead>
    <tbody>
    @foreach($ticketTypes as $ticketType)
        <tr>
            <td>{!! $ticketType->name !!}</td>
            <td>{!! $ticketType->short !!}</td>
            <td>{!! $ticketType->discount !!}</td>
            <td>{!! $ticketType->note !!}</td>
            <td>
                {!! Form::open(['route' => ['ticketTypes.destroy', $ticketType->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <!--<a href="{!! route('ticketTypes.show', [$ticketType->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>-->
                    <a href="{!! route('ticketTypes.edit', [$ticketType->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Вы уверены?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>