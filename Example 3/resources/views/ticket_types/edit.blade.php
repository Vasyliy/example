@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>Тип билетов</h1>
        <ol class="breadcrumb">
            <li><a href="{!! url('/search') !!}"><i class="fa fa-dashboard"></i>Главная</a></li>
            <li><a href="{!! url('/ticketTypes') !!}">Типы билетов</a></li>
            <li class="active">Редактировать Тип билетов</li>
        </ol>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ticketType, ['route' => ['ticketTypes.update', $ticketType->id], 'method' => 'patch']) !!}

                        @include('ticket_types.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection