<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Название:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}

    @foreach(\App\Models\Lang::all() as $lang_item)
        <div class="form-group col-sm-12">
            <label>Название для {{ $lang_item->name }}</label>
            {!! Form::text('translate['.$lang_item->id.']', isset($ticketType) ? $ticketType->getTranslate($lang_item->id) : '', ['class' => 'form-control']) !!}
        </div>
    @endforeach
</div>

<!-- Discount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('short', 'Сокращение:') !!}
    {!! Form::text('short', null, ['class' => 'form-control']) !!}
</div>

<!-- Discount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('discount', 'Скидка в %:') !!}
    {!! Form::number('discount', null, ['class' => 'form-control']) !!}
</div>

<!-- Note Field -->
<div class="form-group col-sm-6">
    {!! Form::label('note', 'Примечание:') !!}
    {!! Form::text('note', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('ticketTypes.index') !!}" class="btn btn-default">Отменить</a>
</div>
