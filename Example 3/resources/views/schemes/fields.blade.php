<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Название:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}

    @foreach(\App\Models\Lang::all() as $lang_item)
        <div class="form-group col-sm-12">
            <label>Перевод для {{ $lang_item->name }}</label>
            {!! Form::text('translate['.$lang_item->id.']', isset($scheme) ? $scheme->getTranslate($lang_item->id) : '', ['class' => 'form-control']) !!}
        </div>
    @endforeach
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('schemes.index') !!}" class="btn btn-default">Отменить</a>
</div>
