@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>Схема</h1>
        <ol class="breadcrumb">
            <li><a href="{!! url('/search') !!}"><i class="fa fa-dashboard"></i>Главная</a></li>
            <li><a href="{!! url('/schemes') !!}">Схемы</a></li>
            <li class="active">Редактировать схему</li>
        </ol>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($scheme, ['route' => ['schemes.update', $scheme->id], 'method' => 'patch']) !!}

                        @include('schemes.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection