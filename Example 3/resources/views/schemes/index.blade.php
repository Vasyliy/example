@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Схемы</h1>
        <ol class="breadcrumb">
            <li><a href="{!! url('/search') !!}"><i class="fa fa-dashboard"></i>Главная</a></li>
            <li class="active">Схемы</li>
        </ol>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                    @include('schemes.table')
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
@endsection

