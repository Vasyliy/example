<table class="table table-responsive" id="schemes-table">
    <thead>
        <tr>
            <th>Название</th>
            <th>Количество мест</th>
            <th colspan="3">Действия</th>
        </tr>
    </thead>
    <tbody>
    @foreach($schemes as $scheme)
        <tr>
            <td>{!! $scheme->name !!}</td>
            <td>{!! count($scheme->places) !!}</td>
            <td>
                {!! Form::open(['route' => ['schemes.destroy', $scheme->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <!--<a href="{!! route('schemes.show', [$scheme->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>-->
                    <!--<a href="{!! route('places.create') !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-plus"></i></a>-->
                    <a href="{!! route('schemes.edit', [$scheme->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Вы уверены?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>