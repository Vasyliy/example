@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Trading Platform Point
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($tradingPlatformPoint, ['route' => ['tradingPlatformPoints.update', $tradingPlatformPoint->id], 'method' => 'patch']) !!}

                        @include('trading_platform_points.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection