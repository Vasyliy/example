<table class="table table-responsive" id="tradingPlatformPoints-table">
    <thead>
        <tr>
            <th>Trading Platform Id</th>
        <th>Point Id</th>
        <th>Name</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($tradingPlatformPoints as $tradingPlatformPoint)
        <tr>
            <td>{!! $tradingPlatformPoint->trading_platform_id !!}</td>
            <td>{!! $tradingPlatformPoint->point_id !!}</td>
            <td>{!! $tradingPlatformPoint->name !!}</td>
            <td>
                {!! Form::open(['route' => ['tradingPlatformPoints.destroy', $tradingPlatformPoint->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('tradingPlatformPoints.show', [$tradingPlatformPoint->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('tradingPlatformPoints.edit', [$tradingPlatformPoint->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>