<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $tradingPlatformPoint->id !!}</p>
</div>

<!-- Trading Platform Id Field -->
<div class="form-group">
    {!! Form::label('trading_platform_id', 'Trading Platform Id:') !!}
    <p>{!! $tradingPlatformPoint->trading_platform_id !!}</p>
</div>

<!-- Point Id Field -->
<div class="form-group">
    {!! Form::label('point_id', 'Point Id:') !!}
    <p>{!! $tradingPlatformPoint->point_id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $tradingPlatformPoint->name !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $tradingPlatformPoint->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $tradingPlatformPoint->updated_at !!}</p>
</div>

