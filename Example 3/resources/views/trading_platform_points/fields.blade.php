<!-- Trading Platform Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('trading_platform_id', 'Trading Platform Id:') !!}
    {!! Form::number('trading_platform_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Point Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('point_id', 'Point Id:') !!}
    {!! Form::number('point_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('tradingPlatformPoints.index') !!}" class="btn btn-default">Cancel</a>
</div>
