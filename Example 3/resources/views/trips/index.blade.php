@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>Рейсы</h1>
        <ol class="breadcrumb">
            <li><a href="{!! url('/search') !!}"><i class="fa fa-dashboard"></i>Главная</a></li>
            <li class="active">Рейсы</li>
        </ol>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title pull-right">
                    <a class="btn btn-primary pull-right" href="{!! route('trips.create') !!}">Добавить</a>
                </h3>
            </div>
            <div class="box-body">
                <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                    <div class="row">
                        {!! Form::open(['route' => 'trips.index', 'role'=>'search', 'method'=>'get']) !!}
                        <div class="form-group col-sm-5">
                            {!! Form::label('route_id', 'Маршрут:') !!}
                            <div class="input-group">
                                {!! Form::select('route_id', \App\Models\Route::getAll(), null, ['class' => 'form-control pull-right', 'placeholder' => 'Все маршруты']) !!}
                            </div>
                        </div>
                        <div class="form-group col-sm-5">
                            {!! Form::label('date', 'Дата:') !!}
                            <div class="input-group date">
                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                {!! Form::text('date', null, ['class' => 'form-control pull-right', 'id'=>'searchdatepicker']) !!}
                                <span class="input-group-addon">
                                    {!! Form::checkbox('deviation') !!}
                                    +
                                    {!! Form::select('deviation_amount', ['1' => '1', '7' => '7', '14' => '14', '30' => '30', '60' => '60'], null, ['class' => 'deviation-amount']) !!}
                                </span>
                            </div>
                        </div>
                        <div class="form-group col-sm-2">
                            {!! Form::submit('Отфильтровать', ['class' => 'form-control btn btn-primary']) !!}
                        </div>
                        {!! Form::close() !!}
                        <!--<div class="col-sm-6">
                            <div class="dataTables_length" id="example1_length">
                                <label>
                                    Show
                                    <select name="example1_length" aria-controls="example1" class="form-control input-sm">
                                        <option value="10">10</option><option value="25">25</option>
                                        <option value="50">50</option><option value="100">100</option>
                                    </select>
                                    entries
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div id="example1_filter" class="dataTables_filter">
                                <label>Search:
                                    <input type="search" class="form-control input-sm" placeholder="" aria-controls="example1">
                                </label>
                            </div>
                        </div>-->
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            @include('trips.table')
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="dataTables_info" id="example1_info">
                                @if( $trips->count() < $trips->total() )
                                Показано от {{ ($trips->currentPage() - 1) * $trips->perPage() + 1 }} до
                                    @if( $trips->currentPage() * $trips->perPage() > $trips->total() )
                                        {{ $trips->total() }}
                                    @else
                                        {{ $trips->currentPage() * $trips->perPage() }}
                                    @endif
                                    из {{ $trips->total() }} рейсов
                                @endif
                            </div>
                        </div>
                        <div class="col-sm-7">
                            <div class="dataTables_paginate paging_simple_numbers pull-right">
                                {{ $trips->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(function () {
            $('#searchdatepicker').datetimepicker({
                format: 'DD-MM-YYYY'
                //,language: 'ru'
                , locale: 'ru'
            });
        });
    </script>
@endsection