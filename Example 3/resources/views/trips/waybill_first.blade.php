@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">
            Путевой лист.
            Маршрут "{{ $trip->route->name }}"
            (Отправка в рейс - {{ $trip->date }})
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">

                <table class="table table-responsive" id="trips-table">
                    <thead>
                    <tr>
                        <th colspan="6">Список пассажиров</th>
                    </tr>
                    <tr>
                        <th>ФИО</th>
                        <th>Телефон</th>
                        <th>E-mail</th>
                        <th>Агент</th>
                        <th>Место</th>
                        <th>От</th>
                        <th>До</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($trip->tickets as $ticket)
                        <tr>
                            <td>
                                {!! isset($ticket->client)?$ticket->client->name1.' '.$ticket->client->name2.' '.$ticket->client->name3 :'-' !!}
                            </td>
                            <td>{!! isset($ticket->client)?$ticket->client->phone:'Нет' !!}</td>
                            <td>{!! isset($ticket->client)?$ticket->client->email:'Нет' !!}</td>
                            <td>{!! isset($ticket->agent)?$ticket->agent->name:'Нет' !!}</td>
                            <td>{!! isset($ticket->place)?$ticket->place->name:'Нет'  !!}</td>
                            <td>{!! $ticket->from->name !!}</td>
                            <td>{!! $ticket->to->name !!}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="text">
            Билетов всего: {{ count($trip->tickets) }}
        </div>
    </div>
@endsection

