<div class="form-group col-sm-6">

    <h3>Точки Маршрута</h3>

    <table class="table table-responsive" id="blocked-points-table">
        <thead>
            <tr><th colspan="5">Блокированные Точки Маршрута</th></tr>
            <tr><th>Точка Маршрута</th><th colspan="3">Действия</th></tr>
        </thead>
        <tbody>
        @foreach($trip->blocked_points as $blocked_point)
        <tr>
            <td>{!! $blocked_point->point->name !!}</td>
            <td>
                {!! Form::open(['route' => ['tripPointBlockeds.destroy', $blocked_point->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Вы уверены?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
        <tr>
            {!! Form::open(['route' => 'tripPointBlockeds.store']) !!}
                {!! Form::hidden('trip_id', $trip->id) !!}
            <td>{!! Form::select('point_id', \App\Models\Point::getAllfromRoute($trip->route_id), null, ['class' => 'form-control', 'placeholder'=>'Выбирете точку для блокировки']) !!}</td>
            <td>
                <div class='btn-group'>
                    {!! Form::submit('Добавить', ['class' => 'btn btn-primary']) !!}
                </div>
            </td>
            {!! Form::close() !!}
        </tr>
        </tbody>
    </table>
</div>

@section('scripts')
    <script>
    </script>
@endsection