    <!-- Route Id Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('route_id', 'Маршрут:') !!} <span>{{ isset($trip->route)?$trip->route->name:'' }}</span>
        <!--{!! Form::select('route_id', \App\Models\Route::getAll(), null, ['class' => 'form-control', 'placeholder' => 'Выберите ...']) !!}-->
    </div>

    <!-- Date Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('date', 'Дата:') !!} <span>{{ isset($trip->date)?$trip->date:'' }}</span>
        <!--{!! Form::text('date', null, ['class' => 'form-control', 'id'=>'datetime_input']) !!}-->
    </div>

    <!-- Sсheme Id Field -->
    <!--<div class="form-group col-sm-12">
        {!! Form::label('scheme_id', 'Схема мест:') !!}
        {!! Form::select('scheme_id', \App\Models\Scheme::getAll(), null, ['class' => 'form-control', 'placeholder' => 'Выберите ...']) !!}
    </div>-->

    <!-- Bus Id Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('bus_id', 'Автобус:') !!}
        {!! Form::select('bus_id', \App\Models\Bus::getAll(), null, ['class' => 'form-control', 'placeholder' => 'Выберите ...']) !!}
    </div>

    <!-- Driver Id Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('driver_id', 'Водитель №1:') !!}
        {!! Form::select('driver_id', \App\Models\Driver::getAll(), null, ['class' => 'form-control', 'placeholder' => 'Выберите ...']) !!}
    </div>

    <!-- Driver2 Id Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('driver2_id', 'Водитель №2:') !!}
        {!! Form::select('driver2_id', \App\Models\Driver::getAll(), null, ['class' => 'form-control', 'placeholder' => 'Выберите ...']) !!}
    </div>

    <!-- Phone Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('phone', 'Телефоны:') !!}
        {!! Form::text('phone', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Submit Field -->
    <div class="form-group col-sm-12">
        {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
        <a href="{!! route('trips.index') !!}" class="btn btn-default">Отменить</a>
    </div>

@section('scripts')
    <script>
        $(function () {
            $('#datetime_input').datetimepicker({
                format: 'DD-MM-YYYY HH:mm'
            });
        });
    </script>
@endsection