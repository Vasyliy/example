    <table class="table table-responsive" id="blocked-points-table">
        <thead>
            <tr><th>Точка Маршрута</th><th colspan="3">Действия</th></tr>
        </thead>
        <tbody>
        @foreach($trip->route->allPoints() as $point)
            <tr>
                <td>{!! $point->name !!}</td>
                <td>
                    {!! Form::open(['route' => ['tripPointBlockeds.destroy', $point->blockedInTripId($trip->id)], 'method' => 'delete']) !!}

                    <div class='btn-group'>
                        @if($point->isBlockedInTrip($trip->id))
                            {!! Form::button('<i class="glyphicon glyphicon-remove"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'title'=>'Разблокировать']) !!}
                        @else
                            <a href="{!! url('/tripPointBlockeds/block', ['trip'=>$trip->id, 'point'=>$point->id]) !!}" class='btn btn-success btn-xs' title="Блокировать"><i class="glyphicon glyphicon-ok"></i></a>
                        @endif
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

@section('scripts')
    <script>
    </script>
@endsection