@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Список брони рейса</h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                <table class="table table-responsive" id="trips-table">
                    <thead>
                    <tr>
                        <th>ФИО</th>
                        <th>Телефон</th>
                        <th>E-mail</th>
                        <th>Агент</th>
                        <th>Место</th>
                        <th>От</th>
                        <th>До</th>
                        <th colspan="3">Действия</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($trip->tickets as $ticket)
                        <tr>
                            <td>
                                {!! isset($ticket->client)?$ticket->client->name1.' '.$ticket->client->name2.' '.$ticket->client->name3 :'-' !!}
                            </td>
                            <td>{!! isset($ticket->client)?$ticket->client->phone:'Нет' !!}</td>
                            <td>{!! isset($ticket->client)?$ticket->client->email:'Нет' !!}</td>
                            <td>{!! isset($ticket->agent)?$ticket->agent->name:'Нет' !!}</td>
                            <td>{!! isset($ticket->place)?$ticket->place->name:'Нет'  !!}</td>
                            <td>{!! $ticket->from->name !!}</td>
                            <td>{!! $ticket->to->name !!}</td>
                            <td>
                                {!! Form::open(['route' => ['tickets.destroy', $ticket->id], 'method' => 'delete']) !!}
                                <div class='btn-group'>
                                    <a href="{!! route('tickets.show', [$ticket->id]) !!}" class='btn btn-default btn-xs' target="_blank"><i class="glyphicon glyphicon-qrcode"></i></a>
                                    <a href="{!! route('tickets.edit', [$ticket->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Вы уверены?')"]) !!}
                                </div>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="text-center">

        </div>
    </div>
@endsection

