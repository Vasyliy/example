<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#2e2e2e">

    <link rel="stylesheet" href="/css/passanger.css">
    <title>Путевой лист</title>
</head>
<body>
    <div class="wrapper">
        <table style="width: 100%;">
            <tbody><tr>
                <td><h1>{{ isset($trip->route) ? $trip->route->name : '' }}</h1></td>
                <td class="text-r size15">{!! $trip->date !!}</td>
            </tr>
            </tbody>
        </table>
        <table class="table waybill">
            <tbody>
            <tr>
                <td width="4%">№</td>
                <td width="33%">Интервал</td>
                <td width="20%">ФИО</td>
                <td width="4%">Мес</td>
                <td width="20%">Примечание</td>
                <td width="8%">Статус</td>
                <td width="11%">Цена</td>
            </tr>
                @foreach($trip->ticketsSorting() as $key => $ticket)
                    <tr>
                        <td>{{ ++$key }}</td>
                        <td>{!! $ticket->from->city->name !!} ({!! $ticket->from->name !!}) - {!! $ticket->to->city->name !!}</td>
                        <td>
                            {!! isset($ticket->client_id)?'<b>'.$ticket->client->name1.' '.$ticket->client->name2.' '.$ticket->client->name3.'</b><p>'.$ticket->client->phone.'</b><p>'.$ticket->client->phone2.'</p>' : '<b>'.$ticket->name1.' '.$ticket->name2.' '.$ticket->name3.'</b>' !!}
                        </td>
                        <td>{!! isset($ticket->place)?$ticket->place->name:'Нет'  !!}</td>
                        <td>
                            <span><b>{{ isset($ticket->agent)?$ticket->agent->name:'' }}</b></span>
                            @if($ticket->type->discount != 0)
                                <span class="red-text">{!! $ticket->type->short . '. ' . $ticket->type->discount !!}%</span>
                            @endif
                            <span class="green-text">{!! $ticket->comment !!}</span>
                        </td>
                        <td>{!! \App\Models\Ticket::VARIANT_SHORT[$ticket->variant_id] !!}</td>
                        <td>
                            {{ $ticket->price - $ticket->prepay }} {{ isset($ticket->currency)?$ticket->currency->short:'' }}
                            @if($ticket->prepay > 0)
                                <p class="green-text">{{ $ticket->prepay }} {{ isset($ticket->currency)?$ticket->currency->short:'' }}</p>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <ul>
            @foreach($trip->getAgentsCount() as $agent)
                <li>{{ $agent->name }}: {{ $agent->count }}</li>
            @endforeach
            <li><b>ВСЕГО: {{ $trip->tickets->count() }}</b></li>
        </ul>
    </div>
</body>
</html>
<script>
    window.onload = function () {
        window.print();
    }
</script>
