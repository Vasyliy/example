@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>Рейс: {{ $trip->route->name }}</h1>
        <ol class="breadcrumb">
            <li><a href="{!! url('/search') !!}"><i class="fa fa-dashboard"></i>Главная</a></li>
            <li><a href="{!! url('/trips') !!}">Рейсы</a></li>
            <li class="active">Редактировать Рейс</li>
        </ol>
   </section>
   <section class="content">
       @include('flash::message')
       @include('adminlte-templates::common.errors')
       <div class="row">
           <div class="col-md-6">
               <div class="box box-primary">
                   <div class="box-header with-border">
                       <h3 class="box-title">Информация про рейс</h3>
                   </div>
                   <div class="box-body">
                           {!! Form::model($trip, ['route' => ['trips.update', $trip->id], 'method' => 'patch']) !!}

                                @include('trips.fields')

                           {!! Form::close() !!}
                   </div>
               </div>
               <div class="box box-primary">
                   <div class="box-header with-border">
                       <h3 class="box-title">Блокированные места</h3>
                   </div>
                   <div class="box-body">
                       {!! Form::model($trip, ['route' => ['trips.blockedPlaces.update', $trip->id], 'method' => 'patch']) !!}

                       @include('trips.blocked_place_fields')

                       {!! Form::close() !!}
                   </div>
               </div>
               <div class="box box-primary">
                   <div class="box-header with-border">
                       <h3 class="box-title">Квоты</h3>
                       <div class="box-tools pull-right">
                           <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                           </button>
                       </div>
                   </div>
                   <div class="box-body no-padding">
                       @include('trips.quotas')
                   </div>
               </div>
           </div>
           <div class="col-md-6">
               <div class="box box-primary">
                   <div class="box-header with-border">
                       <h3 class="box-title">Точки Маршрута</h3>
                   </div>
                   <div class="box-body">
                       @include('trips.trip_points_blocked_new')
                   </div>
               </div>
           </div>
       </div>
   </section>
@endsection