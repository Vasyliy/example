<table class="table table-striped">
    <thead>
    <tr><th>Агент</th><th>Количество билетов</th><th colspan="1"></th></tr>
    </thead>
    <tbody>
    @foreach(\App\Models\Agent::where('id', '<>', \App\Models\Agent::GDAMALER_ID)->get() as $agent)
        <tr>
            {!! Form::open(['route' => 'tripQuotas.store']) !!}
            @php($amount = $trip->getAgentQuotaAmount($agent->id))
                {!! Form::hidden('trip_id', $trip->id) !!}
                {!! Form::hidden('agent_id', $agent->id) !!}
                <td>
                    {!! $agent->name !!}
                    @if($amount)
                        @php($ticketCount = $trip->getAgentTicketsAmount($agent->id))
                        <b>{!! $amount . '/' . $ticketCount . '/' . ($amount - $ticketCount) !!}</b>
                    @endif
                </td>
                <td>
                    {!! Form::number('amount', $amount, ['class' => 'form-control', 'style' => 'width:150px']) !!}
                </td>
                <td>
                    <div class='btn-group'>
                        {!! Form::button('Сохранить', ['type' => 'submit', 'class' => 'btn btn-primary', 'style' => 'width:100px']) !!}
                    </div>
                </td>
            {!! Form::close() !!}
        </tr>
    @endforeach
    </tbody>
</table>