<table class="table table-responsive" id="trips-table">
    <thead>
        <tr>
            <th>Дата/Время</th>
            <th>Маршрут</th>
            <th>Места</th>
            <th>Автобус</th>
            <th>Водитель №1</th>
            <th>Водитель №2</th>
            <th>Статус</th>
            <th colspan="3">Действия</th>
        </tr>
    </thead>
    <tbody>
    @foreach($trips as $trip)
        <tr>
            <td>{!! $trip->date !!}</td>
            <td>{!! isset($trip->route)?$trip->route->name:'Нет' !!}</td>
            <td @if($trip->is_critical) class="badge bg-red" @endif>{!! $trip->free_places !!}</td>
            <td>{!! isset($trip->bus) ? $trip->bus->name . ' (' . $trip->bus->number . ')' :'Нет' !!}</td>
            <td>{!! isset($trip->driver)?$trip->driver->name:'Нет' !!}</td>
            <td>{!! isset($trip->driver2)?$trip->driver2->name:'Нет' !!}</td>
            <td>{{ $trip->statusName() }}</td>
            <td>
                {!! Form::open(['route' => ['trips.destroy', $trip->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! url('/trips/waybill', [$trip->id]) !!}" class='btn btn-default btn-xs' title="Путевой лист" target="_blank"><i class="glyphicon glyphicon-print"></i></a>
                    <a href="{!! url('/trips/tickets', [$trip->id]) !!}" class='btn btn-default btn-xs' title="Список брони"><i class="glyphicon glyphicon-list"></i></a>
                    <a href="{!! route('trips.edit', [$trip->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Вы уверены?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>