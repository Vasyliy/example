<table class="table table-responsive" id="agents-table">
    <thead>
        <tr>
            <th>Название</th>
            <th>Api</th>
            <th>Примечание</th>
            <th>Статус</th>
            <th colspan="3">Действия</th>
        </tr>
    </thead>
    <tbody>
    @foreach($agents as $agent)
        <tr>
            <td>{!! $agent->name !!}</td>
            <td>{!! $agent->api !!}</td>
            <td>{!! $agent->note !!}</td>
            <td>{!! $agent->textStatus() !!}</td>
            <td>
                {!! Form::open(['route' => ['agents.destroy', $agent->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <!--<a href="{!! route('agents.show', [$agent->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>-->
                    <a href="{!! route('agents.edit', [$agent->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Вы уверены?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>