<!-- Name Field -->
<div class="form-group col-sm-12">
    {!! Form::label('name', 'Название:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
    @foreach(\App\Models\Lang::all() as $lang_item)
        <div class="form-group col-sm-12">
            <label>Название для {{ $lang_item->name }}</label>
            {!! Form::text('translate['.$lang_item->id.']', isset($agent) ? $agent->getTranslate($lang_item->id) : '', ['class' => 'form-control']) !!}
        </div>
    @endforeach
</div>

<!-- Api Field -->
<div class="form-group col-sm-12">
    {!! Form::label('api', 'Api:') !!}
    {!! Form::text('api', null, ['class' => 'form-control']) !!}
</div>

<!-- Note Field -->
<div class="form-group col-sm-12">
    {!! Form::label('note', 'Примечание:') !!}
    {!! Form::textarea('note', null, ['id' => 'note_editor', 'class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-12">
    {!! Form::label('status', 'Статус:') !!}
    {!! Form::select('status', [0=>'Выключен', 1=>'Активный',], 1, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('agents.index') !!}" class="btn btn-default">Отменить</a>
</div>

<script>
    document.addEventListener('DOMContentLoaded', function() {
        $(function () {
            // Replace the <textarea id="editor1"> with a CKEditor
            // instance, using default configuration.
            // CKEDITOR.replace('note_editor');
            //bootstrap WYSIHTML5 - text editor
            $('#note_editor').wysihtml5();
        })
    }, false);
</script>