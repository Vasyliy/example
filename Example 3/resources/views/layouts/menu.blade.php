<!--<li class="header">ОСНОВНЫЕ</li>-->
@if(Auth::user()->role == \App\Models\User::ROLE_DISPATCHER_ID || Auth::user()->role == \App\Models\User::ROLE_SIMPLE_ID)
<li class="{{ Request::is('search*') ? 'active' : '' }}">
    <a href="{!! url('search') !!}"><i class="fa fa-search"></i><span>Поиск Билетов</span></a>
</li>
<li class="{{ Request::is('trips*') ? 'active' : '' }}">
    <a href="{!! route('trips.index') !!}"><i class="fa fa-edit"></i><span>Рейсы</span></a>
</li>
<li class="{{ Request::is('tickets*') ? 'active' : '' }}">
    <a href="{!! route('tickets.index') !!}"><i class="fa fa-ticket"></i><span>Билеты</span></a>
</li>
<li class="{{ Request::is('clients*') ? 'active' : '' }}">
    <a href="{!! route('clients.index') !!}"><i class="fa fa-users"></i><span>Пассажиры</span></a>
</li>
@endif
<!--<li class="header">ВСПОМОГАТЕЛЬНЫЕ</li>-->
@if(Auth::user()->role == \App\Models\User::ROLE_SIMPLE_ID)
<li class="treeview
    @if(
        Request::is('routes*')
        OR Request::is('points*')
        OR Request::is('agents*')
        OR Request::is('langs*')
        OR Request::is('currencies*')
        OR Request::is('ticketTypes*')
        OR Request::is('drivers*')
        OR Request::is('buses*')
        OR Request::is('schemes*')
        OR Request::is('countries*')
        OR Request::is('cities*')
        OR Request::is('schedules*')
    )
        active
    @endif
" >
    <a href="#">
        <i class="fa fa-book"></i>
        <span>Справочники</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu"
        @if(
            Request::is('routes*')
            OR Request::is('points*')
            OR Request::is('agents*')
            OR Request::is('langs*')
            OR Request::is('currencies*')
            OR Request::is('ticketTypes*')
            OR Request::is('drivers*')
            OR Request::is('buses*')
            OR Request::is('schemes*')
            OR Request::is('countries*')
            OR Request::is('cities*')
            OR Request::is('schedules*')
        )
            style="display: block;"
        @else
            style="display: none;"
        @endif
    >
        <li class="{{ Request::is('routes*') ? 'active' : '' }}">
            <a href="{!! route('routes.index') !!}"><i class="fa fa-road"></i><span>Маршруты</span></a>
        </li>
        <li class="{{ Request::is('points*') ? 'active' : '' }}">
            <a href="{!! route('points.index') !!}"><i class="fa fa-map-marker"></i><span>Точки маршрутов</span></a>
        </li>
        <li class="{{ Request::is('agents*') ? 'active' : '' }}">
            <a href="{!! route('agents.index') !!}"><i class="fa fa-group"></i><span>Агенты</span></a>
        </li>
        <li class="{{ Request::is('langs*') ? 'active' : '' }}">
            <a href="{!! route('langs.index') !!}"><i class="fa fa-language"></i><span>Языки</span></a>
        </li>
        <li class="{{ Request::is('currencies*') ? 'active' : '' }}">
            <a href="{!! route('currencies.index') !!}"><i class="fa fa-money"></i><span>Валюты</span></a>
        </li>
        <li class="{{ Request::is('ticketTypes*') ? 'active' : '' }}">
            <a href="{!! route('ticketTypes.index') !!}"><i class="fa fa-wheelchair"></i><span>Типы билетов</span></a>
        </li>
        <li class="{{ Request::is('drivers*') ? 'active' : '' }}">
            <a href="{!! route('drivers.index') !!}"><i class="fa fa-user-md"></i><span>Водители</span></a>
        </li>
        <li class="{{ Request::is('buses*') ? 'active' : '' }}">
            <a href="{!! route('buses.index') !!}"><i class="fa fa-bus"></i><span>Автобусы</span></a>
        </li>
        <li class="{{ Request::is('schemes*') ? 'active' : '' }}">
            <a href="{!! route('schemes.index') !!}"><i class="fa fa-tags"></i><span>Схемы Автобусов</span></a>
        </li>
        <li class="{{ Request::is('countries*') ? 'active' : '' }}">
            <a href="{!! route('countries.index') !!}"><i class="fa fa-globe"></i><span>Страны</span></a>
        </li>
        <li class="{{ Request::is('cities*') ? 'active' : '' }}">
            <a href="{!! route('cities.index') !!}"><i class="fa fa-industry"></i><span>Города</span></a>
        </li>
    </ul>
</li>

<!--
<li class="header">Для тестирования</li>
<li class="treeview">
    <a href="#">
        <i class="fa fa-book"></i>Временные<span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
    </a>
    <ul class="treeview-menu">
        <li class="{{ Request::is('routePoints*') ? 'active' : '' }}">
            <a href="{!! route('routePoints.index') !!}"><i class="fa fa-edit"></i><span>Отрезки маршрута</span></a>
        </li>

        <li class="{{ Request::is('places*') ? 'active' : '' }}">
            <a href="{!! route('places.index') !!}"><i class="fa fa-edit"></i><span>Места</span></a>
        </li>
        <li class="{{ Request::is('schedules*') ? 'active' : '' }}">
            <a href="{!! route('schedules.index') !!}"><i class="fa fa-tasks"></i><span>Расписание</span></a>
        </li>
        <li class="{{ Request::is('routePrices*') ? 'active' : '' }}">
            <a href="{!! route('routePrices.index') !!}"><i class="fa fa-newspaper-o"></i><span>Прейскурант</span></a>
        </li>
        <li class="{{ Request::is('tripPointBlockeds*') ? 'active' : '' }}">
            <a href="{!! route('tripPointBlockeds.index') !!}"><i class="fa fa-edit"></i><span>Trip Point Blockeds</span></a>
        </li>
    </ul>
</li>
-->
<li class="treeview
    @if(
        Request::is('dispatcherPhones*')
        OR Request::is('settings*')
        OR Request::is('returns*')
    )
        active
    @endif
        " >
    <a href="#">
        <i class="fa fa-gears"></i>
        <span>Настройки</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu"
        @if(
            Request::is('dispatcherPhones*')
            OR Request::is('settings*')
            OR Request::is('returns*')
        )
            style="display: block;"
        @else
            style="display: none;"
        @endif

    >
        <li class="{{ Request::is('settings/general/1/edit') ? 'active' : '' }}">
            <a href="{!! route('settings.general.edit', [1]) !!}"><i class="fa fa-gears"></i><span>Общие</span></a>
        </li>
        <li class="{{ Request::is('dispatcherPhones*') ? 'active' : '' }}">
            <a href="{!! route('dispatcherPhones.index') !!}"><i class="fa fa-phone"></i><span>Номера Телефонов</span></a>
        </li>
        <li class="{{ Request::is('settings/1/edit') ? 'active' : '' }}">
            <a href="{!! route('settings.edit', [1]) !!}"><i class="fa fa-gears"></i><span>Настройки Билета</span></a>
        </li>
        <li class="{{ Request::is('returns*') ? 'active' : '' }}">
            <a href="{!! route('returns.index') !!}"><i class="fa fa-reply"></i><span>Возврат Билета</span></a>
        </li>
    </ul>
</li>
@if(Auth::user()->admin == 1)
    <li class="treeview
        @if(
            Request::is('users*')
            OR Request::is('activities*')
        )
            active
        @endif
            " >
        <a href="#">
            <i class="fa fa-user-secret"></i>
            <span>Администрирование</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu"
            @if(
                Request::is('users*')
                OR Request::is('activities*')
                OR Request::is('reports*')
            )
            style="display: block;"
            @else
            style="display: none;"
                @endif

        >
            <li class="{{ Request::is('users*') ? 'active' : '' }}">
                <a href="{!! route('users.index') !!}"><i class="fa fa-users"></i><span>Пользователи</span></a>
            </li>
            <li class="{{ Request::is('activities*') ? 'active' : '' }}">
                <a href="{!! route('activities.index') !!}"><i class="fa fa-book"></i><span>Журнал Действий</span></a>
            </li>
            <li class="{{ Request::is('reports*') ? 'active' : '' }}">
                <a href="{!! route('reports.index') !!}"><i class="fa fa-bar-chart"></i><span>Отчёты</span></a>
            </li>
        </ul>
    </li>
@endif
<li class="treeview {{ Request::is('tradingPlatforms*') ? 'active' : '' }}" >
    <a href="#">
        <i class="fa fa-shopping-basket"></i>
        <span>Сервисы Билетов</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu"
        @if(Request::is('tradingPlatforms*') || Request::is('agentTradingPlatforms*'))
            style="display: block;"
        @else
            style="display: none;"
        @endif
    >
        <li class="{{ Request::is('agentTradingPlatforms*') ? 'active' : '' }}">
            <a href="{!! route('agentTradingPlatforms.index') !!}"><i class="fa fa-group"></i><span>Настройки Агенты/Сервисы</span></a>
        </li>
        @foreach(\App\Models\TradingPlatform::getAll() as $key => $platform)
            <li class="{{ Request::is('tradingPlatforms/'.$key.'/edit') ? 'active' : '' }}">
                <a href="{!! route('tradingPlatforms.edit', [$key]) !!}"><i class="fa fa-bus"></i><span>{{ $platform }}</span></a>
            </li>
        @endforeach
    </ul>
</li>
@endif

