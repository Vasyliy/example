<table class="table table-responsive" id="tradingPlatformTicketVariants-table">
    <thead>
        <tr>
            <th>Trading Platform Id</th>
        <th>Variant Id</th>
        <th>Name</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($tradingPlatformTicketVariants as $tradingPlatformTicketVariant)
        <tr>
            <td>{!! $tradingPlatformTicketVariant->trading_platform_id !!}</td>
            <td>{!! $tradingPlatformTicketVariant->variant_id !!}</td>
            <td>{!! $tradingPlatformTicketVariant->name !!}</td>
            <td>
                {!! Form::open(['route' => ['tradingPlatformTicketVariants.destroy', $tradingPlatformTicketVariant->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('tradingPlatformTicketVariants.show', [$tradingPlatformTicketVariant->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('tradingPlatformTicketVariants.edit', [$tradingPlatformTicketVariant->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>