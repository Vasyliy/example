@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Trading Platform Ticket Variant
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'tradingPlatformTicketVariants.store']) !!}

                        @include('trading_platform_ticket_variants.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
