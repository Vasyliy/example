<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $tradingPlatformTicketVariant->id !!}</p>
</div>

<!-- Trading Platform Id Field -->
<div class="form-group">
    {!! Form::label('trading_platform_id', 'Trading Platform Id:') !!}
    <p>{!! $tradingPlatformTicketVariant->trading_platform_id !!}</p>
</div>

<!-- Variant Id Field -->
<div class="form-group">
    {!! Form::label('variant_id', 'Variant Id:') !!}
    <p>{!! $tradingPlatformTicketVariant->variant_id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $tradingPlatformTicketVariant->name !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $tradingPlatformTicketVariant->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $tradingPlatformTicketVariant->updated_at !!}</p>
</div>

