<table class="table table-responsive" id="points-table">
    <thead>
        <tr>
            <th>Город</th>
            <th>Название</th>
            <th colspan="3">Действия</th>
        </tr>
    </thead>
    <tbody>
    @foreach($points as $point)
        <tr>
            <td>{!! $point->city->name !!}</td>
            <td>{!! $point->name !!}</td>
            <td>
                {!! Form::open(['route' => ['points.destroy', $point->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <!--<a href="{!! route('points.show', [$point->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>-->
                    <a href="{!! route('points.edit', [$point->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        @if(isset($point->deleted_at))
                            <a href="{!! url('/points/restore', [$point->id]) !!}" class='btn btn-default btn-xs' title="Восстановить">
                                <i class="glyphicon glyphicon-share"></i>
                            </a>
                        @else
                            {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
                                'type' => 'submit',
                                'class' => 'btn btn-danger btn-xs',
                                'onclick' => "return confirm('Вы уверены?')",
                            ]) !!}
                        @endif

                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>