<!-- City Id Field -->
<div class="form-group col-sm-12">
    {!! Form::label('city_id', 'Город:') !!}
    {!! Form::select('city_id', \App\Models\City::getAllCities(), null, ['class' => 'form-control', 'placeholder'=>'Выберите город ...']) !!}
</div>

<!-- Name Field -->
<div class="form-group col-sm-12">
    {!! Form::label('name', 'Название:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}

    @foreach(\App\Models\Lang::all() as $lang_item)
        <div class="form-group col-sm-12">
            <label>Перевод для {{ $lang_item->name }}</label>
            {!! Form::text('translate['.$lang_item->id.']', isset($point) ? $point->getTranslate($lang_item->id) : '', ['class' => 'form-control']) !!}
        </div>
    @endforeach
</div>

<!-- Lat Field -->
<div class="form-group col-sm-4">
    {!! Form::label('lat', 'Широта:') !!}
    {!! Form::text('lat', null, ['class' => 'form-control', 'id'=>'lat_value']) !!}
</div>
<!-- Lat Field -->
<div class="form-group col-sm-4">
    {!! Form::label('lng', 'Долгота:') !!}
    {!! Form::text('lng', null, ['class' => 'form-control', 'id'=>'lng_value']) !!}
</div>

<div class="form-group col-sm-12">
    <!--<iframe src="https://www.google.com/maps/d/embed?mid=19DzDhHlQ2jT8Aq0TkG8TD0K3aJ_pwVBp" width="100%" height="480"></iframe>-->
    <div
            id="point_map"
            style="height: 300px; width: 100%;"
    ></div>
</div>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('points.index') !!}" class="btn btn-default">Отменить</a>
</div>

@section('scripts')
        <!--<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>-->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC8Wuu1ZzZs2HWvFG0aK_sEmoo4Or4kA14" type="text/javascript"></script>
<!--<script type="text/javascript" src="//maps.googleapis.com/maps/api/js?key=AIzaSyBP7vcBwbc_eOk2sCUzMHNuZnviEI2pcLc&callback=initMap"></script>-->
<script>
    $(function () {
        initMap();
    });

    function initMap() {
        // The location
        @if(isset($point))
            var point = {lat: {{ $point->lat }}, lng: {{ $point->lng }} };
        @else
            var point = {lat: 49.23725926207273, lng: 28.40399800811906};
        @endif

        // The map, centered at Uluru
        var map = new google.maps.Map(
            document.getElementById('point_map'),
            {zoom: 16, center: point}
        );

        // The marker, positioned at Uluru
        var exist_marker = new google.maps.Marker({
            position: point,
            draggable: true,
            title: "Перетяните маркер при необходимости!",
            map: map
        });

        map.addListener('click', function (e) {
            /*window.setTimeout(function() {
             map.panTo(marker.getPosition());
             }, 3000);)*/
            position = e.latLng;
            $('#lat_value').val(position.lat());
            $('#lng_value').val(position.lng());
            exist_marker.setPosition(position);
            map.panTo(position);
        });
    }
</script>
@endsection