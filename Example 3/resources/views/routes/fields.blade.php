    <!-- Name Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('name', 'Название:') !!}
        {!! Form::text('name', null, ['class' => 'form-control']) !!}

        @foreach(\App\Models\Lang::all() as $lang_item)
            <div class="form-group col-sm-12">
                <label>Перевод для {{ $lang_item->name }}</label>
                {!! Form::text('translate['.$lang_item->id.']', isset($route) ? $route->getTranslate($lang_item->id) : '', ['class' => 'form-control']) !!}
            </div>
        @endforeach
    </div>

    <!-- Sсheme Id Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('scheme_id', 'Схема мест:') !!}
        {!! Form::select('scheme_id', \App\Models\Scheme::getAll(), null, ['class' => 'form-control', 'placeholder' => 'Выберите ...']) !!}
    </div>

    <!-- Date Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('started', 'Начало действия:') !!}
        {!! Form::date('started', isset($route) ? $route->started : null, ['class' => 'form-control']) !!}
    </div>

    <!-- Date Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('finished', 'Окончание действия:') !!}
        {!! Form::date('finished', isset($route) ? $route->finished : null, ['class' => 'form-control']) !!}
    </div>

    <!-- Note Field -->
    <!--
    <div class="form-group col-sm-12">
        {!! Form::label('note', 'Условия перевозки багажа:') !!}
        {!! Form::textarea('note', null, ['class' => 'form-control']) !!}
    </div>
    -->

    <!-- Note Field -->
    <!--
    <div class="form-group col-sm-12">
        {!! Form::label('phones', 'Список контактных номеров телефона:') !!}
        {!! Form::textarea('phones', null, ['class' => 'form-control']) !!}
    </div>
    -->

    <!-- Status Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('status', 'Статус:') !!}
        {!! Form::select('status', \App\Models\Route::statuses, null, ['class' => 'form-control']) !!}
    </div>

    <!-- Submit Field -->
    <div class="form-group col-sm-12">
        {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
        <a href="{!! route('routes.index') !!}" class="btn btn-default">Отменить</a>
    </div>


