@section('css')
    <link rel="stylesheet" href="/css/main.css">
@endsection

<div class="bus-scheme-wrap bus-scheme-block-wrap">
    <div class="bus-scheme">
        <div class="driver icon-wrap">
            <img src="/img/driver-icon.png" alt="">
        </div>
        <div class="tv icon-wrap" style="top: 14px;left: 11px;">
            <img src="/img/tv-icon.png" alt="">
            TV
        </div>
        <div class="exit" style="left: 30px;">Exit</div>
        <div class="exit" style="left: 247px;">Exit</div>
        <div class="places-scheme">
            @foreach($route->blockedPlacesInScheme() as $placeInScheme)
                <label class="item" style="left: {{ $placeInScheme['x'] }}px; bottom: {{ $placeInScheme['y'] }}px;">
                    <input
                            type="checkbox"
                            id="{{ $placeInScheme['id'] }}"
                            name="places[]"
                            value="{{ $placeInScheme['id'] }}"
                            @if($placeInScheme['status'] == 1) checked @endif
                    >
                    <span class="index-number">{{ $placeInScheme['num'] }}</span>
                </label>
            @endforeach
        </div>
    </div>
</div>

@section('scripts')
    <script>

    </script>
@endsection