<div>
    <table class="table table-responsive" id="routes-table">
        <thead>
            <tr><th>Из</th><th>День</th><th>Время</th><th>В</th><th>День</th><th>Время</th><th colspan="3">Действия</th></tr>
        </thead>
        <tbody>
        @php ($route_end_point_id = 0)
        @php ($route_end_point_time = 0)
        @php ($route_end_point_day = 0)
        @foreach($route->route_points as $route_point)
            <tr class="route-point-view" style="display: table-row;">
                <td><b>{!! $route_point->from->city->name !!}</b><br>
                    {!! $route_point->from->name !!}
                </td>
                <td>{!! $route_point->from_day !!}</td>
                <td>{!! date('H:i', mktime(0, 0, $route_point->getRealFromTimeInSeconds())) !!}</td>
                <td>
                    <b>{!! $route_point->to->city->name !!}</b><br>
                    {!! $route_point->to->name !!}
                </td>
                <td>{!! $route_point->to_day !!}</td>
                <td>{!! date('H:i', mktime(0, 0, $route_point->getRealToTimeInSeconds())) !!}</td>
                <td>
                    {!! Form::open(['route' => ['routePoints.destroy', $route_point->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        {!! Form::button('<i class="glyphicon glyphicon-edit"></i>', ['type' => 'button', 'class' => 'btn btn-default btn-xs route-point-edit-button']) !!}
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Вы уверены?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
            <tr class="route-point-edit" style="display: none">
                {!! Form::model($route_point, ['route' => ['routePoints.update', $route_point->id], 'method' => 'patch']) !!}
                <td>
                    {!! Form::select('from_point_id', \App\Models\Point::getAll(), null, ['class' => 'form-control fine_select']) !!}
                </td>
                <td>
                    <div class="input-group">
                        {!! Form::number('from_day', $route_point->from_day, ['class' => 'form-control', 'style'=>'width:60px;']) !!}
                    </div>
                </td>
                <td>
                    <div class="input-group">
                        {!! Form::text('from_time', date('H:i', mktime(0, 0, $route_point->getRealFromTimeInSeconds())), ['class' => 'form-control from_time', 'style'=>'width:92px;']) !!}
                    </div>
                </td>
                <td>
                    {!! Form::select('to_point_id', \App\Models\Point::getAll(), null, ['class' => 'form-control fine_select']) !!}
                </td>
                <td>
                    <div class="input-group">
                        {!! Form::number('to_day', $route_point->to_day, ['class' => 'form-control', 'style'=>'width:60px;']) !!}
                    </div>
                </td>
                <td>
                    <div class="input-group">
                        {!! Form::text('to_time', date('H:i', mktime(0, 0, $route_point->getRealToTimeInSeconds())), ['class' => 'form-control to_time', 'style'=>'width:92px;']) !!}
                    </div>
                </td>
                <td>
                    <div class='btn-group'>
                        {!! Form::button('<i class="glyphicon glyphicon-remove"></i>', ['type' => 'button', 'class' => 'btn btn-default btn-xs route-point-close-button']) !!}
                        {!! Form::button('<i class="glyphicon glyphicon-floppy-disk"></i>', ['type' => 'submit', 'class' => 'btn btn-primary btn-xs']) !!}
                    </div>
                </td>
                {!! Form::close() !!}
            </tr>
            @php ($route_end_point_id = $route_point->to_point_id)
            @php ($route_end_point_time = $route_point->getRealToTimeInSeconds())
            @php ($route_end_point_day = $route_point->to_day)
        @endforeach
        <tr>
            {!! Form::open(['route' => 'routePoints.store']) !!}
                {!! Form::hidden('route_id', $route->id) !!}
                {!! Form::hidden('number', $route->getNumber()) !!}
            <td>{!! Form::select('from_point_id', \App\Models\Point::getAll(), $route_end_point_id, ['class' => 'form-control fine_select']) !!}</td>
            <td>
                <div class="input-group">
                    {!! Form::number('from_day', $route_end_point_day, ['class' => 'form-control', 'style'=>'width:60px;']) !!}
                </div>
            </td>
            <td>
                <div class="input-group">
                    {!! Form::text('from_time', date('H:i', mktime(0, 0, $route_end_point_time)), ['class' => 'form-control from_time', 'style'=>'width:92px;']) !!}
                </div>
            </td>
            <td>{!! Form::select('to_point_id', \App\Models\Point::getAll(), null, ['class' => 'form-control fine_select']) !!}</td>
            <td>
                <div class="input-group">
                    {!! Form::number('to_day', $route_end_point_day, ['class' => 'form-control', 'style'=>'width:60px;']) !!}
                </div>
            </td>
            <td>
                <div class="input-group">
                    {!! Form::text('to_time', null, ['class' => 'form-control to_time', 'style'=>'width:92px;']) !!}
                </div>
            </td>
            <td>
                <div class='btn-group'>
                    {!! Form::submit('Добавить', ['class' => 'btn btn-primary']) !!}
                </div>
            </td>
            {!! Form::close() !!}
        </tr>
        </tbody>
    </table>
</div>

@section('scripts')
    <script>
        $(function () {
            $('.from_time').datetimepicker({
                format: 'HH:mm'
            });
            $('.to_time').datetimepicker({
                format: 'HH:mm'
            });

            $('.fine_select').select2({ width: '100%' });

            $(".route-point-edit-button").click(function() {
                $(this).closest(".route-point-view").toggle();
                $(this).closest(".route-point-view").next().toggle();
            });
            $(".route-point-close-button").click(function() {
                $(this).closest(".route-point-edit").toggle();
                $(this).closest(".route-point-edit").prev().toggle();
            });
        });
    </script>
@endsection