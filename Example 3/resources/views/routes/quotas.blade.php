<table class="table table-striped">
    <thead>
    <tr><th>Агент</th><th>Количество билетов</th><th colspan="1"></th></tr>
    </thead>
    <tbody>
    @foreach(\App\Models\Agent::where('id', '<>', \App\Models\Agent::GDAMALER_ID)->get() as $agent)
        <tr>
            {!! Form::open(['route' => 'routeQuotas.store']) !!}
                {!! Form::hidden('route_id', $route->id) !!}
                {!! Form::hidden('agent_id', $agent->id) !!}
                <td>
                    {!! $agent->name !!}
                </td>
                <td>
                    {!! Form::number('amount', $route->getAgentQuotaAmount($agent->id), ['class' => 'form-control', 'style' => 'width:150px']) !!}
                </td>
                <td>
                    <div class='btn-group'>
                        {!! Form::button('Сохранить', ['type' => 'submit', 'class' => 'btn btn-primary', 'style' => 'width:100px']) !!}
                    </div>
                </td>
            {!! Form::close() !!}
        </tr>
    @endforeach
    </tbody>
</table>