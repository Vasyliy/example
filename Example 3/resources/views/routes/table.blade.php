<table class="table table-responsive" id="routes-table">
    <thead>
        <tr>
            <th>Название</th>
            <th>Маршрут</th>
            <th>Схема</th>
            <th>Начало</th>
            <th>Окончание</th>
            <th>Статус</th>
            <th colspan="3">Действия</th>
        </tr>
    </thead>
    <tbody>
    @foreach($routes as $route)
        <tr>
            <td>{!! $route->name !!}</td>
            <td>
                @foreach($route->route_points as $route_point)
                    {{ $route_point->from->city->name }}, {{ $route_point->from->name }} (<b>{{ date('H:i', mktime(0, 0, $route_point->getRealFromTimeInSeconds())) }}</b>)
                    <!-- - {{ $route_point->to->name }} ({{ date('H:i:s', mktime(0, 0, $route_point->to_time)) }}) --> <br/>
                @endforeach
            </td>
            <td>{!! $route->scheme->name !!}</td>
            <td>{!! isset($route->started)?$route->started->format('d-m-Y'):'-' !!}</td>
            <td>{!! isset($route->finished)?$route->finished->format('d-m-Y'):'-' !!}</td>
            <td>{!! \App\Models\Route::statuses[$route->status] !!}</td>
            <td>
                {!! Form::open(['route' => ['routes.destroy', $route->id], 'method' => 'delete']) !!}
                <div class='btn-group'>

                    <!--<a href="{!! route('routes.show', [$route->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>-->
                    <a href="{!! url('/trips/generate', ['route'=>$route->id]) !!}" class='btn btn-default btn-xs @if($route->status == 0) disabled @endif'><i class="glyphicon glyphicon-transfer" title="Создать рейсы"></i></a>
                    @if(isset($route->schedule))
                        <a href="{!! route('schedules.edit', [$route->schedule->id, 'route'=>$route->id]) !!}" class='btn btn-default btn-xs' title="Расписание маршрута"><i class="glyphicon glyphicon-tasks"></i></a>
                    @else
                        <a href="{!! route('schedules.create', ['route' => $route->id]) !!}" class='btn btn-default btn-xs' title="Добавить Расписание маршрута"><i class="glyphicon glyphicon-tasks"></i></a>
                    @endif
                    <a href="{!! url('/routes/clone', ['route'=>$route->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-copy" title="Копировать маршрут"></i></a>
                    <a href="{!! url('/routes/pricelist', ['route'=>$route->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-euro" title="Прейскурант"></i></a>
                    <a href="{!! route('routes.edit', [$route->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Вы уверены?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>