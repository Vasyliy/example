@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>Маршрут</h1>
        <ol class="breadcrumb">
            <li><a href="{!! url('/search') !!}"><i class="fa fa-dashboard"></i>Главная</a></li>
            <li><a href="{!! url('/routes') !!}">Маршруты</a></li>
            <li class="active">Редактировать Маршрут</li>
        </ol>
    </section>
    <section class="content">
        @include('flash::message')
        @include('adminlte-templates::common.errors')
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Информация о Маршруте</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            {!! Form::model($route, ['route' => ['routes.update', $route->id], 'method' => 'patch']) !!}

                            @include('routes.fields')

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Отрезки маршрута</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body">
                        @if(isset($route->schedule))
                            @include('routes.route_points')
                        @else
                            <div class="callout callout-danger">
                                <h4>Не указано расписание!</h4>
                                <p>Чтобы добавлять отрезки маршрута, нужно указать <a
                                            href="{!! route('schedules.create', ['route' => $route->id]) !!}">расписание</a>
                                </p>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Блокированные места</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body">
                        {!! Form::model($route, ['route' => ['routeBlockedPlaces.update', $route->id], 'method' => 'patch']) !!}

                        @include('routes.blocked_place_fields')

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Квоты</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body no-padding">
                        @include('routes.quotas')
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection