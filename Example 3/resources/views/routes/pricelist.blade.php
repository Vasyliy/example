@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>Прейскурант маршрута</h1>
        <ol class="breadcrumb">
            <li><a href="{!! url('/search') !!}"><i class="fa fa-dashboard"></i>Главная</a></li>
            <li><a href="{!! url('/routes') !!}">Маршруты</a></li>
            <li class="active">Прейскурант маршрута</li>
        </ol>
    </section>
    <section class="content">
        @include('adminlte-templates::common.errors')
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Информация о Маршруте</h3>
                    </div>
                    <div class="box-body no-padding" style="overflow: auto;">
                            <table class="table table-striped" id="routes-table">
                                <thead>
                                    <tr>
                                        @foreach($from_points as $from_point)
                                            <th>
                                                <div class="vertical1">
                                                    {{ $from_point->name }} <br/>
                                                    <span style="font-weight:400;">{{ $from_point->from->name }}</span>
                                                </div>
                                            </th>
                                        @endforeach
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($to_points as $to_point)
                                    <tr>
                                        @php($first_enable = true)
                                        @foreach($from_points as $from_point)
                                            @php
                                                if($from_point->from_point_id == $to_point->to_point_id) $first_enable = false;
                                            @endphp
                                            @if($first_enable)
                                                <td title="{{ $from_point->name }} ({{ $from_point->from->name }}) - {{ $to_point->name }} ({{ $to_point->to->name }})">

                                                        @if(isset($route_prices[$from_point->from_point_id][$to_point->to_point_id]))
                                                            <span
                                                                    id="{{ $from_point->from_point_id }}_{{ $to_point->to_point_id }}"
                                                                    class="editable_price"
                                                                    data-from="{{ $from_point->from_point_id }}"
                                                                    data-to="{{ $to_point->to_point_id }}"
                                                                    data-pk="{{ $route->id }}_{{ $from_point->from_point_id }}_{{ $to_point->to_point_id }}_{{ $route_prices[$from_point->from_point_id][$to_point->to_point_id]['currency']->id }}"
                                                            >
                                                                {{ $route_prices[$from_point->from_point_id][$to_point->to_point_id]['price'] }}
                                                            </span>
                                                            <span
                                                                    id="{{ $from_point->from_point_id }}_{{ $to_point->to_point_id }}"
                                                                    class="editable_currency"
                                                                    data-from="{{ $from_point->from_point_id }}"
                                                                    data-to="{{ $to_point->to_point_id }}"
                                                                    data-pk="{{ $route->id }}_{{ $from_point->from_point_id }}_{{ $to_point->to_point_id }}_{{ $route_prices[$from_point->from_point_id][$to_point->to_point_id]['currency']->id }}"
                                                                    data-value="{{ $route_prices[$from_point->from_point_id][$to_point->to_point_id]['currency']->id }}"
                                                            >
                                                                {{ $route_prices[$from_point->from_point_id][$to_point->to_point_id]['currency']->short }}
                                                            </span>
                                                        @else
                                                            <span
                                                                    id="{{ $from_point->from_point_id }}_{{ $to_point->to_point_id }}"
                                                                    class="editable_price"
                                                                    data-from="{{ $from_point->from_point_id }}"
                                                                    data-to="{{ $to_point->to_point_id }}"
                                                                    data-pk="{{ $route->id }}_{{ $from_point->from_point_id }}_{{ $to_point->to_point_id }}_{{ $default_currency->id }}"
                                                            >
                                                                0.00
                                                            </span>
                                                            <span
                                                                    id="{{ $from_point->from_point_id }}_{{ $to_point->to_point_id }}"
                                                                    class="editable_currency"
                                                                    data-from="{{ $from_point->from_point_id }}"
                                                                    data-to="{{ $to_point->to_point_id }}"
                                                                    data-pk="{{ $route->id }}_{{ $from_point->from_point_id }}_{{ $to_point->to_point_id }}_{{ $default_currency->id }}"
                                                                    data-value="{{ $default_currency->id }}"
                                                            >
                                                                    {{ $default_currency->short }}
                                                            </span>
                                                        @endif

                                                </td>
                                            @else
                                                <td style="background-color: #d2d6de;"></td>
                                            @endif
                                        @endforeach
                                        <td>
                                            <div class="horizontal1">
                                                <b>{{ $to_point->name }}</b>
                                                <span style="font-weight:400;">{{ $to_point->to->name }}</span>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
    <style>
        .vertical1{
            writing-mode: vertical-rl;
            transform: rotateZ(180deg);
            white-space: nowrap;
            text-overflow: ellipsis;
            overflow: hidden;
            height: 125px;
            line-height: 1.1;
        }
        .horizontal1{
            writing-mode: horizontal-tb;
            white-space: nowrap;
            text-overflow: ellipsis;
            overflow: hidden;
            width: 200px;
            line-height: 1.1;
        }
    </style>
    <script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
    <script>
        $(function () {
            $.fn.editable.defaults.mode = 'popup';

            $('.editable_price').editable({
                type: 'text',
                toggle: 'click',
                title: 'Введите цену отрезка',
                url: '{{ url('/routePrices/save') }}',
                ajaxOptions: {
                    method: 'GET'
                    //type: 'put',
                    //dataType: 'json'
                },
                success: function(response, newValue) {
                    real_responce = JSON.parse(response);
                    if(!real_responce.success) {
                        return false;
                    }
                }
            });

            $('.editable_currency').editable({
                source: '{!! \App\Models\Currency::getListOnJson() !!}',
                //data: " {'1': 'UAH','2': 'PLN','3': 'CZK'}",
                type: 'select',
                toggle: 'click',
                title: 'Выберите валюту',
                url: '{{ url('/routePrices/saveCurrency') }}',
                ajaxOptions: {
                    method: 'GET'
                },
                success: function(response, newValue) {
                    real_responce = JSON.parse(response);
                    if(!real_responce.success) {
                        return false;
                    }
                }
            });

            /*
            $('.editable_price').editable({
                //type: 'text',
                toggle: 'click',
                title: 'Введите цену отрезка',
                url: '{{ url('/routePrices/save') }}',
                value: {
                    price: "0.00",
                    currency: "1",
                },
                ajaxOptions: {
                    method: 'GET'
                    //type: 'put',
                    //dataType: 'json'
                },
                display: function(value) {
                    if(!value) {
                        $(this).empty();
                        return;
                    }
                    console.log(value);
                    var html = '<b>' + $('<div>').text(value.price).html() + ' ' + $('<div>').text(value.currency_short).html();
                    //var html = '<b>' + $('<div>').text($(this)[0].dataset.valuePrice).html() + ' ' + $('<div>').text($(this)[0].dataset.valueCurrencyShort).html();
                    $(this).html(html);
                },
                success: function(response, newValue) {
                    real_responce = JSON.parse(response);
                    if(!real_responce.success) {
                        return false;
                    }
                }
            });
            */

            $('#address').editable({
                url: '/post',
                value: {
                    city: "Moscow",
                    street: "Lenina",
                    building: "12"
                },
                validate: function(value) {
                    if(value.city == '') return 'city is required!';
                },
                display: function(value) {
                    if(!value) {
                        $(this).empty();
                        return;
                    }
                    var html = '<b>' + $('<div>').text(value.city).html() + '</b>, ' + $('<div>').text(value.street).html() + ' st., bld. ' + $('<div>').text(value.building).html();
                    $(this).html(html);
                }
            });
        });
    </script>
@endsection