    <table class="table table-responsive" id="routes-table">
        <thead>
            <tr><th colspan="4">Прейскурант маршрута</th></tr>
            <tr><th>Из</th><th>В</th><th>Цена</th><th colspan="3">Действия</th></tr>
        </thead>
        <tbody>
        @php ($route_end_point_id = 0)
        @php ($route_end_point_time = 0)
        @foreach($route->route_prices as $route_price)
        <tr>
            <td><b>{!! $route_price->from->city->name !!}</b><br>
                {!! $route_price->from->name !!}
            </td>
            <td>
                <b>{!! $route_price->to->city->name !!}</b><br>
                {!! $route_price->to->name !!}
            </td>
            <td>{!! $route_price->price !!} {!! $route_price->currency->short !!}</td>
            <td>
                {!! Form::open(['route' => ['routePrices.destroy', $route_price->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('routePrices.edit', [$route->id, 'route_price'=>$route_price->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Вы уверены?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
            @php ($route_end_point_id = 0)
            @php ($route_end_point_time = 0)
        @endforeach
        <tr>
            {!! Form::open(['route' => 'routePrices.store']) !!}
                {!! Form::hidden('route_id', $route->id) !!}
            <td>{!! Form::select('from_point_id', \App\Models\Point::getAllfromRoute($route->id), null, ['class' => 'form-control fine_select']) !!}</td>
            <td>{!! Form::select('to_point_id', \App\Models\Point::getAlltoRoute($route->id), null, ['class' => 'form-control fine_select']) !!}</td>
            <td style="white-space: nowrap;">
                        <!--<span class="input-group-addon">$</span>-->
                <div class="input-group" style="display: inline-block;">
                    {!! Form::text('price', null, ['class' => 'form-control', 'placeholder'=>'0.00', 'style'=>'width:50px;']) !!}
                </div>
               <div class="input-group" style="display: inline-block;">
                   {!! Form::select('currency_id', \App\Models\Currency::getAll(), null, ['class' => 'form-control', 'style'=>'width:71px;']) !!}
               </div>
            </td>
            <td>
                <div class='btn-group'>
                    {!! Form::submit('Добавить', ['class' => 'btn btn-primary']) !!}
                </div>
            </td>
            {!! Form::close() !!}
        </tr>
        </tbody>
    </table>

@section('scripts')
    <script>
        $(function () {
        });
    </script>
@endsection