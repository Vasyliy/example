<table class="table table-responsive" id="langs-table">
    <thead>
        <tr>
            <th>Название</th>
            <th>Код</th>
            <th colspan="3">Действия</th>
        </tr>
    </thead>
    <tbody>
    @foreach($langs as $lang)
        <tr>
            <td>{!! $lang->name !!}</td>
            <td>{!! $lang->short !!}</td>
            <td>
                {!! Form::open(['route' => ['langs.destroy', $lang->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <!--<a href="{!! route('langs.show', [$lang->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>-->
                    <a href="{!! route('langs.edit', [$lang->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Вы уверены?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>