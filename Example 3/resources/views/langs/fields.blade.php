<!-- Name Field -->
<div class="form-group col-sm-12">
    {!! Form::label('name', 'Название:') !!}
    {!! Form::text('name', null, ['class' => 'form-control col-sm-6']) !!}
</div>

<!-- Short Field -->
<div class="form-group col-sm-12">
    {!! Form::label('short', 'Сокращение:') !!}
    {!! Form::text('short', null, ['class' => 'form-control col-sm-2']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('langs.index') !!}" class="btn btn-default">Отменить</a>
</div>
