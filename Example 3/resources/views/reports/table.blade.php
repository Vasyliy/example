<table class="table table-responsive" id="activities-table">
    <thead>
        <tr>
            <th>Агент</th>
            <th>Количество</th>
            <th>Сумма</th>
        </tr>
    </thead>
    <tbody>
        @foreach($agents as $agent)
            <tr>
                <td>{!! $agent['name'] !!}</td>
                <td>{!! $agent['count'] !!}</td>
                <td>
                    @foreach($agent['sum'] as $key => $sum)
                        @if($sum > 0)
                            @if (!$loop->first && $lastSum)
                                /
                            @endif
                            {{ $sum }} {{ $key }}
                            <?php $lastSum = true ?>
                        @endif
                    @endforeach
                    <?php $lastSum = false ?>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

<table class="table table-responsive" id="activities-table">
    <thead>
    <tr>
        <th>Всего билетов</th>
        <th>Общая сумма</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>{!! $general['count'] !!}</td>
        <td>
            @foreach($general['sum'] as $key => $sum)
                @if($sum > 0)
                    @if (!$loop->first && $lastSum)
                        /
                    @endif
                    {{ $sum }} {{ $key }}
                    <?php $lastSum = true ?>
                @endif
            @endforeach
            <?php $lastSum = false ?>
        </td>
    </tr>
    </tbody>
</table>