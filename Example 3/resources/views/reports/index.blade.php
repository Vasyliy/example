@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Отчёты</h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Фильтр</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'reports.index', 'role'=>'search', 'method'=>'get']) !!}

                    @include('reports.search_fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        <div class="box box-primary">
            <div class="box-body">
                <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">

                    @include('reports.table')

                </div>
            </div>
        </div>
        <div class="text-center">

        </div>
    </div>
@endsection

