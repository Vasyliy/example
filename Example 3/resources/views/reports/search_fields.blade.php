<div class="form-group col-sm-10">
    {!! Form::label('created_at', 'Дата:') !!}
    <div class="input-group date">
        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
        {!! Form::text('created_at', \Carbon\Carbon::now()->format('d-m-Y'), ['class' => 'form-control pull-right', 'id'=>'searchdatepicker']) !!}
    </div>
</div>
<div class="form-group col-sm-2">
    {!! Form::label('', '') !!}
    {!! Form::submit('Отфильтровать', ['class' => 'form-control btn btn-primary', 'style' => 'margin-top: 5px']) !!}
</div>

@section('scripts')
    <script>
        $(function () {
            $('#searchdatepicker').datetimepicker({
                format: 'DD-MM-YYYY'
                , locale: 'ru'
            });
        });
    </script>
@endsection