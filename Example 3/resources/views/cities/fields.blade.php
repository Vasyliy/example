<!-- Country Id Field -->
<div class="form-group col-sm-12">
    {!! Form::label('country_id', 'Страна:') !!}
    {!! Form::select('country_id', \App\Models\Country::getAllContries(), null, ['class' => 'form-control']) !!}
</div>

<!-- Name Field -->
<div class="form-group col-sm-12">
    {!! Form::label('name', 'Название (по умолчанию):') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}

    @foreach(\App\Models\Lang::all() as $lang_item)
        <div class="form-group col-sm-12">
            <label>Название для {{ $lang_item->name }}</label>
            {!! Form::text('translate['.$lang_item->id.']', isset($city) ? $city->getTranslate($lang_item->id) : '', ['class' => 'form-control']) !!}
        </div>
    @endforeach
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('cities.index') !!}" class="btn btn-default">Отменить</a>
</div>
