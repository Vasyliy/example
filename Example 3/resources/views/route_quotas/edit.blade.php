@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Route Quota
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($routeQuota, ['route' => ['routeQuotas.update', $routeQuota->id], 'method' => 'patch']) !!}

                        @include('route_quotas.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection