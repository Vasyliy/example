<table class="table table-responsive" id="routeQuotas-table">
    <thead>
        <tr>
            <th>Route Id</th>
        <th>Agent Id</th>
        <th>Amount</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($routeQuotas as $routeQuota)
        <tr>
            <td>{!! $routeQuota->route_id !!}</td>
            <td>{!! $routeQuota->agent_id !!}</td>
            <td>{!! $routeQuota->amount !!}</td>
            <td>
                {!! Form::open(['route' => ['routeQuotas.destroy', $routeQuota->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('routeQuotas.show', [$routeQuota->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('routeQuotas.edit', [$routeQuota->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>