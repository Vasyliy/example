<!-- Name Field -->
<div class="form-group col-sm-12">
    {!! Form::label('name', 'Имя пользователя:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-12">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Password Field -->
<div class="form-group col-sm-12">
    {!! Form::label('password', 'Пароль:') !!}
    {!! Form::password('password', ['class' => 'form-control']) !!}
</div>

<!-- Admin Field -->
<div class="form-group col-sm-12">
    {!! Form::label('admin', 'Администратор:') !!}
    {!! Form::checkbox('admin', 1, isset($user) AND $user->admin == 1) !!}
</div>

<!-- Role Field -->
<div class="form-group col-sm-12">
    {!! Form::label('role', 'Роль:') !!}
    {!! Form::select('role', \App\Models\User::ROLES, null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('users.index') !!}" class="btn btn-default">Отменить</a>
</div>
