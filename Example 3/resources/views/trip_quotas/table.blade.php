<table class="table table-responsive" id="tripQuotas-table">
    <thead>
        <tr>
            <th>Trip Id</th>
        <th>Agent Id</th>
        <th>Amount</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($tripQuotas as $tripQuota)
        <tr>
            <td>{!! $tripQuota->trip_id !!}</td>
            <td>{!! $tripQuota->agent_id !!}</td>
            <td>{!! $tripQuota->amount !!}</td>
            <td>
                {!! Form::open(['route' => ['tripQuotas.destroy', $tripQuota->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('tripQuotas.show', [$tripQuota->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('tripQuotas.edit', [$tripQuota->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>