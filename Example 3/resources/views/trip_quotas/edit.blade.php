@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Trip Quota
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($tripQuota, ['route' => ['tripQuotas.update', $tripQuota->id], 'method' => 'patch']) !!}

                        @include('trip_quotas.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection