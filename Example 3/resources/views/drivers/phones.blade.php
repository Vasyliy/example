<div>
    <table class="table table-responsive" id="drivers-table">
        <thead>
        <tr><th colspan="5">Телефоны</th></tr>
        <tr><th>№</th><th>Номер</th><th colspan="3">Действия</th></tr>
        </thead>
        <tbody>
        @foreach($driver->phones as $phone)
            <tr>
                <td>{!! $phone->id !!}</td>
                <td>{!! $phone->phone !!}</td>
                <td>
                    {!! Form::open(['route' => ['driverPhones.destroy', $phone->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <!--<a href="{!! route('driverPhones.edit', [$phone->id, 'driver'=>$driver->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>-->
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Вы уверены?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <a class="btn btn-primary" href="{!! route('driverPhones.create', ['driver'=>$driver->id]) !!}">Добавить Телефон</a>
</div>
