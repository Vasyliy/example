<!-- Name Field -->
<div class="form-group col-sm-12">
    {!! Form::label('name', 'ФИО:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}

    @foreach(\App\Models\Lang::all() as $lang_item)
        <div class="form-group col-sm-12">
            <label>Перевод для {{ $lang_item->name }}</label>
            {!! Form::text('translate['.$lang_item->id.']', isset($driver) ? $driver->getTranslate($lang_item->id) : '', ['class' => 'form-control']) !!}
        </div>
    @endforeach
</div>

<!-- License Field -->
<div class="form-group col-sm-12">
    {!! Form::label('license', 'Лицензия:') !!}
    {!! Form::text('license', null, ['class' => 'form-control']) !!}
</div>

<!-- Note Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('note', 'Примечания:') !!}
    {!! Form::textarea('note', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-12">
    {!! Form::label('status', 'Статус:') !!}
    {!! Form::select('status', [0=>'Выключен', 1=>'Активный',], 1, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('drivers.index') !!}" class="btn btn-default">Отменить</a>
</div>

