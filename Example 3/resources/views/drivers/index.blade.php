@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>Водители</h1>
        <ol class="breadcrumb">
            <li><a href="{!! url('/search') !!}"><i class="fa fa-dashboard"></i>Главная</a></li>
            <li class="active">Водители</li>
        </ol>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title pull-right">
                    <a class="btn btn-primary pull-right" href="{!! route('drivers.create') !!}">Добавить</a>
                </h3>
            </div>
            <div class="box-body">
                    @include('drivers.table')
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
@endsection

