@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>Водитель</h1>
        <ol class="breadcrumb">
            <li><a href="{!! url('/search') !!}"><i class="fa fa-dashboard"></i>Главная</a></li>
            <li><a href="{!! url('/drivers') !!}">Водители</a></li>
            <li class="active">Редактировать Водителя</li>
        </ol>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($driver, ['route' => ['drivers.update', $driver->id], 'method' => 'patch']) !!}

                        @include('drivers.fields')

                   {!! Form::close() !!}
               </div>

               @include('drivers.phones')
           </div>
       </div>
   </div>
@endsection