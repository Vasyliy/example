<table class="table table-responsive" id="drivers-table">
    <thead>
        <tr>
            <th>ФИО</th>
            <th>Лицензия</th>
            <th>Примечания</th>
            <th>Статус</th>
            <th colspan="3">Действия</th>
        </tr>
    </thead>
    <tbody>
    @foreach($drivers as $driver)
        <tr>
            <td>{!! $driver->name !!}</td>
            <td>{!! $driver->license !!}</td>
            <td>{!! $driver->note !!}</td>
            <td>{!! $driver->textStatus() !!}</td>
            <td>
                {!! Form::open(['route' => ['drivers.destroy', $driver->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <!--<a href="{!! route('drivers.show', [$driver->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>-->
                    <a href="{!! route('drivers.edit', [$driver->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Вы уверены?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>