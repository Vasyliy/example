@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>Расписание: {{ isset($route_model) ? $route_model->name:'' }}</h1>
        <ol class="breadcrumb">
            <li><a href="{!! url('/search') !!}"><i class="fa fa-dashboard"></i>Главная</a></li>
            <li><a href="{!! url('/routes') !!}">Маршруты</a></li>
            <li class="active">Создать Расписание</li>
        </ol>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'schedules.store']) !!}

                        @include('schedules.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
