<!-- Route Id Field -->
<!--<div class="form-group col-sm-12">
    {!! Form::label('route_id', 'Маршрут:') !!}

    {!! Form::select('route_id', \App\Models\Route::getAll(), isset($route)?$route:null, ['class' => 'form-control', 'placeholder'=> 'Выберите маршрут...', 'disabled'=>'disabled']) !!}
</div>-->

{!! Form::hidden('route_id', isset($route)?$route:0 ) !!}

<!-- Patern Field -->
<div class="form-group col-sm-12">


    {!! Form::label('patern', 'Дни отправки:') !!}
    <br>
    {!! Form::checkbox('patern[]', \Carbon\Carbon::MONDAY, isset($schedule) ? $schedule->parsePatern() : false, ['id' => 'patern1']) !!}
    {!! Form::label('patern1', '- понедельник') !!}
    <br>
    {!! Form::checkbox('patern[]', \Carbon\Carbon::TUESDAY, isset($schedule) ? $schedule->parsePatern() : false, ['id' => 'patern2']) !!}
    {!! Form::label('patern2', '- вторник') !!}
    <br>
    {!! Form::checkbox('patern[]', \Carbon\Carbon::WEDNESDAY, isset($schedule) ? $schedule->parsePatern() : false, ['id' => 'patern3']) !!}
    {!! Form::label('patern3', '- среда') !!}
    <br>
    {!! Form::checkbox('patern[]', \Carbon\Carbon::THURSDAY, isset($schedule) ? $schedule->parsePatern() : false, ['id' => 'patern4']) !!}
    {!! Form::label('patern4', '- четверг') !!}
    <br>
    {!! Form::checkbox('patern[]', \Carbon\Carbon::FRIDAY, isset($schedule) ? $schedule->parsePatern() : false, ['id' => 'patern5']) !!}
    {!! Form::label('patern5', '- пятница') !!}
    <br>
    {!! Form::checkbox('patern[]', \Carbon\Carbon::SATURDAY, isset($schedule) ? $schedule->parsePatern() : false, ['id' => 'patern6']) !!}
    {!! Form::label('patern6', '- суббота') !!}
    <br>
    {!! Form::checkbox('patern[]', \Carbon\Carbon::SUNDAY, isset($schedule) ? $schedule->parsePatern() : false, ['id' => 'patern7']) !!}
    {!! Form::label('patern7', '- воскресенье') !!}
</div>

<!-- Date Field -->
<div class="form-group col-sm-12">
    {!! Form::label('time', 'Время отправления:') !!}
    {!! Form::text('time', null, ['class' => 'form-control', 'id' => 'time_to_trip']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('routes.index') !!}" class="btn btn-default">Отменить</a>
</div>


@section('scripts')
    <script>
        $(function () {
            $('#time_to_trip').datetimepicker({
                format: 'HH:mm'
            });
        });
    </script>
@endsection