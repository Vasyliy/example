<!-- User Id Field -->
<!--<div class="form-group col-sm-12">
    {!! Form::label('user_id', 'Пользователь (Системный):') !!}
    {!! Form::select('user_id', \App\Models\User::getAll()->prepend('Без привязки', '0'), null, ['class' => 'form-control']) !!}
</div>-->

<!-- Name Field -->
<div class="form-group col-sm-12">
    {!! Form::label('name1', 'Фамилия:') !!}
    {!! Form::text('name1', null, ['class' => 'form-control']) !!}
</div>
<!-- Name Field -->
<div class="form-group col-sm-12">
    {!! Form::label('name2', 'Имя:') !!}
    {!! Form::text('name2', null, ['class' => 'form-control']) !!}
</div>
<!-- Name Field -->
<div class="form-group col-sm-12">
    {!! Form::label('name3', 'Отчество:') !!}
    {!! Form::text('name3', null, ['class' => 'form-control']) !!}
</div>
<!-- Phone Field -->
<div class="form-group col-sm-12">
    {!! Form::label('phone', 'Телефон №1: ') !!}
    {!! Form::text('phone', null, ['class' => 'form-control phone', 'id' => 'phone']) !!}
</div>
<!-- Phone2 Field -->
<div class="form-group col-sm-12">
    {!! Form::label('phone2', 'Телефон №2: ') !!}
    {!! Form::text('phone2', null, ['class' => 'form-control phone', 'id' => 'phone2']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-12">
    {!! Form::label('email', 'EMail:') !!}
    {!! Form::text('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Comment Field -->
<div class="form-group col-sm-12">
    {!! Form::label('comment', 'Примечание:') !!}
    {!! Form::textarea('comment', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('clients.index') !!}" class="btn btn-default">Отменить</a>
</div>

@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/10.0.2/css/intlTelInput.css">
@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/14.0.3/js/intlTelInput.min.js"></script>
    <script>
        $(function () {
            var phone = document.querySelector("#phone"),
                phone2 = document.querySelector("#phone2");
            var iti = window.intlTelInput(phone, {onlyCountries:["pl","ua","cz"]});
            var iti2 = window.intlTelInput(phone2, {onlyCountries:["pl","ua","cz"]});
            Inputmask(["+38 099 999 9999", "+48 999 999 999", "+42 099 999 9999"]).mask($(".phone"));
            phone.addEventListener('countrychange', function(e) {
                phone.value = iti.getSelectedCountryData().dialCode;
            });
            phone2.addEventListener('countrychange', function(e) {
                phone2.value = iti2.getSelectedCountryData().dialCode;
            });
        });
    </script>
@endsection