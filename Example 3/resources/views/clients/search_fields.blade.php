<div class="form-group col-sm-6">
    {!! Form::label('name1', 'Фамилия:') !!}
    {!! Form::select('name1', \App\Models\Client::getAllNames(), null, ['class' => 'form-control select2', 'placeholder'=>'Фамилия']) !!}
</div>

<div class="form-group col-sm-5">
    {!! Form::label('phone', 'Телефон:') !!}
    {!! Form::select('phone', \App\Models\Client::getAllPhones(), null, ['class' => 'form-control select2', 'placeholder'=>'Телефон']) !!}
</div>

<div class="form-group col-sm-1">
    <label></label>
    <div class="input-group">
        {!! Form::submit('Искать', ['class' => 'btn btn-primary']) !!}
    </div>
</div>

@section('scripts')
    <script>
        $(function () {
            $('.select2').select2({
                language: "ru"
            });
        });
    </script>
@endsection