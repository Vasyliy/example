@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>Пассажиры</h1>
        <ol class="breadcrumb">
            <li><a href="{!! url('/search') !!}"><i class="fa fa-dashboard"></i>Главная</a></li>
            <li class="active">Пассажиры</li>
        </ol>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Поиск клиетов</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    {!! Form::model($clients, ['route' => ['clients.index'], 'method' => 'get']) !!}

                        @include('clients.search_fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title pull-right">
                    <a class="btn btn-primary pull-right" href="{!! route('clients.create') !!}">Добавить</a>
                </h3>
            </div>
            <div class="box-body">
                <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                    <div class="row">
                        <div class="col-sm-12">
                            @include('clients.table')
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="dataTables_info" id="example1_info">
                                @if( $clients->count() < $clients->total() )
                                    Показано от {{ ($clients->currentPage() - 1) * $clients->perPage() + 1 }} до
                                    @if( $clients->currentPage() * $clients->perPage() > $clients->total() )
                                        {{ $clients->total() }}
                                    @else
                                        {{ $clients->currentPage() * $clients->perPage() }}
                                    @endif
                                    из {{ $clients->total() }} пассажиров
                                @endif
                            </div>
                        </div>
                        <div class="col-sm-7">
                            <div class="dataTables_paginate paging_simple_numbers pull-right">
                                {{ $clients->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
@endsection

