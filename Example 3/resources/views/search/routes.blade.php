<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">Найденные рейсы</h3>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">
                <table class="table table-responsive" id="routes-table">
                    <thead>
                    <tr>
                        <th>Дата</th>
                        <th>Название маршрута</th>
                        <th>Интервал</th>
                        <th>Мест</th>
                        <!--<th>Занято</th>-->
                        <th colspan="3">Действия</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($trips as $trip)
                        <tr>
                            <td>{!! $trip->date !!}</td>
                            <td>{!! $trip->route->name !!}</td>
                            <td>{!! $trip->endPointsName() !!}</td>
                            <td>{!! $trip->free_places !!}</td>
                            <!--<td>{!! count($trip->tickets) !!}</td>-->
                            <td>
                                {!! Form::open(['route' => ['trips.destroy', $trip->id], 'method' => 'delete']) !!}
                                <div class='btn-group'>
                                    <a href="{!! route('tickets.create', ['trip'=>$trip->id, 'from'=>$from, 'to'=>$to]) !!}"
                                       class='btn btn-success btn-xs' title="Забронировать место">Бронировать</a>
                                </div>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-5">
                <div class="dataTables_info" id="example1_info">
                    @if( $trips->count() < $trips->total() )
                        Показано от {{ ($trips->currentPage() - 1) * $trips->perPage() + 1 }} до
                        @if( $trips->currentPage() * $trips->perPage() > $trips->total() )
                            {{ $trips->total() }}
                        @else
                            {{ $trips->currentPage() * $trips->perPage() }}
                        @endif
                        из {{ $trips->total() }} рейсов
                    @endif
                </div>
            </div>
            <div class="col-sm-7">
                <div class="dataTables_paginate paging_simple_numbers pull-right">
                    {{ $trips->appends([
                        'from' => app('request')->input('from'),
                        'to' => app('request')->input('to'),
                        'date' => app('request')->input('date'),
                        'deviation' => app('request')->input('deviation'),
                        'deviation_amount' => app('request')->input('deviation_amount')
                    ])->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
