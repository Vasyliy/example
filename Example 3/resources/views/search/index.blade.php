@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Поиск</h1>
        <ol class="breadcrumb">
            <li><a href="{!! url('/search') !!}"><i class="fa fa-dashboard"></i>Главная</a></li>
            <li class="active">Поиск</li>
        </ol>
        <!--<h1 class="pull-right">
            <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('places.create') !!}">Добавить</a>
        </h1>-->
    </section>
    <div class="content" id="app">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Поиск билетов</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    {!! Form::model($search, ['method' => 'get']) !!}

                    <div class="form-group col-sm-4">
                        {!! Form::label('from', 'Откуда:') !!}
                        {!! Form::select('from', \App\Models\Point::getAllfrom(), null, ['class' => 'form-control select2', 'placeholder'=>'', 'id'=>'from_selector']) !!}
                    </div>

                    <div class="form-group col-sm-4">
                        {!! Form::label('to', 'Куда:') !!}
                        {!! Form::select('to', \App\Models\Point::getAllto(), null, ['class' => 'form-control select2', 'placeholder'=>'', 'id'=>'to_selector']) !!}
                    </div>

                    <div class="form-group col-sm-3">
                        {!! Form::label('date', 'Когда:') !!}

                        <div class="input-group">
                            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                            {!! Form::text('date', null, ['class' => 'form-control', 'id' => 'date', 'data-inputmask' => "'alias': 'dd/mm/yyyy'", 'data-mask' => ""]) !!}
                            <span class="input-group-addon">
                                {!! Form::checkbox('deviation') !!}
                                +
                                {!! Form::select('deviation_amount', ['1' => '1', '7' => '7', '14' => '14', '30' => '30', '60' => '60'], null, ['class' => 'deviation-amount']) !!}
                            </span>
                        </div>
                    </div>

                    <div class="form-group col-sm-1">
                        <label></label>
                        <div class="input-group">
                            {!! Form::submit('Искать', ['class' => 'btn btn-primary']) !!}
                        </div>
                    </div>

                    {!! Form::close() !!}
                </div>

            </div>
        </div>
        @if(isset($trips) AND isset($trips[0]))
            @include('search.routes')
        @endif
    </div>
@endsection

@section('scripts')
    <script>
        $(function () {
            $('#date').datetimepicker({
                format: 'DD-MM-YYYY',
                minDate: moment().format('YYYY MM DD')
            });

            //$('#from_selector').select2();

            $('.select2').select2({
                language: "ru"
            });

        });
    </script>
@endsection