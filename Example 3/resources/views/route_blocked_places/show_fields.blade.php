<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $routeBlockedPlace->id !!}</p>
</div>

<!-- Route Id Field -->
<div class="form-group">
    {!! Form::label('route_id', 'Route Id:') !!}
    <p>{!! $routeBlockedPlace->route_id !!}</p>
</div>

<!-- Place Id Field -->
<div class="form-group">
    {!! Form::label('place_id', 'Place Id:') !!}
    <p>{!! $routeBlockedPlace->place_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $routeBlockedPlace->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $routeBlockedPlace->updated_at !!}</p>
</div>

