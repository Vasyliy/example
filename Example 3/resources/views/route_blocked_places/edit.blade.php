@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Route Blocked Place
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($routeBlockedPlace, ['route' => ['routeBlockedPlaces.update', $routeBlockedPlace->id], 'method' => 'patch']) !!}

                        @include('route_blocked_places.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection