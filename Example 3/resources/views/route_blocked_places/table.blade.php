<table class="table table-responsive" id="routeBlockedPlaces-table">
    <thead>
        <tr>
            <th>Route Id</th>
        <th>Place Id</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($routeBlockedPlaces as $routeBlockedPlace)
        <tr>
            <td>{!! $routeBlockedPlace->route_id !!}</td>
            <td>{!! $routeBlockedPlace->place_id !!}</td>
            <td>
                {!! Form::open(['route' => ['routeBlockedPlaces.destroy', $routeBlockedPlace->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('routeBlockedPlaces.show', [$routeBlockedPlace->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('routeBlockedPlaces.edit', [$routeBlockedPlace->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>