<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $ticket->id !!}</p>
</div>

<!-- Trip Id Field -->
<div class="form-group">
    {!! Form::label('trip_id', 'Trip Id:') !!}
    <p>{!! $ticket->trip_id !!}</p>
</div>

<!-- From Point Id Field -->
<div class="form-group">
    {!! Form::label('from_point_id', 'From Point Id:') !!}
    <p>{!! $ticket->from_point_id !!}</p>
</div>

<!-- To Point Id Field -->
<div class="form-group">
    {!! Form::label('to_point_id', 'To Point Id:') !!}
    <p>{!! $ticket->to_point_id !!}</p>
</div>

<!-- Place Id Field -->
<div class="form-group">
    {!! Form::label('place_id', 'Place Id:') !!}
    <p>{!! $ticket->place_id !!}</p>
</div>

<!-- Client Id Field -->
<div class="form-group">
    {!! Form::label('client_id', 'Client Id:') !!}
    <p>{!! $ticket->client_id !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $ticket->deleted_at !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $ticket->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $ticket->updated_at !!}</p>
</div>

