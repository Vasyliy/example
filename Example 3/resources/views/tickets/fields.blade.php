<!-- Place Id Field -->
<div class="form-group col-sm-12">
    {!! Form::label('place_id', 'Место:') !!}
    @include('tickets.scheme')
</div>

<!-- Ticket Variant -->
<div class="form-group col-sm-12">
    {!! Form::label('variant_id', 'Вариант:') !!}
    {!! Form::select('variant_id', \App\Models\Ticket::VARIANT, 1, ['class' => 'form-control']) !!}
</div>

<!-- Agent -->
<div class="form-group col-sm-12">
    {!! Form::label('agent_id', 'Агент:') !!}
    {!! Form::select('agent_id', $trip->getAgents(), null, ['class' => 'form-control']) !!}
</div>

    {!! Form::hidden('trip_id', $trip->id) !!}
    {!! Form::hidden('from_point_id', $from) !!}
    {!! Form::hidden('to_point_id', $to) !!}

<div class="form-group col-sm-12">
    <table class="table">
        <tbody><tr>
            <td><b>Рейс:</b></td>
            <td style="width: 100%;">{{ isset($trip) ? $trip->route->name . ' - ' . $trip->date : '' }}</td>
        </tr>
        <tr>
            <td><b>Интервал:</b></td>
            <td style="width: 100%;">{{ (isset($from) ? \App\Models\Point::getPointFullName($from) : 'x') . ' - ' . (isset($to) ? \App\Models\Point::getPointFullName($to) : 'x') }}</td>
        </tr>
        <tr>
            <td><b>Цены:</b></td>
            <td style="width: 100%;">
                <ul>
                    @foreach(\App\Models\TicketType::get() as $type)
                        <li>- {{ $type->name . ($type->discount ? '(' . $type->discount . '%)' : '') }}: {{ \App\Models\Ticket::defaultPrice($from, $to, $trip->route_id) * (1 - $type->discount / 100) }} {{ \App\Models\Currency::find(\App\Models\Ticket::defaultCurrency($from, $to, $trip->route_id))->short }}</li>
                    @endforeach
                </ul>
            </td>
        </tr>
        </tbody></table>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <button class="btn btn-primary" v-on:click.prevent="clientsValidate()">Сохранить</button>
    <a href="{!! route('tickets.index') !!}" class="btn btn-default">Отменить</a>
</div>

