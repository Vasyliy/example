<table class="table table-responsive" id="tickets-table">
    <thead>
        <tr>
            <th>Рейс</th>
            <th>Интервал</th>
            <th>Место</th>
            <th>Пассажир</th>
            <th>Агент</th>
            <th>Тип билета</th>
            <th>Вариант</th>
            <th>Дата</th>
            <th colspan="3">Действия</th>
        </tr>
    </thead>
    <tbody>
    @foreach($tickets as $ticket)
        <tr>
            <td>{!! isset($ticket->trip->route) ? $ticket->trip->route->name : '' !!} ({{ isset($ticket->trip) ? $ticket->trip->date : '' }})</td>
            <td>{!! $ticket->from->city->name !!} - {!! $ticket->to->city->name !!}</td>
            <td>{!! $ticket->place->name !!}</td>
            <td>{!! $ticket->client_id?$ticket->client->name1.' '.$ticket->client->name2:$ticket->name1.' '.$ticket->name2 !!}</td>
            <td>{!! isset($ticket->agent)?$ticket->agent->name:'-' !!}</td>
            <td>{!! $ticket->type->name !!}</td>
            <td>{!! \App\Models\Ticket::VARIANT[$ticket->variant_id] !!}</td>
            <td>{!! $ticket->created_at->format('d-m-Y') !!}</td>
            <td>
                {!! Form::open(['route' => ['tickets.destroy', $ticket->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('tickets.show', [$ticket->id]) !!}" class='btn btn-default btn-xs' target="_blank"><i class="glyphicon glyphicon-qrcode"></i></a>
                    @if($ticket->variant_id != 3)
                    <a href="{!! url('/tickets/cancel', [$ticket->id]) !!}" class='btn btn-default btn-xs' title='Отменить'><i class="glyphicon glyphicon-remove"></i></a>
                    @endif
                    <a href="{!! route('tickets.edit', [$ticket->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Вы уверены?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>