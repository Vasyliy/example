@section('css')
    <link rel="stylesheet" href="/css/main.css">
@endsection

<div class="bus-scheme-wrap">
    <div class="bus-scheme">
        <div class="driver icon-wrap">
            <img src="/img/driver-icon.png" alt="">
        </div>
        <div class="tv icon-wrap" style="top: 14px;left: 11px;">
            <img src="/img/tv-icon.png" alt="">
            TV
        </div>
        <div class="exit" style="left: 30px;">Exit</div>
        <div class="exit" style="left: 247px;">Exit</div>
        <div class="places-scheme">
            @foreach($trip->placesInScheme($from, $to) as $placeInScheme)
                <label class="item" title='{{ $placeInScheme['client'] }}' style="left: {{ $placeInScheme['x'] }}px; bottom: {{ $placeInScheme['y'] }}px;">
                    <input
                            type="checkbox"
                            id="{{ $placeInScheme['id'] }}"
                            name="places[]"
                            value="{{ $placeInScheme['id'] }}"
                            v-model="places"
                            v-on:click="placesFormsRefresh({{ $placeInScheme['id'] }}, {{ $placeInScheme['num'] }})"
                            @if($placeInScheme['status'] == 1) disabled @endif
                            class="
                            @if($placeInScheme['variant_id'] == 1) reserved @endif
                            @if($placeInScheme['variant_id'] == 2) blocked @endif
                                "
                    >
                    <span class="index-number">{{ $placeInScheme['num'] }}</span>
                </label>
            @endforeach
        </div>
    </div>
</div>

{{--
@section('scripts')
<script>
    $(function () {
        var placesScheme = [
            {{ $trip->placeInScheme($from, $to) }}
        ];
        for (var i = 0; i < placesScheme.length; i++) {
            if(placesScheme[i].status == 1){
                //$('.places-scheme').append('<label class="item" style="left: ' + placesScheme[i].x + 'px;bottom: ' + placesScheme[i].y + 'px"><input type="checkbox" name="places[]" value="' + placesScheme[i].num + '" disabled><span class="index-number">' + placesScheme[i].id + '</span></label>');
            }else{
                //$('.places-scheme').append('<label class="item" style="left: ' + placesScheme[i].x + 'px;bottom: ' + placesScheme[i].y + 'px"><input type="checkbox" name="places[]" value="' + placesScheme[i].num + '"><span class="index-number">' + placesScheme[i].id + '</span></label>');
            }
        }
    });
</script>
@endsection
--}}