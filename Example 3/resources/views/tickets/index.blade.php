@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>Билеты</h1>
        <ol class="breadcrumb">
            <li><a href="{!! url('/search') !!}"><i class="fa fa-dashboard"></i>Главная</a></li>
            <li class="active">Билеты</li>
        </ol>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Фильтр</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'tickets.index', 'role'=>'search', 'method'=>'get']) !!}

                    @include('tickets.search_fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        <div class="box box-primary">
            <!--<div class="box-header">
                <h3 class="box-title pull-right">
                    <a class="btn btn-primary pull-right" href="{!! route('tickets.create') !!}">Добавить</a>
                </h3>
            </div>-->
            <div class="box-body">
                <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                    @include('tickets.table')
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="dataTables_info" id="example1_info">
                                @if( $tickets->count() < $tickets->total() )
                                    Показано от {{ ($tickets->currentPage() - 1) * $tickets->perPage() + 1 }} до
                                    @if( $tickets->currentPage() * $tickets->perPage() > $tickets->total() )
                                        {{ $tickets->total() }}
                                    @else
                                        {{ $tickets->currentPage() * $tickets->perPage() }}
                                    @endif
                                    из {{ $tickets->total() }} билетов
                                @endif
                            </div>
                        </div>
                        <div class="col-sm-7">
                            <div class="dataTables_paginate paging_simple_numbers pull-right">
                                {{ $tickets->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="text-center">
        
        </div>
    </div>
@endsection

