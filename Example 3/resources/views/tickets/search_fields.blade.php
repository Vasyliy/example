<div class="form-group col-sm-4">
    {!! Form::label('route_id', 'Маршрут:') !!}
    {!! Form::select('route_id', \App\Models\Route::getAll(), null, ['class' => 'form-control pull-right', 'placeholder' => 'Все маршруты']) !!}
</div>
<div class="form-group col-sm-4">
    {!! Form::label('date', 'Дата рейса:') !!}
    <div class="input-group date">
        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
        {!! Form::text('date', null, ['class' => 'form-control pull-right', 'id'=>'searchdatepicker']) !!}
        <span class="input-group-addon">
            {!! Form::checkbox('date_deviation') !!}
            +
            {!! Form::select('date_deviation_amount', ['1' => '1', '7' => '7', '14' => '14', '30' => '30', '60' => '60'], null, ['class' => 'deviation-amount']) !!}
        </span>
    </div>
</div>
<div class="form-group col-sm-4">
    {!! Form::label('created_at', 'Дата покупки/брони:') !!}
    <div class="input-group date">
        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
        {!! Form::text('created_at', null, ['class' => 'form-control pull-right', 'id'=>'searchdatepicker2']) !!}
        <span class="input-group-addon">
            {!! Form::checkbox('created_at_deviation') !!}
            +
            {!! Form::select('created_at_deviation_amount', ['1' => '1', '7' => '7', '14' => '14', '30' => '30', '60' => '60'], null, ['class' => 'deviation-amount']) !!}
        </span>
    </div>
</div>
<div class="form-group col-sm-4">
    {!! Form::label('name1', 'Фамилия:') !!}
    {!! Form::select('name1', \App\Models\Client::getAllNames(), null, ['class' => 'form-control select2 pull-right', 'placeholder'=>'Фамилия']) !!}
</div>

<div class="form-group col-sm-4">
    {!! Form::label('phone', 'Телефон:') !!}
    {!! Form::select('phone', \App\Models\Client::getAllPhones(), null, ['class' => 'form-control select2 pull-right', 'placeholder'=>'Телефон']) !!}
</div>

<div class="form-group col-sm-4">
    {!! Form::label('', '') !!}
    {!! Form::submit('Отфильтровать', ['class' => 'form-control btn btn-primary', 'style' => 'margin-top: 5px']) !!}
</div>

@section('scripts')
    <script>
        $(function () {
            $('.select2').select2({
                language: "ru"
            });
            $('#searchdatepicker').datetimepicker({
                format: 'DD-MM-YYYY'
                , locale: 'ru'
            });
            $('#searchdatepicker2').datetimepicker({
                format: 'DD-MM-YYYY'
                , locale: 'ru'
            });
        });
    </script>
@endsection