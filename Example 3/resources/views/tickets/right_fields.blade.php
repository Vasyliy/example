<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li v-for="place in places" class="">
            <a v-bind:href="'#tab_'+place" data-toggle="tab" aria-expanded="false">
                Место @{{ placesArray[place].name }}
                        <!--<span
                        class="badge bg-red"
                        v-if="hasErrorPlace(place)"
                >
                    @{{ clients[place].errors.length }}
                        </span>-->
            </a>
        </li>
        <!--<li class="pull-right header"><i class="fa fa-ticket"></i>Информация о Пассажирах/Билетах</li>-->
    </ul>
    <div class="tab-content">
        <div v-for="place in places" class="tab-pane" v-bind:id="'tab_'+place">
            <div class="row">
                <!--<div class="form-group col-sm-12">
                <label>Билет на место:</label> @{{ placesArray[place].name }}
                        </div>-->
                <input
                        type="hidden"
                        v-bind:name="'client_id['+place+']'"
                        v-model="clients[place].client_id"
                />
                <!-- ФИО -->
                <div class="form-group col-sm-12"
                     v-bind:class="hasErrorClass(place, 'name1')"
                >
                    <label for="name1">Фамилия *</label>
                    <input
                            type="text"
                            v-bind:name="'name1['+place+']'"
                            v-model="clients[place].name1"
                            v-on:keyup="clientsAutocomplite(place)"
                            class="form-control"
                    />
                <span
                        class="help-block"
                        v-if="hasError(place, 'name1')"
                >@{{ clients[place].errors.name1[0] }}</span>
                    <div class="autocomplete-list-wrapper">
                        <ul class="list-group">
                            <li class="list-group-item"
                                v-for="item in clientsAuto"
                            v-on:click="setAutocomplite(item.id, place)"
                            style="cursor: pointer;"
                        >
                            @{{ item.name1 }} @{{ item.name2 }} @{{ item.name3 }} (@{{ item.phone }} - @{{ item.phone2 }} - @{{ item.email}})
                        </li>
                    </ul>
                </div>
                </div>
                <!-- ФИО -->
                <div class="form-group col-sm-12"
                     v-bind:class="hasErrorClass(place, 'name2')"
                >
                    <label for="name2">Имя *</label>
                    <input type="text"
                           v-bind:name="'name2['+place+']'"
                           v-model="clients[place].name2"
                           class="form-control"
                    />
                <span
                        class="help-block"
                        v-if="hasError(place, 'name2')"
                >@{{ clients[place].errors.name2[0] }}</span>
                </div>

                <!-- ФИО -->
                <div class="form-group col-sm-12"
                     v-bind:class="hasErrorClass(place, 'name3')"
                >
                    <label for="name3">Отчество</label>
                    <input
                            type="text"
                            v-bind:name="'name3['+place+']'"
                            v-model="clients[place].name3"
                            class="form-control"
                    />
                <span
                        class="help-block"
                        v-if="hasError(place, 'name3')"
                >@{{ clients[place].errors.name3[0] }}</span>
                </div>

                <!-- Телефон 1 -->
                <div class="form-group col-sm-12"
                     v-bind:class="hasErrorClass(place, 'phone')"
                >
                    <label for="phone">Телефон №1</label>
                    <vue-phone-input
                            type="text"
                            v-bind:name="'phone['+place+']'"
                            :phone-number="clients[place].phone"
                            v-on:value-changed="updatePhone(place, $event)"
                            class="form-control">
                    </vue-phone-input>
                <span
                        class="help-block"
                        v-if="hasError(place, 'phone')"
                >@{{ clients[place].errors.phone[0] }}</span>
                </div>

                <!-- Телефон 2 -->
                <div class="form-group col-sm-12"
                     v-bind:class="hasErrorClass(place, 'phone2')"
                >
                    <label for="phone2">Телефон №2</label>
                    <vue-phone-input
                            type="text"
                            v-bind:name="'phone2['+place+']'"
                            :phone-number="clients[place].phone2"
                            v-on:value-changed="updatePhone2(place, $event)"
                            class="form-control">
                    </vue-phone-input>
                <span
                        class="help-block"
                        v-if="hasError(place, 'phone2')"
                >@{{ clients[place].errors.phone2[0] }}</span>
                </div>

                <!-- E-mail -->
                <div class="form-group col-sm-12"
                     v-bind:class="hasErrorClass(place, 'email')"
                >
                    <label for="email">E-mail</label>
                    <input
                            type="text"
                            v-bind:name="'email['+place+']'"
                            v-model="clients[place].email"
                            v-on:keyup="clientsAutocomplite(place)"
                            class="form-control"
                    />
                <span
                        class="help-block"
                        v-if="hasError(place, 'email')"
                >@{{ clients[place].errors.email[0] }}</span>
                </div>

                <input
                        type="hidden"
                        v-bind:name="'price['+place+']'"
                        class="form-control"
                        value="{{ \App\Models\Ticket::defaultPrice($from, $to, $trip->route_id) }}"
                />

                <input
                        type="hidden"
                        v-bind:name="'currency_id['+place+']'"
                        value="{{ \App\Models\Ticket::defaultCurrency($from, $to, $trip->route_id) }}"
                />
                <!-- Ticket type -->
                <div class="form-group col-sm-12">
                    <label for="type_id">Тип Билета *</label>
                    <select
                            type="text"
                            v-bind:name="'type_id['+place+']'"
                            class="form-control"
                            placeholder="Задайте тип билета"
                    >
                        @foreach(\App\Models\TicketType::getAll() as $ticket_type_id=>$ticket_type)
                            <option value="{{ $ticket_type_id }}">{{ $ticket_type }}</option>
                        @endforeach
                    </select>
                </div>

                <!-- Prepay -->
                <div class="form-group col-sm-12">
                    <label for="prepay">Аванс</label>
                <input
                        type="number"
                        value="0"
                        name="prepay"
                        class="form-control"
                        v-bind:name="'prepay['+place+']'"
                ></input>
                </div>

                <!-- Comment -->
                <div class="form-group col-sm-12">
                    <label for="comment">Примечание</label>
                <textarea
                        name="comment"
                        class="form-control"
                        v-bind:name="'comment['+place+']'"
                ></textarea>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
    <!-- /.tab-content -->
</div>

