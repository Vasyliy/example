<!doctype html>
<html style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;margin: 0;padding: 0;border: 0;outline: 0;font-size: 100%;vertical-align: baseline;background: transparent;">
<head style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;">
	<meta charset="UTF-8"
		  style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;">
	<meta name="viewport" content="width=device-width, initial-scale=1"
		  style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;">
	<meta name="theme-color" content="#2e2e2e"
		  style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;">

	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet"
		  style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;">
	<title style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;">Билет</title>
</head>
<body style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: 'Roboto', Arial, sans-serif;margin: 0;padding: 0;border: 0;outline: 0;font-size: 12px;vertical-align: baseline;background: transparent;">

<table class="ticket-wrap"
	   style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;border-collapse: collapse;padding: 0;vertical-align: top;width: 740px;margin: 0 auto;">
	<tr style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;">
		<td style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;vertical-align: top;">
			<table style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;border-collapse: collapse;padding: 0;vertical-align: top;">
				<tr style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;">
					<td class="logo-wrap"
						style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;vertical-align: top;width: 247px;padding: 10px 0 18px;">
						<img src="{{ url('/') }}/img/logo.png" alt="" class="logo"
							 style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;margin: 0;padding: 0;border: none;outline: 0;font-size: 100%;vertical-align: baseline;background: transparent;display: block;">
					</td>
					<td class="va-m"
						style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;vertical-align: middle;">
						<p style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;margin: 0;padding: 0;border: 0;outline: 0;font-size: 100%;vertical-align: baseline;background: transparent;">
							Номер билета {{ $ticket->id }}</p>
						<!--<p style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;margin: 0;padding: 0;border: 0;outline: 0;font-size: 100%;vertical-align: baseline;background: transparent;">
							Защитный код {{ $ticket->id }}</p>-->
					</td>
					@php($url = url('/tickets/view', [$ticket->ticket_viewer]))
					<td rowspan="2" class="qr-wrap"
						style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;vertical-align: top;width: 120px;padding-top: 24px;">
					{!! QrCode::size(150)->generate($url); !!}
						<!--<img src="" alt=""
							 style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;margin: 0;padding: 0;border: none;outline: 0;font-size: 100%;vertical-align: baseline;background: transparent;display: block;width: 100%;">
						<!--<img src="/img/qr.jpg" alt=""
							 style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;margin: 0;padding: 0;border: none;outline: 0;font-size: 100%;vertical-align: baseline;background: transparent;display: block;width: 100%;">-->
					</td>
				</tr>
				<tr style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;">
					<td colspan="2"
						style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;vertical-align: top;">
						<p class="mb15"
						   style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;margin: 0;padding: 0;border: 0;outline: 0;font-size: 100%;vertical-align: baseline;background: transparent;margin-bottom: 15px;">
							Диспетчер {{ \App\Models\DispatcherPhone::forTicket() }}</p>
						<p class="mb15"
						   style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;margin: 0;padding: 0;border: 0;outline: 0;font-size: 100%;vertical-align: baseline;background: transparent;margin-bottom: 15px;">
							Дата покупки: {{ $ticket->created_at }}</p>
					</td>
				</tr>
				<tr style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;">
					<td colspan="3"
						style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;vertical-align: top;">
						<p class="ticket-title"
						   style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;margin: 0;padding: 0;border: 0;outline: 0;font-size: 16px;vertical-align: baseline;background: transparent;text-transform: uppercase;">
							Этот билет действительный для проезда. Предоставьте обслуживающему персоналу при посадке в
							транспортное средство</p>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;">
		<td style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;vertical-align: top;">
			<h4 style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;margin: 20px 0 9px;padding: 0;border: 0;outline: 0;font-size: 16px;vertical-align: baseline;background: transparent;">
				Подтверждение для перевозчика</h4></td>
	</tr>
	<tr style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;">
		<td style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;vertical-align: top;">
			<table class="ticket-table"
				   style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;border-collapse: collapse;padding: 0;vertical-align: top;border: 1px solid #666666;margin-bottom: 20px;width: 100%;">
				<tr style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;">
					<td style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;vertical-align: top;border: 1px solid #dbd9d9;padding: 12px 7px;border-left: 0;border-top: 0;width: 40%;">
						<p style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;margin: 0;padding: 0;border: 0;outline: 0;font-size: 100%;vertical-align: baseline;background: transparent;">
							Пассажир</p>
						<h3 style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;margin: 0;padding: 0;border: 0;outline: 0;font-size: 15px;vertical-align: baseline;background: transparent;text-transform: uppercase;">
							@if(isset($ticket->client)) {{ $ticket->client->name1 }} {{ $ticket->client->name2 }} {{ $ticket->client->name3 }} @else - @endif</h3>
					</td>
					<td colspan="2"
						style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;vertical-align: top;border: 1px solid #dbd9d9;padding: 12px 7px;border-top: 0;width: 22%;">
						<p style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;margin: 0;padding: 0;border: 0;outline: 0;font-size: 100%;vertical-align: baseline;background: transparent;">
							Маршрут</p>
						<p style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;margin: 0;padding: 0;border: 0;outline: 0;font-size: 100%;vertical-align: baseline;background: transparent;">
							{{ $trip->route->name }} ({{ $trip->date }})</p>
					</td>
					<td class="ok payment-status"
						style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;vertical-align: middle;color: #73b252;width: 106px;text-align: center;border: 1px solid #dbd9d9;padding: 12px 7px; border-top: 0;width: 12%;">
						@if($ticket->variant_id == 0)
                            <img class="dib" src="/img/check.png" alt=""
							 style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;margin: 0;padding: 0;border: none;outline: 0;font-size: 100%;vertical-align: baseline;background: transparent;display: inline-block;">
                        @endif
                            {{ \App\Models\Ticket::VARIANT[$ticket->variant_id] }}
					</td>
					<!--<td class="ok payment-status"-->
						<!--style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;vertical-align: middle;color: #cc2528;width: 106px;text-align: center;border: 1px solid #dbd9d9;padding: 12px 7px; border-top: 0;width: 12%;">-->
						<!--<img class="dib" src="/img/close.png" alt=""-->
							 <!--style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;margin: 0;padding: 0;border: none;outline: 0;font-size: 100%;vertical-align: baseline;background: transparent;display: inline-block;">Отменен-->
					<!--</td>-->
					<!--<td class="ok payment-status"-->
						<!--style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;vertical-align: middle;color: #929292;width: 106px;text-align: center;border: 1px solid #dbd9d9;padding: 12px 7px; border-top: 0;width: 12%;">-->
						<!--<img class="dib" src="/img/lock.png" alt=""-->
							 <!--style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;margin: 0;padding: 0;border: none;outline: 0;font-size: 100%;vertical-align: baseline;background: transparent;display: inline-block;">Бронь-->
					<!--</td>-->
					<td style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;vertical-align: top;border: 1px solid #dbd9d9;padding: 12px 7px;border-right: 0;border-top: 0;width: 26%;">
						<p style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;margin: 0;padding: 0;border: 0;outline: 0;font-size: 100%;vertical-align: baseline;background: transparent;">
							Номер билета {{ $ticket->id }}</p>
						<!--<p style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;margin: 0;padding: 0;border: 0;outline: 0;font-size: 100%;vertical-align: baseline;background: transparent;">
							Защитный код {{ $ticket->id }}</p>-->
					</td>
				</tr>
				<tr style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;">
					<td class="no-border"
						style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;vertical-align: top;border: 0 !important;padding: 12px 7px;border-left: 0;">
						<p style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;margin: 0;padding: 0;border: 0;outline: 0;font-size: 100%;vertical-align: baseline;background: transparent;">
							Место отправления</p>
						<p style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;margin: 0;padding: 0;border: 0;outline: 0;font-size: 100%;vertical-align: baseline;background: transparent;">
							{{ $ticket->fromPoint()->from->city->name }}, {{ $ticket->fromPoint()->from->name }}</p>
					</td>
					<td class="no-border"
						style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;vertical-align: top;border: 0 !important;padding: 12px 7px;">
						<p style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;margin: 0;padding: 0;border: 0;outline: 0;font-size: 100%;vertical-align: baseline;background: transparent;">
							Дата</p>
						<p style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;margin: 0;padding: 0;border: 0;outline: 0;font-size: 100%;vertical-align: baseline;background: transparent;">
							{{ $trip->getDateAsCarbon()->addDay($ticket->fromPoint()->from_day)->format('d.m.Y') }}</p>
					</td>
					<td class="no-border"
						style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;vertical-align: top;border: 0 !important;padding: 12px 7px;">
						<p style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;margin: 0;padding: 0;border: 0;outline: 0;font-size: 100%;vertical-align: baseline;background: transparent;">
							Время</p>
						<p style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;margin: 0;padding: 0;border: 0;outline: 0;font-size: 100%;vertical-align: baseline;background: transparent;">
							{{ date('H:i', mktime(0, 0, $ticket->fromPoint()->getRealFromTimeInSeconds())) }}</p>
					</td>
					<!--<td rowspan="2" class="bb-0"
						style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;vertical-align: top;border: 1px solid #dbd9d9;padding: 12px 7px;border-bottom: 0 !important;">
						<p style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;margin: 0;padding: 0;border: 0;outline: 0;font-size: 100%;vertical-align: baseline;background: transparent;">
							номер места указывает диспетчер при посадке в автобус</p>
					</td>-->
					<td rowspan="2" class="bb-0"
						style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;vertical-align: top;border: 1px solid #dbd9d9;padding: 12px 7px;border-bottom: 0 !important;vertical-align: middle;text-align: center;">
						<p style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;margin: 0;padding: 0;border: 0;outline: 0;font-size: 100%;vertical-align: baseline;background: transparent;">
							место {{ $ticket->place->name }}</p>
					</td>
					<td style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;vertical-align: top;border: 1px solid #dbd9d9;padding: 12px 7px;border-right: 0;">
						<p style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;margin: 0;padding: 0;border: 0;outline: 0;font-size: 100%;vertical-align: baseline;background: transparent;">
							TRF : {{ \App\Models\Ticket::defaultPrice($ticket->from_point_id, $ticket->to_point_id, $trip->route_id) }} {{ isset($ticket->currency)?$ticket->currency->short:'' }} </p>
							@if($ticket->type->discount != 0)
						<p style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;margin: 0;padding: 0;border: 0;outline: 0;font-size: 100%;vertical-align: baseline;background: transparent;">
							Скидка: {{ $ticket->type->name }}</p>
							@endif
					</td>
				</tr>
				<tr style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;">
					<td class="no-border"
						style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;vertical-align: top;border: 0 !important;padding: 12px 7px;border-left: 0;">
						<p style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;margin: 0;padding: 0;border: 0;outline: 0;font-size: 100%;vertical-align: baseline;background: transparent;">
							Место прибытия</p>
						<p style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;margin: 0;padding: 0;border: 0;outline: 0;font-size: 100%;vertical-align: baseline;background: transparent;">
							{{ $ticket->toPoint()->to->city->name }}, {{ $ticket->toPoint()->to->name }}</p>
					</td>
					<td class="no-border"
						style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;vertical-align: top;border: 0 !important;padding: 12px 7px;">
						<p style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;margin: 0;padding: 0;border: 0;outline: 0;font-size: 100%;vertical-align: baseline;background: transparent;">
							Дата</p>
						<p style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;margin: 0;padding: 0;border: 0;outline: 0;font-size: 100%;vertical-align: baseline;background: transparent;">
							{{ $trip->getDateAsCarbon()->addDay($ticket->toPoint()->to_day)->format('d.m.Y') }}</p>
					</td>
					<td class="no-border"
						style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;vertical-align: top;border: 0 !important;padding: 12px 7px;">
						<p style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;margin: 0;padding: 0;border: 0;outline: 0;font-size: 100%;vertical-align: baseline;background: transparent;">
							Время</p>
						<p style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;margin: 0;padding: 0;border: 0;outline: 0;font-size: 100%;vertical-align: baseline;background: transparent;">
                            {{ date('H:i', mktime(0, 0, $ticket->toPoint()->getRealToTimeInSeconds())) }}</p>
					</td>
					<td style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;vertical-align: top;border: 1px solid #dbd9d9;padding: 12px 7px;border-right: 0;border-bottom: 0;">
						<p style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;margin: 0;padding: 0;border: 0;outline: 0;font-size: 100%;vertical-align: baseline;background: transparent;">
							TRF : {{ $ticket->price - $ticket->prepay }} {{ isset($ticket->currency)?$ticket->currency->short:'' }} </p>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;">
		<td style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;vertical-align: top;">
			<table class="info-table"
				   style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;border-collapse: collapse;padding: 0;vertical-align: top;border-bottom: 1px solid #dbd9d9;margin-bottom: 20px;">
				<tr style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;">
					<td class="small-text map-wrap"
						style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;vertical-align: top;font-size: 9px;width: 350px;padding-bottom: 20px;">
						<!--<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d8514.72286786315!2d28.438958980238347!3d49.226748312077916!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sru!2sua!4v1534429068572"
								width="350" height="250" frameborder="0"
								style="border: 0;-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;margin: 0;padding: 0;outline: 0;font-size: 100%;vertical-align: baseline;background: transparent;width: 100%;margin-bottom: 15px;"
								allowfullscreen></iframe>-->
                        <img src="//maps.googleapis.com/maps/api/staticmap?zoom=16&size=350x250&sensor=false&maptype=roadmap&markers=color:red|{{ $ticket->fromPoint()->from->lat }}, {{ $ticket->fromPoint()->from->lng }}&key=AIzaSyC8Wuu1ZzZs2HWvFG0aK_sEmoo4Or4kA14">
						<p style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;margin: 0;padding: 0;border: 0;outline: 0;font-size: 100%;vertical-align: baseline;background: transparent;">
							{{ $settings->map_comment }}</p>
					</td>
					<td class="contacts-cell"
						style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;vertical-align: top;width: 202px;padding: 0 15px;">
						<p style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;margin: 0;padding: 0;border: 0;outline: 0;font-size: 100%;vertical-align: baseline;background: transparent;">
							<b style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;margin: 0;padding: 0;border: 0;outline: 0;font-size: 100%;vertical-align: baseline;background: transparent;">Контакты
								по рейсу:</b></p>
						@foreach(\App\Models\DispatcherPhone::all() as $dispatcherPhone)
							<p style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;margin: 0;padding: 0;border: 0;outline: 0;font-size: 100%;vertical-align: baseline;background: transparent;">
								{{ $dispatcherPhone->phone }}</p>
							@endforeach
						<br style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;">
						<!--<p style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;margin: 0;padding: 0;border: 0;outline: 0;font-size: 100%;vertical-align: baseline;background: transparent;">
							<b style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;margin: 0;padding: 0;border: 0;outline: 0;font-size: 100%;vertical-align: baseline;background: transparent;">Диспетчер:</b>
						</p>
						<p style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;margin: 0;padding: 0;border: 0;outline: 0;font-size: 100%;vertical-align: baseline;background: transparent;">
							FREE LINE 0800 60 32 14 (UKR),</p>
						<p style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;margin: 0;padding: 0;border: 0;outline: 0;font-size: 100%;vertical-align: baseline;background: transparent;">
							+48 222 081 932</p>-->
					</td>
					<td style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;vertical-align: top;">
						<!--<p style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;margin: 0;padding: 0;border: 0;outline: 0;font-size: 100%;vertical-align: baseline;background: transparent;">
							<b style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;margin: 0;padding: 0;border: 0;outline: 0;font-size: 100%;vertical-align: baseline;background: transparent;">Перевозчик:</b>
						</p>
						<p style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;margin: 0;padding: 0;border: 0;outline: 0;font-size: 100%;vertical-align: baseline;background: transparent;">
							GDamaler</p>
						<br style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;">-->
						<p style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;margin: 0;padding: 0;border: 0;outline: 0;font-size: 100%;vertical-align: baseline;background: transparent;">
							<b style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;margin: 0;padding: 0;border: 0;outline: 0;font-size: 100%;vertical-align: baseline;background: transparent;">Багаж:</b>
						</p>
						<p style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;margin: 0;padding: 0;border: 0;outline: 0;font-size: 100%;vertical-align: baseline;background: transparent;">
							{{ $settings->baggage_comment }}
						</p>
						<br style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;">
						<p style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;margin: 0;padding: 0;border: 0;outline: 0;font-size: 100%;vertical-align: baseline;background: transparent;">
							<b style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;margin: 0;padding: 0;border: 0;outline: 0;font-size: 100%;vertical-align: baseline;background: transparent;">Дополнительная
								информация:</b></p>
						<p style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;margin: 0;padding: 0;border: 0;outline: 0;font-size: 100%;vertical-align: baseline;background: transparent;">
							{{ $settings->additional_comment }}
						</p>
						<br style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;">
						<p style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;margin: 0;padding: 0;border: 0;outline: 0;font-size: 100%;vertical-align: baseline;background: transparent;">
							<b style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;margin: 0;padding: 0;border: 0;outline: 0;font-size: 100%;vertical-align: baseline;background: transparent;">Условия
								возврата билета:</b></p>
						<p style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;margin: 0;padding: 0;border: 0;outline: 0;font-size: 100%;vertical-align: baseline;background: transparent;">
							@foreach($returns as $return)
								<p style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;margin: 0;padding: 0;border: 0;outline: 0;font-size: 100%;vertical-align: baseline;background: transparent;">
									{{ $return->interval }}: {{ $ticket->price * $return->factor }} {{ isset($ticket->currency)?$ticket->currency->short:'' }}</p>
							@endforeach
							{!! nl2br($settings->return_comment) !!}
						</p>

					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr class="small-text"
		style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;font-size: 9px;">
		<td colspan="3" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;vertical-align: top;">
			<p style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;margin: 0;padding: 0;border: 0;outline: 0;font-size: 100%;vertical-align: baseline;background: transparent;">
				{{ $settings->agreement_comment }}</p>
			<br style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;">
		</td>
	</tr>
	<!--<tr class="small-text"
		style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;font-size: 9px;">
		<td style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;vertical-align: top;">
			<table style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;border-collapse: collapse;padding: 0;vertical-align: top;">
				<tr style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;">
					<td style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;vertical-align: top;padding-right: 5px;">
						But I must explain to you how all this mistaken idea of
						denouncing pleasure and praising pain was born and I
						will give you a complete account of the system, and
						expound the actual teachings of the great explorer of the
						truth, the master-builder of human happiness. No one
						rejects, dislikes, or avoids pleasure itself, because it is
						pleasure, but because those who do not know how to
						pursue pleasure rationally encounter consequences that
						are extremely painful. Nor again is there anyone who loves
						or pursues or desires to obtain pain of itself, because it is
						pain, but because occasionally circumstances occur in
						which toil and pain can procure him some great pleasure.
						voids a pain that produces no resultant pleasure?
					</td>
					<td style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;vertical-align: top;padding-right: 5px;">
						But I must explain to you how all this mistaken idea of
						denouncing pleasure and praising pain was born and I
						will give you a complete account of the system, and
						expound the actual teachings of the great explorer of the
						truth, the master-builder of human happiness. No one
						rejects, dislikes, or avoids pleasure itself, because it is
						pleasure, but because those who do not know how to
						pursue pleasure rationally encounter consequences that
						are extremely painful. Nor again is there anyone who loves
						or pursues or desires to obtain pain of itself, because it is
						pain, but because occasionally circumstances occur in
						which toil and pain can procure him some great pleasure.
						voids a pain that produces no resultant pleasure?
					</td>
					<td style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);-webkit-box-sizing: border-box;box-sizing: border-box;font-family: inherit;vertical-align: top;">
						But I must explain to you how all this mistaken idea of
						denouncing pleasure and praising pain was born and I
						will give you a complete account of the system, and
						expound the actual teachings of the great explorer of the
						truth, the master-builder of human happiness. No one
						rejects, dislikes, or avoids pleasure itself, because it is
						pleasure, but because those who do not know how to
						pursue pleasure rationally encounter consequences that
						are extremely painful. Nor again is there anyone who loves
						or pursues or desires to obtain pain of itself, because it is
						pain, but because occasionally circumstances occur in
						which toil and pain can procure him some great pleasure.
						voids a pain that produces no resultant pleasure?
					</td>
				</tr>
			</table>
		</td>
	</tr>-->
</table>
    <script>
        window.onload = function () {
			window.print();
		}
    </script>

</body>
</html>