@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>Билет</h1>
        <ol class="breadcrumb">
            <li><a href="{!! url('/search') !!}"><i class="fa fa-dashboard"></i>Главная</a></li>
            <li><a href="{!! url('/tickets') !!}">Билеты</a></li>
            <li class="active">Создать Билет</li>
        </ol>
    </section>
    <section class="content" id="ticketform">
        @include('adminlte-templates::common.errors')
        <div class="row">
            {!! Form::open(['route' => 'tickets.store', 'id'=>'tickets_form']) !!}
            <div class="col-md-6">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Информация о брони</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            @include('tickets.fields')
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Информация о Пассажирах/Билетах</h3>
                    </div>
                    <div class="box-body">
                        @include('tickets.right_fields')
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </section>
@endsection
