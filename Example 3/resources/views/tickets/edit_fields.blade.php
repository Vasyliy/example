<div class="col-sm-6" xmlns:v-bind="http://www.w3.org/1999/xhtml">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Информация о билете</h3>
        </div>
        <div class="box-body">
            <div class="form-group col-sm-6">
                {!! Form::label('prepay', 'Аванс:') !!}
                {!! Form::number('prepay', null, ['class' => 'form-control', 'value' => '0']) !!}
            </div>
            <div class="form-group col-sm-6">
                {!! Form::label('place', 'Место:') !!}
                {!! Form::number('place', $ticket->place->name, ['class' => 'form-control', 'value' => '0', 'disabled'=>'disabled']) !!}
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                    {!! Form::label('variant_id', 'Вариант:') !!}
                    {!! Form::select('variant_id', \App\Models\Ticket::VARIANT, $ticket->variant_id, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('type_id', 'Тип Билета:') !!}
                    {!! Form::select('type_id', \App\Models\TicketType::getAll(), $ticket->type_id, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('agent_id', 'Агент:') !!}
                    {!! Form::select('agent_id', \App\Models\Agent::getAll(), $ticket->agent_id, ['class' => 'form-control select2']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('trip_id', 'Рейс:') !!}
                    {!! Form::hidden('trip_id', $trip->id) !!}
                    {!! Form::text('trip_id', isset($trip) ? $trip->route->name . ' (' . $trip->date . ')' : '-', ['class' => 'form-control', 'disabled'=>'disabled']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('trip_date', 'Дата:') !!}
                    <div class="input-group date">
                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                        <input id="searchdatepicker" name="trip_date" type="text" value="{{ $trip->date }}" @click="
                        hideScheme" class="form-control pull-right">
                    </div>
                </div>
                <input
                        type="hidden"
                        name="place_id"
                        v-model="client.place_id"
                />
                <div v-if="show_scheme">
                    <div class="alert alert-danger">
                        Текущее место занято, необходимо указать новое место.
                    </div>
                    <div class="bus-scheme-wrap">
                        <div class="bus-scheme">
                            <div class="driver icon-wrap">
                                <img src="/img/driver-icon.png" alt="">
                            </div>
                            <div class="tv icon-wrap" style="top: 14px;left: 11px;">
                                <img src="/img/tv-icon.png" alt="">
                                TV
                            </div>
                            <div class="exit" style="left: 30px;">Exit</div>
                            <div class="exit" style="left: 247px;">Exit</div>
                            <div class="places-scheme">
                                <label class="item" v-for="place in places" :title='place.client'
                                       :style="{ left: place.x + 'px', bottom: place.y + 'px' }">
                                    <input
                                            :disabled="place.status == 1"
                                            v-bind:class="{ reserved: place.variant_id == 1, blocked: place.variant_id == 2, checked: place.id == client.place_id }"
                                            v-on:click="setPlace(place.id)"
                                    >
                                    <span class="index-number">@{{ place.num }}</span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('from_point_id', 'Из:') !!}
                    {!! Form::hidden('from_point_id', $from) !!}
                    {!! Form::select('from_point_id', \App\Models\Point::getAllfromRoute($trip->route_id), null, ['class' => 'form-control select2 from_point_id', 'id' => 'from_point_id']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('to_point_id', 'В:') !!}
                    {!! Form::hidden('to_point_id', $to) !!}
                    {!! Form::select('to_point_id', \App\Models\Point::getAlltoRoute($trip->route_id), null, ['class' => 'form-control select2 to_point_id', 'id' => 'to_point_id']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('comment', 'Примечание:') !!}
                    {!! Form::text('comment', null, ['class' => 'form-control']) !!}
                </div>
                {!! Form::hidden('url', url()->previous()) !!}
                <button class="btn btn-primary" v-on:click.prevent="validate()">Сохранить</button>
                <a href="{!! url()->previous() != url()->current() ? url()->previous() : route('tickets.index') !!}" class="btn btn-default">Отменить</a>
            </div>
        </div>
    </div>
</div>
<div class="col-sm-6">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Клиент</h3>
        </div>
        <div class="box-body">
            <input type="hidden" v-bind:name="'client_id'" v-model="client.id"/>
            <!-- ФИО -->
            <div class="form-group col-sm-12"
                 v-bind:class="hasErrorClass('name1')">
                <label for="name1">Фамилия *</label>
                <input
                        type="text"
                        v-bind:name="'name1'"
                        v-model="client.name1"
                        v-on:keyup="clientsAutocomplite()"
                        class="form-control"
                />
                <span
                        class="help-block"
                        v-if="hasError('name1')"
                >@{{ client.errors.name1[0] }}</span>
                <div class="autocomplete-list-wrapper">
                    <ul class="list-group">
                        <li class="list-group-item"
                            v-for="item in clientsAuto"
                            v-on:click="setAutocomplite(item.id)"
                            style="cursor: pointer;"
                        >
                            @{{ item.name1 }} @{{ item.name2 }} @{{ item.name3 }} (@{{ item.phone }}
                            - @{{ item.phone2 }} - @{{ item.email}})
                        </li>
                    </ul>
                </div>
            </div>
            <div class="form-group col-sm-12"
                 v-bind:class="hasErrorClass('name2')">
                <label for="name2">Имя *</label>
                <input type="text"
                       v-bind:name="'name2'"
                       v-model="client.name2"
                       v-on:keyup="setNewClient()"
                       class="form-control"
                />
                <span
                        class="help-block"
                        v-if="hasError('name2')"
                >@{{ client.errors.name2[0] }}</span>
            </div>
            <div class="form-group col-sm-12"
                 v-bind:class="hasErrorClass('name3')">
                <label for="name3">Отчество</label>
                <input type="text"
                       v-bind:name="'name3'"
                       v-model="client.name3"
                       v-on:keyup="setNewClient()"
                       class="form-control"
                />
            </div>
            <div class="form-group col-sm-12"
                 v-bind:class="hasErrorClass('phone')">
                <label for="phone">Телефон №1</label>
                <vue-phone-input
                        type="text"
                        v-bind:name="'phone'"
                        :phone-number="client.phone"
                        v-on:value-changed="updatePhone($event)"
                        class="form-control">
                </vue-phone-input>
                <span
                        class="help-block"
                        v-if="hasError('phone')"
                >@{{ client.errors.phone[0] }}</span>
            </div>
            <div class="form-group col-sm-12"
                 v-bind:class="hasErrorClass('phone2')">
                <label for="phone2">Телефон №2</label>
                <vue-phone-input
                        type="text"
                        v-bind:name="'phone2'"
                        :phone-number="client.phone2"
                        v-on:value-changed="updatePhone2($event)"
                        class="form-control">
                </vue-phone-input>
                <span
                        class="help-block"
                        v-if="hasError('phone2')"
                >@{{ client.errors.phone2[0] }}</span>
            </div>
            <div class="form-group col-sm-12"
                 v-bind:class="hasErrorClass('email')">
                <label for="email">E-mail</label>
                <input
                        type="text"
                        v-bind:name="'email'"
                        v-model="client.email"
                        v-on:keyup="clientsAutocomplite()"
                        class="form-control"
                />
                <span
                        class="help-block"
                        v-if="hasError('email')"
                >@{{ client.errors.email[0] }}</span>
            </div>
        </div>
    </div>
</div>

@section('scripts')
    <script>
        var from_point_id = {{ $from }};
        var to_point_id = {{ $to }};
        $(function () {
            $('.from_point_id').select2({
                language: "ru"
            }).on("select2:select", function (e) {
                var selected_element = $(e.currentTarget);
                from_point_id = selected_element.val();
            });

            $('.to_point_id').select2({
                language: "ru"
            }).on("select2:select", function (e) {
                var selected_element = $(e.currentTarget);
                to_point_id = selected_element.val();
            });

            $('#searchdatepicker').datetimepicker({
                format: 'DD-MM-YYYY',
                minDate: moment().format('YYYY MM DD'),
                locale: 'ru'
            });
        });

        var ticketClient = new Vue({
            el: '#ticketEdit',
            data: {
                clientsAuto: [],
                places: [],
                show_scheme: false,
                client: {
                    id: '{!! $ticket->client->id !!}',
                    place_id: '{!! $ticket->place_id !!}',
                    name1: '{!! $ticket->client->name1 !!}',
                    name2: '{!! $ticket->client->name2 !!}',
                    name3: '{!! $ticket->client->name3 !!}',
                    phone: '{!! $ticket->client->phone !!}',
                    oldPhone: '{!! $ticket->client->phone !!}',
                    phone2: '{!! $ticket->client->phone2 !!}',
                    email: '{!! $ticket->client->email !!}',
                    errors: {}
                }
            },
            methods: {
                setNewPlace: function setNewPlace(place_id) {
                    this.client.place_id = place_id;
                },
                updatePhone: function updatePhone(event) {
                    this.client.phone = event;
                    if (this.client.phone != this.client.oldPhone) {
                        this.clientsAutocomplite();
                        this.client.oldPhone = this.client.phone;
                    }
                },
                updatePhone2: function updatePhone2(event) {
                    this.client.phone2 = event;
                    this.setNewClient();
                },
                setNewClient: function clientsAutocomplite() {
                    this.client['id'] = 0;
                },
                hideScheme: function hideScheme() {
                    this.show_scheme = false;
                },
                clientsAutocomplite: function clientsAutocomplite() {
                    this.setNewClient();
                    if (this.client['name1'].toString().length > 2 ||
                            this.client['phone'].toString().length > 8 ||
                            this.client['email'].toString().length > 2) {
                        $.getJSON('/clients/clientsAutocomplite', {
                            name1: this.client['name1'],
                            phone: this.client['phone'],
                            email: this.client['email']
                        }, function (clients) {
                            this.clientsAuto = clients;
                        }.bind(this));
                    }
                },
                setAutocomplite: function setAutocomplite(client_id) {
                    this.client['id'] = client_id;
                    this.client['name1'] = this.clientsAuto[client_id].name1;
                    this.client['name2'] = this.clientsAuto[client_id].name2;
                    this.client['name3'] = this.clientsAuto[client_id].name3;
                    this.client['phone'] = this.clientsAuto[client_id].phone;
                    this.client['phone2'] = this.clientsAuto[client_id].phone2;
                    this.client['email'] = this.clientsAuto[client_id].email;
                    this.clientsAuto = [];
                },
                setPlace: function setPlace(place_id) {
                    this.client.place_id = place_id;
                },
                clientValidate: function clientValidate() {
                    var client = {
                        name1: this.client['name1'],
                        name2: this.client['name2'],
                        name3: this.client['name3'],
                        phone: this.client['phone'],
                        phone2: this.client['phone2'],
                        email: this.client['email']
                    };

                    this.client.errors = {};

                    $.getJSON('/tickets/checkclient', client, function (result) {
                        if (result != 'ok') {
                            Vue.set(this.client, 'errors', result);
                        }
                    }.bind(this));

                    return false;
                },
                placeValidate: function placeValidate() {
                    var place = {
                        route_id: {!! $trip->route->id !!},
                        date: document.getElementById('searchdatepicker').value,
                        from_point_id: from_point_id,
                        to_point_id: to_point_id,
                        place_id: this.client['place_id'],
                        ticket_id: {!! $ticket->id !!}
                    };

                    $.getJSON('/tickets/checkplace', place, function (result) {
                        if (result == 'ok') {
                            $('#ticketEdit').submit();
                        } else {
                            this.show_scheme = true;
                            this.places = result;
                        }
                    }.bind(this));

                    return false;
                },
                validate: function validate() {
                    this.clientValidate();
                    this.placeValidate();
                },
                hasError: function hasError(field) {
                    if (this.client['errors'][field]) {
                        return true;
                    } else {
                        return false;
                    }
                },
                hasErrorClass: function hasErrorClass(field) {
                    if (this.client['errors'][field]) {
                        return 'has-error';
                    } else {
                        return '';
                    }
                }
            }
        });
    </script>
@endsection

