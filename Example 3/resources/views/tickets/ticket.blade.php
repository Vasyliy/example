<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Билет</title>
</head>
<body>
<style>
    html {
        -webkit-text-size-adjust: 100%;
        -ms-text-size-adjust: 100%;
    }

    input, textarea, button {
        outline: none;
        font: inherit;
        margin: 0;
    }

    input:active, textarea:active, select:active {
        outline: none;
    }

    input:focus, select:focus {
        outline: none;
    }

    button, input[type="submit"], input[type="button"] {
        -webkit-appearance: none;
        cursor: pointer;
    }

    h1, .h1, h2, .h2, h3, .h3 {
        margin: 0;
    }

    * {
        -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
        -webkit-box-sizing: border-box;
        box-sizing: border-box;
        font-family: inherit;
    }

    html, body, div, span, applet, object, iframe,
    h1, h2, h3, h4, h5, h6, p, blockquote, pre,
    a, abbr, acronym, address, big, cite, code,
    del, dfn, em, font, img, ins, kbd, q, s, samp,
    small, strike, strong, sub, sup, tt, var,
    b, u, i, center,
    dl, dt, dd, ol, ul, li,
    fieldset, form, label, legend, caption {
        margin: 0;
        padding: 0;
        border: 0;
        outline: 0;
        font-size: 100%;
        vertical-align: baseline;
        background: transparent;
    }

    header, nav, section, article, aside, footer {
        display: block;
    }

    textarea {
        resize: none;
        font: inherit;
    }

    ol, ul {
        list-style: none;
    }

    img {
        border: none;
        display: block;
    }

    input {
        vertical-align: middle;
    }

    a {
        text-decoration: none;
    }

    a:hover {
        text-decoration: none;
    }

    ul, li {
        list-style-type: none;
    }

    body {
        font-family: 'Roboto', Arial, sans-serif;
        font-size: 12px;
        line-height: 1.25;
    }

    .ticket-wrap {
        width: 740px;
        margin: 0 auto;
        padding: 10px 0;
    }
    .ticket-wrap:not(:last-child){
        border-bottom: 2px dashed #000000;
    }
    table{
        border-collapse: collapse;
        width: 100%;
        text-align: center;
    }
    td{
        border: 1px solid #000000;
        padding: 7px 2px;
    }
    .logo{
        height: 57px;
        margin: 0 auto;
    }
    .name{
        text-transform: uppercase;
        font-weight: 700;
        margin-top: 5px;
        height: 15px;
    }
    @page { size: auto;  margin: 0mm; }
</style>

<div class="ticket-wrap">
    <table>
        <tr>
            <td colspan="2">
                <img src="/img/logo.png" alt="" class="logo">
            </td>
            <td colspan="3">
                <p>Квиток та багажна квитанція</p>
                <p>Passenger ticket and</p>
                <p>luggage check</p>
            </td>
            <td colspan="3">
                ag {{ $ticket->id }}
            </td>
        </tr>
        <tr>
            <td colspan="2" >
                <p>Маршрут / Route</p>
                <p>{{ $trip->route->name }}</p>
            </td>
            <td colspan="2">
                <p>Інтервал / Interval</p>
                <p>{{ $ticket->fromPoint()->from->city->name }} - {{ $ticket->toPoint()->to->city->name }}</p>
            </td>
            <td colspan="4" rowspan="2" style="vertical-align: top;">
                <p>Штамп і дата продажу</p>
                <p>Stump of issue. Date</p>
                <br>
                <p>{{ $ticket->created_at->format('d-m-Y H:i') }}</p>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <p>Прізвище, І., Б. &nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;&nbsp; Name , surname</p>
                <p class="name">{{ $ticket->client_id?$ticket->client->name1.' '.$ticket->client->name2:$ticket->name1.' '.$ticket->name2 }}</p>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <p>Відправлення / Departure</p>
            </td>
            <td rowspan="2">
                <p>Вид квитка</p>
                <p>Kind of ticket</p>
            </td>
            <td rowspan="2">
                <p>Місце</p>
                <p>Seat</p>
            </td>
            <td colspan="4">
                <p>Вартість, {{ $ticket->currency_id?$ticket->currency->short:'' }} / Fare, {{ $ticket->currency_id?$ticket->currency->short:'' }}(cost)</p>
            </td>
        </tr>
        <tr>
            <td>
                <p>Дата</p>
                <p>Date</p>
            </td>
            <td>
                <p>Час</p>
                <p>Time</p>
            </td>
            <td>
                <p>Осіб</p>
                <p>Persons</p>
            </td>
            <td>
                <p>Багажу</p>
                <p>Luggage</p>
            </td>
            <td>
                <p>Передоплата</p>
                <p>Prepayment</p>
            </td>
            <td height="50">
                <p>ВСЬОГО</p>
                <p>Total</p>
            </td>
        </tr>
        <tr>
            <td width="15%">{{ $trip->getDateAsCarbon()->addDay($ticket->fromPoint()->from_day)->format('d.m.Y') }}</td>
            <td width="15%">{{ date('H:i', mktime(0, 0, $ticket->fromPoint()->getRealFromTimeInSeconds())) }}</td>
            <td width="15%">{{ $ticket->type->name }}</td>
            <td width="15%">{{ $ticket->place->name }}</td>
            <td width="10%">1</td>
            <td width="10%">1</td>
            <td width="10%">{{ $ticket->prepay }} {{ $ticket->currency_id?$ticket->currency->short:'' }}</td>
            <td width="10%" height="99">{{ $ticket->price - $ticket->prepay }} {{ $ticket->currency_id?$ticket->currency->short:'' }}</td>
        </tr>
    </table>
</div>
<div class="ticket-wrap">
    <table>
        <tr>
            <td colspan="2">
                <img src="/img/logo.png" alt="" class="logo">
            </td>
            <td colspan="3">
                <p>Квиток та багажна квитанція</p>
                <p>Passenger ticket and</p>
                <p>luggage check</p>
            </td>
            <td colspan="3">
                ag {{ $ticket->id }}
            </td>
        </tr>
        <tr>
            <td colspan="2" >
                <p>Маршрут / Route</p>
                <p>{{ $trip->route->name }}</p>
            </td>
            <td colspan="2">
                <p>Інтервал / Interval</p>
                <p>{{ $ticket->fromPoint()->from->city->name }} - {{ $ticket->toPoint()->to->city->name }}</p>
            </td>
            <td colspan="4" rowspan="2" style="vertical-align: top;">
                <p>Штамп і дата продажу</p>
                <p>Stump of issue. Date</p>
                <br>
                <p>{{ $ticket->created_at->format('d-m-Y H:i') }}</p>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <p>Прізвище, І., Б. &nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;&nbsp; Name , surname</p>
                <p class="name">{{ $ticket->client_id?$ticket->client->name1.' '.$ticket->client->name2:$ticket->name1.' '.$ticket->name2 }}</p>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <p>Відправлення / Departure</p>
            </td>
            <td rowspan="2">
                <p>Вид квитка</p>
                <p>Kind of ticket</p>
            </td>
            <td rowspan="2">
                <p>Місце</p>
                <p>Seat</p>
            </td>
            <td colspan="4">
                <p>Вартість, {{ $ticket->currency_id?$ticket->currency->short:'' }} / Fare, {{ $ticket->currency_id?$ticket->currency->short:'' }}(cost)</p>
            </td>
        </tr>
        <tr>
            <td>
                <p>Дата</p>
                <p>Date</p>
            </td>
            <td>
                <p>Час</p>
                <p>Time</p>
            </td>
            <td>
                <p>Осіб</p>
                <p>Persons</p>
            </td>
            <td>
                <p>Багажу</p>
                <p>Luggage</p>
            </td>
            <td>
                <p>Передоплата</p>
                <p>Prepayment</p>
            </td>
            <td height="50">
                <p>ВСЬОГО</p>
                <p>Total</p>
            </td>
        </tr>
        <tr>
            <td width="15%">{{ $trip->getDateAsCarbon()->addDay($ticket->fromPoint()->from_day)->format('d.m.Y') }}</td>
            <td width="15%">{{ date('H:i', mktime(0, 0, $ticket->fromPoint()->getRealFromTimeInSeconds())) }}</td>
            <td width="15%">{{ $ticket->type->name }}</td>
            <td width="15%">{{ $ticket->place->name }}</td>
            <td width="10%">1</td>
            <td width="10%">1</td>
            <td width="10%">{{ $ticket->prepay }} {{ $ticket->currency_id?$ticket->currency->short:'' }}</td>
            <td width="10%" height="99">{{ $ticket->price - $ticket->prepay }} {{ $ticket->currency_id?$ticket->currency->short:'' }}</td>
        </tr>
    </table>
</div>
<div class="ticket-wrap">
    <table>
        <tr>
            <td colspan="2">
                <img src="/img/logo.png" alt="" class="logo">
            </td>
            <td colspan="3">
                <p>Квиток та багажна квитанція</p>
                <p>Passenger ticket and</p>
                <p>luggage check</p>
            </td>
            <td colspan="3">
                ag {{ $ticket->id }}
            </td>
        </tr>
        <tr>
            <td colspan="2" >
                <p>Маршрут / Route</p>
                <p>{{ $trip->route->name }}</p>
            </td>
            <td colspan="2">
                <p>Інтервал / Interval</p>
                <p>{{ $ticket->fromPoint()->from->city->name }} - {{ $ticket->toPoint()->to->city->name }}</p>
            </td>
            <td colspan="4" rowspan="2" style="vertical-align: top;">
                <p>Штамп і дата продажу</p>
                <p>Stump of issue. Date</p>
                <br>
                <p>{{ $ticket->created_at->format('d-m-Y H:i') }}</p>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <p>Прізвище, І., Б. &nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;&nbsp; Name , surname</p>
                <p class="name">{{ $ticket->client_id?$ticket->client->name1.' '.$ticket->client->name2:$ticket->name1.' '.$ticket->name2 }}</p>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <p>Відправлення / Departure</p>
            </td>
            <td rowspan="2">
                <p>Вид квитка</p>
                <p>Kind of ticket</p>
            </td>
            <td rowspan="2">
                <p>Місце</p>
                <p>Seat</p>
            </td>
            <td colspan="4">
                <p>Вартість / Fare, {{ $ticket->currency_id?$ticket->currency->short:'' }}(cost)</p>
            </td>
        </tr>
        <tr>
            <td>
                <p>Дата</p>
                <p>Date</p>
            </td>
            <td>
                <p>Час</p>
                <p>Time</p>
            </td>
            <td>
                <p>Осіб</p>
                <p>Persons</p>
            </td>
            <td>
                <p>Багажу</p>
                <p>Luggage</p>
            </td>
            <td>
                <p>Передоплата</p>
                <p>Prepayment</p>
            </td>
            <td height="50">
                <p>ВСЬОГО</p>
                <p>Total</p>
            </td>
        </tr>
        <tr>
            <td width="15%">{{ $trip->getDateAsCarbon()->addDay($ticket->fromPoint()->from_day)->format('d.m.Y') }}</td>
            <td width="15%">{{ date('H:i', mktime(0, 0, $ticket->fromPoint()->getRealFromTimeInSeconds())) }}</td>
            <td width="15%">{{ $ticket->type->name }}</td>
            <td width="15%">{{ $ticket->place->name }}</td>
            <td width="10%">1</td>
            <td width="10%">1</td>
            <td width="10%">{{ $ticket->prepay }} {{ $ticket->currency_id?$ticket->currency->short:'' }}</td>
            <td width="10%" height="99">{{ $ticket->price - $ticket->prepay }} {{ $ticket->currency_id?$ticket->currency->short:'' }}</td>
        </tr>
    </table>
    <script>
        window.onload = function() {
            window.print();
        }
    </script>
</div>

</body>
</html>