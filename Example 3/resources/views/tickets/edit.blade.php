@extends('layouts.app')

@section('css')
    <link rel="stylesheet" href="/css/main.css">
@endsection

@section('content')
    <section class="content-header">
        <h1>Билет</h1>
        <ol class="breadcrumb">
            <li><a href="{!! url('/search') !!}"><i class="fa fa-dashboard"></i>Главная</a></li>
            <li><a href="{!! url('/tickets') !!}">Билеты</a></li>
            <li class="active">Редактировать Билет</li>
        </ol>
   </section>
   <div class="content">
       @include('flash::message')
       @include('adminlte-templates::common.errors')
       {!! Form::model($ticket, ['route' => ['tickets.update', $ticket->id], 'method' => 'patch', 'id'=>'ticketEdit', 'style' => 'display: flex; flex-direction: row;']) !!}

            @include('tickets.edit_fields')

       {!! Form::close() !!}
   </div>
@endsection