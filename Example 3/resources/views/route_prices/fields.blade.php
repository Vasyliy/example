<!-- Route Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('route_id', 'Маршрут:') !!}
    {!! Form::number('route_id', null, ['class' => 'form-control']) !!}
</div>

<!-- From Point Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('from_point_id', 'Из:') !!}
    {!! Form::number('from_point_id', null, ['class' => 'form-control']) !!}
</div>

<!-- To Point Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('to_point_id', 'До:') !!}
    {!! Form::number('to_point_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('price', 'Цена:') !!}
    {!! Form::text('price', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('currency_id', 'Валюта:') !!}
    {!! Form::number('currency_id', \App\Models\Currency::getAll(),  null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('routePrices.index') !!}" class="btn btn-default">Отменить</a>
</div>
