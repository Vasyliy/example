<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $routePrice->id !!}</p>
</div>

<!-- Route Id Field -->
<div class="form-group">
    {!! Form::label('route_id', 'Route Id:') !!}
    <p>{!! $routePrice->route_id !!}</p>
</div>

<!-- From Point Id Field -->
<div class="form-group">
    {!! Form::label('from_point_id', 'From Point Id:') !!}
    <p>{!! $routePrice->from_point_id !!}</p>
</div>

<!-- To Point Id Field -->
<div class="form-group">
    {!! Form::label('to_point_id', 'To Point Id:') !!}
    <p>{!! $routePrice->to_point_id !!}</p>
</div>

<!-- Price Field -->
<div class="form-group">
    {!! Form::label('price', 'Price:') !!}
    <p>{!! $routePrice->price !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $routePrice->deleted_at !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $routePrice->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $routePrice->updated_at !!}</p>
</div>

