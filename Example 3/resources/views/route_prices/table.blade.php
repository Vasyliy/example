<table class="table table-responsive" id="routePrices-table">
    <thead>
        <tr>
            <th>Маршрут</th>
            <th>Отрезок от</th>
            <th>до</th>
            <th>Цена</th>
            <th>Валюта</th>
            <th colspan="3">Действия</th>
        </tr>
    </thead>
    <tbody>
    @foreach($routePrices as $routePrice)
        <tr>
            <td>{!! $routePrice->route_id !!}</td>
            <td>{!! $routePrice->from->name !!}</td>
            <td>{!! $routePrice->to->name !!}</td>
            <td>{!! $routePrice->price !!}</td>
            <td>{!! $routePrice->currency->short !!}</td>
            <td>
                {!! Form::open(['route' => ['routePrices.destroy', $routePrice->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <!--<a href="{!! route('routePrices.show', [$routePrice->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>-->
                    <a href="{!! route('routePrices.edit', [$routePrice->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Вы уверены?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>