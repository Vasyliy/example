<div class="form-group col-sm-4">
    {!! Form::label('user_id', 'Пользователь:') !!}
    {!! Form::select('user_id', \App\Models\User::getAll(), null, ['class' => 'form-control select2 pull-right', 'placeholder' => 'Все пользователи']) !!}
</div>
<div class="form-group col-sm-3">
    {!! Form::label('type', 'Тип действия:') !!}
    {!! Form::select('type', \App\Models\Activity::TYPES, null, ['class' => 'form-control pull-right', 'placeholder' => 'Все типы']) !!}
</div>
<div class="form-group col-sm-3">
    {!! Form::label('created_at', 'Дата:') !!}
    <div class="input-group date">
        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
        {!! Form::text('created_at', null, ['class' => 'form-control pull-right', 'id'=>'searchdatepicker']) !!}
    </div>
</div>
<div class="form-group col-sm-2">
    {!! Form::label('', '') !!}
    {!! Form::submit('Отфильтровать', ['class' => 'form-control btn btn-primary', 'style' => 'margin-top: 5px']) !!}
</div>

@section('scripts')
    <script>
        $(function () {
            $('.select2').select2({
                language: "ru"
            });
            $('#searchdatepicker').datetimepicker({
                format: 'DD-MM-YYYY'
                , locale: 'ru'
            });
        });
    </script>
@endsection