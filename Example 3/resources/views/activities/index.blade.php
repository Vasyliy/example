@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Журнал Действий</h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Фильтр</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'activities.index', 'role'=>'search', 'method'=>'get']) !!}

                    @include('activities.search_fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        <div class="box box-primary">
            <div class="box-body">
                <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">

                    @include('activities.table')

                    <div class="row">
                        <div class="col-sm-5">
                            <div class="dataTables_info" id="example1_info">
                                @if( $activities->count() < $activities->total() )
                                    Показано от {{ ($activities->currentPage() - 1) * $activities->perPage() + 1 }} до
                                    @if( $activities->currentPage() * $activities->perPage() > $activities->total() )
                                        {{ $activities->total() }}
                                    @else
                                        {{ $activities->currentPage() * $activities->perPage() }}
                                    @endif
                                    из {{ $activities->total() }} действий
                                @endif
                            </div>
                        </div>
                        <div class="col-sm-7">
                            <div class="dataTables_paginate paging_simple_numbers pull-right">
                                {{ $activities->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
@endsection

