<table class="table table-responsive" id="activities-table">
    <thead>
        <tr>
        <th>Пользователь</th>
        <th>Тип действия</th>
        <th>Действие</th>
        <th>Дата</th>
    </thead>
    <tbody>
    @foreach($activities as $activity)
        <tr>
            <td>{!! $activity->user->name !!}</td>
            <td>{!! \App\Models\Activity::TYPES[$activity->type] !!}</td>
            <td>{!! $activity->description !!}</td>
            <td>{!! $activity->created_at->format('d-m-Y H:i:s') !!}</td>
        </tr>
    @endforeach
    </tbody>
</table>