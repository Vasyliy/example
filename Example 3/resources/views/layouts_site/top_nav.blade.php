<div class="navbar-header">
    <a href="#" class="navbar-brand"><b>Gdamaler</b> online</a>
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
        <i class="fa fa-bars"></i>
    </button>
</div>

<!-- Collect the nav links, forms, and other content for toggling -->
<div class="collapse navbar-collapse pull-left" id="navbar-collapse">
    <ul class="nav navbar-nav">
        <li>
            <a href="#"><i class="fa fa-road"></i> <span>Маршруты</span> <span class="sr-only">(current)</span></a>
        </li>
        <li>
            <a href="#"><i class="fa fa-ticket"></i> <span>Билеты</span></a>
        </li>
        <li>
            <a href="#"><i class="fa fa-ticket"></i> <span>Расписание</span></a>
        </li>
        <!--
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-tasks"></i> <span>Расписание</span> <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li class="divider"></li>
                <li><a href="#">Separated link</a></li>
                <li class="divider"></li>
                <li><a href="#">One more separated link</a></li>
            </ul>
        </li>
        -->
    </ul>
    <form class="navbar-form navbar-left" role="search">
        <div class="form-group">
            <input type="text" class="form-control" id="navbar-search-input" placeholder="Search">
        </div>
    </form>
</div>
<!-- /.navbar-collapse -->
<!-- Navbar Right Menu -->
<div class="navbar-custom-menu">
    <ul class="nav navbar-nav">
        <!-- Messages: style can be found in dropdown.less-->
        <!--
        <li class="dropdown messages-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-envelope-o"></i>
                <span class="label label-success">4</span>
            </a>
            <ul class="dropdown-menu">
                <li class="header">You have 4 messages</li>
                <li>
                    <ul class="menu">
                        <li>
                            <a href="#">
                                <div class="pull-left">
                                    <img src="http://infyom.com/images/logo/blue_logo_150x150.jpg" class="img-circle" alt="User Image">
                                </div>
                                <h4>
                                    Support Team
                                    <small><i class="fa fa-clock-o"></i> 5 mins</small>
                                </h4>
                                <p>Why not buy a new awesome theme?</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="footer"><a href="#">See All Messages</a></li>
            </ul>
        </li>
        -->
        <!-- /.messages-menu -->

        <!-- Notifications Menu -->
        <!--
        <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-bell-o"></i>
                <span class="label label-warning">10</span>
            </a>
            <ul class="dropdown-menu">
                <li class="header">You have 10 notifications</li>
                <li>
                    <ul class="menu">
                        <li>
                            <a href="#">
                                <i class="fa fa-users text-aqua"></i> 5 new members joined today
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="footer"><a href="#">View all</a></li>
            </ul>
        </li>
        -->
        <!-- Tasks Menu -->
        <!--
        <li class="dropdown tasks-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-flag-o"></i>
                <span class="label label-danger">9</span>
            </a>
            <ul class="dropdown-menu">
                <li class="header">You have 9 tasks</li>
                <li>
                    <ul class="menu">
                        <li>
                            <a href="#">
                                <h3>
                                    Design some buttons
                                    <small class="pull-right">20%</small>
                                </h3>
                                <div class="progress xs">
                                    <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                        <span class="sr-only">20% Complete</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="footer">
                    <a href="#">View all tasks</a>
                </li>
            </ul>
        </li>
        -->
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-language"></i> <span>RU</span> <span class="caret"></span>
            </a>
            <ul class="dropdown-menu" role="menu">
                @foreach(\App\Models\Lang::all() as $lang_item)
                    <li>
                        <a href="#" style="text-transform: uppercase;">
                            {{ $lang_item->short }} - {{ $lang_item->name }}
                        </a>
                    </li>
                @endforeach
            </ul>
        </li>
    @if(\Illuminate\Support\Facades\Auth::check())
        <li class="">
            <!-- Menu Toggle Button -->
            <a href="{{ url('/search') }}">
                <i class="fa fa-gears"></i>
                <span class="hidden-xs">Панель управления</span>
            </a>
        </li>
        <!-- User Account Menu -->
        <li class="dropdown user user-menu">
            <!-- Menu Toggle Button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <!-- The user image in the navbar-->
                <img src="http://infyom.com/images/logo/blue_logo_150x150.jpg" class="user-image" alt="User Image">
                <!-- hidden-xs hides the username on small devices so only the image appears. -->
                <span class="hidden-xs">{!! Auth::user()->name !!}</span>
            </a>
            <ul class="dropdown-menu">
                <li class="user-header">
                    <img src="http://infyom.com/images/logo/blue_logo_150x150.jpg" class="img-circle" alt="User Image">
                    <p>
                        {!! Auth::user()->name !!}
                        <small>Member since {!! Auth::user()->created_at->format('M. Y') !!}</small>
                    </p>
                </li>
                <!-- Menu Body -->
                <li class="user-body">
                    <div class="row">
                        <!--
                        <div class="col-xs-6 text-center">
                            <a href="{{ url('/routes') }}">Панель управления</a>
                        </div>
                        <div class="col-xs-4 text-center">
                            <a href="#">Sales</a>
                        </div>
                        <div class="col-xs-4 text-center">
                            <a href="#">Friends</a>
                        </div>
                        -->
                    </div>
                </li>
                <!-- Menu Footer-->
                <li class="user-footer">
                    <div class="pull-left">
                        <a href="#" class="btn btn-default btn-flat">Profile</a>
                    </div>
                    <div class="pull-right">
                        <form action="{{ url('logout') }}" method="post">
                            {{ csrf_field() }}
                            <button class="btn btn-default btn-flat">Sign out</button>
                        </form>

                    </div>
                </li>
            </ul>
        </li>
    @else
        <li class="">
            <a href="{{ url('/login') }}">Login</a>
        </li>
    @endif
    </ul>
</div>