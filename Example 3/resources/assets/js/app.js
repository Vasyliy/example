/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
//window.vueResource = require('vue-resource');

Vue.use(require('vue-resource'));

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

//Vue.component('example', require('./components/Example.vue'));
import VueMask from 'v-mask';
Vue.use(VueMask);
Vue.component('vue-phone-input', require('./components/Phone.vue'));
//var clients = {};
//var placesArray = {};
var client = {
    ticket_num: 0,
    name1: '',
    name2: '',
    name3: '',
    phone: '',
    phone2: '',
    email: '',
    ticket_type: '',
    comment: ''
};

new Vue({
    el: '#ticketform',
    data: {
        testvar: 'Example First Ok22 !!!',
        testvar2: '12121212123333',
        places: [],
        placesArray: [],
        clients: [],
        clientsAuto: [],
        client: {
            ticket_num: 0,
            name1: '',
            name2: '',
            name3: '',
            oldPhone: '',
            phone: '',
            phone2: '',
            email: '',
            ticket_type: '',
            comment: ''
        }
    },
    created: function created() {
        console.log('Created function run !');
        $.getJSON('/places/list', function (places) {
            console.log(places);
            var len = places.length;
            for (var i = 0; i < len; i++) {
                placesArray.push({
                    key: oFullResponse.results[i].label,
                    sortable: true,
                    resizeable: true
                });
            }
            this.placesArray = places;
        }.bind(this));
    },
    computed: {
        // геттер вычисляемого значения
        /*reversedMessage: function () {
         // `this` указывает на экземпляр vm
         return this.testvar.split('').reverse().join('')
         }*/
    },
    methods: {
        updatePhone: function updatePhone(place, event){
            this.clients[place].phone = event;
            if(this.clients[place].phone != this.clients[place].oldPhone) {
                this.clientsAutocomplite(place);
                this.clients[place].oldPhone = this.clients[place].phone;
            }
        },
        updatePhone2: function updatePhone2(place, event){
            this.clients[place].phone2 = event;
        },
        greet: function greet(event) {
            // `this` внутри методов указывает на экземпляр Vue
            // `event` — нативное событие DOM
            if (event) {
                alert(event.target.tagName);
            }
        },
        /**
         * Изменить набор форм для выбранных мест
         * @param place_id
         * @param place_num
         */
        placesFormsRefresh: function placesFormsRefresh(place_id, place_num) {
            //this.clients[place_id] = this.makeNewClient();
            Vue.set(this.clients, place_id, this.makeNewClient());
            this.clients[place_id].ticket_num = place_num;
        },
        testGeting: function testGeting() {
            console.log('Function testGeting Started');
            // GET /someUrl
            /* this.$http.get('/someUrl').then(response => {
             // get body data
             this.someData = response.body;
             }, response => {
             // error callback
             });
             */
            /*$.getJSON('/clients/list', function (clients) {
             console.log(clients);
             this.clients = clients;
             }.bind(this));*/
        },
        clientsAutocomplite: function clientsAutocomplite(place) {
            if (this.clients[place]['name1'].toString().length > 2 ||
                this.clients[place]['phone'].toString().length > 8 ||
                this.clients[place]['email'].toString().length > 2) {
                $.getJSON('/clients/clientsAutocomplite', {
                    name1: this.clients[place]['name1'],
                    phone: this.clients[place]['phone'],
                    email: this.clients[place]['email']
                }, function (clients) {
                    this.clientsAuto = clients;
                }.bind(this));
            }
        },
        makeNewClient: function makeNewClient() {
            return {
                ticket_num: 0,
                client_id: 0,
                name1: '',
                name2: '',
                name3: '',
                phone: '',
                phone2: '',
                email: '',
                ticket_type: '',
                comment: '',
                errors: {}
            };
        },
        setAutocomplite: function setAutocomplite(client_id, place_id) {
            this.clients[place_id]['client_id'] = client_id;
            this.clients[place_id]['name1'] = this.clientsAuto[client_id].name1;
            this.clients[place_id]['name2'] = this.clientsAuto[client_id].name2;
            this.clients[place_id]['name3'] = this.clientsAuto[client_id].name3;
            this.clients[place_id]['phone'] = this.clientsAuto[client_id].phone;
            this.clients[place_id]['phone2'] = this.clientsAuto[client_id].phone2;
            this.clients[place_id]['email'] = this.clientsAuto[client_id].email;
            this.clientsAuto = [];
        },
        clientsValidate: function clientsValidate() {

            var clients = {};
            for (var place_key in this.places) {
                //console.log(this.places[place_key]);
                var place = this.places[place_key];
                clients[place] = {
                    name1: this.clients[place]['name1'],
                    name2: this.clients[place]['name2'],
                    name3: this.clients[place]['name3'],
                    phone: this.clients[place]['phone'],
                    phone2: this.clients[place]['phone2'],
                    email: this.clients[place]['email']
                };
                this.clients[place].errors = {};
            }

            $.getJSON('/tickets/checkclients', clients, function (result) {
                if (result == 'ok') {
                    $('#tickets_form').submit();
                } else {
                    for (var place in result) {
                        Vue.set(this.clients[place], 'errors', result[place]);
                        //this.clients[place].errors = result[place];
                    }
                }
            }.bind(this));

            return false;
        },
        hasError: function hasError(place, field) {
            if (typeof this.clients[place] != 'undefined' && typeof this.clients[place]['errors'] != 'undefined' && typeof this.clients[place]['errors'][field] != 'undefined') {
                return true;
            } else {
                return false;
            }
        },
        hasErrorPlace: function hasErrorPlace(place) {
            if (typeof this.clients[place] != 'undefined' && typeof this.clients[place]['errors'] != 'undefined') {
                return true;
            } else {
                return false;
            }
        },
        hasErrorClass: function hasErrorClass(place, field) {
            if (typeof this.clients[place] != 'undefined' && typeof this.clients[place]['errors'] != 'undefined' && typeof this.clients[place]['errors'][field] != 'undefined') {
                return 'has-error';
            } else {
                return '';
            }
        }
    }
});
