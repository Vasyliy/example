<?php

namespace App\Repositories;

use App\Models\Point;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PointRepository
 * @package App\Repositories
 * @version July 5, 2018, 6:33 pm UTC
 *
 * @method Point findWithoutFail($id, $columns = ['*'])
 * @method Point find($id, $columns = ['*'])
 * @method Point first($columns = ['*'])
*/
class PointRepository extends TraclateBaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'city_id',
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Point::class;
    }
}
