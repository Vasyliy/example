<?php

namespace App\Repositories;

use App\Models\TradingPlatform;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class TradingPlatformRepository
 * @package App\Repositories
 * @version September 27, 2018, 6:08 pm UTC
 *
 * @method TradingPlatform findWithoutFail($id, $columns = ['*'])
 * @method TradingPlatform find($id, $columns = ['*'])
 * @method TradingPlatform first($columns = ['*'])
*/
class TradingPlatformRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'refresh_time_1',
        'refresh_time_2',
        'refresh_time_3',
        'options'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TradingPlatform::class;
    }
}
