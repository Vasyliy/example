<?php

namespace App\Repositories;

use App\Models\Bus;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class BusRepository
 * @package App\Repositories
 * @version July 4, 2018, 1:09 pm UTC
 *
 * @method Bus findWithoutFail($id, $columns = ['*'])
 * @method Bus find($id, $columns = ['*'])
 * @method Bus first($columns = ['*'])
*/
class BusRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'sсheme_id',
        'name',
        'model',
        'number',
        'note'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Bus::class;
    }
}
