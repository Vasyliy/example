<?php

namespace App\Repositories;

use App\Models\Settings;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class SettingsRepository
 * @package App\Repositories
 * @version August 19, 2018, 7:50 am UTC
 *
 * @method Settings findWithoutFail($id, $columns = ['*'])
 * @method Settings find($id, $columns = ['*'])
 * @method Settings first($columns = ['*'])
*/
class SettingsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'baggage_comment',
        'additional_comment',
        'return_comment',
        'agreement_comment',
        'map_comment',
        'notification_email',
        'critical_places_number'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Settings::class;
    }
}
