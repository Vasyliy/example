<?php

namespace App\Repositories;

use App\Models\TradingPlatformTicketVariant;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class TradingPlatformTicketVariantRepository
 * @package App\Repositories
 * @version September 27, 2018, 5:56 pm UTC
 *
 * @method TradingPlatformTicketVariant findWithoutFail($id, $columns = ['*'])
 * @method TradingPlatformTicketVariant find($id, $columns = ['*'])
 * @method TradingPlatformTicketVariant first($columns = ['*'])
*/
class TradingPlatformTicketVariantRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'trading_platform_id',
        'variant_id',
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TradingPlatformTicketVariant::class;
    }
}
