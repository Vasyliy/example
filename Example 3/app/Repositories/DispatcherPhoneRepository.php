<?php

namespace App\Repositories;

use App\Models\DispatcherPhone;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class DispatcherPhoneRepository
 * @package App\Repositories
 * @version August 19, 2018, 7:49 am UTC
 *
 * @method DispatcherPhone findWithoutFail($id, $columns = ['*'])
 * @method DispatcherPhone find($id, $columns = ['*'])
 * @method DispatcherPhone first($columns = ['*'])
*/
class DispatcherPhoneRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'phone'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return DispatcherPhone::class;
    }
}
