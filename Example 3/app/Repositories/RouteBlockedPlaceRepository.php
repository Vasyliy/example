<?php

namespace App\Repositories;

use App\Models\RouteBlockedPlace;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class RouteBlockedPlaceRepository
 * @package App\Repositories
 * @version September 20, 2018, 2:35 pm UTC
 *
 * @method RouteBlockedPlace findWithoutFail($id, $columns = ['*'])
 * @method RouteBlockedPlace find($id, $columns = ['*'])
 * @method RouteBlockedPlace first($columns = ['*'])
*/
class RouteBlockedPlaceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'route_id',
        'place_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return RouteBlockedPlace::class;
    }
}
