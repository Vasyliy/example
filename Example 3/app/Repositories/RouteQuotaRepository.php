<?php

namespace App\Repositories;

use App\Models\RouteQuota;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class RouteQuotaRepository
 * @package App\Repositories
 * @version October 11, 2018, 8:46 pm UTC
 *
 * @method RouteQuota findWithoutFail($id, $columns = ['*'])
 * @method RouteQuota find($id, $columns = ['*'])
 * @method RouteQuota first($columns = ['*'])
*/
class RouteQuotaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'route_id',
        'agent_id',
        'amount'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return RouteQuota::class;
    }
}
