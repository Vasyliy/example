<?php

namespace App\Repositories;

use App\Models\TripQuota;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class TripQuotaRepository
 * @package App\Repositories
 * @version October 11, 2018, 9:05 pm UTC
 *
 * @method TripQuota findWithoutFail($id, $columns = ['*'])
 * @method TripQuota find($id, $columns = ['*'])
 * @method TripQuota first($columns = ['*'])
*/
class TripQuotaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'trip_id',
        'agent_id',
        'amount'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TripQuota::class;
    }
}
