<?php

namespace App\Repositories;

use App\Models\Lang;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class LangRepository
 * @package App\Repositories
 * @version June 27, 2018, 6:05 am UTC
 *
 * @method Lang findWithoutFail($id, $columns = ['*'])
 * @method Lang find($id, $columns = ['*'])
 * @method Lang first($columns = ['*'])
*/
class LangRepository extends TraclateBaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'short'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Lang::class;
    }
}
