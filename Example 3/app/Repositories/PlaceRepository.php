<?php

namespace App\Repositories;

use App\Models\Place;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PlaceRepository
 * @package App\Repositories
 * @version July 11, 2018, 10:05 am UTC
 *
 * @method Place findWithoutFail($id, $columns = ['*'])
 * @method Place find($id, $columns = ['*'])
 * @method Place first($columns = ['*'])
*/
class PlaceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'sсheme_id',
        'name',
        'status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Place::class;
    }
}
