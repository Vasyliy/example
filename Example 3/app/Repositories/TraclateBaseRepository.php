<?php

namespace App\Repositories;

use App\Models\Currency;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class CurrencyRepository
 * @package App\Repositories
 * @version July 2, 2018, 10:34 am UTC
 *
 * @method Currency findWithoutFail($id, $columns = ['*'])
 * @method Currency find($id, $columns = ['*'])
 * @method Currency first($columns = ['*'])
*/
class TraclateBaseRepository extends BaseRepository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'short',
        'symbol',
        'exchange'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Currency::class;
    }

    public function update(array $attributes, $id){
        $result = parent::update($attributes, $id);

        $this->translate($attributes['translate'], $result);
        return $result;
    }

    public function create(array $attributes){
        $result = parent::create($attributes);

        if(isset($attributes['translate'])){
            $this->translate($attributes['translate'], $result);
        }

        return $result;
    }

    public function translate($translates, $item){
        foreach ($translates as $lang_id=>$translate){
            if(isset($translate) AND $translate != ''){
                $item->setTranslate($lang_id, $translate);
            }
        }
    }
}
