<?php

namespace App\Repositories;

use App\Models\TradingPlatformRoutes;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class TradingPlatformRoutesRepository
 * @package App\Repositories
 * @version September 27, 2018, 5:59 pm UTC
 *
 * @method TradingPlatformRoutes findWithoutFail($id, $columns = ['*'])
 * @method TradingPlatformRoutes find($id, $columns = ['*'])
 * @method TradingPlatformRoutes first($columns = ['*'])
*/
class TradingPlatformRoutesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'trading_platform_id',
        'route_id',
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TradingPlatformRoutes::class;
    }
}
