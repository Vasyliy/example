<?php

namespace App\Repositories;

use App\Models\RoutePrice;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class RoutePriceRepository
 * @package App\Repositories
 * @version July 11, 2018, 2:34 pm UTC
 *
 * @method RoutePrice findWithoutFail($id, $columns = ['*'])
 * @method RoutePrice find($id, $columns = ['*'])
 * @method RoutePrice first($columns = ['*'])
*/
class RoutePriceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'route_id',
        'from_point_id',
        'to_point_id',
        'price'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return RoutePrice::class;
    }
}
