<?php

namespace App\Repositories;

use App\Models\AgentTradingPlatform;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class AgentTradingPlatformRepository
 * @package App\Repositories
 * @version September 28, 2018, 2:17 pm UTC
 *
 * @method AgentTradingPlatform findWithoutFail($id, $columns = ['*'])
 * @method AgentTradingPlatform find($id, $columns = ['*'])
 * @method AgentTradingPlatform first($columns = ['*'])
*/
class AgentTradingPlatformRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'agent_id',
        'trading_platform_id',
        'options'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return AgentTradingPlatform::class;
    }
}
