<?php

namespace App\Repositories;

use App\Models\Returns;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ReturnsRepository
 * @package App\Repositories
 * @version August 21, 2018, 10:47 am UTC
 *
 * @method Returns findWithoutFail($id, $columns = ['*'])
 * @method Returns find($id, $columns = ['*'])
 * @method Returns first($columns = ['*'])
*/
class ReturnsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'interval',
        'factor'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Returns::class;
    }
}
