<?php

namespace App\Repositories;

use App\Models\TicketType;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class TicketTypeRepository
 * @package App\Repositories
 * @version July 4, 2018, 12:30 pm UTC
 *
 * @method TicketType findWithoutFail($id, $columns = ['*'])
 * @method TicketType find($id, $columns = ['*'])
 * @method TicketType first($columns = ['*'])
*/
class TicketTypeRepository extends TraclateBaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'discount',
        'note'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TicketType::class;
    }
}
