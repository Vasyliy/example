<?php

namespace App\Repositories;

use App\Models\RoutePoint;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class RoutePointRepository
 * @package App\Repositories
 * @version July 9, 2018, 9:49 am UTC
 *
 * @method RoutePoint findWithoutFail($id, $columns = ['*'])
 * @method RoutePoint find($id, $columns = ['*'])
 * @method RoutePoint first($columns = ['*'])
*/
class RoutePointRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'route_id',
        'from_point_id',
        'to_point_id',
        'number',
        'price',
        'distance',
        'from_day',
        'from_time',
        'to_day',
        'to_time'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return RoutePoint::class;
    }
}
