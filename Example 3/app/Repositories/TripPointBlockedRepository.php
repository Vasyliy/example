<?php

namespace App\Repositories;

use App\Models\TripPointBlocked;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class TripPointBlockedRepository
 * @package App\Repositories
 * @version August 8, 2018, 1:02 pm UTC
 *
 * @method TripPointBlocked findWithoutFail($id, $columns = ['*'])
 * @method TripPointBlocked find($id, $columns = ['*'])
 * @method TripPointBlocked first($columns = ['*'])
*/
class TripPointBlockedRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'trip_id',
        'point_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TripPointBlocked::class;
    }
}
