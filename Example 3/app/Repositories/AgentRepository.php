<?php

namespace App\Repositories;

use App\Models\Agent;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class AgentRepository
 * @package App\Repositories
 * @version July 3, 2018, 9:54 am UTC
 *
 * @method Agent findWithoutFail($id, $columns = ['*'])
 * @method Agent find($id, $columns = ['*'])
 * @method Agent first($columns = ['*'])
*/
class AgentRepository extends TraclateBaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'api',
        'note',
        'status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Agent::class;
    }
}
