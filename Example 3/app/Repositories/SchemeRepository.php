<?php

namespace App\Repositories;

use App\Models\Scheme;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class SchemeRepository
 * @package App\Repositories
 * @version July 9, 2018, 10:05 am UTC
 *
 * @method Scheme findWithoutFail($id, $columns = ['*'])
 * @method Scheme find($id, $columns = ['*'])
 * @method Scheme first($columns = ['*'])
*/
class SchemeRepository extends TraclateBaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Scheme::class;
    }
}
