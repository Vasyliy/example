<?php

namespace App\Repositories;

use App\Models\TradingPlatformPoint;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class TradingPlatformPointRepository
 * @package App\Repositories
 * @version September 27, 2018, 5:53 pm UTC
 *
 * @method TradingPlatformPoint findWithoutFail($id, $columns = ['*'])
 * @method TradingPlatformPoint find($id, $columns = ['*'])
 * @method TradingPlatformPoint first($columns = ['*'])
*/
class TradingPlatformPointRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'trading_platform_id',
        'point_id',
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TradingPlatformPoint::class;
    }
}
