<?php

namespace App\Repositories;

use App\Models\Ticket;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class TicketRepository
 * @package App\Repositories
 * @version July 17, 2018, 6:10 am UTC
 *
 * @method Ticket findWithoutFail($id, $columns = ['*'])
 * @method Ticket find($id, $columns = ['*'])
 * @method Ticket first($columns = ['*'])
*/
class TicketRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'trip_id',
        'from_point_id',
        'to_point_id',
        'place_id',
        'client_id',
        'type_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Ticket::class;
    }
}
