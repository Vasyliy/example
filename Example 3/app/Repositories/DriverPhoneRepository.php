<?php

namespace App\Repositories;

use App\Models\DriverPhone;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class DriverPhoneRepository
 * @package App\Repositories
 * @version July 4, 2018, 4:29 pm UTC
 *
 * @method DriverPhone findWithoutFail($id, $columns = ['*'])
 * @method DriverPhone find($id, $columns = ['*'])
 * @method DriverPhone first($columns = ['*'])
*/
class DriverPhoneRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'driver_id',
        'phone'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return DriverPhone::class;
    }
}
