<?php

namespace App\Criteria;

use Carbon\Carbon;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use Illuminate\Http\Request;

/**
 * Class ActivityFiltersCriteria.
 *
 * @package namespace App\Criteria;
 */
class ActivityFiltersCriteria implements CriteriaInterface
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if ($this->request->has('user_id'))
            $model = $model->where('user_id', $this->request->input('user_id'));
        if ($this->request->has('type'))
            $model = $model->where('type', $this->request->input('type'));
        if ($this->request->has('created_at'))
            $model = $model->where('created_at', '>=', Carbon::createFromFormat('d-m-Y', $this->request->input('created_at'))->setTime(0, 0, 0))
                ->where('created_at', '<=', Carbon::createFromFormat('d-m-Y', $this->request->input('created_at'))->setTime(23, 59, 59));

        return $model;
    }
}
