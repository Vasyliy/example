<?php

namespace App\Criteria;

use App\Models\Client;
use App\Models\Trip;
use Carbon\Carbon;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use Illuminate\Http\Request;

/**
 * Class TicketFiltersCriteria.
 *
 * @package namespace App\Criteria;
 */
class TicketFiltersCriteria implements CriteriaInterface
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Apply criteria in query repository
     *
     * @param string $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->where('variant_id', '!=', 2);
        if ($this->request->has('created_at')) {
            if ($this->request->has('created_at_deviation')) {
                $model = $model->where('created_at', '>=', Carbon::createFromFormat('d-m-Y', $this->request->input('created_at'))->setTime(0, 0, 0))
                    ->where('created_at', '<=', Carbon::createFromFormat('d-m-Y', $this->request->input('created_at'))->addDays($this->request->input('created_at_deviation_amount'))->setTime(23, 59, 59));
            } else {
                $model = $model->where('created_at', '>=', Carbon::createFromFormat('d-m-Y', $this->request->input('created_at'))->setTime(0, 0, 0))
                    ->where('created_at', '<=', Carbon::createFromFormat('d-m-Y', $this->request->input('created_at'))->setTime(23, 59, 59));
            }
        }
        if ($this->request->has('date')) {
            if ($this->request->has('date_deviation')) {
                $trips = Trip::where('date', '>=', Carbon::createFromFormat('d-m-Y', $this->request->input('date'))->setTime(0, 0, 0))
                    ->where('date', '<=', Carbon::createFromFormat('d-m-Y', $this->request->input('date'))->addDays($this->request->input('date_deviation_amount'))->setTime(23, 59, 59))
                    ->pluck('id');
            } else {
                $trips = Trip::where('date', '>=', Carbon::createFromFormat('d-m-Y', $this->request->input('date'))->setTime(0, 0, 0))
                    ->where('date', '<=', Carbon::createFromFormat('d-m-Y', $this->request->input('date'))->setTime(23, 59, 59))
                    ->pluck('id');
            }
            $model = $model->whereIn('trip_id', $trips);
        }
        if ($this->request->has('route_id')) {
            $trips = Trip::where('route_id', $this->request->input('route_id'))
                ->pluck('id');
            $model = $model->whereIn('trip_id', $trips);
        }
        if ($this->request->has('name1')) {
            $clients = Client::where('name1', $this->request->input('name1'))
                ->pluck('id');
            $model = $model->whereIn('client_id', $clients);
        }
        if ($this->request->has('phone')) {
            $clients = Client::where('phone', $this->request->input('phone'))
                ->pluck('id');
            $model = $model->whereIn('client_id', $clients);
        }
        return $model;
    }
}
