<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use Illuminate\Http\Request;

/**
 * Class ClientsAutocompliteCriteria.
 *
 * @package namespace App\Criteria;
 */
class ClientsAutocompliteCriteria implements CriteriaInterface
{

    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if($this->request->has('name1') AND $this->request->input('name1') != '') {
            $model = $model->where('name1', 'like', '%' .$this->request->input('name1') . '%');
        }
        if($this->request->has('phone') AND $this->request->input('phone') != '') {
            $model = $model->where(function ($query) {
                $query->where('phone', 'like', '%' .$this->request->input('phone') . '%')
                    ->orWhere('phone2', 'like', '%' .$this->request->input('phone') . '%');
            });
        }
        if($this->request->has('email') AND $this->request->input('email') != '') {
            $model = $model->where('email', 'like', '%' .$this->request->input('email') . '%');
        }
        return $model;
    }
}
