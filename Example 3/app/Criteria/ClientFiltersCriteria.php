<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use Illuminate\Http\Request;

/**
 * Class ClientFiltersCriteria.
 *
 * @package namespace App\Criteria;
 */
class ClientFiltersCriteria implements CriteriaInterface
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if ($this->request->has('name1'))
            $model = $model->where('name1', $this->request->input('name1'));
        if ($this->request->has('phone'))
            $model = $model->where('phone', $this->request->input('phone'));

        return $model;
    }
}
