<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use Illuminate\Http\Request;
use Carbon\Carbon;

/**
 * Class TripDateFiltersCriteria.
 *
 * @package namespace App\Criteria;
 */
class TripDateFiltersCriteria implements CriteriaInterface
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if($this->request->has('date')) {
            if($this->request->has('deviation')){
                $model = $model->where('date', '>=', Carbon::createFromFormat('d-m-Y', $this->request->input('date'))->setTime(0, 0, 0))
                    ->where('date', '<=', Carbon::createFromFormat('d-m-Y', $this->request->input('date'))->addDays($this->request->input('deviation_amount'))->setTime(23, 59, 59));
            }else{
                $model = $model->where('date', '>=', Carbon::createFromFormat('d-m-Y', $this->request->input('date'))->setTime(0, 0, 0))
                    ->where('date', '<=', Carbon::createFromFormat('d-m-Y', $this->request->input('date'))->setTime(23, 59, 59));
            }
        } else {
            $model = $model->where('date', '>=', Carbon::now()->setTime(0, 0, 0));
        }
        return $model;
    }
}
