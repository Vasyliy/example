<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use Illuminate\Http\Request;
use Carbon\Carbon;

/**
 * Class TripFiltersCriteria.
 *
 * @package namespace App\Criteria;
 */
class TripFiltersCriteria implements CriteriaInterface
{

    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if($this->request->has('route_id')){
            $model = $model->where('route_id', '=', $this->request->input('route_id') );
        }
        return $model;
    }
}
