<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTradingPlatformPointRequest;
use App\Http\Requests\UpdateTradingPlatformPointRequest;
use App\Repositories\TradingPlatformPointRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class TradingPlatformPointController extends AppBaseController
{
    /** @var  TradingPlatformPointRepository */
    private $tradingPlatformPointRepository;

    public function __construct(TradingPlatformPointRepository $tradingPlatformPointRepo)
    {
        $this->tradingPlatformPointRepository = $tradingPlatformPointRepo;
    }

    /**
     * Display a listing of the TradingPlatformPoint.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->tradingPlatformPointRepository->pushCriteria(new RequestCriteria($request));
        $tradingPlatformPoints = $this->tradingPlatformPointRepository->all();

        return view('trading_platform_points.index')
            ->with('tradingPlatformPoints', $tradingPlatformPoints);
    }

    /**
     * Show the form for creating a new TradingPlatformPoint.
     *
     * @return Response
     */
    public function create()
    {
        return view('trading_platform_points.create');
    }

    /**
     * Store a newly created TradingPlatformPoint in storage.
     *
     * @param CreateTradingPlatformPointRequest $request
     *
     * @return Response
     */
    public function store(CreateTradingPlatformPointRequest $request)
    {
        $input = $request->all();

        $tradingPlatformPoint = $this->tradingPlatformPointRepository->create($input);

        Flash::success('Станция успешно сохранена.');

        return redirect(route('tradingPlatforms.edit', $input['trading_platform_id']));
    }

    /**
     * Display the specified TradingPlatformPoint.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $tradingPlatformPoint = $this->tradingPlatformPointRepository->findWithoutFail($id);

        if (empty($tradingPlatformPoint)) {
            Flash::error('Trading Platform Point not found');

            return redirect(route('tradingPlatformPoints.index'));
        }

        return view('trading_platform_points.show')->with('tradingPlatformPoint', $tradingPlatformPoint);
    }

    /**
     * Show the form for editing the specified TradingPlatformPoint.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $tradingPlatformPoint = $this->tradingPlatformPointRepository->findWithoutFail($id);

        if (empty($tradingPlatformPoint)) {
            Flash::error('Trading Platform Point not found');

            return redirect(route('tradingPlatformPoints.index'));
        }

        return view('trading_platform_points.edit')->with('tradingPlatformPoint', $tradingPlatformPoint);
    }

    /**
     * Update the specified TradingPlatformPoint in storage.
     *
     * @param  int              $id
     * @param UpdateTradingPlatformPointRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTradingPlatformPointRequest $request)
    {
        $tradingPlatformPoint = $this->tradingPlatformPointRepository->findWithoutFail($id);

        if (empty($tradingPlatformPoint)) {
            Flash::error('Trading Platform Point not found');

            return redirect(route('tradingPlatformPoints.index'));
        }

        $tradingPlatformPoint = $this->tradingPlatformPointRepository->update($request->all(), $id);

        Flash::success('Trading Platform Point updated successfully.');

        return redirect(route('tradingPlatforms.edit', $request->input('trading_platform_id')));
    }

    /**
     * Remove the specified TradingPlatformPoint from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $tradingPlatformPoint = $this->tradingPlatformPointRepository->findWithoutFail($id);

        if (empty($tradingPlatformPoint)) {
            Flash::error('Trading Platform Point not found');

            return redirect(route('tradingPlatformPoints.index'));
        }

        $this->tradingPlatformPointRepository->delete($id);

        Flash::success('Станция успешно удалена.');

        return redirect(route('tradingPlatforms.edit', $tradingPlatformPoint->trading_platform_id));
    }
}
