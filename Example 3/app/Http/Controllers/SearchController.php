<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Search;
use Carbon\Carbon;

class SearchController extends Controller
{
    public function index(Request $request)
    {
        $input = $request->all();

        $trips = null;
        $from = 0; $to = 0;
        $search = new Search;
        if(isset($input['from'])){
            if($request->has('deviation')){
                $date_min = (new Carbon($input['date']))->setTime(0,0,0);
                $date_max = (new Carbon($input['date']))->addDays($input['deviation_amount'])->setTime(23,59,59);
            }else{
                $date_min = (new Carbon($input['date']))->setTime(0,0,0);
                $date_max = (new Carbon($input['date']))->setTime(23,59,59);
            }

            $trips = $search->getTrips($input['from'], $input['to'], $date_min, $date_max);
            $from = $input['from'];
            $to = $input['to'];
        }

        return view('search.index')
            ->with('search', $search)
            ->with('trips', $trips)
            ->with('from', $from)
            ->with('to', $to)
            ;
    }

}
