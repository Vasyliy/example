<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateAgentTradingPlatformRequest;
use App\Http\Requests\UpdateAgentTradingPlatformRequest;
use App\Repositories\AgentTradingPlatformRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class AgentTradingPlatformController extends AppBaseController
{
    /** @var  AgentTradingPlatformRepository */
    private $agentTradingPlatformRepository;

    public function __construct(AgentTradingPlatformRepository $agentTradingPlatformRepo)
    {
        $this->agentTradingPlatformRepository = $agentTradingPlatformRepo;
    }

    /**
     * Display a listing of the AgentTradingPlatform.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->agentTradingPlatformRepository->pushCriteria(new RequestCriteria($request));
        $agentTradingPlatforms = $this->agentTradingPlatformRepository->paginate(20);

        return view('agent_trading_platforms.index')
            ->with('agentTradingPlatforms', $agentTradingPlatforms);
    }

    /**
     * Show the form for creating a new AgentTradingPlatform.
     *
     * @return Response
     */
    public function create()
    {
        return view('agent_trading_platforms.create');
    }

    /**
     * Store a newly created AgentTradingPlatform in storage.
     *
     * @param CreateAgentTradingPlatformRequest $request
     *
     * @return Response
     */
    public function store(CreateAgentTradingPlatformRequest $request)
    {
        $input = $request->all();

        $agentTradingPlatform = $this->agentTradingPlatformRepository->create($input);

        Flash::success('Настройки успешно сохранены.');

        return redirect(route('agentTradingPlatforms.edit', $agentTradingPlatform->id));
    }

    /**
     * Display the specified AgentTradingPlatform.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $agentTradingPlatform = $this->agentTradingPlatformRepository->findWithoutFail($id);

        if (empty($agentTradingPlatform)) {
            Flash::error('Agent Trading Platform not found');

            return redirect(route('agentTradingPlatforms.index'));
        }

        return view('agent_trading_platforms.show')->with('agentTradingPlatform', $agentTradingPlatform);
    }

    /**
     * Show the form for editing the specified AgentTradingPlatform.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $agentTradingPlatform = $this->agentTradingPlatformRepository->findWithoutFail($id);

        if (empty($agentTradingPlatform)) {
            Flash::error('Agent Trading Platform not found');

            return redirect(route('agentTradingPlatforms.index'));
        }

        return view('agent_trading_platforms.edit')->with('agentTradingPlatform', $agentTradingPlatform);
    }

    /**
     * Update the specified AgentTradingPlatform in storage.
     *
     * @param  int              $id
     * @param UpdateAgentTradingPlatformRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAgentTradingPlatformRequest $request)
    {
        $agentTradingPlatform = $this->agentTradingPlatformRepository->findWithoutFail($id);

        if (empty($agentTradingPlatform)) {
            Flash::error('Agent Trading Platform not found');

            return redirect(route('agentTradingPlatforms.index'));
        }

        $input = $request->all();
        if(isset($input['options']))
            $input['options'] = json_encode($input['options']);

        $agentTradingPlatform = $this->agentTradingPlatformRepository->update($input, $id);

        Flash::success('Настройки успешно сохранены.');

        return redirect(route('agentTradingPlatforms.edit', $id));
    }

    /**
     * Remove the specified AgentTradingPlatform from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $agentTradingPlatform = $this->agentTradingPlatformRepository->findWithoutFail($id);

        if (empty($agentTradingPlatform)) {
            Flash::error('Agent Trading Platform not found');

            return redirect(route('agentTradingPlatforms.index'));
        }

        $this->agentTradingPlatformRepository->delete($id);

        Flash::success('Agent Trading Platform deleted successfully.');

        return redirect(route('agentTradingPlatforms.index'));
    }
}
