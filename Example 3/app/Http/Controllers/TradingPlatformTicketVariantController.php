<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTradingPlatformTicketVariantRequest;
use App\Http\Requests\UpdateTradingPlatformTicketVariantRequest;
use App\Models\TradingPlatformTicketVariant;
use App\Repositories\TradingPlatformTicketVariantRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class TradingPlatformTicketVariantController extends AppBaseController
{
    /** @var  TradingPlatformTicketVariantRepository */
    private $tradingPlatformTicketVariantRepository;

    public function __construct(TradingPlatformTicketVariantRepository $tradingPlatformTicketVariantRepo)
    {
        $this->tradingPlatformTicketVariantRepository = $tradingPlatformTicketVariantRepo;
    }

    /**
     * Display a listing of the TradingPlatformTicketVariant.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->tradingPlatformTicketVariantRepository->pushCriteria(new RequestCriteria($request));
        $tradingPlatformTicketVariants = $this->tradingPlatformTicketVariantRepository->all();

        return view('trading_platform_ticket_variants.index')
            ->with('tradingPlatformTicketVariants', $tradingPlatformTicketVariants);
    }

    /**
     * Show the form for creating a new TradingPlatformTicketVariant.
     *
     * @return Response
     */
    public function create()
    {
        return view('trading_platform_ticket_variants.create');
    }

    /**
     * Store a newly created TradingPlatformTicketVariant in storage.
     *
     * @param CreateTradingPlatformTicketVariantRequest $request
     *
     * @return Response
     */
    public function store(CreateTradingPlatformTicketVariantRequest $request)
    {
        $input = $request->all();

        $tradingPlatformTicketVariant = $this->tradingPlatformTicketVariantRepository->create($input);

        Flash::success('Trading Platform Ticket Variant saved successfully.');

        return redirect(route('tradingPlatformTicketVariants.index'));
    }

    /**
     * Display the specified TradingPlatformTicketVariant.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $tradingPlatformTicketVariant = $this->tradingPlatformTicketVariantRepository->findWithoutFail($id);

        if (empty($tradingPlatformTicketVariant)) {
            Flash::error('Trading Platform Ticket Variant not found');

            return redirect(route('tradingPlatformTicketVariants.index'));
        }

        return view('trading_platform_ticket_variants.show')->with('tradingPlatformTicketVariant', $tradingPlatformTicketVariant);
    }

    /**
     * Show the form for editing the specified TradingPlatformTicketVariant.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $tradingPlatformTicketVariant = $this->tradingPlatformTicketVariantRepository->findWithoutFail($id);

        if (empty($tradingPlatformTicketVariant)) {
            Flash::error('Trading Platform Ticket Variant not found');

            return redirect(route('tradingPlatformTicketVariants.index'));
        }

        return view('trading_platform_ticket_variants.edit')->with('tradingPlatformTicketVariant', $tradingPlatformTicketVariant);
    }

    /**
     * Update the specified TradingPlatformTicketVariant in storage.
     *
     * @param  int              $id
     * @param Request $request
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $input = $request->all();

        foreach ($input['variants'] as $key => $variant) {
            if (isset($variant))
                TradingPlatformTicketVariant::updateOrCreate(
                    ['trading_platform_id' => $id, 'variant_id' => $key],
                    ['name' => $variant]
                );
        }

        Flash::success('Статусы билетов успешно сохранены.');

        return redirect(route('tradingPlatforms.edit', $id));
    }

    /**
     * Remove the specified TradingPlatformTicketVariant from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $tradingPlatformTicketVariant = $this->tradingPlatformTicketVariantRepository->findWithoutFail($id);

        if (empty($tradingPlatformTicketVariant)) {
            Flash::error('Trading Platform Ticket Variant not found');

            return redirect(route('tradingPlatformTicketVariants.index'));
        }

        $this->tradingPlatformTicketVariantRepository->delete($id);

        Flash::success('Trading Platform Ticket Variant deleted successfully.');

        return redirect(route('tradingPlatformTicketVariants.index'));
    }
}
