<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateRoutePointRequest;
use App\Http\Requests\UpdateRoutePointRequest;
use App\Models\RoutePoint;
use App\Repositories\RoutePointRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class RoutePointController extends AppBaseController
{
    /** @var  RoutePointRepository */
    private $routePointRepository;

    public function __construct(RoutePointRepository $routePointRepo)
    {
        $this->routePointRepository = $routePointRepo;
    }

    /**
     * Display a listing of the RoutePoint.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->routePointRepository->pushCriteria(new RequestCriteria($request));
        $routePoints = $this->routePointRepository->all();

        return view('route_points.index')
            ->with('routePoints', $routePoints);
    }

    /**
     * Show the form for creating a new RoutePoint.
     *
     * @return Response
     */
    public function create()
    {
        return view('route_points.create');
    }

    /**
     * Store a newly created RoutePoint in storage.
     *
     * @param CreateRoutePointRequest $request
     *
     * @return Response
     */
    public function store(CreateRoutePointRequest $request)
    {
        $input = $request->all();
        $input['from_time'] = RoutePoint::getOffsetTimeInSeconds($input['route_id'], $input['from_time']);
        $input['to_time'] = RoutePoint::getOffsetTimeInSeconds($input['route_id'], $input['to_time']);

        $routePoint = $this->routePointRepository->create($input);

        Flash::success('Route Point saved successfully.');

        return redirect(route('routes.edit', [$routePoint->route_id]));
    }

    /**
     * Display the specified RoutePoint.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $routePoint = $this->routePointRepository->findWithoutFail($id);

        if (empty($routePoint)) {
            Flash::error('Route Point not found');

            return redirect(route('routePoints.index'));
        }

        return view('route_points.show')->with('routePoint', $routePoint);
    }

    /**
     * Show the form for editing the specified RoutePoint.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $routePoint = $this->routePointRepository->findWithoutFail($id);

        if (empty($routePoint)) {
            Flash::error('Route Point not found');

            return redirect(route('routePoints.index'));
        }

        return view('route_points.edit')->with('routePoint', $routePoint);
    }

    /**
     * Update the specified RoutePoint in storage.
     *
     * @param  int              $id
     * @param UpdateRoutePointRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRoutePointRequest $request)
    {
        $routePoint = $this->routePointRepository->findWithoutFail($id);

        if (empty($routePoint)) {
            Flash::error('Route Point not found');

            return redirect(route('routePoints.index'));
        }

        $input = $request->all();

        $input['from_time'] = RoutePoint::getOffsetTimeInSeconds($routePoint->route_id, $input['from_time']);
        $input['to_time'] = RoutePoint::getOffsetTimeInSeconds($routePoint->route_id, $input['to_time']);

        $routePoint = $this->routePointRepository->update($input, $id);

        Flash::success('Route Point updated successfully.');

        return redirect(route('routes.edit', [$routePoint->route_id]));
    }

    /**
     * Remove the specified RoutePoint from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $routePoint = $this->routePointRepository->findWithoutFail($id);

        if (empty($routePoint)) {
            Flash::error('Route Point not found');

            return redirect(route('routePoints.index'));
        }

        $route_id = $routePoint->route_id;

        $this->routePointRepository->delete($id);

        Flash::success('Route Point deleted successfully.');

        return redirect(route('routes.edit', [$route_id]));
    }
}
