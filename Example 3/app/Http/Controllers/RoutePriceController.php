<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateRoutePriceRequest;
use App\Http\Requests\UpdateRoutePriceRequest;
use App\Models\Currency;
use App\Models\RoutePrice;
use App\Repositories\RoutePriceRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\Validator;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class RoutePriceController extends AppBaseController
{
    /** @var  RoutePriceRepository */
    private $routePriceRepository;

    public function __construct(RoutePriceRepository $routePriceRepo)
    {
        $this->routePriceRepository = $routePriceRepo;
    }

    /**
     * Display a listing of the RoutePrice.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->routePriceRepository->pushCriteria(new RequestCriteria($request));
        $routePrices = $this->routePriceRepository->all();

        return view('route_prices.index')
            ->with('routePrices', $routePrices);
    }

    /**
     * Show the form for creating a new RoutePrice.
     *
     * @return Response
     */
    public function create()
    {
        return view('route_prices.create');
    }

    /**
     * Store a newly created RoutePrice in storage.
     *
     * @param CreateRoutePriceRequest $request
     *
     * @return Response
     */
    public function store(CreateRoutePriceRequest $request)
    {
        $input = $request->all();

        $routePrice = $this->routePriceRepository->create($input);

        Flash::success('Route Price saved successfully.');

        return redirect(route('routes.edit', [$routePrice->route_id]));
    }

    /**
     * Display the specified RoutePrice.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $routePrice = $this->routePriceRepository->findWithoutFail($id);

        if (empty($routePrice)) {
            Flash::error('Route Price not found');

            return redirect(route('routePrices.index'));
        }

        return view('route_prices.show')->with('routePrice', $routePrice);
    }

    /**
     * Show the form for editing the specified RoutePrice.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $routePrice = $this->routePriceRepository->findWithoutFail($id);

        if (empty($routePrice)) {
            Flash::error('Route Price not found');

            return redirect(route('routePrices.index'));
        }

        return view('route_prices.edit')->with('routePrice', $routePrice);
    }

    /**
     * Update the specified RoutePrice in storage.
     *
     * @param  int              $id
     * @param UpdateRoutePriceRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRoutePriceRequest $request)
    {
        $routePrice = $this->routePriceRepository->findWithoutFail($id);

        if (empty($routePrice)) {
            Flash::error('Route Price not found');

            return redirect(route('routePrices.index'));
        }

        $routePrice = $this->routePriceRepository->update($request->all(), $id);

        Flash::success('Route Price updated successfully.');

        return redirect(route('routePrices.index'));
    }

    /**
     * Remove the specified RoutePrice from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $routePrice = $this->routePriceRepository->findWithoutFail($id);

        if (empty($routePrice)) {
            Flash::error('Route Price not found');

            return redirect(route('routePrices.index'));
        }

        $this->routePriceRepository->delete($id);

        Flash::success('Route Price deleted successfully.');

        return redirect(route('routePrices.index'));
    }

    public function save(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'value' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return json_encode([
                'success'=> false,
                'msg'=> 'Введите цену отрезка'
            ]);
        }

        $params = explode('_', $input['pk']);

        $route_id = $params[0];
        $from_id = $params[1];
        $to_id = $params[2];
        $currency_id = $params[3];

        $routePrice = RoutePrice::where([
            'route_id' => $route_id,
            'from_point_id' => $from_id,
            'to_point_id' => $to_id
        ])->first();

        if(!isset($routePrice)) {
            $routePrice = new RoutePrice();
            $routePrice->route_id = $route_id;
            $routePrice->from_point_id = $from_id;
            $routePrice->to_point_id = $to_id;
        }

        $routePrice->price = $input['value'];
        $routePrice->currency_id = $currency_id;
        if($routePrice->save()){
            return json_encode(['success'=> true]);
        }else{
            return json_encode([
                'success'=> false,
                'msg'=> 'server error'
            ]);
        }
    }

    public function saveCurrency(Request $request)
    {
        $input = $request->all();

        $params = explode('_', $input['pk']);

        $route_id = $params[0];
        $from_id = $params[1];
        $to_id = $params[2];

        $routePrice = RoutePrice::where([
            'route_id' => $route_id,
            'from_point_id' => $from_id,
            'to_point_id' => $to_id
        ])->first();

        if(isset($routePrice)) {
            $routePrice->currency_id = $input['value'];
            if($routePrice->save()){
                return json_encode(['success'=> true]);
            }
        }else{
            return json_encode([
                'success'=> false,
                'msg'=> 'server error'
            ]);
        }
    }
}
