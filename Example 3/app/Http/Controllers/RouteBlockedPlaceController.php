<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateRouteBlockedPlaceRequest;
use App\Http\Requests\UpdateRouteBlockedPlaceRequest;
use App\Models\RouteBlockedPlace;
use App\Repositories\RouteBlockedPlaceRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class RouteBlockedPlaceController extends AppBaseController
{
    /** @var  RouteBlockedPlaceRepository */
    private $routeBlockedPlaceRepository;

    public function __construct(RouteBlockedPlaceRepository $routeBlockedPlaceRepo)
    {
        $this->routeBlockedPlaceRepository = $routeBlockedPlaceRepo;
    }

    /**
     * Display a listing of the RouteBlockedPlace.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->routeBlockedPlaceRepository->pushCriteria(new RequestCriteria($request));
        $routeBlockedPlaces = $this->routeBlockedPlaceRepository->all();

        return view('route_blocked_places.index')
            ->with('routeBlockedPlaces', $routeBlockedPlaces);
    }

    /**
     * Show the form for creating a new RouteBlockedPlace.
     *
     * @return Response
     */
    public function create()
    {
        return view('route_blocked_places.create');
    }

    /**
     * Store a newly created RouteBlockedPlace in storage.
     *
     * @param CreateRouteBlockedPlaceRequest $request
     *
     * @return Response
     */
    public function store(CreateRouteBlockedPlaceRequest $request)
    {
        $input = $request->all();

        $routeBlockedPlace = $this->routeBlockedPlaceRepository->create($input);

        Flash::success('Route Blocked Place saved successfully.');

        return redirect(route('routeBlockedPlaces.index'));
    }

    /**
     * Display the specified RouteBlockedPlace.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $routeBlockedPlace = $this->routeBlockedPlaceRepository->findWithoutFail($id);

        if (empty($routeBlockedPlace)) {
            Flash::error('Route Blocked Place not found');

            return redirect(route('routeBlockedPlaces.index'));
        }

        return view('route_blocked_places.show')->with('routeBlockedPlace', $routeBlockedPlace);
    }

    /**
     * Show the form for editing the specified RouteBlockedPlace.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $routeBlockedPlace = $this->routeBlockedPlaceRepository->findWithoutFail($id);

        if (empty($routeBlockedPlace)) {
            Flash::error('Route Blocked Place not found');

            return redirect(route('routeBlockedPlaces.index'));
        }

        return view('route_blocked_places.edit')->with('routeBlockedPlace', $routeBlockedPlace);
    }

    /**
     * Update the specified RouteBlockedPlace in storage.
     *
     * @param  int              $id
     * @param UpdateRouteBlockedPlaceRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRouteBlockedPlaceRequest $request)
    {
        $places = $request->places;
        RouteBlockedPlace::where('route_id', $id)->whereNotIn('place_id', $places)->delete();
        foreach ($places as $place) {
            $blockedPlace = RouteBlockedPlace::where('route_id', $id)->where('place_id', $place)->first();
            if(!isset($blockedPlace)) {
                RouteBlockedPlace::Create(
                    ['route_id' => $id, 'place_id' => $place]
                );
            }
        }

        Flash::success('Блокированные места успешно сохранены.');

        return redirect(route('routes.edit', [$id]));
    }

    /**
     * Remove the specified RouteBlockedPlace from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $routeBlockedPlace = $this->routeBlockedPlaceRepository->findWithoutFail($id);

        if (empty($routeBlockedPlace)) {
            Flash::error('Route Blocked Place not found');

            return redirect(route('routeBlockedPlaces.index'));
        }

        $this->routeBlockedPlaceRepository->delete($id);

        Flash::success('Route Blocked Place deleted successfully.');

        return redirect(route('routeBlockedPlaces.index'));
    }
}
