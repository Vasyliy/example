<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTradingPlatformRoutesRequest;
use App\Http\Requests\UpdateTradingPlatformRoutesRequest;
use App\Repositories\TradingPlatformRoutesRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class TradingPlatformRoutesController extends AppBaseController
{
    /** @var  TradingPlatformRoutesRepository */
    private $tradingPlatformRoutesRepository;

    public function __construct(TradingPlatformRoutesRepository $tradingPlatformRoutesRepo)
    {
        $this->tradingPlatformRoutesRepository = $tradingPlatformRoutesRepo;
    }

    /**
     * Display a listing of the TradingPlatformRoutes.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->tradingPlatformRoutesRepository->pushCriteria(new RequestCriteria($request));
        $tradingPlatformRoutes = $this->tradingPlatformRoutesRepository->all();

        return view('trading_platform_routes.index')
            ->with('tradingPlatformRoutes', $tradingPlatformRoutes);
    }

    /**
     * Show the form for creating a new TradingPlatformRoutes.
     *
     * @return Response
     */
    public function create()
    {
        return view('trading_platform_routes.create');
    }

    /**
     * Store a newly created TradingPlatformRoutes in storage.
     *
     * @param CreateTradingPlatformRoutesRequest $request
     *
     * @return Response
     */
    public function store(CreateTradingPlatformRoutesRequest $request)
    {
        $input = $request->all();

        $tradingPlatformRoutes = $this->tradingPlatformRoutesRepository->create($input);

        Flash::success('Маршрут успешно сохранён.');
        
        return redirect(route('agentTradingPlatforms.edit', $input['agent_trading_platform_id']));
    }

    /**
     * Display the specified TradingPlatformRoutes.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $tradingPlatformRoutes = $this->tradingPlatformRoutesRepository->findWithoutFail($id);

        if (empty($tradingPlatformRoutes)) {
            Flash::error('Trading Platform Routes not found');

            return redirect(route('tradingPlatformRoutes.index'));
        }

        return view('trading_platform_routes.show')->with('tradingPlatformRoutes', $tradingPlatformRoutes);
    }

    /**
     * Show the form for editing the specified TradingPlatformRoutes.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $tradingPlatformRoutes = $this->tradingPlatformRoutesRepository->findWithoutFail($id);

        if (empty($tradingPlatformRoutes)) {
            Flash::error('Trading Platform Routes not found');

            return redirect(route('tradingPlatformRoutes.index'));
        }

        return view('trading_platform_routes.edit')->with('tradingPlatformRoutes', $tradingPlatformRoutes);
    }

    /**
     * Update the specified TradingPlatformRoutes in storage.
     *
     * @param  int              $id
     * @param UpdateTradingPlatformRoutesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTradingPlatformRoutesRequest $request)
    {
        $tradingPlatformRoutes = $this->tradingPlatformRoutesRepository->findWithoutFail($id);

        if (empty($tradingPlatformRoutes)) {
            Flash::error('Trading Platform Routes not found');

            return redirect(route('tradingPlatformRoutes.index'));
        }

        $tradingPlatformRoutes = $this->tradingPlatformRoutesRepository->update($request->all(), $id);

        Flash::success('Trading Platform Routes updated successfully.');

        return redirect(route('tradingPlatformRoutes.index'));
    }

    /**
     * Remove the specified TradingPlatformRoutes from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $tradingPlatformRoutes = $this->tradingPlatformRoutesRepository->findWithoutFail($id);

        if (empty($tradingPlatformRoutes)) {
            Flash::error('Trading Platform Routes not found');

            return redirect(route('tradingPlatformRoutes.index'));
        }

        $this->tradingPlatformRoutesRepository->delete($id);

        Flash::success('Маршрут успешно удалён.');

        return redirect(route('agentTradingPlatforms.edit', $tradingPlatformRoutes->agent_trading_platform_id));
    }
}
