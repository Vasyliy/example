<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTicketTypeRequest;
use App\Http\Requests\UpdateTicketTypeRequest;
use App\Repositories\TicketTypeRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class TicketTypeController extends AppBaseController
{
    /** @var  TicketTypeRepository */
    private $ticketTypeRepository;

    public function __construct(TicketTypeRepository $ticketTypeRepo)
    {
        $this->ticketTypeRepository = $ticketTypeRepo;
    }

    /**
     * Display a listing of the TicketType.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ticketTypeRepository->pushCriteria(new RequestCriteria($request));
        $ticketTypes = $this->ticketTypeRepository->all();

        return view('ticket_types.index')
            ->with('ticketTypes', $ticketTypes);
    }

    /**
     * Show the form for creating a new TicketType.
     *
     * @return Response
     */
    public function create()
    {
        return view('ticket_types.create');
    }

    /**
     * Store a newly created TicketType in storage.
     *
     * @param CreateTicketTypeRequest $request
     *
     * @return Response
     */
    public function store(CreateTicketTypeRequest $request)
    {
        $input = $request->all();

        $ticketType = $this->ticketTypeRepository->create($input);

        Flash::success('Ticket Type saved successfully.');

        return redirect(route('ticketTypes.index'));
    }

    /**
     * Display the specified TicketType.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ticketType = $this->ticketTypeRepository->findWithoutFail($id);

        if (empty($ticketType)) {
            Flash::error('Ticket Type not found');

            return redirect(route('ticketTypes.index'));
        }

        return view('ticket_types.show')->with('ticketType', $ticketType);
    }

    /**
     * Show the form for editing the specified TicketType.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ticketType = $this->ticketTypeRepository->findWithoutFail($id);

        if (empty($ticketType)) {
            Flash::error('Ticket Type not found');

            return redirect(route('ticketTypes.index'));
        }

        return view('ticket_types.edit')->with('ticketType', $ticketType);
    }

    /**
     * Update the specified TicketType in storage.
     *
     * @param  int              $id
     * @param UpdateTicketTypeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTicketTypeRequest $request)
    {
        $ticketType = $this->ticketTypeRepository->findWithoutFail($id);

        if (empty($ticketType)) {
            Flash::error('Ticket Type not found');

            return redirect(route('ticketTypes.index'));
        }

        $ticketType = $this->ticketTypeRepository->update($request->all(), $id);

        Flash::success('Ticket Type updated successfully.');

        return redirect(route('ticketTypes.index'));
    }

    /**
     * Remove the specified TicketType from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ticketType = $this->ticketTypeRepository->findWithoutFail($id);

        if (empty($ticketType)) {
            Flash::error('Ticket Type not found');

            return redirect(route('ticketTypes.index'));
        }

        $this->ticketTypeRepository->delete($id);

        Flash::success('Ticket Type deleted successfully.');

        return redirect(route('ticketTypes.index'));
    }
}
