<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateRouteQuotaRequest;
use App\Http\Requests\UpdateRouteQuotaRequest;
use App\Models\RouteQuota;
use App\Models\Trip;
use App\Models\TripQuota;
use App\Repositories\RouteQuotaRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class RouteQuotaController extends AppBaseController
{
    /** @var  RouteQuotaRepository */
    private $routeQuotaRepository;

    public function __construct(RouteQuotaRepository $routeQuotaRepo)
    {
        $this->routeQuotaRepository = $routeQuotaRepo;
    }

    /**
     * Display a listing of the RouteQuota.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->routeQuotaRepository->pushCriteria(new RequestCriteria($request));
        $routeQuotas = $this->routeQuotaRepository->all();

        return view('route_quotas.index')
            ->with('routeQuotas', $routeQuotas);
    }

    /**
     * Show the form for creating a new RouteQuota.
     *
     * @return Response
     */
    public function create()
    {
        return view('route_quotas.create');
    }

    /**
     * Store a newly created RouteQuota in storage.
     *
     * @param CreateRouteQuotaRequest $request
     *
     * @return Response
     */
    public function store(CreateRouteQuotaRequest $request)
    {
        $input = $request->all();

        //$routeQuota = $this->routeQuotaRepository->create($input);
        RouteQuota::setQuota($input['route_id'], $input['agent_id'], $input['amount']);

        Flash::success('Квота успешно сохранена');

        return redirect(route('routes.edit', $input['route_id']));
    }

    /**
     * Display the specified RouteQuota.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $routeQuota = $this->routeQuotaRepository->findWithoutFail($id);

        if (empty($routeQuota)) {
            Flash::error('Route Quota not found');

            return redirect(route('routeQuotas.index'));
        }

        return view('route_quotas.show')->with('routeQuota', $routeQuota);
    }

    /**
     * Show the form for editing the specified RouteQuota.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $routeQuota = $this->routeQuotaRepository->findWithoutFail($id);

        if (empty($routeQuota)) {
            Flash::error('Route Quota not found');

            return redirect(route('routeQuotas.index'));
        }

        return view('route_quotas.edit')->with('routeQuota', $routeQuota);
    }

    /**
     * Update the specified RouteQuota in storage.
     *
     * @param  int              $id
     * @param UpdateRouteQuotaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRouteQuotaRequest $request)
    {
        $routeQuota = $this->routeQuotaRepository->findWithoutFail($id);

        if (empty($routeQuota)) {
            Flash::error('Route Quota not found');

            return redirect(route('routeQuotas.index'));
        }

        $routeQuota = $this->routeQuotaRepository->update($request->all(), $id);

        Flash::success('Route Quota updated successfully.');

        return redirect(route('routeQuotas.index'));
    }

    /**
     * Remove the specified RouteQuota from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $routeQuota = $this->routeQuotaRepository->findWithoutFail($id);

        if (empty($routeQuota)) {
            Flash::error('Route Quota not found');

            return redirect(route('routeQuotas.index'));
        }

        $this->routeQuotaRepository->delete($id);

        Flash::success('Route Quota deleted successfully.');

        return redirect(route('routeQuotas.index'));
    }
}
