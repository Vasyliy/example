<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateReturnsRequest;
use App\Http\Requests\UpdateReturnsRequest;
use App\Repositories\ReturnsRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class ReturnsController extends AppBaseController
{
    /** @var  ReturnsRepository */
    private $returnsRepository;

    public function __construct(ReturnsRepository $returnsRepo)
    {
        $this->returnsRepository = $returnsRepo;
    }

    /**
     * Display a listing of the Returns.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->returnsRepository->pushCriteria(new RequestCriteria($request));
        $returns = $this->returnsRepository->all();

        return view('returns.index')
            ->with('returns', $returns);
    }

    /**
     * Show the form for creating a new Returns.
     *
     * @return Response
     */
    public function create()
    {
        return view('returns.create');
    }

    /**
     * Store a newly created Returns in storage.
     *
     * @param CreateReturnsRequest $request
     *
     * @return Response
     */
    public function store(CreateReturnsRequest $request)
    {
        $input = $request->all();

        $returns = $this->returnsRepository->create($input);

        Flash::success('Returns saved successfully.');

        return redirect(route('returns.index'));
    }

    /**
     * Display the specified Returns.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $returns = $this->returnsRepository->findWithoutFail($id);

        if (empty($returns)) {
            Flash::error('Returns not found');

            return redirect(route('returns.index'));
        }

        return view('returns.show')->with('returns', $returns);
    }

    /**
     * Show the form for editing the specified Returns.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $returns = $this->returnsRepository->findWithoutFail($id);

        if (empty($returns)) {
            Flash::error('Returns not found');

            return redirect(route('returns.index'));
        }

        return view('returns.edit')->with('returns', $returns);
    }

    /**
     * Update the specified Returns in storage.
     *
     * @param  int              $id
     * @param UpdateReturnsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateReturnsRequest $request)
    {
        $returns = $this->returnsRepository->findWithoutFail($id);

        if (empty($returns)) {
            Flash::error('Returns not found');

            return redirect(route('returns.index'));
        }

        $returns = $this->returnsRepository->update($request->all(), $id);

        Flash::success('Returns updated successfully.');

        return redirect(route('returns.index'));
    }

    /**
     * Remove the specified Returns from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $returns = $this->returnsRepository->findWithoutFail($id);

        if (empty($returns)) {
            Flash::error('Returns not found');

            return redirect(route('returns.index'));
        }

        $this->returnsRepository->delete($id);

        Flash::success('Returns deleted successfully.');

        return redirect(route('returns.index'));
    }
}
