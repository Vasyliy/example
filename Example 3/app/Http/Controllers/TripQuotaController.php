<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTripQuotaRequest;
use App\Http\Requests\UpdateTripQuotaRequest;
use App\Models\TripQuota;
use App\Repositories\TripQuotaRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class TripQuotaController extends AppBaseController
{
    /** @var  TripQuotaRepository */
    private $tripQuotaRepository;

    public function __construct(TripQuotaRepository $tripQuotaRepo)
    {
        $this->tripQuotaRepository = $tripQuotaRepo;
    }

    /**
     * Display a listing of the TripQuota.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->tripQuotaRepository->pushCriteria(new RequestCriteria($request));
        $tripQuotas = $this->tripQuotaRepository->all();

        return view('trip_quotas.index')
            ->with('tripQuotas', $tripQuotas);
    }

    /**
     * Show the form for creating a new TripQuota.
     *
     * @return Response
     */
    public function create()
    {
        return view('trip_quotas.create');
    }

    /**
     * Store a newly created TripQuota in storage.
     *
     * @param CreateTripQuotaRequest $request
     *
     * @return Response
     */
    public function store(CreateTripQuotaRequest $request)
    {
        $input = $request->all();

        //$tripQuota = $this->tripQuotaRepository->create($input);
        TripQuota::setQuota($input['trip_id'], $input['agent_id'], $input['amount']);

        Flash::success('Квота успешно сохранена');

        return redirect(route('trips.edit', $input['trip_id']));
    }

    /**
     * Display the specified TripQuota.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $tripQuota = $this->tripQuotaRepository->findWithoutFail($id);

        if (empty($tripQuota)) {
            Flash::error('Trip Quota not found');

            return redirect(route('tripQuotas.index'));
        }

        return view('trip_quotas.show')->with('tripQuota', $tripQuota);
    }

    /**
     * Show the form for editing the specified TripQuota.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $tripQuota = $this->tripQuotaRepository->findWithoutFail($id);

        if (empty($tripQuota)) {
            Flash::error('Trip Quota not found');

            return redirect(route('tripQuotas.index'));
        }

        return view('trip_quotas.edit')->with('tripQuota', $tripQuota);
    }

    /**
     * Update the specified TripQuota in storage.
     *
     * @param  int              $id
     * @param UpdateTripQuotaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTripQuotaRequest $request)
    {
        $tripQuota = $this->tripQuotaRepository->findWithoutFail($id);

        if (empty($tripQuota)) {
            Flash::error('Trip Quota not found');

            return redirect(route('tripQuotas.index'));
        }

        $tripQuota = $this->tripQuotaRepository->update($request->all(), $id);

        Flash::success('Trip Quota updated successfully.');

        return redirect(route('tripQuotas.index'));
    }

    /**
     * Remove the specified TripQuota from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $tripQuota = $this->tripQuotaRepository->findWithoutFail($id);

        if (empty($tripQuota)) {
            Flash::error('Trip Quota not found');

            return redirect(route('tripQuotas.index'));
        }

        $this->tripQuotaRepository->delete($id);

        Flash::success('Trip Quota deleted successfully.');

        return redirect(route('tripQuotas.index'));
    }
}
