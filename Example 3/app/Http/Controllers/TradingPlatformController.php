<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTradingPlatformRequest;
use App\Http\Requests\UpdateTradingPlatformRequest;
use App\Repositories\TradingPlatformRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class TradingPlatformController extends AppBaseController
{
    /** @var  TradingPlatformRepository */
    private $tradingPlatformRepository;

    public function __construct(TradingPlatformRepository $tradingPlatformRepo)
    {
        $this->tradingPlatformRepository = $tradingPlatformRepo;
    }

    /**
     * Display a listing of the TradingPlatform.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->tradingPlatformRepository->pushCriteria(new RequestCriteria($request));
        $tradingPlatforms = $this->tradingPlatformRepository->all();

        return view('trading_platforms.index')
            ->with('tradingPlatforms', $tradingPlatforms);
    }

    /**
     * Show the form for creating a new TradingPlatform.
     *
     * @return Response
     */
    public function create()
    {
        return view('trading_platforms.create');
    }

    /**
     * Store a newly created TradingPlatform in storage.
     *
     * @param CreateTradingPlatformRequest $request
     *
     * @return Response
     */
    public function store(CreateTradingPlatformRequest $request)
    {
        $input = $request->all();

        $tradingPlatform = $this->tradingPlatformRepository->create($input);

        Flash::success('Площадка продаж успешно создана.');

        return redirect(route('tradingPlatforms.edit', $tradingPlatform->id));
    }

    /**
     * Display the specified TradingPlatform.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $tradingPlatform = $this->tradingPlatformRepository->findWithoutFail($id);

        if (empty($tradingPlatform)) {
            Flash::error('Trading Platform not found');

            return redirect(route('tradingPlatforms.index'));
        }

        return view('trading_platforms.show')->with('tradingPlatform', $tradingPlatform);
    }

    /**
     * Show the form for editing the specified TradingPlatform.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $tradingPlatform = $this->tradingPlatformRepository->findWithoutFail($id);

        if (empty($tradingPlatform)) {
            Flash::error('Trading Platform not found');

            return redirect(route('tradingPlatforms.index'));
        }

        return view('trading_platforms.edit')
            ->with('tradingPlatform', $tradingPlatform)
            ->with('ticketVariants', $tradingPlatform->ticketVariants->pluck('name', 'variant_id'));
    }

    /**
     * Update the specified TradingPlatform in storage.
     *
     * @param  int              $id
     * @param UpdateTradingPlatformRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTradingPlatformRequest $request)
    {
        $tradingPlatform = $this->tradingPlatformRepository->findWithoutFail($id);

        if (empty($tradingPlatform)) {
            Flash::error('Trading Platform not found');

            return redirect(route('tradingPlatforms.index'));
        }

        $input = $request->all();
        if(isset($input['options']))
            $input['options'] = json_encode($input['options']);

        $tradingPlatform = $this->tradingPlatformRepository->update($input, $id);

        Flash::success('Настройки успешно сохранены.');

        return redirect(route('tradingPlatforms.edit', $id));
    }

    /**
     * Remove the specified TradingPlatform from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $tradingPlatform = $this->tradingPlatformRepository->findWithoutFail($id);

        if (empty($tradingPlatform)) {
            Flash::error('Trading Platform not found');

            return redirect(route('tradingPlatforms.index'));
        }

        $this->tradingPlatformRepository->delete($id);

        Flash::success('Плащадка успешно удалена.');

        return redirect(route('tradingPlatforms.index'));
    }
}
