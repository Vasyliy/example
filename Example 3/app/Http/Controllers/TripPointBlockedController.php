<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTripPointBlockedRequest;
use App\Http\Requests\UpdateTripPointBlockedRequest;
use App\Models\TripPointBlocked;
use App\Repositories\TripPointBlockedRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class TripPointBlockedController extends AppBaseController
{
    /** @var  TripPointBlockedRepository */
    private $tripPointBlockedRepository;

    public function __construct(TripPointBlockedRepository $tripPointBlockedRepo)
    {
        $this->tripPointBlockedRepository = $tripPointBlockedRepo;
    }

    /**
     * Display a listing of the TripPointBlocked.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->tripPointBlockedRepository->pushCriteria(new RequestCriteria($request));
        $tripPointBlockeds = $this->tripPointBlockedRepository->all();

        return view('trip_point_blockeds.index')
            ->with('tripPointBlockeds', $tripPointBlockeds);
    }

    /**
     * Show the form for creating a new TripPointBlocked.
     *
     * @return Response
     */
    public function create()
    {
        return view('trip_point_blockeds.create');
    }

    /**
     * Store a newly created TripPointBlocked in storage.
     *
     * @param CreateTripPointBlockedRequest $request
     *
     * @return Response
     */
    public function store(CreateTripPointBlockedRequest $request)
    {
        $input = $request->all();

        $tripPointBlocked = $this->tripPointBlockedRepository->create($input);

        Flash::success('Trip Point Blocked saved successfully.');

        return redirect(route('trips.edit', [$tripPointBlocked->trip_id]));
    }

    /**
     * Display the specified TripPointBlocked.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $tripPointBlocked = $this->tripPointBlockedRepository->findWithoutFail($id);

        if (empty($tripPointBlocked)) {
            Flash::error('Trip Point Blocked not found');

            return redirect(route('tripPointBlockeds.index'));
        }

        return view('trip_point_blockeds.show')->with('tripPointBlocked', $tripPointBlocked);
    }

    /**
     * Show the form for editing the specified TripPointBlocked.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $tripPointBlocked = $this->tripPointBlockedRepository->findWithoutFail($id);

        if (empty($tripPointBlocked)) {
            Flash::error('Trip Point Blocked not found');

            return redirect(route('tripPointBlockeds.index'));
        }

        return view('trip_point_blockeds.edit')->with('tripPointBlocked', $tripPointBlocked);
    }

    /**
     * Update the specified TripPointBlocked in storage.
     *
     * @param  int              $id
     * @param UpdateTripPointBlockedRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTripPointBlockedRequest $request)
    {
        $tripPointBlocked = $this->tripPointBlockedRepository->findWithoutFail($id);

        if (empty($tripPointBlocked)) {
            Flash::error('Trip Point Blocked not found');

            return redirect(route('tripPointBlockeds.index'));
        }

        $tripPointBlocked = $this->tripPointBlockedRepository->update($request->all(), $id);

        Flash::success('Trip Point Blocked updated successfully.');

        return redirect(route('tripPointBlockeds.index'));
    }

    /**
     * Remove the specified TripPointBlocked from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $tripPointBlocked = $this->tripPointBlockedRepository->findWithoutFail($id);

        if (empty($tripPointBlocked)) {
            Flash::error('Точку не возможно разблокировать');

            return redirect(route('tripPointBlockeds.index'));
        }
        $trip_id = $tripPointBlocked->trip_id;
        $this->tripPointBlockedRepository->delete($id);

        Flash::success('Точка разблокирована удачно.');

        return redirect(route('trips.edit', [$trip_id]));
    }

    public function block($trip, $point)
    {
        //$input = $request->all();
        //$tripPointBlocked = $this->tripPointBlockedRepository->create($input);
        $trip_point_block = new TripPointBlocked();
        $trip_point_block->trip_id = $trip;
        $trip_point_block->point_id = $point;
        ;
        if (!$trip_point_block->save()) {
            Flash::error('Точку не возможно заблокировать');

            return redirect(route('trips.edit', [$trip]));
        }
        Flash::success('Точка заблокирована удачно.');

        return redirect(route('trips.edit', [$trip_point_block->trip_id]));
    }
}
