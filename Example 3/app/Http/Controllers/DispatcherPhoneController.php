<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateDispatcherPhoneRequest;
use App\Http\Requests\UpdateDispatcherPhoneRequest;
use App\Repositories\DispatcherPhoneRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class DispatcherPhoneController extends AppBaseController
{
    /** @var  DispatcherPhoneRepository */
    private $dispatcherPhoneRepository;

    public function __construct(DispatcherPhoneRepository $dispatcherPhoneRepo)
    {
        $this->dispatcherPhoneRepository = $dispatcherPhoneRepo;
    }

    /**
     * Display a listing of the DispatcherPhone.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->dispatcherPhoneRepository->pushCriteria(new RequestCriteria($request));
        $dispatcherPhones = $this->dispatcherPhoneRepository->all();

        return view('dispatcher_phones.index')
            ->with('dispatcherPhones', $dispatcherPhones);
    }

    /**
     * Show the form for creating a new DispatcherPhone.
     *
     * @return Response
     */
    public function create()
    {
        return view('dispatcher_phones.create');
    }

    /**
     * Store a newly created DispatcherPhone in storage.
     *
     * @param CreateDispatcherPhoneRequest $request
     *
     * @return Response
     */
    public function store(CreateDispatcherPhoneRequest $request)
    {
        $input = $request->all();

        $dispatcherPhone = $this->dispatcherPhoneRepository->create($input);

        Flash::success('Dispatcher Phone saved successfully.');

        return redirect(route('dispatcherPhones.index'));
    }

    /**
     * Display the specified DispatcherPhone.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $dispatcherPhone = $this->dispatcherPhoneRepository->findWithoutFail($id);

        if (empty($dispatcherPhone)) {
            Flash::error('Dispatcher Phone not found');

            return redirect(route('dispatcherPhones.index'));
        }

        return view('dispatcher_phones.show')->with('dispatcherPhone', $dispatcherPhone);
    }

    /**
     * Show the form for editing the specified DispatcherPhone.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $dispatcherPhone = $this->dispatcherPhoneRepository->findWithoutFail($id);

        if (empty($dispatcherPhone)) {
            Flash::error('Dispatcher Phone not found');

            return redirect(route('dispatcherPhones.index'));
        }

        return view('dispatcher_phones.edit')->with('dispatcherPhone', $dispatcherPhone);
    }

    /**
     * Update the specified DispatcherPhone in storage.
     *
     * @param  int              $id
     * @param UpdateDispatcherPhoneRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDispatcherPhoneRequest $request)
    {
        $dispatcherPhone = $this->dispatcherPhoneRepository->findWithoutFail($id);

        if (empty($dispatcherPhone)) {
            Flash::error('Dispatcher Phone not found');

            return redirect(route('dispatcherPhones.index'));
        }

        $dispatcherPhone = $this->dispatcherPhoneRepository->update($request->all(), $id);

        Flash::success('Dispatcher Phone updated successfully.');

        return redirect(route('dispatcherPhones.index'));
    }

    /**
     * Remove the specified DispatcherPhone from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $dispatcherPhone = $this->dispatcherPhoneRepository->findWithoutFail($id);

        if (empty($dispatcherPhone)) {
            Flash::error('Dispatcher Phone not found');

            return redirect(route('dispatcherPhones.index'));
        }

        $this->dispatcherPhoneRepository->delete($id);

        Flash::success('Dispatcher Phone deleted successfully.');

        return redirect(route('dispatcherPhones.index'));
    }
}
