<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateScheduleRequest;
use App\Http\Requests\UpdateScheduleRequest;
use App\Models\Route;
use App\Repositories\ScheduleRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class ScheduleController extends AppBaseController
{
    /** @var  ScheduleRepository */
    private $scheduleRepository;

    public function __construct(ScheduleRepository $scheduleRepo)
    {
        $this->scheduleRepository = $scheduleRepo;
    }

    /**
     * Display a listing of the Schedule.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->scheduleRepository->pushCriteria(new RequestCriteria($request));
        $schedules = $this->scheduleRepository->all();

        return view('schedules.index')
            ->with('schedules', $schedules);
    }

    /**
     * Show the form for creating a new Schedule.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $route = 0;
        $route_model = null;
        if(isset($request['route'])){
            $route = $request['route'];
            $route_model = Route::find($route);
         }
        return view('schedules.create')
            ->with('route', $route)
            ->with('route_model', $route_model)
            ;
    }

    /**
     * Store a newly created Schedule in storage.
     *
     * @param CreateScheduleRequest $request
     *
     * @return Response
     */
    public function store(CreateScheduleRequest $request)
    {
        $input = $request->all();

        $input['patern'] = implode(',', $input['patern']);
        $schedule = $this->scheduleRepository->create($input);

        Flash::success('Schedule saved successfully.');

        //return redirect(route('schedules.edit', [$schedule->id, 'route'=>$schedule->route_id]));
        return redirect(route('routes.index'));
    }

    /**
     * Display the specified Schedule.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $schedule = $this->scheduleRepository->findWithoutFail($id);

        if (empty($schedule)) {
            Flash::error('Schedule not found');

            return redirect(route('schedules.index'));
        }

        return view('schedules.show')->with('schedule', $schedule);
    }

    /**
     * Show the form for editing the specified Schedule.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $schedule = $this->scheduleRepository->findWithoutFail($id);

        if (empty($schedule)) {
            Flash::error('Schedule not found');

            return redirect(route('schedules.index'));
        }

        return view('schedules.edit')
            ->with('schedule', $schedule)
            ->with('route_model', $schedule->route)
            ->with('route', $schedule->route->id);
    }

    /**
     * Update the specified Schedule in storage.
     *
     * @param  int              $id
     * @param UpdateScheduleRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateScheduleRequest $request)
    {
        $input = $request->all();
        $input['patern'] = implode(',', $input['patern']);

        $schedule = $this->scheduleRepository->findWithoutFail($id);

        if (empty($schedule)) {
            Flash::error('Schedule not found');

            return redirect(route('schedules.index'));
        }

        $schedule = $this->scheduleRepository->update($input, $id);

        Flash::success('Schedule updated successfully.');

        return redirect(route('routes.index'));
    }

    /**
     * Remove the specified Schedule from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $schedule = $this->scheduleRepository->findWithoutFail($id);

        if (empty($schedule)) {
            Flash::error('Schedule not found');

            return redirect(route('schedules.index'));
        }

        $this->scheduleRepository->delete($id);

        Flash::success('Schedule deleted successfully.');

        return redirect(route('schedules.index'));
    }
}
