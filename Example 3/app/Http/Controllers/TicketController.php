<?php

namespace App\Http\Controllers;

use App\Criteria\TicketFiltersCriteria;
use App\Http\Requests\CreateTicketRequest;
use App\Http\Requests\UpdateTicketRequest;
use App\Models\Client;
use App\Models\Point;
use App\Models\Returns;
use App\Models\Settings;
use App\Models\Ticket;
use App\Models\TicketType;
use App\Models\Trip;
use App\Repositories\TicketRepository;
use App\Http\Controllers\AppBaseController;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\Validator;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class TicketController extends AppBaseController
{
    /** @var  TicketRepository */
    private $ticketRepository;

    public function __construct(TicketRepository $ticketRepo)
    {
        $this->ticketRepository = $ticketRepo;
    }

    /**
     * Display a listing of the Ticket.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ticketRepository->pushCriteria(new RequestCriteria($request));
        $this->ticketRepository->pushCriteria(new TicketFiltersCriteria($request));
        $tickets = $this->ticketRepository->orderby('created_at', 'desc')->paginate(20);

        return view('tickets.index')
            ->with('tickets', $tickets);
    }

    /**
     * Show the form for creating a new Ticket.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $input = $request->all();
        $from = 0; $to = 0;

        if(isset($input['trip'])){
            $trip = Trip::find($input['trip']);
            $from = $input['from'];
            $to = $input['to'];
        }

        if(!isset($trip)) $trip = new Trip();

        return view('tickets.create')->with('trip', $trip)
            ->with('from', $from)
            ->with('to', $to)
            ;
    }

    /**
     * Store a newly created Ticket in storage.
     *
     * @param CreateTicketRequest $request
     *
     * @return Response
     */
    public function store(CreateTicketRequest $request)
    {
        $input = $request->all();
        //var_dump($input);
        ///*
        $count = 0;
        foreach ($input['places'] as $place_id){
            //var_dump($place_id);

            if($input['client_id'][$place_id] == 0){
                //var_dump($input['client_id'][$place_id]);
                $client = new Client();
                $client->name1 = $input['name1'][$place_id];
                $client->name2 = $input['name2'][$place_id];
                $client->name3 = $input['name3'][$place_id];
                $client->phone = $input['phone'][$place_id];
                $client->phone2 = $input['phone2'][$place_id];
                $client->email = $input['email'][$place_id];
                if(!$client->save()){
                    print_r($client->getErrrors());
                }
            }else{
                $client = Client::find($input['client_id'][$place_id]);
            }
            if(isset($client)){
                $ticket = new Ticket();
                $ticket_type = TicketType::find($input['type_id'][$place_id]);
                if(!isset($ticket_type)){
                    $ticket_type = TicketType::find(1);
                }
                $ticket->trip_id = $input['trip_id'];
                $ticket->from_point_id = $input['from_point_id'];
                $ticket->to_point_id = $input['to_point_id'];
                $ticket->place_id = $place_id;
                $ticket->client_id = $client->id;
                $ticket->agent_id = $input['agent_id'];
                $ticket->price = $input['price'][$place_id] * ( (100 - $ticket_type->discount) / 100);
                $ticket->currency_id = $input['currency_id'][$place_id];
                $ticket->type_id = $input['type_id'][$place_id];
                $ticket->variant_id = $input['variant_id'];
                $ticket->prepay = $input['prepay'][$place_id];
                $ticket->comment = $input['comment'][$place_id];

                if($ticket->isTicketRealCreated()){
                    if($ticket->save()){
                        $count++;
                    }
                }else{
                    return redirect(route('tickets.create', ['trip'=>$input['trip_id'], 'from'=>$input['from_point_id'], 'to'=>$input['to_point_id']]));
                }

            }

            //$input['place_id'] = $place_id;
            //$ticket = $this->ticketRepository->create($input);
        }

        Flash::success($count.' tickets saved successfully.');

        return redirect(route('tickets.index'));
        //*/
    }

    /**
     * Display the specified Ticket.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ticket = $this->ticketRepository->findWithoutFail($id);

        if (empty($ticket)) {
            Flash::error('Ticket not found');

            return redirect(route('tickets.index'));
        }

        $trip = Trip::find($ticket->trip_id);

        return view('tickets.ticket')
            ->with('ticket', $ticket)
            ->with('trip', $trip)
            ->with('settings', Settings::find(1))
            ->with('returns', Returns::all());
    }

    /**
     * Show the form for editing the specified Ticket.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ticket = $this->ticketRepository->findWithoutFail($id);

        if (empty($ticket)) {
            Flash::error('Ticket not found');

            return redirect(route('tickets.index'));
        }

        $trip = $ticket->trip;

        return view('tickets.edit')->with('ticket', $ticket)->with('trip', $trip)
            ->with('from', $ticket->from_point_id)
            ->with('to', $ticket->to_point_id)
            ;
    }

    /**
     * Update the specified Ticket in storage.
     *
     * @param  int              $id
     * @param UpdateTicketRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTicketRequest $request)
    {
        $ticket = $this->ticketRepository->findWithoutFail($id);

        if (empty($ticket)) {
            Flash::error('Ticket not found');

            return redirect(route('tickets.index'));
        }

        $input = $request->all();

        if($input['client_id'] == 0){
            $client = new Client();
            $client->name1 = $input['name1'];
            $client->name2 = $input['name2'];
            $client->name3 = $input['name3'];
            $client->phone = $input['phone'];
            $client->phone2 = $input['phone2'];
            $client->email = $input['email'];
            $client->save();
            $input['client_id'] = $client->id;
        }

        $newPrice = Ticket::defaultPrice($input['from_point_id'], $input['to_point_id'], $ticket->trip->route_id);
        if ($newPrice) {
            if (Carbon::createFromFormat('d-m-Y H:i', $ticket->trip->date)->format('d-m-Y') != $input['trip_date']) {
                $trip = Trip::where('route_id', $ticket->trip->route_id)
                    ->where('date', '>=', Carbon::createFromFormat('d-m-Y', $input['trip_date'])->setTime(0, 0, 0))
                    ->where('date', '<=', Carbon::createFromFormat('d-m-Y', $input['trip_date'])->setTime(23, 59, 59))
                    ->first();
                if ($trip) {
                    $ticketType = TicketType::find($input['type_id']);
                    $input['price'] = $newPrice * ((100 - $ticketType->discount) / 100);
                    $input['trip_id'] = $trip->id;
                    $ticket = $this->ticketRepository->update($input, $id);

                    Flash::success('Билет успешно обновлён.');
                } else {
                    Flash::error('На ' . $input['trip_date'] . ' рейс не найден.');
                }
            } else {
                $ticketType = TicketType::find($input['type_id']);
                $input['price'] = $newPrice * ((100 - $ticketType->discount) / 100);
                $ticket = $this->ticketRepository->update($input, $id);

                Flash::success('Билет успешно обновлён.');
            }
        } else {
            Flash::error('Не указана цена для интервала: ' . Point::getPointFullName($input['from_point_id']) . ' - ' . Point::getPointFullName($input['to_point_id']));
        }

        return redirect($input['url']);
    }

    /**
     * Remove the specified Ticket from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ticket = $this->ticketRepository->findWithoutFail($id);

        if (empty($ticket)) {
            Flash::error('Ticket not found');

            return redirect(route('tickets.index'));
        }

        $this->ticketRepository->delete($id);

        Flash::success('Билет успешно удалён');

        return back();
    }

    public function view($viewer){
        $ticket = Ticket::where('ticket_viewer', $viewer)->first();
        if (empty($ticket)) {
            Flash::error('Билет не найден');

            return redirect(route('tickets.index'));
        }

        return view('tickets.ticket')
            ->with('ticket', $ticket)
            ->with('trip', $ticket->trip)
            ->with('settings', Settings::find(1))
            ->with('returns', Returns::all());
    }

    /**
     * Validate Ticket Form.
     *
     * @param CreateTicketRequest $request
     *
     * @return Response
     */
    public function checkclients(Request $request)
    {
        $result = [];
        $input = $request->all();
        //var_dump($input);
        ///*
        foreach ($input as $place_num=>$place_client){

            $validator = Validator::make([
                'name1'=>$place_client['name1'],
                'name2'=>$place_client['name2'],
                'name3'=>$place_client['name3'],
                'phone'=>$place_client['phone'],
                'phone2'=>$place_client['phone2'],
                'email'=>$place_client['email']
            ], Client::$rules, Client::$messages, Client::$names);

            if ($validator->fails()) {
                $result[$place_num] = $validator->errors();
            }
        }
        if(empty($result)){
            return json_encode('ok');
        }else{
            Flash::error('Некоторые данные пасажиров неправильно заполнены');
            return json_encode($result);
        }
    }

    public function checkclient(Request $request)
    {
        $result = null;
        $input = $request->all();

        $validator = Validator::make([
            'name1'=>$input['name1'],
            'name2'=>$input['name2'],
            'name3'=>$input['name3'],
            'phone'=>$input['phone'],
            'phone2'=>$input['phone2'],
            'email'=>$input['email']
        ], Client::$rules, Client::$messages, Client::$names);

        if ($validator->fails()) {
            $result = $validator->errors();
        }

        if(empty($result)){
            return json_encode('ok');
        }else{
            return json_encode($result);
        }
    }

    public function checkPlace(Request $request)
    {
        $result = null;
        $input = $request->all();
        $trip = Trip::where('route_id', $input['route_id'])
            ->where('date', '>=', Carbon::createFromFormat('d-m-Y', $input['date'])->setTime(0, 0, 0))
            ->where('date', '<=', Carbon::createFromFormat('d-m-Y', $input['date'])->setTime(23, 59, 59))
            ->first();

        if ($trip->isFreePlace($input['from_point_id'], $input['to_point_id'], $input['place_id'], $input['ticket_id'])) {
            return json_encode('ok');
        } else {
            return json_encode($trip->placesInScheme($input['from_point_id'], $input['to_point_id']));
        }
    }

    public function cancel($id)
    {
        $ticket = $this->ticketRepository->findWithoutFail($id);

        if (empty($ticket)) {
            Flash::error(' Билет не найден');

            return redirect(route('tickets.index'));
        }

        $ticket->variant_id = 3;
        $ticket->save();

        return redirect(route('tickets.index'));
    }
}
