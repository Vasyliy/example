<?php

namespace App\Http\Controllers;

use App\Criteria\TripDateFiltersCriteria;
use App\Criteria\TripFiltersCriteria;
use App\Http\Requests\CreateTripRequest;
use App\Http\Requests\UpdateTripRequest;
use App\Models\Route;
use App\Models\Ticket;
use App\Models\Trip;
use App\Repositories\TripRepository;
use App\Http\Controllers\AppBaseController;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class TripController extends AppBaseController
{
    /** @var  TripRepository */
    private $tripRepository;

    public function __construct(TripRepository $tripRepo)
    {
        $this->tripRepository = $tripRepo;
    }

    /**
     * Display a listing of the Trip.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->tripRepository->pushCriteria(new RequestCriteria($request));
        $this->tripRepository->pushCriteria(new TripFiltersCriteria($request));
        $this->tripRepository->pushCriteria(new TripDateFiltersCriteria($request));
        $trips = $this->tripRepository
            ->paginate(10);

        return view('trips.index')
            ->with('trips', $trips);
        //    */

    }

    /**
     * Show the form for creating a new Trip.
     *
     * @return Response
     */
    public function create()
    {
        return view('trips.create');
    }

    /**
     * Store a newly created Trip in storage.
     *
     * @param CreateTripRequest $request
     *
     * @return Response
     */
    public function store(CreateTripRequest $request)
    {
        $input = $request->all();

        $trip = $this->tripRepository->create($input);

        Flash::success('Trip saved successfully.');

        return redirect(route('trips.index'));
    }

    /**
     * Display the specified Trip.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $trip = $this->tripRepository->findWithoutFail($id);

        if (empty($trip)) {
            Flash::error('Trip not found');

            return redirect(route('trips.index'));
        }

        return view('trips.show')->with('trip', $trip);
    }

    /**
     * Show the form for editing the specified Trip.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $trip = $this->tripRepository->findWithoutFail($id);

        if (empty($trip)) {
            Flash::error('Trip not found');

            return redirect(route('trips.index'));
        }

        return view('trips.edit')->with('trip', $trip);
    }

    /**
     * Update the specified Trip in storage.
     *
     * @param  int $id
     * @param UpdateTripRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTripRequest $request)
    {
        $trip = $this->tripRepository->findWithoutFail($id);

        if (empty($trip)) {
            Flash::error('Trip not found');

            return redirect(route('trips.index'));
        }

        $trip = $this->tripRepository->update($request->all(), $id);

        Flash::success('Trip updated successfully.');

        return redirect(route('trips.index'));
    }

    /**
     * Remove the specified Trip from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $trip = $this->tripRepository->findWithoutFail($id);

        if (empty($trip)) {
            Flash::error('Trip not found');

            return redirect(route('trips.index'));
        }

        $this->tripRepository->delete($id);

        Flash::success('Trip deleted successfully.');

        return redirect(route('trips.index'));
    }

    public function tickets($trip)
    {
        $trip = Trip::find($trip);

        return view('trips.tickets')
            ->with('trip', $trip);
    }

    public function waybill($trip)
    {
        return view('trips.waybill')
            ->with('trip', Trip::find($trip));
    }

    public function generate($route)
    {
        if (Route::generateTrips($route))
            Flash::success('Рейсы были созданы согласно расписания');

        return redirect(route('trips.index'));
    }

    public function updateBlockedPlaces($trip, Request $request)
    {
        $places = $request->places;
        $trip = Trip::find($trip);

        if ($places) {
            $trip->allTickets()->where('variant_id', 2)->whereNotIn('place_id', $places)->delete();

            foreach ($places as $place) {
                $ticket = $trip->tickets->where('place_id', $place)->first();
                if(!isset($ticket)) {
                    Ticket::Create(
                        [
                            'trip_id' => $trip->id,
                            'place_id' => $place,
                            'variant_id' => 2,
                            'agent_id' => 1,
                            'type_id' => 1,
                            'from_point_id' => $trip->route->getFromPoint()->from_point_id,
                            'to_point_id' => $trip->route->getToPoint()->to_point_id,
                        ]
                    );
                }
            }
        } else {
            $trip->allTickets()->where('variant_id', 2)->delete();
        }

        Flash::success('Блокированные места успешно сохранены.');

        return redirect(route('trips.edit', [$trip]));
    }
}
