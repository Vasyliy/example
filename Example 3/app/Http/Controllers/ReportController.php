<?php

namespace App\Http\Controllers;

use App\Models\Agent;
use App\Models\Currency;
use App\Models\Ticket;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->has('created_at')) {
            $from = Carbon::createFromFormat('d-m-Y', $request->input('created_at'))->setTime(0, 0, 0);
            $to = Carbon::createFromFormat('d-m-Y', $request->input('created_at'))->setTime(23, 59, 59);
        } else {
            $from = Carbon::now()->setTime(0, 0, 0);
            $to = Carbon::now()->setTime(23, 59, 59);
        }

        $agents = Agent::select('agents.id', 'agents.name', DB::raw('COUNT(tickets.id) as count'))
            ->join('tickets', 'agents.id', '=', 'tickets.agent_id')
            ->where('tickets.created_at', '>=', $from)
            ->where('tickets.created_at', '<=', $to)
            ->where('tickets.variant_id', '<', 2)
            ->whereNull('tickets.deleted_at')
            ->groupBy('name')
            ->get();
        $currencies = Currency::select('id', 'short')->get();

        $agentsReport = array();
        $generalReport['count'] = 0;
        $generalReport['sum'] = array();

        foreach ($agents as $agent) {
            $agentsReport[$agent->id]['count'] = $agent->count;
            $agentsReport[$agent->id]['name'] = $agent->name;
            $generalReport['count'] = $generalReport['count'] ? $generalReport['count'] + $agent->count : $agent->count;

            foreach ($currencies as $currency) {
                $agentsReport[$agent->id]['sum'][$currency->short] = Ticket::where('created_at', '>=', $from)
                    ->where('created_at', '<=', $to)
                    ->where('agent_id', $agent->id)
                    ->where('currency_id', $currency->id)
                    ->where('variant_id', '<', 2)
                    ->whereNull('deleted_at')
                    ->sum('price')
                    / 100;
                $generalReport['sum'][$currency->short] = array_key_exists($currency->short, $generalReport['sum']) ? $generalReport['sum'][$currency->short] + $agentsReport[$agent->id]['sum'][$currency->short] : $agentsReport[$agent->id]['sum'][$currency->short];
            }
        }

        return view('reports.index')
            ->with('agents', $agentsReport)
            ->with('general', $generalReport);
    }
}
