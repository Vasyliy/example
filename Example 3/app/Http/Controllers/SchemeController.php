<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSchemeRequest;
use App\Http\Requests\UpdateSchemeRequest;
use App\Repositories\SchemeRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class SchemeController extends AppBaseController
{
    /** @var  SchemeRepository */
    private $schemeRepository;

    public function __construct(SchemeRepository $schemeRepo)
    {
        $this->schemeRepository = $schemeRepo;
    }

    /**
     * Display a listing of the Scheme.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->schemeRepository->pushCriteria(new RequestCriteria($request));
        $schemes = $this->schemeRepository->all();

        return view('schemes.index')
            ->with('schemes', $schemes);
    }

    /**
     * Show the form for creating a new Scheme.
     *
     * @return Response
     */
    public function create()
    {
        return view('schemes.create');
    }

    /**
     * Store a newly created Scheme in storage.
     *
     * @param CreateSchemeRequest $request
     *
     * @return Response
     */
    public function store(CreateSchemeRequest $request)
    {
        $input = $request->all();

        $scheme = $this->schemeRepository->create($input);

        Flash::success('Scheme saved successfully.');

        return redirect(route('schemes.index'));
    }

    /**
     * Display the specified Scheme.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $scheme = $this->schemeRepository->findWithoutFail($id);

        if (empty($scheme)) {
            Flash::error('Scheme not found');

            return redirect(route('schemes.index'));
        }

        return view('schemes.show')->with('scheme', $scheme);
    }

    /**
     * Show the form for editing the specified Scheme.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $scheme = $this->schemeRepository->findWithoutFail($id);

        if (empty($scheme)) {
            Flash::error('Scheme not found');

            return redirect(route('schemes.index'));
        }

        return view('schemes.edit')->with('scheme', $scheme);
    }

    /**
     * Update the specified Scheme in storage.
     *
     * @param  int              $id
     * @param UpdateSchemeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSchemeRequest $request)
    {
        $scheme = $this->schemeRepository->findWithoutFail($id);

        if (empty($scheme)) {
            Flash::error('Scheme not found');

            return redirect(route('schemes.index'));
        }

        $scheme = $this->schemeRepository->update($request->all(), $id);

        Flash::success('Scheme updated successfully.');

        return redirect(route('schemes.index'));
    }

    /**
     * Remove the specified Scheme from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $scheme = $this->schemeRepository->findWithoutFail($id);

        if (empty($scheme)) {
            Flash::error('Scheme not found');

            return redirect(route('schemes.index'));
        }

        $this->schemeRepository->delete($id);

        Flash::success('Scheme deleted successfully.');

        return redirect(route('schemes.index'));
    }
}
