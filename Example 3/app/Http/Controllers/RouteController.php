<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateRouteRequest;
use App\Http\Requests\UpdateRouteRequest;
use App\Models\Currency;
use App\Models\Route;
use App\Repositories\RouteRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class RouteController extends AppBaseController
{
    /** @var  RouteRepository */
    private $routeRepository;

    public function __construct(RouteRepository $routeRepo)
    {
        $this->routeRepository = $routeRepo;
    }

    /**
     * Display a listing of the Route.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->routeRepository->pushCriteria(new RequestCriteria($request));
        $routes = $this->routeRepository->all();

        return view('routes.index')
            ->with('routes', $routes);
    }

    /**
     * Show the form for creating a new Route.
     *
     * @return Response
     */
    public function create()
    {
        return view('routes.create');
    }

    /**
     * Store a newly created Route in storage.
     *
     * @param CreateRouteRequest $request
     *
     * @return Response
     */
    public function store(CreateRouteRequest $request)
    {
        $input = $request->all();

        $route = $this->routeRepository->create($input);

        Flash::success('Route saved successfully.');

        return redirect(route('routes.index'));
    }

    /**
     * Display the specified Route.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $route = $this->routeRepository->findWithoutFail($id);

        if (empty($route)) {
            Flash::error('Route not found');

            return redirect(route('routes.index'));
        }

        return view('routes.show')->with('route', $route);
    }

    /**
     * Show the form for editing the specified Route.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $route = $this->routeRepository->findWithoutFail($id);

        if (empty($route)) {
            Flash::error('Route not found');

            return redirect(route('routes.index'));
        }

        return view('routes.edit')->with('route', $route);
    }

    /**
     * Update the specified Route in storage.
     *
     * @param  int              $id
     * @param UpdateRouteRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRouteRequest $request)
    {
        $route = $this->routeRepository->findWithoutFail($id);

        if (empty($route)) {
            Flash::error('Route not found');

            return redirect(route('routes.index'));
        }

        $route = $this->routeRepository->update($request->all(), $id);

        Flash::success('Route updated successfully.');

        return redirect(route('routes.index'));
    }

    /**
     * Remove the specified Route from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $route = $this->routeRepository->findWithoutFail($id);

        if (empty($route)) {
            Flash::error('Route not found');

            return redirect(route('routes.index'));
        }

        $route->route_points()->delete();
        $this->routeRepository->delete($id);

        Flash::success('Route deleted successfully.');

        return redirect(route('routes.index'));
    }

    public function clone($route)
    {
        $new_route = Route::cloneRoute($route);

        if (empty($new_route)) {
            Flash::error('Ошибка копирования маршрута');
            return redirect(route('routes.index'));
        }

        Flash::success('Маршрут скопирован удачно.');

        return redirect(route('routes.index'));
    }

    public function pricelist($route)
    {
        $route = $this->routeRepository->findWithoutFail($route);

        if (empty($route)) {
            Flash::error('Route not found');

            return redirect(route('routes.index'));
        }

        $route_prices = [];
        foreach($route->route_prices as $route_price){
            $route_prices[$route_price->from_point_id][$route_price->to_point_id] = [
                'price' => number_format( $route_price->price, 2, '.', ''),
                'currency' => $route_price->currency,
            ];

        }
        return view('routes.pricelist')
            ->with('route', $route)
            ->with('from_points', $route->getAllFromPoints())
            ->with('to_points', $route->getAllToPoints())
            ->with('route_prices', $route_prices)
            ->with('default_currency', Currency::find(1))
            ;
    }
}
