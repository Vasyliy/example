<?php

namespace App\Http\Controllers;

class HomeController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function tickets()
    {
        return view('tickets');
    }

    public function schedules()
    {
        return view('schedules');
    }
}
