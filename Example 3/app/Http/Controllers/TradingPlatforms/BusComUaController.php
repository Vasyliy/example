<?php

namespace App\Http\Controllers\TradingPlatforms;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Mail\NotificationMail;
use App\Models\Settings;
use App\Models\Place;
use App\Models\Ticket;
use App\Models\TradingPlatform;
use App\Models\TradingPlatformTicketVariant;
use App\Models\Trip;
use Carbon\Carbon;
use DB;

class BusComUaController extends Controller
{
    public static function getCookie($username, $password)
    {
        $data = array('username' => $username, 'password' => $password);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://crr.bus.com.ua/login");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);

        $result = curl_exec($ch);
        preg_match_all('/^Set-Cookie:\s*([^;]*)/mi', $result, $matches);
        if(!array_key_exists(0, $matches[1]))
            return false;
        return $matches[1][0];
    }

    public static function getView($cookie, $cid, $agent, $date)
    {
        $data = array(
            'busstation' => $agent,
            '_cid' => $cid,
            'begindate' => $date,
            'enddate' => $date,
            'Submit' => 'Запрос ведомостей'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://crr.bus.com.ua/trip/view");
        curl_setopt($ch, CURLOPT_COOKIE, $cookie);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_exec($ch);
    }

    public static function getTickets($username, $password, $cid, $agent, $route, $date, $trip_source = null)
    {
        $data = array(
            'round_num' => $route,
            'date' => $date,
            'trip_source' => $trip_source,
            'cid' => $cid,
            'bs_code' => $agent
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://crr.bus.com.ua/trip/get_report?" . http_build_query($data));
        $cookie = self::getCookie($username, $password);
        $result = array();
        if ($cookie) {
            //self::getView($cookie, $cid, $agent, $date);
            curl_setopt($ch, CURLOPT_COOKIE, $cookie);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $str = curl_exec($ch);
            $table = strstr($str, 'border="1"');
            $table = strstr($table, '<tbody>');
            $table = strstr($table, '</tbody>', true);
            $table = explode('</tr>', $table, -2);

            foreach ($table as $key => $row) {
                $row = explode('</td>', $row);
                array_walk($row, function (&$n) {
                    $n = trim(strip_tags($n));
                });
                $result[$key]['place'] = $row[0];
                $result[$key]['variant'] = $row[1];
                $result[$key]['num'] = $row[3];
                $result[$key]['from_point'] = $row[4];
                $result[$key]['to_point'] = $row[5];
            }
        }
        return $result;
    }

    public static function update($from = 0, $to = 0)
    {
        $currentDate = Carbon::today('GMT+3')->addDay($from);
        $updateToDate = Carbon::today('GMT+3')->addDay($to);
        while ($currentDate <= $updateToDate) {
            $fromDate = $currentDate->setTimeFromTimeString('00:00:00')->format('Y-m-d H:i:s');
            $toDate = $currentDate->setTimeFromTimeString('23:59:00')->format('Y-m-d H:i:s');
            $date = $currentDate->format('d.m.y');
            $platform = TradingPlatform::find(TradingPlatform::BUS_COM_UA_ID);
            foreach ($platform->agentsOptions as $agentOptions) {
                $options = json_decode($agentOptions->options);
                foreach ($agentOptions->routes as $route) {
                    $trip = Trip::where('route_id', $route->route_id)->whereBetween('date', [$fromDate, $toDate])->first();
                    if (isset($trip)) {
                        $tickets = self::getTickets($options->username, $options->password, $options->cid, $options->agent_number, $route->name, $date);
                        if (!empty($tickets)) {
                            Ticket::where('trip_id', $trip->id)
                                ->where('agent_id', $agentOptions->agent_id)
                                ->where('variant_id', '<>', Ticket::VARIANT_CANCEL_ID)
                                ->update(['variant_id' => Ticket::VARIANT_CANCEL_ID]);
                            foreach ($tickets as $ticket) {
                                $fromPoint = $platform->points->where('name', $ticket['from_point'])->first();
                                $toPoint = $platform->points->where('name', $ticket['to_point'])->first();
                                $variant = TradingPlatformTicketVariant::where('trading_platform_id', $platform->id)->where('name', 'LIKE', '%' . $ticket['variant'] . '%')->first();
                                $place = Place::where('scheme_id', $trip->scheme_id)->where('name', $ticket['place'])->first();
                                if ($fromPoint && $toPoint && $variant) {
                                    $place_id = $place ? $place->id : $trip->getFreePlaceMaxId($fromPoint->point_id, $toPoint->point_id);
                                    $place_id = $place_id ? $place_id : $trip->getFreePlaceMaxId($fromPoint->point_id, $toPoint->point_id, true);
                                    if ($place_id) {
                                        if (!Ticket::where('trip_id', $trip->id)
                                            ->where('trading_platform_place', $ticket['place'])
                                            ->where('agent_id', $agentOptions->agent_id)
                                            ->where('to_point_id', $fromPoint->point_id)
                                            ->count()
                                        ) {
                                            Ticket::updateOrCreate(
                                                [
                                                    'trip_id' => $trip->id,
                                                    'trading_platform_place' => $ticket['place'],
                                                    'from_point_id' => $fromPoint->point_id,
                                                    'to_point_id' => $toPoint->point_id,
                                                    'agent_id' => $agentOptions->agent_id,
                                                    'number' => $ticket['num'],
                                                    'type_id' => 1
                                                ],
                                                [
                                                    'variant_id' => $variant->variant_id,
                                                    'place_id' => $place_id
                                                ]
                                            );
                                        }
                                    }
                                } else {
                                    if (Settings::find(1)->notification_email) {
                                        $notification = 'Ошибка bus.com.ua! ' . $date . ' Агент - ' . $agentOptions->agent->name;
                                        if (!$fromPoint)
                                            $notification .= '. Точка отправления - ' . $ticket['from_point'];
                                        if (!$toPoint)
                                            $notification .= '. Точка прибытия - ' . $ticket['to_point'];
                                        if (!$variant)
                                            $notification .= '. Вариант - ' . $ticket['variant'];
                                        $notification .= '.';
                                        Mail::to(Settings::find(1)->notification_email)->send(new NotificationMail($notification));
                                    }
                                }
                            }
                        }
                    }
                }
            }
            $currentDate->addDay(1);
        }

        return true;
    }
}
