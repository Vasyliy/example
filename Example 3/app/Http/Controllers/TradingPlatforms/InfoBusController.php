<?php

namespace App\Http\Controllers\TradingPlatforms;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Mail\NotificationMail;
use App\Models\Settings;
use App\Models\Ticket;
use App\Models\Currency;
use App\Models\TradingPlatform;
use App\Models\AgentTradingPlatform;
use App\Models\TradingPlatformTicketVariant;
use App\Models\Trip;
use Carbon\Carbon;
use DB;

class InfoBusController extends Controller
{
    public static function getTickets($login, $password, $dateFrom, $dateUntil)
    {
        $data = array(
            'login' => $login,
            'password' => $password,
            'date_from' => $dateFrom,
            'date_until' => $dateUntil,
            'type_date' => 'departure'
        );

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "https://api.bussystem.eu/server/curl_dispatcher/get_tickets.php");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);

        $str = curl_exec($ch);
        curl_close($ch);

        $str = strstr($str, '<item>');
        $str = strstr($str, '</root>', true);
        $items = explode('</item>', $str, -1);

        $result = array();

        foreach ($items as $key => $item) {
            preg_match('/ticket_id="(.*?)".*status="(.*?)".*TRF="(.*?)".*currency="(.*?)".*route_name="(.*?)".*interval="(.*?)".*date_from="(.*?)".*route_id="(.*?)".*point_from_id="(.*?)".*point_to_id="(.*?)".*name="(.*?)".*surname="(.*?)"/s', $item, $match);
            if ($match) {
                $result[$key]['ticket_id'] = $match[$i = 1];
                $result[$key]['status'] = $match[++$i];
                $result[$key]['TRF'] = $match[++$i];
                $result[$key]['currency'] = $match[++$i];
                $result[$key]['route_name'] = $match[++$i];
                $result[$key]['interval'] = $match[++$i];
                $result[$key]['date_from'] = $match[++$i];
                $result[$key]['route_id'] = $match[++$i];
                $result[$key]['point_from_id'] = $match[++$i];
                $result[$key]['point_to_id'] = $match[++$i];
                $result[$key]['name'] = $match[++$i];
                $result[$key]['surname'] = $match[++$i];
            }
        }

        return $result;
    }

    public static function update($from = 0, $to = 0)
    {
        $fromDate = Carbon::today('GMT+3')->addDay($from)->format('Y-m-d');
        $toDate = Carbon::today('GMT+3')->addDay($to)->format('Y-m-d');
        $platform = TradingPlatform::find(TradingPlatform::INFO_BUS_ID);
        $options = json_decode($platform->options);
        $tickets = self::getTickets($options->login, $options->password, $fromDate, $toDate);

        if (!empty($tickets)) {
            $agentOptions = AgentTradingPlatform::where('trading_platform_id', TradingPlatform::INFO_BUS_ID)->first();
            $routeNames = $agentOptions->routes;
            foreach ($tickets as $ticket) {
                $extTicket = Ticket::where('agent_id', $agentOptions->agent_id)
                    ->where('number', $ticket['ticket_id'])
                    ->first();
                $variant = TradingPlatformTicketVariant::where('trading_platform_id', $platform->id)->where('name', 'LIKE', '%' . $ticket['status'] . '%')->first();
                if (!$extTicket) {
                    $route = $routeNames->where('name', $ticket['route_id'])->first();
                    if ($route) {
                        $trip = Trip::where('route_id', $route->route_id)->whereBetween('date', [$ticket['date_from'] . ' 00:00:00', $ticket['date_from'] . ' 23:59:00'])->first();
                        $currency = Currency::where('short', $ticket['currency'])->first();
                        $fromPoint = $platform->points->where('name', $ticket['point_from_id'])->first();
                        $toPoint = $platform->points->where('name', $ticket['point_to_id'])->first();
                        if ($trip && $fromPoint && $toPoint && $variant && $currency) {
                            $place_id = $trip->getFreePlaceMaxId($fromPoint->point_id, $toPoint->point_id);
                            $place_id = $place_id ? $place_id : $trip->getFreePlaceMaxId($fromPoint->point_id, $toPoint->point_id, true);
                            if ($place_id) {
                                Ticket::create(
                                    [
                                        'trip_id' => $trip->id,
                                        'from_point_id' => $fromPoint->point_id,
                                        'to_point_id' => $toPoint->point_id,
                                        'agent_id' => $agentOptions->agent_id,
                                        'number' => $ticket['ticket_id'],
                                        'place_id' => $place_id,
                                        'type_id' => 1,
                                        'price' => $ticket['TRF'],
                                        'currency_id' => $currency->id,
                                        'variant_id' => $variant->variant_id,
                                        'name1' => $ticket['name'],
                                        'name2' => $ticket['surname']
                                    ]
                                );
                            }
                        } else {
                            if (Settings::find(1)->notification_email) {
                                $notification = 'Ошибка InfoBUS! ' . $ticket['date_from'];
                                if (!$trip)
                                    $notification .= '. Рейс по маршруту #' . $ticket['route_id'] . ' ' . $ticket['route_name'];
                                if (!$fromPoint)
                                    $notification .= '. Точка отправления #' . $ticket['point_from_id'] . ' ' . $ticket['interval'];
                                if (!$toPoint)
                                    $notification .= '. Точка прибытия #' . $ticket['point_to_id'] . ' ' . $ticket['interval'];
                                if (!$variant)
                                    $notification .= '. Вариант - ' . $ticket['status'];
                                if (!$currency)
                                    $notification .= '. Валюта - ' . $ticket['currency'];
                                $notification .= '.';
                                Mail::to(Settings::find(1)->notification_email)->send(new NotificationMail($notification));
                            }
                        }
                    } else {
                        if (Settings::find(1)->notification_email) {
                            Mail::to(Settings::find(1)->notification_email)->send(new NotificationMail('Ошибка InfoBUS! ' . $ticket['date_from'] . '. Маршрут #' . $ticket['route_id'] . ' ' . $ticket['route_name'] . '.'));
                        }
                    }
                } elseif ($variant && $variant->variant_id != $extTicket->variant_id) {
                    $extTicket->update(['variant_id' => $variant->variant_id]);
                }
            }
        }
        return true;
    }
}
