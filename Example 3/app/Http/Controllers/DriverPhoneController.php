<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateDriverPhoneRequest;
use App\Http\Requests\UpdateDriverPhoneRequest;
use App\Models\Driver;
use App\Repositories\DriverPhoneRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class DriverPhoneController extends AppBaseController
{
    /** @var  DriverPhoneRepository */
    private $driverPhoneRepository;

    public function __construct(DriverPhoneRepository $driverPhoneRepo)
    {
        $this->driverPhoneRepository = $driverPhoneRepo;
    }

    /**
     * Display a listing of the DriverPhone.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->driverPhoneRepository->pushCriteria(new RequestCriteria($request));
        $driverPhones = $this->driverPhoneRepository->all();

        return view('driver_phones.index')
            ->with('driverPhones', $driverPhones);
    }

    /**
     * Show the form for creating a new DriverPhone.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $driver = Driver::find($request->input('driver'));

        if (isset($driver)){
            return view('driver_phones.create')->with('driver', $driver);
        }
    }

    /**
     * Store a newly created DriverPhone in storage.
     *
     * @param CreateDriverPhoneRequest $request
     *
     * @return Response
     */
    public function store(CreateDriverPhoneRequest $request)
    {
        $input = $request->all();

        $driverPhone = $this->driverPhoneRepository->create($input);

        Flash::success('Driver Phone saved successfully.');

        return redirect(route('drivers.edit', [$driverPhone->driver_id]));
    }

    /**
     * Display the specified DriverPhone.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $driverPhone = $this->driverPhoneRepository->findWithoutFail($id);

        if (empty($driverPhone)) {
            Flash::error('Driver Phone not found');

            return redirect(route('driverPhones.index'));
        }

        return view('driver_phones.show')->with('driverPhone', $driverPhone);
    }

    /**
     * Show the form for editing the specified DriverPhone.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id, Request $request)
    {
        $driverPhone = $this->driverPhoneRepository->findWithoutFail($id);

        $driver = Driver::find($request->input('driver'));

        if (empty($driverPhone)) {
            Flash::error('Driver Phone not found');

            return redirect(route('driverPhones.index'));
        }

        return view('driver_phones.edit')->with('driverPhone', $driverPhone)->with('driver', $driver);
    }

    /**
     * Update the specified DriverPhone in storage.
     *
     * @param  int              $id
     * @param UpdateDriverPhoneRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDriverPhoneRequest $request)
    {
        $driverPhone = $this->driverPhoneRepository->findWithoutFail($id);

        if (empty($driverPhone)) {
            Flash::error('Driver Phone not found');

            return redirect(route('driverPhones.index'));
        }

        $driverPhone = $this->driverPhoneRepository->update($request->all(), $id);

        Flash::success('Driver Phone updated successfully.');

        return redirect(route('drivers.edit', [$driverPhone->driver_id]));
    }

    /**
     * Remove the specified DriverPhone from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $driverPhone = $this->driverPhoneRepository->findWithoutFail($id);

        if (empty($driverPhone)) {
            Flash::error('Driver Phone not found');

            return redirect(route('driverPhones.index'));
        }

        $driver_id = $driverPhone->driver_id;

        $this->driverPhoneRepository->delete($id);

        Flash::success('Driver Phone deleted successfully.');

        return redirect(route('drivers.edit', [$driver_id]));
    }
}
