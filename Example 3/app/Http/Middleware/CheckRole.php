<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string  $role
     * @return mixed
     */
    public function handle($request, Closure $next, $role1, $role2 = -1)
    {
        if (Auth::user()) {
            $userRole = Auth::user()->role;
            if ($userRole == $role1 || $userRole == $role2)
                return $next($request);
        }

        return redirect('/');
    }
}
