<?php

namespace App\Mail;

use App\Models\Trip;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CriticalPlacesNumberMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($trip_id)
    {
        $this->trip = Trip::find($trip_id);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if(isset($this->trip)) {
            $subject = isset($this->trip->route) ?
                $this->trip->route->name . ' ' . $this->trip->date . ' - Критическое количество мест' :
                $this->trip->date . ' - Критическое количество мест';
            return $this->view('emails.message')
                ->subject($subject)
                ->with('title', $subject)
                ->with('text', $subject);
        }
    }
}
