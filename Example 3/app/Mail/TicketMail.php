<?php

namespace App\Mail;

use App\Models\Returns;
use App\Models\Settings;
use App\Models\Ticket;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TicketMail extends Mailable
{
    use Queueable, SerializesModels;

    public $ticket;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($ticket_id)
    {
        $this->ticket = Ticket::find($ticket_id);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if(isset($this->ticket)){
            return $this->view('tickets.ticket_letter')
                ->with('ticket', $this->ticket)
                ->with('trip', $this->ticket->trip)
                ->with('settings', Settings::find(1))
                ->with('returns', Returns::all());
        }
    }
}
