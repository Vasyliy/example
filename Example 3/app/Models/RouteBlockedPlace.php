<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class RouteBlockedPlace
 * @package App\Models
 * @version September 20, 2018, 2:35 pm UTC
 *
 * @property integer route_id
 * @property integer place_id
 */
class RouteBlockedPlace extends Model
{
    use SoftDeletes;

    public $table = 'route_blocked_places';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'route_id',
        'place_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'route_id' => 'integer',
        'place_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'route_id' => 'required integer',
        'place_id' => 'required integer'
    ];

    
}
