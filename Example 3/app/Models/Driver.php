<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Driver
 * @package App\Models
 * @version July 4, 2018, 4:28 pm UTC
 *
 * @property string name
 * @property string license
 * @property string note
 * @property integer status
 */
class Driver extends Model
{
    use SoftDeletes, Translated;

    public $table = 'drivers';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'license',
        'note',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'license' => 'string',
        'note' => 'string',
        'status' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|max:255',
    ];

    public function textStatus(){
        if($this->status == 0){
            return 'Выключен';
        }elseif ($this->status == 1){
            return 'Активный';
        }else{
            return 'Неопределён';
        }
    }

    public function phones(){
        return $this->hasMany('App\Models\DriverPhone', 'driver_id');
    }

    public static function getAll(){
        return self::pluck('name', 'id');
    }
}
