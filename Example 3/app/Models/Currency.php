<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

/**
 * Class Currency
 * @package App\Models
 * @version July 2, 2018, 10:34 am UTC
 *
 * @property string name
 * @property string short
 * @property integer exchange
 */
class Currency extends Model
{
    use SoftDeletes, Translated;

    public $table = 'currencies';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'short',
        'symbol',
        'exchange'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'short' => 'string',
        'symbol' => 'string',
        'exchange' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|max:255|unique:currencies,name',
        'short' => 'required|unique:currencies|max:3|min:3',
        'symbol' => 'required|unique:currencies|max:3',
    ];

    public static function getAll(){
        return self::pluck('short', 'id');
    }

    public function getExchangeAttribute($value)
    {
        return $value / 100;
    }

    public function setExchangeAttribute($value)
    {
        if(isset($value)){
            $this->attributes['exchange'] = $value * 100;
        }else{
            $this->attributes['exchange'] = 0;
        }
    }

    public static function getListOnJson(){
        $result = [];
        foreach (self::all() as $currency){
            $result[] = [
                'value' => $currency->id,
                'text' => $currency->short
            ];
        }
        return json_encode($result);
    }

    public static function getSymbolListOnJson(){
        $result = [];
        foreach (self::all() as $currency){
            $result[] = [
                'value' => $currency->id,
                'text' => $currency->symbol
            ];
        }
        return json_encode($result);
    }
}
