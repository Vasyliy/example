<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class TradingPlatformRoutes
 * @package App\Models
 * @version September 27, 2018, 5:59 pm UTC
 *
 * @property integer agent_trading_platform_id
 * @property integer route_id
 * @property string name
 */
class TradingPlatformRoutes extends Model
{
    use SoftDeletes;

    public $table = 'trading_platform_routes';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'agent_trading_platform_id',
        'route_id',
        'name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'agent_trading_platform_id' => 'integer',
        'route_id' => 'integer',
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'agent_trading_platform_id' => 'required|numeric',
        'route_id' => 'required|numeric',
        'name' => 'required|string'
    ];

    public static $messages = [
        'name.required' => 'Необходимо указать название',
    ];

    public function agentTradingPlatform(){
        return $this->belongsTo('App\Models\AgentTradingPlatform', 'agent_trading_platform_id');
    }

    public function route(){
        return $this->belongsTo('App\Models\Route', 'route_id');
    }
}
