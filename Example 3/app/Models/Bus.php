<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

/**
 * Class Bus
 * @package App\Models
 * @version July 4, 2018, 1:09 pm UTC
 *
 * @property integer sсheme_id
 * @property string name
 * @property string model
 * @property string number
 * @property string note
 */
class Bus extends Model
{
    use SoftDeletes;

    public $table = 'buses';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'scheme_id',
        'name',
        'model',
        'number',
        'note'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'scheme_id' => 'integer',
        'name' => 'string',
        'model' => 'string',
        'number' => 'string',
        'note' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|max:255',
        'number' => 'max:255',
    ];

    public function scheme(){
        return $this->belongsTo('App\Models\Scheme', 'scheme_id');
    }

    public static function getAll(){
        return self::select(DB::raw("CONCAT(buses.name, ' (', buses.number, ')') AS name"), 'id')->pluck('name', 'id');
    }
}
