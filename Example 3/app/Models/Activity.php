<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

/**
 * Class Activity
 * @package App\Models
 * @version October 31, 2018, 4:06 pm EET
 *
 * @property integer user_id
 * @property integer type
 * @property string description
 */
class Activity extends Model
{
    use SoftDeletes;

    public $table = 'activities';

    protected $dates = ['deleted_at'];

    const TYPE_TICKETS = 0;
    const TYPES = [
        self::TYPE_TICKETS => 'Билеты',
    ];

    public $fillable = [
        'user_id',
        'type',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'user_id' => 'integer',
        'type' => 'integer',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function user(){
        return $this->belongsTo('App\Models\User', 'user_id')->withDefault([
            'name' => 'Сервис билетов'
        ]);
    }

    public static function log($type, $description){
        $userId = Auth::user() ? Auth::user()->id : null;

        self::create([
            'user_id' => $userId,
            'type' => $type,
            'description' => $description
        ]);
    }

    public static function logTicket($ticket){
        switch ($ticket->variant_id) {
            case Ticket::VARIANT_BOOKED_ID:
                self::log(self::TYPE_TICKETS, 'Забронировано билет <a target="_blank" href=/tickets/'. $ticket->id .'/edit>' . $ticket->id . '</a>');
                break;
            case Ticket::VARIANT_BUY_ID:
                self::log(self::TYPE_TICKETS, 'Куплено билет <a target="_blank" href=/tickets/'. $ticket->id .'/edit>' . $ticket->id . '</a>');
                break;
            case Ticket::VARIANT_CANCEL_ID:
                self::log(self::TYPE_TICKETS, 'Отменено билет <a target="_blank" href=/tickets/'. $ticket->id .'/edit>' . $ticket->id . '</a>');
                break;
            case Ticket::VARIANT_DELETE_ID:
                self::log(self::TYPE_TICKETS, 'Удалено билет '. $ticket->id);
                break;
        }
    }
}
