<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class TradingPlatform
 * @package App\Models
 * @version September 27, 2018, 6:08 pm UTC
 *
 * @property string name
 * @property integer refresh_time_1
 * @property integer refresh_time_2
 * @property integer refresh_time_3
 * @property string options
 */
class TradingPlatform extends Model
{
    use SoftDeletes;

    public $table = 'trading_platforms';

    const BUS_COM_UA_ID = 1;
    const INFO_BUS_ID = 2;

    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'refresh_time_1',
        'refresh_time_2',
        'refresh_time_3',
        'options'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'refresh_time_1' => 'integer',
        'refresh_time_2' => 'integer',
        'refresh_time_3' => 'integer',
        'options' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|string',
        'refresh_time_1' => 'required|numeric',
        'refresh_time_2' => 'required|numeric',
        'refresh_time_3' => 'required|numeric',
    ];

    public static $messages = [
        'name.required' => 'Необходимо указать название',
        'refresh_time_1.required' => 'Необходимо указать время обновления (1-й день)',
        'refresh_time_2.required' => 'Необходимо указать время обновления (2-3 дни)',
        'refresh_time_3.required' => 'Необходимо указать время обновления (4-14 дни)',
        'refresh_time_1.numeric' => 'Время обновления (1-й день) должно быть числом',
        'refresh_time_2.numeric' => 'Время обновления (2-3 дни) должно быть числом',
        'refresh_time_3.numeric' => 'Время обновления (4-14 дни) должно быть числом'
    ];

    public static function getAll(){
        return self::pluck('name', 'id');
    }

    public function ticketVariants(){
        return $this->hasMany('App\Models\TradingPlatformTicketVariant', 'trading_platform_id');
    }

    public function points(){
        return $this->hasMany('App\Models\TradingPlatformPoint', 'trading_platform_id');
    }

    public function agents(){
        return $this->belongsToMany('App\Models\Agent', 'agent_trading_platforms', 'trading_platform_id', 'agent_id');
    }

    public function agentsOptions(){
        return $this->hasMany('App\Models\AgentTradingPlatform', 'trading_platform_id');
    }
}
