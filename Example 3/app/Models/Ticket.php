<?php

namespace App\Models;

use App\Mail\TicketMail;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

/**
 * Class Ticket
 * @package App\Models
 * @version July 17, 2018, 6:10 am UTC
 *
 * @property integer trip_id
 * @property integer from_point_id
 * @property integer to_point_id
 * @property integer place_id
 * @property integer client_id
 * @property integer agent_id
 * @property integer type_id
 * @property integer variant_id
 * @property string ticket_viewer
 * @property integer price
 * @property integer prepay
 * @property integer currency_id
 * @property string comment
 */
class Ticket extends Model
{
    use SoftDeletes;

    public $table = 'tickets';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    const VARIANT_BUY_ID = 0;
    const VARIANT_BOOKED_ID = 1;
    const VARIANT_BLOCK_ID = 2;
    const VARIANT_CANCEL_ID = 3;
    const VARIANT_DELETE_ID = 4;
    const VARIANT = [
        self::VARIANT_BUY_ID => 'Куплено',
        self::VARIANT_BOOKED_ID => 'Бронировано',
        self::VARIANT_BLOCK_ID => 'Блокировано',
        self::VARIANT_CANCEL_ID => 'Отменено',
        self::VARIANT_DELETE_ID => 'Удалено',
    ];
    const VARIANT_SHORT = [
        self::VARIANT_BUY_ID => 'Куплено',
        self::VARIANT_BOOKED_ID => 'Бронь',
        self::VARIANT_BLOCK_ID => 'Блок',
        self::VARIANT_CANCEL_ID => 'Отменено',
        self::VARIANT_DELETE_ID => 'Удалено',
    ];

    protected $dates = ['deleted_at'];


    public $fillable = [
        'trip_id',
        'from_point_id',
        'to_point_id',
        'place_id',
        'client_id',
        'agent_id',
        'type_id',
        'variant_id',
        'number',
        'trading_platform_place',
        'ticket_viewer',
        'price',
        'prepay',
        'currency_id',
        'comment',
        'name1',
        'name2',
        'name3'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'trip_id' => 'integer',
        'from_point_id' => 'integer',
        'to_point_id' => 'integer',
        'place_id' => 'integer',
        'client_id' => 'integer',
        'agent_id' => 'integer',
        'type_id' => 'integer',
        'variant_id' => 'integer',
        'number' => 'integer',
        'trading_platform_place' => 'string',
        'ticket_viewer' => 'string',
        'price' => 'integer',
        'prepay' => 'integer',
        'currency_id' => 'integer',
        'comment' => 'string',
        'name1' => 'string',
        'name2' => 'string',
        'name3' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public static function boot()
    {
        parent::boot();

        self::creating(function($model){
            do {
                $token = str_random(20);
                $ticket = self::where('ticket_viewer', $token)->first();
            }while(isset($ticket));

            if ($model->prepay > 0 && $model->variant_id == self::VARIANT_BOOKED_ID)
                $model->variant_id = self::VARIANT_BUY_ID;

           $model->ticket_viewer = $token;
        });

        self::created(function($model){
            $freePlacesCount = $model->trip->getFreePlacesCount();
            if ($model->trip->isCriticalPlacesNumber($freePlacesCount) && !$model->trip->is_critical)
                $model->trip->setCritical();
            if (isset($model->client->email) && $model->variant_id < 2)
                Mail::to($model->client->email)->queue(new TicketMail($model->id));
            $model->trip->setFreePlacesCount($freePlacesCount);
            if(Auth::user())
                Activity::logTicket($model);
            return true;
        });

        self::updating(function($model){
            if ($model->prepay > 0 && $model->variant_id == self::VARIANT_BOOKED_ID)
                $model->variant_id = self::VARIANT_BUY_ID;
            if (Auth::user() && $model->variant_id != self::find($model->id)->variant_id)
                Activity::logTicket($model);
        });

        self::updated(function($model){
            $model->trip->setFreePlacesCount();
        });

        self::deleting(function($model){
            $model->update(['variant_id' => self::VARIANT_DELETE_ID]);
        });
    }

    public function trip(){
        return $this->belongsTo('App\Models\Trip', 'trip_id');
    }

    public function from(){
        return $this->belongsTo('App\Models\Point', 'from_point_id');
    }

    public function to(){
        return $this->belongsTo('App\Models\Point', 'to_point_id');
    }

    public function place(){
        return $this->belongsTo('App\Models\Place', 'place_id');
    }

    public function client(){
        return $this->belongsTo('App\Models\Client', 'client_id')->withDefault([
            'id' => '0',
            'name1' => '',
            'name2' => '',
            'name3' => '',
            'phone' => '',
            'email' => ''
        ]);
    }

    public function agent(){
        return $this->belongsTo('App\Models\Agent', 'agent_id');
    }

    public function type(){
        return $this->belongsTo('App\Models\TicketType', 'type_id');
    }

    public function currency(){
        return $this->belongsTo('App\Models\Currency', 'currency_id');
    }

    public function fromPoint(){
        $point = RoutePoint::where('from_point_id', '=', $this->from_point_id)
            ->where('route_id', '=', $this->trip->route_id)
            ->first();
        return $point;
    }

    public function toPoint(){
        $point = RoutePoint::where('to_point_id', '=', $this->to_point_id)
            ->where('route_id', '=', $this->trip->route_id)
            ->first();
        return $point;
    }

    // Преобразователь прайса
    public function getPriceAttribute($value)
    {
        return $value / 100;
    }

    // Преобразователь прайса
    public function setPriceAttribute($value)
    {
        $this->attributes['price'] = intval($value * 100);
    }

    public static function defaultPrice($from, $to, $route_id = null){
        $price = 0;
        $route_price = RoutePrice::where('from_point_id', $from);

        if ($route_id)
            $route_price = $route_price->where('route_id', $route_id);

        $route_price = $route_price->where('to_point_id', $to)
            ->first();
        if(isset($route_price)){
            $price = $route_price->price;
        }
        return $price;
    }

    public static function defaultCurrency($from, $to, $route_id = null){
        $currency = 0;
        $route_price = RoutePrice::where('from_point_id', $from);

        if ($route_id)
            $route_price = $route_price->where('route_id', $route_id);

        $route_price = $route_price->where('to_point_id', $to)
            ->first();
        if(isset($route_price)){
            $currency = $route_price->currency_id;
        }
        return $currency;
    }

    /**
     * Прверяет возможно ли создать этот билет на это место и рейс
     * TODO: Переделать проверку на один запрос
     */
    public function isTicketRealCreated(){

        $from_point_array = [];
        $to_point_array = [];

        $from_ticket_route = RoutePoint::where('from_point_id', '=', $this->from_point_id)
            ->where('route_id', '=', $this->trip->route_id)
            ->first();
        $to_ticket_route = RoutePoint::where('to_point_id', '=', $this->to_point_id)
            ->where('route_id', '=', $this->trip->route_id)
            ->first();
        $in_ticket_route_points = RoutePoint::where('route_id', '=', $this->trip->route_id)
            ->where('number', '>=', $from_ticket_route->number)
            ->where('number', '<=', $to_ticket_route->number)
            ->get();
        foreach ($in_ticket_route_points as $in_ticket_route_point){
            $from_point_array[] = $in_ticket_route_point->from_point_id;
            $to_point_array[] = $in_ticket_route_point->to_point_id;
        }
        $tickets = self::where('trip_id', $this->trip_id)
            ->where('place_id', '=', $this->place_id)
            ->where('variant_id', '<>', self::VARIANT_CANCEL_ID)
            ->whereIn('from_point_id', $from_point_array)
            ->whereIn('to_point_id', $to_point_array)
            ->get();
        return !isset($tickets[0]);
    }
}
