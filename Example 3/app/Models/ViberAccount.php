<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ViberAccount extends Model
{
    private $url_api = "https://chatapi.viber.com/pa/";

    private $token = "485ebc199227d055-3113304706776bf2-d8a567d3c71dde5a";

    public function message_post
    (
        $from,          // ID администратора Public Account.
        array $sender,  // Данные отправителя.
        $text           // Текст.
    )
    {
        $data['from']   = $from;
        $data['sender'] = $sender;
        $data['type']   = 'text';
        $data['text']   = $text;
        $data = [
            "receiver" => $from,
            //"min_api_version" => 1,
            "sender" => $sender,
            //"tracking_data" => "tracking data",
            "type" => "text",
            "text" => $text
        ];

        return $this->call_api('post', $data);
    }

    private function call_api($method, $data)
    {
        $url = $this->url_api.$method;

        $options = array(
            'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\nX-Viber-Auth-Token: ".$this->token."\r\n",
                'method'  => 'POST',
                'content' => json_encode($data)
            )
        );
        $context  = stream_context_create($options);
        $response = file_get_contents($url, false, $context);
        return json_decode($response);
    }
}
