<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Agent
 * @package App\Models
 * @version July 3, 2018, 9:54 am UTC
 *
 * @property string name
 * @property string api
 * @property string note
 * @property integer status
 */
class Agent extends Model
{
    use SoftDeletes, Translated;

    public $table = 'agents';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    const GDAMALER_ID = 1;

    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'api',
        'note',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'api' => 'string',
        'note' => 'string',
        'status' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|max:255',
        'api' => 'max:255',
    ];

    public function textStatus(){
        if($this->status == 0){
            return 'Выключен';
        }elseif ($this->status == 1){
            return 'Активный';
        }else{
            return 'Неопределён';
        }
    }

    public static function getAll(){
        return self::pluck('name', 'id');
    }

    public function tradingPlatforms(){
        return $this->belongsToMany('App\Models\TradingPlatform', 'agent_trading_platforms', 'agent_id', 'trading_platform_id');
    }

    public function tradingPlatformOptions($trading_platform_id){
        return $this->hasMany('App\Models\AgentTradingPlatform', 'agent_id')
            ->where('trading_platform_id', $trading_platform_id)
            ->first();
    }
}
