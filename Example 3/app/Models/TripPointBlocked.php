<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class TripPointBlocked
 * @package App\Models
 * @version August 8, 2018, 1:02 pm UTC
 *
 * @property integer trip_id
 * @property integer point_id
 */
class TripPointBlocked extends Model
{
    use SoftDeletes;

    public $table = 'trip_point_blocked';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'trip_id',
        'point_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'trip_id' => 'integer',
        'point_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function point(){
        return $this->belongsTo('App\Models\Point', 'point_id');
    }
}
