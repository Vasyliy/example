<?php

namespace App\Models;

use App\Mail\CriticalPlacesNumberMail;
use Illuminate\Support\Facades\Mail;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

/**
 * Class Trip
 * @package App\Models
 * @version July 17, 2018, 8:06 am UTC
 *
 * @property integer route_id
 * @property integer scheme_id
 * @property integer bus_id
 * @property integer driver_id
 * @property integer status
 * @property string phone
 * @property string|\Carbon\Carbon date
 */
class Trip extends Model
{
    use SoftDeletes;

    public $table = 'trips';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    const STATUSES = [
        0 => 'Ожидается',
        1 => 'Начат',
        2 => 'Окончен',
        3 => 'Отменён',
    ];

    protected $dates = ['deleted_at', 'date'];

    public $fillable = [
        'route_id',
        'scheme_id',
        'bus_id',
        'driver_id',
        'driver2_id',
        'status',
        'phone',
        'date',
        'is_critical',
        'free_places'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'route_id' => 'integer',
        'scheme_id' => 'integer',
        'bus_id' => 'integer',
        'driver_id' => 'integer',
        'driver2_id' => 'integer',
        'phone' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function getDateAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y H:i');
    }

    public function setDateAttribute($value)
    {
        $this->attributes['date'] = Carbon::createFromFormat('d-m-Y H:i', $value)->toDateTimeString();
    }
    /*
    public function route_points(){
        return $this->hasMany('App\Models\RoutePoint', 'route_id');
    }

    public function route_prices(){
        return $this->hasMany('App\Models\RoutePrice', 'route_id');
    }
    */

    public function route(){
        return $this->belongsTo('App\Models\Route', 'route_id');
    }

    public function scheme(){
        return $this->belongsTo('App\Models\Scheme', 'scheme_id');
    }

    public function bus(){
        return $this->belongsTo('App\Models\Bus', 'bus_id');
    }

    public function driver(){
        return $this->belongsTo('App\Models\Driver', 'driver_id');
    }

    public function driver2(){
        return $this->belongsTo('App\Models\Driver', 'driver2_id');
    }

    public function tickets(){
        return $this->hasMany('App\Models\Ticket', 'trip_id')
            ->where('tickets.variant_id', '<', 2);
    }

    public function allTickets(){
        return $this->hasMany('App\Models\Ticket', 'trip_id');
    }

    public function ticketsSorting(){
        return Ticket::select('tickets.*')
            ->selectRaw('min(rp.number) as from_number')
            ->leftJoin('route_points as rp', function ($join) {
                $join->on('rp.from_point_id', 'tickets.from_point_id')
                    ->where('rp.route_id', '=', DB::raw("'".$this->route_id."'"))
                    ->whereNull('rp.deleted_at');;
            })
            ->where('tickets.trip_id', '=', $this->id)
            ->where('tickets.variant_id', '<', 2)
            ->groupBy('tickets.id')
            ->orderBy('from_number', 'asc')
            ->get();
    }

    public function blocked_points(){
        return $this->hasMany('App\Models\TripPointBlocked', 'trip_id');
    }

    public function statusName(){
        return self::STATUSES[$this->status];
    }

    public function getFreePlaces(){
        return Place::leftJoin('tickets', function ($join) {
                $join->on('tickets.place_id', '=', 'places.id')->where('tickets.trip_id', '=', $this->id);
            })
            ->where('places.scheme_id', $this->scheme_id)
            ->whereNull('tickets.id')
            ->pluck('places.name', 'places.id')
            ->all();
    }

    public function getFreePlacesCount($from = NULL, $to = NULL){
        $from = $from == NULL ? $this->route->getFromPoint()->from_point_id : $from;
        $to = $to == NULL ? $this->route->getToPoint()->to_point_id : $to;
        $places = DB::select(
            "
            select
            COUNT(*) as count
            from
            (
                select
                    COUNT(DISTINCT(t_r.id)) as tickets_on_place
                from places
                       LEFT JOIN trips ON trips.scheme_id = places.scheme_id AND trips.id = '$this->id' AND trips.deleted_at is null
                       LEFT JOIN route_points rp_from ON trips.route_id = rp_from.route_id AND rp_from.from_point_id = '$from' AND rp_from.deleted_at is null
                       LEFT JOIN route_points rp_to ON trips.route_id = rp_to.route_id AND rp_to.to_point_id = '$to' AND rp_to.deleted_at is null
                       LEFT JOIN route_points rp ON trips.route_id = rp.route_id AND rp.number BETWEEN rp_from.number AND rp_to.number AND rp.deleted_at is null
                       LEFT JOIN tickets t ON t.trip_id = trips.id AND t.place_id = places.id AND t.variant_id != 3 AND t.deleted_at is null
                       LEFT JOIN route_points t_rp_from ON trips.route_id = t_rp_from.route_id AND t_rp_from.from_point_id = t.from_point_id AND t_rp_from.deleted_at is null
                       LEFT JOIN route_points t_rp_to ON trips.route_id = t_rp_to.route_id AND t_rp_to.to_point_id = t.to_point_id AND t_rp_to.deleted_at is null
                       LEFT JOIN tickets t_r ON t_r.trip_id = trips.id AND t_r.place_id = places.id AND t_r.variant_id != 3 AND t_r.deleted_at is null
                                                AND t_rp_from.number < rp_to.number AND t_rp_to.number > rp_from.number
                where
                    places.deleted_at is null
                group by places.id
            ) as t
            where
              tickets_on_place = 0
            ;
            "
        );
        return $places[0]->count;
    }

    public function setFreePlacesCount($count = null){
        $count = $count ? $count : $this->getFreePlacesCount();
        $this->update(['free_places' => $count]);
    }

    public function getFreePlaceMaxId($from, $to, $last = false){
        $lastPlaces = "AND places.id <= 47";
        if($last)
            $lastPlaces = '';
        $place = DB::select(
            "
            select
            max(id) id
            from
            (
                select
                    places.id
                    , t_r.id as ticket
                from places
                       LEFT JOIN trips ON trips.scheme_id = places.scheme_id AND trips.id = '$this->id' AND trips.deleted_at is null
                       LEFT JOIN route_points rp_from ON trips.route_id = rp_from.route_id AND rp_from.from_point_id = '$from' AND rp_from.deleted_at is null
                       LEFT JOIN route_points rp_to ON trips.route_id = rp_to.route_id AND rp_to.to_point_id = '$to' AND rp_to.deleted_at is null
                       LEFT JOIN route_points rp ON trips.route_id = rp.route_id AND rp.number BETWEEN rp_from.number AND rp_to.number AND rp.deleted_at is null
                       LEFT JOIN tickets t ON t.trip_id = trips.id AND t.place_id = places.id AND t.variant_id != 3 AND t.deleted_at is null
                       LEFT JOIN route_points t_rp_from ON trips.route_id = t_rp_from.route_id AND t_rp_from.from_point_id = t.from_point_id AND t_rp_from.deleted_at is null
                       LEFT JOIN route_points t_rp_to ON trips.route_id = t_rp_to.route_id AND t_rp_to.to_point_id = t.to_point_id AND t_rp_to.deleted_at is null
                       LEFT JOIN tickets t_r ON t_r.trip_id = trips.id AND t_r.place_id = places.id AND t_r.variant_id != 3 AND t_r.deleted_at is null
                                                AND t_rp_from.number < rp_to.number AND t_rp_to.number > rp_from.number
                where
                    places.deleted_at is null
                    " . $lastPlaces . "
                group by places.id
            ) as t
            where
              ticket is null
            ;
            "
        );

        return $place[0]->id;
    }

    public function isFreePlace($from, $to, $place, $exept = null)
    {
        $places = DB::select(
            "
                select
                    t_r.id as ticket
                from places
                       LEFT JOIN trips ON trips.scheme_id = places.scheme_id AND trips.id = '$this->id' AND trips.deleted_at is null
                       LEFT JOIN route_points rp_from ON trips.route_id = rp_from.route_id AND rp_from.from_point_id = '$from' AND rp_from.deleted_at is null
                       LEFT JOIN route_points rp_to ON trips.route_id = rp_to.route_id AND rp_to.to_point_id = '$to' AND rp_to.deleted_at is null
                       LEFT JOIN route_points rp ON trips.route_id = rp.route_id AND rp.number BETWEEN rp_from.number AND rp_to.number AND rp.deleted_at is null
                       LEFT JOIN tickets t ON t.trip_id = trips.id AND t.place_id = places.id AND t.variant_id != 3 AND t.deleted_at is null AND t.id != '$exept'
                       LEFT JOIN route_points t_rp_from ON trips.route_id = t_rp_from.route_id AND t_rp_from.from_point_id = t.from_point_id AND t_rp_from.deleted_at is null
                       LEFT JOIN route_points t_rp_to ON trips.route_id = t_rp_to.route_id AND t_rp_to.to_point_id = t.to_point_id AND t_rp_to.deleted_at is null
                       LEFT JOIN tickets t_r ON t_r.trip_id = trips.id AND t_r.place_id = places.id AND t_r.variant_id != 3 AND t_r.deleted_at is null
                                                AND t_rp_from.number < rp_to.number AND t_rp_to.number > rp_from.number
                where
                    places.deleted_at is null
                    AND places.id = '$place'
                group by places.id
            ;
            "
        );

        if ($places[0]->ticket)
            return false;
        else
            return true;
    }

    public function isCriticalPlacesNumber($count = null){
        $count = $count ? $count : $this->getFreePlacesCount();
        if ($count < Settings::find(1)->critical_places_number) // get critical number of places
            return true;
        return false;
    }

    public function setCritical(){
        $this->update(['is_critical' => 1]);
        if (Settings::find(1)->notification_email)
            Mail::to(Settings::find(1)->notification_email)->send(new CriticalPlacesNumberMail($this->id)); // send email "critical number of places"
    }

    public function getPlaces($from, $to){
        $trip = $this->id;
        $places = DB::select(
            "
            select
                places.x
                , places.y
                , places.name
                , places.id
                , trips.id as trip_id
                , COUNT(DISTINCT(t_r.id)) as tickets_on_place
                , GROUP_CONCAT(distinct t_r.id) as tickets_id
            from places
                   LEFT JOIN trips ON trips.scheme_id = places.scheme_id AND trips.id = '$trip' AND trips.deleted_at is null
                   LEFT JOIN route_points rp_from ON trips.route_id = rp_from.route_id AND rp_from.from_point_id = '$from' AND rp_from.deleted_at is null
                   LEFT JOIN route_points rp_to ON trips.route_id = rp_to.route_id AND rp_to.to_point_id = '$to' AND rp_to.deleted_at is null
                   LEFT JOIN route_points rp ON trips.route_id = rp.route_id AND rp.number BETWEEN rp_from.number AND rp_to.number AND rp.deleted_at is null
                   LEFT JOIN tickets t ON t.trip_id = trips.id AND t.place_id = places.id AND t.variant_id != 3 AND t.deleted_at is null
                   LEFT JOIN route_points t_rp_from ON trips.route_id = t_rp_from.route_id AND t_rp_from.from_point_id = t.from_point_id AND t_rp_from.deleted_at is null
                   LEFT JOIN route_points t_rp_to ON trips.route_id = t_rp_to.route_id AND t_rp_to.to_point_id = t.to_point_id AND t_rp_to.deleted_at is null
                   LEFT JOIN tickets t_r ON t_r.trip_id = trips.id AND t_r.place_id = places.id AND t_r.variant_id != 3 AND t_r.deleted_at is null 
                                            AND t_rp_from.number < rp_to.number AND t_rp_to.number > rp_from.number
            where
                places.deleted_at is null
            group by places.id
            ;
            "
        );

        /*$results = [];
        foreach ($places as $place){
            $results[$place->id] = $place;
        }
        return $results;*/
        return $places;
    }

    public function getSalePlaces(){
        return Place::leftJoin('tickets', function ($join) {
            $join->on('tickets.place_id', '=', 'places.id')->where('tickets.trip_id', '=', $this->id);
        })
            ->where('places.scheme_id', $this->scheme_id)
            ->whereNotNull('tickets.id')
            ->pluck('places.name', 'places.id')
            ->all();
    }

    public static function getAll(){
        return self::pluck('route_id', 'id');
    }

    public function placesInScheme($from, $to){
        $results = [];
        foreach($this->getPlaces($from, $to) as $place){

            $tickets_id = explode(',', $place->tickets_id);
            if(isset($tickets_id[0])) $firstTicket = Ticket::find($tickets_id[0]);

            $client = '';
            if(isset($firstTicket->client))
                $client = $firstTicket->client->name1.' '.$firstTicket->client->name2.' '.$firstTicket->client->name3.' '.$firstTicket->client->phone;
            elseif(isset($firstTicket->agent_id))
                $client = $firstTicket->agent->name . ': ' . $firstTicket->number;

            $results[] = [
                'x' => $place->x,
                'y' => $place->y,
                'num' => $place->name,
                'id' => $place->id,
                'variant_id' => isset($firstTicket) ? $firstTicket->variant_id : -1,
                'status' => $place->tickets_on_place == 0 ? 0 : 1,
                'client' => $client
            ];
        }
        return $results;
    }

    /**
     * Строка название маршрута из города в город
     * @return string
     */
    public function endPointsName(){
        $first_point = $this->route->getFromPoint();
        $end_point = $this->route->getToPoint();
        return $first_point->from->city->name.'('.date('H:i', mktime(0, 0, $first_point->getRealFromTimeInSeconds())).')'.' -> '.$end_point->to->city->name.'('.date('H:i', mktime(0, 0, $end_point->getRealToTimeInSeconds())).')';
    }

    public function getDateAsCarbon(){
        return Carbon::parse($this->date);
    }

    public function getAgentsCount(){
        return Agent::select('agents.name', DB::raw('COUNT(tickets.id) as count'))
            ->join('tickets', 'agents.id', '=', 'tickets.agent_id')
            ->where('tickets.trip_id', $this->id)
            ->where('tickets.variant_id', '<', 2)
            ->whereNull('tickets.deleted_at')
            ->groupBy('name')
            ->get();
    }

    public function getAgents(){
        $gdamaler = Agent::where('id', Agent::GDAMALER_ID)->pluck('name', 'id')->toArray();
        $agents = Agent::select('agents.id', 'agents.name')
            ->join('trip_quotas', 'agents.id', '=', 'trip_quotas.agent_id')
            ->where('trip_quotas.trip_id', $this->id)
            ->where('trip_quotas.amount', '<>', 0)
            ->whereNull('trip_quotas.deleted_at')
            ->pluck('name', 'id')
            ->toArray();
        foreach($agents as $agent_id => $agent){
            $quota = $this->getAgentQuotaAmount($agent_id);
            $ticketCount = $this->getAgentTicketsAmount($agent_id);
            $agents[$agent_id] = $agent . ' ' . $quota . '/' . $ticketCount . '/' . ($quota - $ticketCount);
        }
        $agents = $gdamaler + $agents;
        return $agents;
    }

    public function getAgentQuotaAmount($agent_id){
        $quota = $this->hasMany('App\Models\TripQuota', 'trip_id')
            ->where('agent_id', $agent_id)
            ->first();
        if($quota)
            return $quota->amount;
        return '';
    }

    public function getAgentTicketsAmount($agent_id){
        return $this->hasMany('App\Models\Ticket', 'trip_id')
            ->where('variant_id', '<', 2)
            ->where('agent_id', $agent_id)
            ->count();
    }

    public function blockedPlacesInScheme(){
        $results = [];
        foreach($this->getBlockedPlaces() as $place){
            $results[] = [
                'x' => $place->x,
                'y' => $place->y,
                'num' => $place->name,
                'id' => $place->id,
                'status' => $place->blocked == 0 ? 0 : 1
            ];
        }
        return $results;
    }

    public function getBlockedPlaces(){
        $places = DB::select(
            "
            select
              places.*
              , IF((blocked.id is not null), 1, 0) as blocked
            from places
              LEFT JOIN tickets blocked ON blocked.trip_id = '$this->id'
                AND blocked.place_id = places.id
                AND blocked.variant_id = 2
                AND blocked.deleted_at is null
            where
              places.deleted_at is null
            group by places.id
            order by places.id
            ;
            "
        );
        return $places;
    }
}
