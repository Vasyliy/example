<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

/**
 * Class Schedule
 * @package App\Models
 * @version July 17, 2018, 6:07 am UTC
 *
 * @property integer route_id
 * @property string patern
 * @property string time
 */
class Schedule extends Model
{
    use SoftDeletes;

    public $table = 'schedules';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'route_id',
        'patern',
        'time',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'route_id' => 'integer',
        'patern' => 'string',
        'time' => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'patern' => 'required',
        'time' => 'required',
    ];

    public static $messages = [
        'patern.required' => 'Необходимо указать дни отправки',
        'time.required' => 'Необходимо указать время отправления',
    ];

    public function route(){
        return $this->belongsTo('App\Models\Route', 'route_id');
    }

    /**
     * Парсит расписание, возвращает массив дней недели в которые происходит рейсы
     * @return array
     */
    public function parsePatern(){
        return explode(',', $this->patern);
    }

    public function getTimeAttribute($value)
    {
        return Carbon::parse($value)->format('H:i');
    }

    public function setTimeAttribute($value)
    {
        $this->attributes['time'] = Carbon::createFromFormat('H:i', $value)->toTimeString();
    }
}
