<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Settings
 * @package App\Models
 * @version August 19, 2018, 7:50 am UTC
 *
 * @property string baggage_comment
 * @property string additional_comment
 * @property string return_comment
 * @property string agreement_comment
 * @property string map_comment
 */
class Settings extends Model
{
    use SoftDeletes;

    public $table = 'settings';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'baggage_comment',
        'additional_comment',
        'return_comment',
        'agreement_comment',
        'map_comment',
        'notification_email',
        'critical_places_number'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'baggage_comment' => 'string',
        'additional_comment' => 'string',
        'return_comment' => 'string',
        'agreement_comment' => 'string',
        'map_comment' => 'string',
        'notification_email' => 'string',
        'critical_places_number' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'notification_email' => 'nullable|email',
    ];

    public static $messages = [
        'notification_email.email' => 'Email для уведомлений должен быть действительным электронным адресом.',
    ];
    
}
