<?php

namespace App\Models;

use Carbon\Carbon;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

/**
 * Class Route
 * @package App\Models
 * @version July 9, 2018, 9:37 am UTC
 *
 * @property string name
 * @property integer scheme_id
 * @property string|\Carbon\Carbon started
 * @property string|\Carbon\Carbon finished
 * @property string note
 * @property string phones
 * @property integer status
 */
class Route extends Model
{
    use SoftDeletes, Translated;

    public $table = 'routes';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    const STATUS_ACTIVE = 1;
    const STATUS_PASSIVE = 0;

    const statuses = [
        self::STATUS_PASSIVE => 'Не Активный',
        self::STATUS_ACTIVE => 'Активный',
    ];

    protected $dates = [
        'started',
        'finished',
        'deleted_at',

    ];


    public $fillable = [
        'name',
        'scheme_id',
        'started',
        'finished',
        'note',
        'phones',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function route_points(){
        return $this->hasMany('App\Models\RoutePoint', 'route_id');
    }

    public function route_prices(){
        return $this->hasMany('App\Models\RoutePrice', 'route_id');
    }

    public function scheme(){
        return $this->belongsTo('App\Models\Scheme', 'scheme_id');
    }

    public function schedule(){
        return $this->hasOne('App\Models\Schedule', 'route_id');
    }

    public static function getAll(){
        return self::pluck('name', 'id');
    }

    public function allPoints(){
            $points = Point::leftJoin('route_points', 'route_points.from_point_id', '=', 'points.id')
                ->where('route_points.route_id', '=', $this->id)
                ->get();
            return $points;
    }

    /**
     * Номер следующего отрезка в маршруте
     */
    public function getNumber(){
        $route_id = $this->id;
        $number = DB::select("
            select
              if(max(number) is null, 1, max(number+1)) as number
            from route_points
            where route_points.route_id = '$route_id';
        ");
        return $number[0]->number;
    }

    public static function cloneRoute($route){
        $route = Route::find($route);
        if(isset($route)){
            // Клонируем сам маршрут
            /*
            $new_route = new Route();
            $new_route->setAttributes($route->getAttributes());
            var_dump($new_route->getAttributes());
            */
            //$new_route->save();
            $new_route = $route->replicate();
            $new_route->status = Route::STATUS_PASSIVE;
            $new_route->push();

            // Копирование точек маршрута
            foreach ($route->route_points as $route_point){
                $new_route_point = $route_point->replicate();
                $new_route_point->route_id = $new_route->id;
                $new_route_point->push();
            }

            // Копирование прайса маршрута
            foreach ($route->route_prices as $route_price){
                $new_route_price = $route_price->replicate();
                $new_route_price->route_id = $new_route->id;
                $new_route_price->push();
            }

            return $new_route;
        }
        return null;
    }

    public static function generateTrips($route = null){
        $from_date = Carbon::today();
        $to_date = (new Carbon())->addMonth(3);
        $created = false;
        if($route)
            $routes = Route::where('status', '=', Route::STATUS_ACTIVE)
                ->where('id', $route)
                ->get();
        else
            $routes = Route::where('status', '=', Route::STATUS_ACTIVE)
                ->get();

        foreach ($routes as $route){
            if(isset($route->schedule)){    // Если есть расписание то выставляем
                $schedule = $route->schedule;
                $current_date = $from_date->copy();
                while (
                    $current_date <= $to_date
                    AND $current_date <= $route->finished
                ){
                    if(in_array($current_date->dayOfWeek, $schedule->parsePatern()) && $current_date >= $route->started){
                        $trip = Trip::where('route_id', $route->id)
                            ->where('date', $current_date->setTimeFromTimeString($schedule->time))
                            ->first();
                        if(!isset($trip)){
                            $trip = new Trip;
                            $trip->route_id = $route->id;
                            $trip->scheme_id = $route->scheme_id;
                            $trip->bus_id = null;
                            $trip->driver_id = null;
                            $trip->status = Trip::STATUSES[0];
                            $trip->phone = '';
                            $trip->date = $current_date->setTimeFromTimeString($schedule->time)->format('d-m-Y H:i');
                            $trip->free_places = $route->scheme->places->count();
                            $trip->save();
                            $created = true;

                            $blockedPlaces = RouteBlockedPlace::where('route_id', $route->id)->get();
                            foreach ($blockedPlaces as $blockedPlace) {
                                $ticket = new Ticket();
                                $ticket->trip_id = $trip->id;
                                $ticket->from_point_id = $route->getFromPoint()->from_point_id;
                                $ticket->to_point_id = $route->getToPoint()->to_point_id;
                                $ticket->place_id = $blockedPlace->place_id;
                                $ticket->agent_id = 1;
                                $ticket->type_id = 1;
                                $ticket->variant_id = 2;
                                $ticket->save();
                            }

                            $quotas = RouteQuota::where('route_id', $route->id)->get();
                            foreach ($quotas as $quota) {
                                TripQuota::setQuota($trip->id, $quota->agent_id, $quota->amount);
                            }
                        }
                    }
                    $current_date->addDay(1);
                }
            }
        }

        return $created;
    }

    public function getAllFromPoints(){
        return Point::getAllfromRouteModels($this->id);
    }

    public function getAllToPoints(){
        return Point::getAlltoRouteModels($this->id);
    }

    public function getFromPoint(){
        return Cache::rememberForever('route_from_point' . $this->id, function () {
            return $this->route_points()->where('number', $this->route_points()->min('number'))->first();
        });
    }

    public function getToPoint(){
        return Cache::rememberForever('route_to_point' . $this->id, function () {
            return $this->route_points()->where('number', $this->route_points()->max('number'))->first();
        });
    }

    public function blockedPlacesInScheme(){
        $results = [];
        foreach($this->getBlockedPlaces() as $place){
            $results[] = [
                'x' => $place->x,
                'y' => $place->y,
                'num' => $place->name,
                'id' => $place->id,
                'status' => $place->blocked == 0 ? 0 : 1
            ];
        }
        return $results;
    }

    public function getBlockedPlaces(){
        $places = DB::select(
            "
            select
              places.*
              , IF((blocked.id is not null), 1, 0) as blocked
            from places
              LEFT JOIN routes ON routes.scheme_id = places.scheme_id AND routes.id = '$this->id' AND routes.deleted_at is null
              LEFT JOIN route_blocked_places blocked ON blocked.route_id = routes.id AND blocked.place_id = places.id AND blocked.deleted_at is null
            where
              places.deleted_at is null
            group by places.id
            order by places.id
            ;
            "
        );
        return $places;
    }

    public function getAgentQuotaAmount($agent_id){
        $quota = $this->hasMany('App\Models\RouteQuota', 'route_id')
            ->where('agent_id', $agent_id)
            ->first();
        if($quota)
            return $quota->amount;
        return '';
    }
}
