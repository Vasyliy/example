<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

trait Translated
{
    /**
     * Есть ли перевод на данный язык
     * @param $lang
     * @return bool
     */
    public function isTranslate($lang){
        $translate = DB::table($this->table.'_names')
            ->where('item_id', $this->id)
            ->where('lang_id', $lang)
            ->value('name');
        if(isset($translate)){
            return true;
        }
        return false;
    }

    /**
     * Получить перевод
     * @param $lang
     * @return string
     */
    public function getTranslate($lang){
        $translate = DB::table($this->table.'_names')
            ->where('item_id', $this->id)
            ->where('lang_id', $lang)
            ->value('name');
        if(!isset($translate)){
            $translate = '';
        }
        return $translate;
    }

    /**
     * Сохранить перевод
     * @param $lang
     * @return string
     */
    public function setTranslate($lang, $translate){
        $translate_item = DB::table($this->table.'_names')
            ->where('item_id', $this->id)
            ->where('lang_id', $lang)
            ->first();
        if(isset($translate_item)){
            // Редактируем перевод
            DB::table($this->table.'_names')
                ->where('id', $translate_item->id)
                ->update([
                    'name' => $translate
                ]);
        }else{
            // Добавляем перевод
            DB::table($this->table.'_names')
                ->insert([
                    'item_id' => $this->id,
                    'lang_id' => $lang,
                    'name' => $translate
                ]);
        }
    }
}