<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class User
 * @package App\Models
 * @version August 29, 2018, 7:21 am UTC
 *
 * @property string name
 * @property string email
 * @property string password
 * @property string remember_token
 * @property integer admin
 * @property integer role
 */
class User extends Authenticatable
{
    use SoftDeletes, Notifiable;

    const TABLE_NAME = 'users';
    public $table = self::TABLE_NAME;

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    const ROLE_SIMPLE_ID = 0;
    const ROLE_DISPATCHER_ID = 1;
    const ROLE_AGENT_ID = 2;
    const ROLES = [
        self::ROLE_SIMPLE_ID => 'Обычный',
        self::ROLE_DISPATCHER_ID => 'Диспетчер',
        self::ROLE_AGENT_ID => 'Агент',
    ];

    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'email',
        'password',
        'remember_token',
        'admin',
        'role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'email' => 'string',
        'password' => 'string',
        'remember_token' => 'string',
        'admin' => 'integer',
        'role' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|string|unique:users,name,{$id},id,deleted_at,NULL',
        'email' => 'required|email|unique:users,email,{$id},id,deleted_at,NULL',
        'password' => 'required|string'
    ];

    public static function getAll(){
        return self::pluck('name', 'id');
    }
}
