<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class DispatcherPhone
 * @package App\Models
 * @version August 19, 2018, 7:49 am UTC
 *
 * @property string phone
 */
class DispatcherPhone extends Model
{
    use SoftDeletes;

    public $table = 'dispatcher_phones';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'phone'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'phone' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public static function getAll(){
        return self::pluck('phone', 'id');
    }

    public static function forTicket(){
        $result = '';
        $phones = self::all();
        if(isset($phones[0])){
            $result .= $phones[0]->phone;
        }

        if(isset($phones[1])){
            $result .= ', ' . $phones[1]->phone;
        }

        return $result;
    }
}
