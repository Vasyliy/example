<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Lang
 * @package App\Models
 * @version June 27, 2018, 6:05 am UTC
 *
 * @property string name
 * @property string short
 */
class Lang extends Model
{
    use SoftDeletes;

    const TABLE_NAME = 'langs';
    public $table = self::TABLE_NAME;

    const ID_LANG_EN = 1;
    const ID_LANG_RU = 2;
    const ID_LANG_UA = 3;
    const ID_LANG_PO = 4;
    const ID_LANG_CZ = 5;

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'short'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'short' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|unique:langs|max:255',
        'short' => 'required|unique:langs|max:2',
    ];

    
}
