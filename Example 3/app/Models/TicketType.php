<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class TicketType
 * @package App\Models
 * @version July 4, 2018, 12:30 pm UTC
 *
 * @property string name
 * @property integer discount
 * @property string note
 */
class TicketType extends Model
{
    use SoftDeletes, Translated;

    public $table = 'ticket_types';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'short',
        'discount',
        'note'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'short' => 'string',
        'discount' => 'integer',
        'note' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|unique:currencies|max:255',
        'discount' => 'required|min:0|max:100',
    ];

    public static function getAll(){
        return self::pluck('name', 'id');
    }
}
