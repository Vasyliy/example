<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class DriverPhone
 * @package App\Models
 * @version July 4, 2018, 4:29 pm UTC
 *
 * @property integer driver_id
 * @property string phone
 */
class DriverPhone extends Model
{
    use SoftDeletes;

    public $table = 'driver_phones';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'driver_id',
        'phone'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'driver_id' => 'integer',
        'phone' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        //'name' => 'required|max:3|min:3',
    ];

    
}
