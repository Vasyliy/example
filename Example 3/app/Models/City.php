<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class City
 * @package App\Models
 * @version July 3, 2018, 6:58 pm UTC
 *
 * @property integer country_id
 * @property string name
 */
class City extends Model
{
    use SoftDeletes, Translated;

    public $table = 'cities';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'country_id',
        'name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'country_id' => 'integer',
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function country(){
        return $this->belongsTo('App\Models\Country', 'country_id');
    }

    public static function getAllCities(){
        return self::pluck('name', 'id');
    }
}
