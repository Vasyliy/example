<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class Search extends Model
{
    public function getTrips($from, $to, $date_min, $date_max)
    {
        $routes_id = DB::table('routes')
            //->select(['routes.id'])
            ->leftJoin('route_points', 'route_points.route_id', '=', 'routes.id')
            ->leftJoin('route_prices', function($join) use ($from, $to){
                    $join->on('route_prices.route_id', '=', 'routes.id');
                    $join->on('route_prices.from_point_id', '=', DB::raw("'".$from."'"));
                    $join->on('route_prices.to_point_id', '=', DB::raw("'".$to."'"));
                })
            ->whereNotNull('route_prices.id')
            ->where(function($query) use ($from, $to){
                $query->where('route_points.from_point_id', '=', $from);
                $query->orWhere('route_points.to_point_id', '=', $to);
            })
            ->groupBy('routes.id')
            ->pluck('routes.id')
            ->toArray();

        //print_r($routes_id);

        $trips = Trip::leftJoin('trip_point_blocked', function($join) use ($from){
            $join->on('trip_point_blocked.trip_id', '=', 'trips.id');
            $join->on('trip_point_blocked.point_id', '=', DB::raw("'".$from."'"));
            $join->whereNull('trip_point_blocked.deleted_at');
        })
            ->whereIn('route_id', $routes_id)
            ->whereDate('date', '>=', $date_min)
            ->whereDate('date', '<=', $date_max)
            ->whereNull('trip_point_blocked.id')
            //->whereNull('trip_point_blocked.deleted_at')
            ->select('trips.*')
            ->paginate(20);

         return $trips;
    }
}
