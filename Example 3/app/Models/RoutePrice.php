<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class RoutePrice
 * @package App\Models
 * @version July 11, 2018, 2:34 pm UTC
 *
 * @property integer route_id
 * @property integer from_point_id
 * @property integer to_point_id
 * @property integer price
 * @property integer currency_id
 */
class RoutePrice extends Model
{
    use SoftDeletes;

    public $table = 'route_prices';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'route_id',
        'from_point_id',
        'to_point_id',
        'price',
        'currency_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'route_id' => 'integer',
        'from_point_id' => 'integer',
        'to_point_id' => 'integer',
        //'price' => 'integer',
        'currency_id' => 'integer',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function route(){
        return $this->belongsTo('App\Models\Route', 'route_id');
    }

    public function from(){
        return $this->belongsTo('App\Models\Point', 'from_point_id');
    }

    public function to(){
        return $this->belongsTo('App\Models\Point', 'to_point_id');
    }

    public function currency(){
        return $this->belongsTo('App\Models\Currency', 'currency_id');
    }


    // Преобразователь прайса
    public function getPriceAttribute($value)
    {
        return $value / 100;
    }

    // Преобразователь прайса
    public function setPriceAttribute($value)
    {
        $this->attributes['price'] = intval($value * 100);
    }

}
