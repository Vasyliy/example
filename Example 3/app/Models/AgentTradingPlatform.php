<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class AgentTradingPlatform
 * @package App\Models
 * @version September 28, 2018, 2:17 pm UTC
 *
 * @property integer agent_id
 * @property integer trading_platform_id
 * @property string options
 */
class AgentTradingPlatform extends Model
{
    use SoftDeletes;

    public $table = 'agent_trading_platforms';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'agent_id',
        'trading_platform_id',
        'options'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'agent_id' => 'integer',
        'trading_platform_id' => 'integer',
        'options' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'agent_id' => 'required|numeric',
        'trading_platform_id' => 'required|numeric',
        'options' => 'nullable'
    ];

    public function tradingPlatform(){
        return $this->belongsTo('App\Models\TradingPlatform', 'trading_platform_id');
    }

    public function agent(){
        return $this->belongsTo('App\Models\Agent', 'agent_id');
    }

    public function routes(){
        return $this->hasMany('App\Models\TradingPlatformRoutes', 'agent_trading_platform_id');
    }
}
