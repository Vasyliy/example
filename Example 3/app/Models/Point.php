<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

/**
 * Class Point
 * @package App\Models
 * @version July 5, 2018, 6:33 pm UTC
 *
 * @property integer city_id
 * @property string name
 */
class Point extends Model
{
    use NewSoftDeletes, Translated;

    public $table = 'points';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'city_id',
        'name',
        'lat',
        'lng'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'city_id' => 'integer',
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function city(){
        return $this->belongsTo('App\Models\City', 'city_id');
    }

    public static function getAll(){
        //return self::pluck('name', 'id');
        return self::leftJoin('cities', 'points.city_id', '=', 'cities.id')
            //->whereNull('points.deleted_at')
            ->select(DB::raw("CONCAT(cities.name, ' - ', points.name, '') AS point"),'points.id')
            ->pluck('point', 'points.id')
            ->all();
    }

    public static function getAllfromRoute($route_id){
        return RoutePoint::join('points', 'route_points.from_point_id', '=', 'points.id')
            ->where('route_points.route_id', '=', $route_id)
            ->leftJoin('cities', 'points.city_id', '=', 'cities.id')
            ->select(DB::raw("CONCAT(cities.name, ' (', points.name, ')') AS point"),'points.id')
            ->pluck('point', 'points.id')
            ->all();
    }

    public static function getAlltoRoute($route_id){
        return RoutePoint::join('points', 'route_points.to_point_id', '=', 'points.id')
            ->where('route_points.route_id', '=', $route_id)
            ->leftJoin('cities', 'points.city_id', '=', 'cities.id')
            ->select(DB::raw("CONCAT(cities.name, ' (', points.name, ')') AS point"),'points.id')
            ->pluck('point', 'points.id')
            ->all();
    }

    public static function getAllfromRouteModels($route_id){
        return RoutePoint::join('points', 'route_points.from_point_id', '=', 'points.id')
            ->where('route_points.route_id', '=', $route_id)
            ->leftJoin('cities', 'points.city_id', '=', 'cities.id')
            ->get();
    }

    public static function getAlltoRouteModels($route_id){
        return RoutePoint::join('points', 'route_points.to_point_id', '=', 'points.id')
            ->where('route_points.route_id', '=', $route_id)
            ->leftJoin('cities', 'points.city_id', '=', 'cities.id')
            ->get();
    }

    public static function getAllfrom(){
        return RoutePoint::join('points', 'route_points.from_point_id', '=', 'points.id')
            ->leftJoin('cities', 'points.city_id', '=', 'cities.id')
            //->whereNull('points.deleted_at')
            ->select(DB::raw("CONCAT(cities.name, ' (', points.name, ')') AS point"),'points.id')
            ->pluck('point', 'points.id')
            ->all();
    }

    public static function getAllto(){
        return RoutePoint::join('points', 'route_points.to_point_id', '=', 'points.id')
            ->leftJoin('cities', 'points.city_id', '=', 'cities.id')
            ->select(DB::raw("CONCAT(cities.name, ' (', points.name, ')') AS point"),'points.id')
            ->pluck('point', 'points.id')
            ->all();
    }

    public static function getPointFullName($point_id){
        $point = RoutePoint::join('points', 'route_points.from_point_id', '=', 'points.id')
            ->leftJoin('cities', 'points.city_id', '=', 'cities.id')
            ->where('points.id', $point_id)
            ->select(DB::raw("CONCAT(cities.name, ' (', points.name, ')') AS name"))
            ->first();

        return $point->name;
    }

    public function isBlockedInTrip($trip){
        $blocked_point = TripPointBlocked::where([
            'point_id'=>$this->id,
            'trip_id'=>$trip
        ])->first();

        return isset($blocked_point);
    }

    public function blockedInTripId($trip){
        $blocked_point = TripPointBlocked::where([
            'point_id'=>$this->id,
            'trip_id'=>$trip
        ])->first();

        return isset($blocked_point)?$blocked_point->id:0;
    }
}
