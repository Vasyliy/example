<?php

namespace App\Models;

use Carbon\Carbon;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Client
 * @package App\Models
 * @version July 25, 2018, 9:12 am UTC
 *
 * @property integer user_id
 * @property string name1
 * @property string name2
 * @property string name3
 * @property string phone
 * @property string phone2
 * @property string email
 * @property string comment
 */
class Client extends Model
{
    use SoftDeletes;

    public $table = 'clients';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'user_id',
        'name1',
        'name2',
        'name3',
        'phone',
        'phone2',
        'email',
        'comment',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'name1' => 'string',
        'name2' => 'string',
        'name3' => 'string',
        'phone' => 'string',
        'phone2' => 'string',
        'email' => 'string',
        'comment' => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name1' => 'required|max:255',
        'name2' => 'required|max:255',
        'email' => 'nullable|email',
    ];

    public static $messages = [
        'name1' => 'Фамилия должна быть заполнена',
        'name2' => 'Имя должно быть заполнено',
        'email' => 'E-Mail должен быть правильный',
    ];

    public static $names = [
        'name1' => 'Фамилия',
        'name2' => 'Имя',
        'name3' => 'Отчество',
        'phone' => 'Номер телефона №1',
        'phone2' => 'Номер телефона №2',
        'email' => 'E-Mail',
    ];

    public static function getAll(){
        $list = self::pluck('name1', 'id');
        $list->prepend('Без пассажира', 0);
        return $list;
    }

    public static function getAllNames(){
        $list = self::whereNotNull('name1')->pluck('name1', 'name1');
        return $list;
    }

    public static function getAllPhones(){
        $list = self::whereNotNull('phone')->pluck('phone', 'phone');
        return $list;
    }

    public function lastTicketDate(){
        $ticket_date = Ticket::join('trips', 'tickets.trip_id', '=', 'trips.id')
            ->where('client_id', '=', $this->id)
            ->where('trips.date', '<', Carbon::now())
            ->orderBy('trips.date', 'desc')
            ->select('trips.date')
            ->first();

        if(isset($ticket_date)){
            return Carbon::parse($ticket_date->date)->format('d-m-Y H:i');
        }else{
            return '-';
        }
    }
}
