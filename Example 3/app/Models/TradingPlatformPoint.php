<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class TradingPlatformPoint
 * @package App\Models
 * @version September 27, 2018, 5:53 pm UTC
 *
 * @property integer trading_platform_id
 * @property integer point_id
 * @property string name
 */
class TradingPlatformPoint extends Model
{
    use SoftDeletes;

    public $table = 'trading_platform_points';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'trading_platform_id',
        'point_id',
        'name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'trading_platform_id' => 'integer',
        'point_id' => 'integer',
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'trading_platform_id' => 'required|numeric',
        'point_id' => 'required|numeric',
        'name' => 'required|string'
    ];

    public static $messages = [
        'name.required' => 'Необходимо указать название',
    ];

    public function tradingPlatform(){
        return $this->belongsTo('App\Models\TradingPlatform', 'trading_platform_id');
    }

    public function point(){
        return $this->belongsTo('App\Models\Point', 'point_id');
    }
}
