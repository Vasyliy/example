<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Scheme
 * @package App\Models
 * @version July 9, 2018, 10:05 am UTC
 *
 * @property string name
 */
class Scheme extends Model
{
    use SoftDeletes, Translated;

    public $table = 'schemes';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function places(){
        return $this->hasMany('App\Models\Place', 'scheme_id');
    }

    public static function getAll(){
        return self::pluck('name', 'id');
    }


}
