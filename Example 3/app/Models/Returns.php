<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Returns
 * @package App\Models
 * @version August 21, 2018, 10:47 am UTC
 *
 * @property string interval
 * @property integer factor
 */
class Returns extends Model
{
    use SoftDeletes;

    public $table = 'returns';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'interval',
        'factor'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'interval' => 'string',
        'factor' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    // Преобразователь прайса
    public function getFactorAttribute($value)
    {
        return number_format($value / 100, 2, '.', '');
    }

    // Преобразователь прайса
    public function setFactorAttribute($value)
    {
        $this->attributes['factor'] = intval($value * 100);
    }
}
