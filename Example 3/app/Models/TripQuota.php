<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class TripQuota
 * @package App\Models
 * @version October 11, 2018, 9:05 pm UTC
 *
 * @property integer trip_id
 * @property integer agent_id
 * @property integer amount
 */
class TripQuota extends Model
{
    use SoftDeletes;

    public $table = 'trip_quotas';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'trip_id',
        'agent_id',
        'amount'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'trip_id' => 'integer',
        'agent_id' => 'integer',
        'amount' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'trip_id' => 'integer',
        'agent_id' => 'integer',
        'amount' => 'integer'
    ];

    public static function setQuota($trip_id, $agent_id, $amount){
        self::updateOrCreate(
            ['trip_id' => $trip_id, 'agent_id' => $agent_id],
            ['amount' => $amount]
        );
        return true;
    }

    public static function getQuota($trip_id, $agent_id) {
        $amount = self::where('trip_id', $trip_id)->where('agent_id', $agent_id)->first()->amount;
    }
}
