<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Place
 * @package App\Models
 * @version July 11, 2018, 10:05 am UTC
 *
 * @property integer sсheme_id
 * @property string name
 * @property integer status
 */
class Place extends Model
{
    use SoftDeletes;

    public $table = 'places';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'sсheme_id',
        'name',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'sсheme_id' => 'integer',
        'name' => 'string',
        'status' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function scheme(){
        return $this->belongsTo('App\Models\Scheme', 'scheme_id');
    }


}
