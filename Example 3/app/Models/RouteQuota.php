<?php

namespace App\Models;

use Carbon\Carbon;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class RouteQuota
 * @package App\Models
 * @version October 11, 2018, 8:46 pm UTC
 *
 * @property integer route_id
 * @property integer agent_id
 * @property integer amount
 */
class RouteQuota extends Model
{
    use SoftDeletes;

    public $table = 'route_quotas';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'route_id',
        'agent_id',
        'amount'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'route_id' => 'integer',
        'agent_id' => 'integer',
        'amount' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'route_id' => 'integer',
        'agent_id' => 'integer',
        'amount' => 'integer'
    ];

    public static function boot()
    {
        parent::boot();

        self::created(function($model){
            $model->setTripsQuotas();
        });

        self::updated(function($model){
            $model->setTripsQuotas();
        });
    }

    public static function setQuota($route_id, $agent_id, $amount){
        self::updateOrCreate(
            ['route_id' => $route_id, 'agent_id' => $agent_id],
            ['amount' => $amount]
        );
        return true;
    }

    public function setTripsQuotas(){
        $today = Carbon::today();
        $trips = Trip::where('route_id', $this->route_id)->where('date', '>=', $today)->pluck('id');
        foreach($trips as $trip){
            TripQuota::setQuota($trip, $this->agent_id, $this->amount);
        }
    }
}
