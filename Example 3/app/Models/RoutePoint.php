<?php

namespace App\Models;

use Carbon\Carbon;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Cache;

/**
 * Class RoutePoint
 * @package App\Models
 * @version July 9, 2018, 9:49 am UTC
 *
 * @property integer route_id
 * @property integer from_point_id
 * @property integer to_point_id
 * @property integer number
 * @property integer price
 * @property integer distance
 * @property integer from_day
 * @property string|\Carbon\Carbon from_time
 * @property integer to_day
 * @property string|\Carbon\Carbon to_time
 */
class RoutePoint extends Model
{
    use SoftDeletes;

    public $table = 'route_points';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'route_id',
        'from_point_id',
        'to_point_id',
        'number',
        'price',
        'distance',
        'from_day',
        'from_time',
        'to_day',
        'to_time'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'route_id' => 'integer',
        'from_point_id' => 'integer',
        'to_point_id' => 'integer',
        'number' => 'integer',
        'price' => 'integer',
        'distance' => 'integer',
        'from_day' => 'integer',
        'to_day' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public static function boot()
    {
        parent::boot();

        self::created(function($model){
            Cache::forget('route_from_point' . $model->route_id);
            Cache::forget('route_to_point' . $model->route_id);
        });
        self::updated(function($model){
            Cache::forget('route_from_point' . $model->route_id);
            Cache::forget('route_to_point' . $model->route_id);
        });
        self::deleted(function($model){
            Cache::forget('route_from_point' . $model->route_id);
            Cache::forget('route_to_point' . $model->route_id);
        });
    }

    public function route(){
        return $this->belongsTo('App\Models\Route', 'route_id');
    }

    public function from(){
        return $this->belongsTo('App\Models\Point', 'from_point_id');
    }

    public function to(){
        return $this->belongsTo('App\Models\Point', 'to_point_id');
    }

    public function getFullFromAttribute()
    {
        return $this->from->city->name . ' (' . $this->from->name . ')';
    }

    public function getFullToAttribute()
    {
        return $this->to->city->name . ' (' . $this->to->name . ')';
    }

    public function getRealFromTimeInSeconds()
    {
        return isset($this->route->schedule) ? Carbon::parse($this->route->schedule->time)->secondsSinceMidnight() + $this->from_time : $this->from_time;
    }

    public function getRealToTimeInSeconds()
    {
        return isset($this->route->schedule) ? Carbon::parse($this->route->schedule->time)->secondsSinceMidnight() + $this->to_time : $this->to_time;
    }

    public static function getOffsetTimeInSeconds($route_id, $time)
    {
        $result = Carbon::parse($time)->secondsSinceMidnight() - Carbon::parse(Route::find($route_id)->schedule->time)->secondsSinceMidnight();
        if ($result < 0)
            $result += 86400; // +24 hours

        return $result;
    }
}
