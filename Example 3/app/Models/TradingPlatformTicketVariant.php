<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class TradingPlatformTicketVariant
 * @package App\Models
 * @version September 27, 2018, 5:56 pm UTC
 *
 * @property integer trading_platform_id
 * @property integer variant_id
 * @property string name
 */
class TradingPlatformTicketVariant extends Model
{
    use SoftDeletes;

    public $table = 'trading_platform_ticket_variants';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'trading_platform_id',
        'variant_id',
        'name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'trading_platform_id' => 'integer',
        'variant_id' => 'integer',
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'trading_platform_id' => 'numeric|required',
        'variant_id' => 'numeric|required',
        'name' => 'string|required'
    ];

    public function tradingPlatform(){
        return $this->belongsTo('App\Models\TradingPlatform', 'trading_platform_id');
    }
}
