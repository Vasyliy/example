<?php

namespace App\Console;

use App\Models\TradingPlatform;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Schema;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\TripSchedule::class,
        Commands\UpdateTicketsFromBusComUa::class,
        Commands\UpdateTicketsFromInfoBus::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('trip:schedule')->dailyAt('06:00');

        if(Schema::hasTable('trading_platforms')) {
            $busComUa = TradingPlatform::find(TradingPlatform::BUS_COM_UA_ID);
            if($busComUa) {
                $schedule->command('updatetickets:buscomua')->cron('*/'.$busComUa->refresh_time_1.' * * * * *')->withoutOverlapping();
                $schedule->command('updatetickets:buscomua 1 2')->cron('*/'.$busComUa->refresh_time_2.' * * * * *')->withoutOverlapping();
                $schedule->command('updatetickets:buscomua 3 13')->cron('*/'.$busComUa->refresh_time_3.' * * * * *')->withoutOverlapping();
            }

            $infoBus = TradingPlatform::find(TradingPlatform::INFO_BUS_ID);
            if($infoBus) {
                $schedule->command('updatetickets:infobus')->cron('*/'.$infoBus->refresh_time_1.' * * * * *')->withoutOverlapping();
                $schedule->command('updatetickets:infobus 1 2')->cron('*/'.$infoBus->refresh_time_2.' * * * * *')->withoutOverlapping();
                $schedule->command('updatetickets:infobus 3 13')->cron('*/'.$infoBus->refresh_time_3.' * * * * *')->withoutOverlapping();
            }
        }
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
