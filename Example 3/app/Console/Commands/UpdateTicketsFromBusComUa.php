<?php

namespace App\Console\Commands;

use App\Http\Controllers\TradingPlatforms\BusComUaController;
use Illuminate\Console\Command;

class UpdateTicketsFromBusComUa extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'updatetickets:buscomua {from?} {to?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Обновить билеты из bus.com.ua';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        return BusComUaController::update($this->argument('from'), $this->argument('to'));
    }
}
