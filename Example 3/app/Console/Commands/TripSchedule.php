<?php

namespace App\Console\Commands;

use App\Models\Route;
use Illuminate\Console\Command;

class TripSchedule extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'trip:schedule';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Выставить рейсы согласно расписания';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        return Route::generateTrips();
    }
}
