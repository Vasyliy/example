<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\TradingPlatforms\InfoBusController;

class UpdateTicketsFromInfoBus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'updatetickets:infobus {from?} {to?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Обновить билеты из InfoBus';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        return InfoBusController::update($this->argument('from'), $this->argument('to'));
    }
}
