<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\AgentTradingPlatform;

class AddCidToAgentOptionsForBusComUa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $agents = AgentTradingPlatform::get();

        foreach ($agents as $agent) {
            $options = json_decode($agent->options);
            if ($options->username == "ГРЕЧАНЮК") {
                $options->cid = 'ГРЕЧАНЮК В.И.';
            } elseif ($options->username == "90679") {
                $options->cid = '90679';
            }
            $agent->options = json_encode($options);
            $agent->save();
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
