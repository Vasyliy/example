<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Models\RoutePoint;
use App\Models\Route;

class SetOffsetTimeForRoutePoints extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $routePoints = RoutePoint::all();
        foreach ($routePoints as $routePoint) {
            $routePoint->update([
                'from_time' => self::getOffsetTimeInSeconds($routePoint->route_id, $routePoint->from_time),
                'to_time' => self::getOffsetTimeInSeconds($routePoint->route_id, $routePoint->to_time),
            ]);
        }
    }

    public static function getOffsetTimeInSeconds($route_id, $time)
    {
        $route = Route::find($route_id);
        $result = isset($route->schedule) ? $time - \Carbon\Carbon::parse($route->schedule->time)->secondsSinceMidnight() : $time;
        if ($result < 0)
            $result += 86400; // +24 hours

        return $result;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
