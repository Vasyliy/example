<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFreePlacesForTrips extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trips', function(Blueprint $table){
            $table->integer("free_places")->length(3)->unsigned()->nullable()->after('is_critical');
        });

        $from = \Carbon\Carbon::today();
        $to = \Carbon\Carbon::today()->addMonth();
        $trips = \App\Models\Trip::whereBetween('date', [$from, $to])->get();
        foreach($trips as $trip){
            $trip->setFreePlacesCount();
        }
        \App\Models\Trip::where('date', '>=', $to)->update(['free_places' => 53]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trips', function (Blueprint $table) {
            $table->dropColumn("free_places");
        });
    }
}
