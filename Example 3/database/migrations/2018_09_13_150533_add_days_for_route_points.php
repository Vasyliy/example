<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDaysForRoutePoints extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('route_points', function(Blueprint $table){
            $table->integer('from_day')->default(0)->after('distance');
            $table->integer('to_day')->default(0)->after('from_time');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('route_points', function (Blueprint $table) {
            $table->dropColumn('to_day');
            $table->dropColumn('from_day');
        });
    }
}
