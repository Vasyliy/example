<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddedAnyLangTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Переводы справочников --------------------------------------------
        Schema::create('currencies_names', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('item_id');
            $table->integer('lang_id');
            $table->string('name');
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
        Schema::create('buses_names', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('item_id');
            $table->integer('lang_id');
            $table->string('name');
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
        Schema::create('schemes_names', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('item_id');
            $table->integer('lang_id');
            $table->string('name');
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
        Schema::create('places_names', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('item_id');
            $table->integer('lang_id');
            $table->string('name');
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
        Schema::create('drivers_names', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('item_id');
            $table->integer('lang_id');
            $table->string('name');
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
        Schema::create('agents_names', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('item_id');
            $table->integer('lang_id');
            $table->string('name');
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
        Schema::create('ticket_types_names', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('item_id');
            $table->integer('lang_id');
            $table->string('name');
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('currencies_names');
        Schema::dropIfExists('buses_names');
        Schema::dropIfExists('schemes_names');
        Schema::dropIfExists('places_names');
        Schema::dropIfExists('drivers_names');
        Schema::dropIfExists('agents_names');
        Schema::dropIfExists('ticket_types_names');
    }
}
