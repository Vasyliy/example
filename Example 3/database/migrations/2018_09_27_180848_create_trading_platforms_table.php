<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTradingPlatformsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trading_platforms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('refresh_time_1');
            $table->integer('refresh_time_2');
            $table->integer('refresh_time_3');
            $table->text('options')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('trading_platforms');
    }
}
