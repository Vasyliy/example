<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MaskForClientPhone extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $clients = \App\Models\Client::whereNotNull('phone')->get();
        foreach ($clients as $client) {
            $phone = str_replace(' ', '', $client->phone);
            if (substr($phone, 0, 1) == '0') {
                if (strlen($phone) == 10)
                    $client->update(['phone' => '+38 ' . substr($phone, 0, 3) . ' ' . substr($phone, 3, 3) . ' ' . substr($phone, 6)]);
            } elseif (substr($phone, 0, 4) == '+380') {
                if (strlen($phone) == 13)
                    $client->update(['phone' => substr($phone, 0, 3) . ' ' . substr($phone, 3, 3) . ' ' . substr($phone, 6, 3) . ' ' . substr($phone, 9)]);
            } elseif (substr($phone, 0, 3) == '380') {
                if (strlen($phone) == 12)
                    $client->update(['phone' => '+' . substr($phone, 0, 2) . ' ' . substr($phone, 2, 3) . ' ' . substr($phone, 5, 3) . ' ' . substr($phone, 8)]);
            } elseif (substr($phone, 0, 3) == '+48') {
                if (strlen($phone) == 12)
                    $client->update(['phone' => substr($phone, 0, 3) . ' ' . substr($phone, 3, 3) . ' ' . substr($phone, 6, 3) . ' ' . substr($phone, 9)]);
            } elseif (substr($phone, 0, 2) == '48') {
                if (strlen($phone) == 11)
                    $client->update(['phone' => '+' . substr($phone, 0, 2) . ' ' . substr($phone, 2, 3) . ' ' . substr($phone, 5, 3) . ' ' . substr($phone, 8)]);
            } elseif (substr($phone, 0, 4) == '+420') {
                if (strlen($phone) == 13)
                    $client->update(['phone' => substr($phone, 0, 3) . ' ' . substr($phone, 3, 3) . ' ' . substr($phone, 6, 3) . ' ' . substr($phone, 9)]);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
