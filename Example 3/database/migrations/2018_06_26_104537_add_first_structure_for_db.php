<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFirstStructureForDb extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('routes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('scheme_id')->default(0);
            $table->string('name');
            $table->timestamp('started')->nullable();
            $table->timestamp('finished')->nullable();
            $table->text('note')->nullable();
            $table->text('phones')->nullable();
            $table->integer('status')->default(1);
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
        Schema::create('routes_names', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('item_id');
            $table->integer('lang_id');
            $table->string('name');
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
        Schema::create('route_points', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('route_id');
            $table->integer('from_point_id');
            $table->integer('to_point_id');
            $table->integer('number')->default(0);
            $table->integer('price')->default(0);
            $table->integer('distance')->default(0);
            $table->integer('from_time')->default(0);
            $table->integer('to_time')->default(0);
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
        Schema::create('points', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('city_id');
            $table->string('name');
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
        Schema::create('points_names', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('item_id');
            $table->integer('lang_id');
            $table->string('name');
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
        Schema::create('route_prices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('route_id');
            $table->integer('from_point_id');
            $table->integer('to_point_id');
            $table->integer('price')->default(0);
            $table->integer('currency_id')->default(0);
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
        Schema::create('trips', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('route_id');
            $table->integer('scheme_id')->default(0);
            $table->integer('bus_id')->default(0);
            $table->integer('driver_id')->default(0);
            $table->integer('status')->default(0);
            $table->string('phone')->nullable();
            $table->dateTime('date');
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
        Schema::create('trip_point_blocked', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('trip_id');
            $table->integer('point_id');
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
        Schema::create('schedules', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('route_id');
            $table->string('patern')->default('');
            $table->time('time');
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
        Schema::create('tickets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('trip_id');
            $table->integer('from_point_id');
            $table->integer('to_point_id');
            $table->integer('place_id');
            $table->integer('client_id')->default(0);
            $table->integer('agent_id')->default(0);
            $table->integer('type_id');
            $table->integer('variant_id')->default(0);
            $table->string('ticket_viewer')->nullable();
            $table->integer('price')->default(0);
            $table->integer('currency_id')->default(0);
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->default(0);
            $table->string('name1');
            $table->string('name2');
            $table->string('name3')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->text('comment')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });


        // Справочники --------------------------------------------
        Schema::create('langs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('short');
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
        Schema::create('currencies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->default('');
            $table->string('short', 3);
            $table->integer('exchange')->default(100);
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
        Schema::create('buses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('scheme_id')->default(0);
            $table->string('name');
            $table->string('model')->nullable();
            $table->string('number')->nullable();
            $table->text('note')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
        Schema::create('schemes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
        Schema::create('places', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('scheme_id')->default(0);
            $table->string('name');
            $table->integer('x')->default(0);
            $table->integer('y')->default(0);
            $table->integer('status')->default(0);
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
        Schema::create('drivers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->default(0);
            $table->string('name');
            $table->string('license')->nullable();
            $table->text('note')->nullable();
            $table->integer('status')->default(0);
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
        Schema::create('driver_phones', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('driver_id');
            $table->string('phone');
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
        Schema::create('dispatcher_phones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('phone');
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
        Schema::create('agents', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->default(0);
            $table->string('name');
            $table->string('api')->nullable();
            $table->text('note')->nullable();
            $table->integer('status')->default(0);
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
        Schema::create('ticket_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('discount')->default(0);
            $table->text('note')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });

        // Настройки билета
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->text('baggage_comment')->nullable();
            $table->text('additional_comment')->nullable();
            $table->text('return_comment')->nullable();
            $table->text('agreement_comment')->nullable();
            $table->text('map_comment')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
        DB::table('settings')->insert([
            'id' => 1,
            'baggage_comment' => '1 шт. до 30 кг (40x40x80) - бесплатно, каждый след. от 10 EUR',
            'additional_comment' => 'Перевозка животных и велосипедов в собранном состоянии разрешена, доплата - уточняйте у диспетчера',
            'return_comment' => ' ',
            'agreement_comment' => ' ',
            'map_comment' => 'Указанное место отправления на карте может отличаться от места,указанного в билете. Место отправления, указанное в билете имеет приоритет. В случае обнаружения неточности, сообщите администратору системы по контактам Call Centre',
        ]);
        // Настройки Возврата Билетов
        Schema::create('returns', function (Blueprint $table) {
            $table->increments('id');
            $table->string('interval');
            $table->integer('factor')->default(0);
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('routes');
        Schema::dropIfExists('routes_names');
        Schema::dropIfExists('route_points');
        Schema::dropIfExists('points');
        Schema::dropIfExists('points_names');
        Schema::dropIfExists('route_prices');
        Schema::dropIfExists('trips');
        Schema::dropIfExists('trip_point_blocked');
        Schema::dropIfExists('schedules');
        Schema::dropIfExists('tickets');
        Schema::dropIfExists('clients');

        // Справочники --------------------------------------------
        Schema::dropIfExists('langs');
        Schema::dropIfExists('currencies');
        Schema::dropIfExists('buses');
        Schema::dropIfExists('schemes');
        Schema::dropIfExists('places');
        Schema::dropIfExists('drivers');
        Schema::dropIfExists('driver_phones');
        Schema::dropIfExists('dispatcher_phones');
        Schema::dropIfExists('agents');
        Schema::dropIfExists('ticket_types');

        Schema::dropIfExists('settings');
        Schema::dropIfExists('returns');
    }
}
