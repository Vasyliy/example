<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddClientNameForTickets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tickets', function(Blueprint $table){
            $table->string('name1')->nullable()->after('client_id');
            $table->string('name2')->nullable()->after('name1');
            $table->string('name3')->nullable()->after('name2');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $table->dropColumn("name3");
        $table->dropColumn("name2");
        $table->dropColumn("name1");
    }
}
