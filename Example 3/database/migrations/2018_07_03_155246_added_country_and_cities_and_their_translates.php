<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddedCountryAndCitiesAndTheirTranslates extends Migration
{
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
        Schema::create('countries_names', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('item_id');
            $table->integer('lang_id');
            $table->string('name');
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
        Schema::create('cities', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('country_id');
            $table->string('name');
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
        Schema::create('cities_names', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('item_id');
            $table->integer('lang_id');
            $table->string('name');
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('countries');
        Schema::dropIfExists('countries_names');
        Schema::dropIfExists('cities');
        Schema::dropIfExists('cities_names');
    }
}
