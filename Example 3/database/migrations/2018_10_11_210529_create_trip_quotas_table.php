<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTripQuotasTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trip_quotas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('trip_id')->nullable()->unsigned();
            $table->integer('agent_id')->nullable()->unsigned();
            $table->integer('amount')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('trip_quotas');
    }
}
