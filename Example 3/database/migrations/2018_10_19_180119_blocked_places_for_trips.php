<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BlockedPlacesForTrips extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $from = \Carbon\Carbon::today()->addDays(3);
        $routes = \App\Models\Route::select('id')->get();
        foreach ($routes as $route) {
            $trips = \App\Models\Trip::where('date', '>', $from)->where('route_id', $route->id)->select('id')->get();
            $blockedPlaces = \App\Models\RouteBlockedPlace::where('route_id', $route->id)->select('id', 'place_id')->get();
            foreach ($blockedPlaces as $blockedPlace) {
                foreach ($trips as $trip) {
                    \App\Models\Ticket::firstOrCreate(
                        ['trip_id' => $trip->id, 'place_id' => $blockedPlace->place_id],
                        [
                            'from_point_id' => $route->getFromPoint()->from_point_id,
                            'to_point_id' => $route->getToPoint()->to_point_id,
                            'agent_id' => 1,
                            'type_id' => 1,
                            'variant_id' => 2,
                        ]
                    );
                }
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
