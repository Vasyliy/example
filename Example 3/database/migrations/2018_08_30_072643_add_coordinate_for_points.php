<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCoordinateForPoints extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('points', function(Blueprint $table){
            $table->float("lat", 18, 15)->default(49.23725926207273)->after('name');
            $table->float("lng", 18, 15)->default(28.40399800811906)->after('lat');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('points', function (Blueprint $table) {
            $table->dropColumn("lng");
            $table->dropColumn("lat");
        });
    }
}
