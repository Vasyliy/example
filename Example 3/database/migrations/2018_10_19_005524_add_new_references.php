<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewReferences extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('route_points', function (Blueprint $table) {
            $table->unsignedInteger('from_point_id')->nullable()->change();
            $table->unsignedInteger('to_point_id')->nullable()->change();
            $table->unsignedInteger('route_id')->nullable()->change();
            $table->unsignedSmallInteger('number')->default(0)->change();
            $table->foreign('from_point_id')->references('id')->on('points')->onDelete('cascade');
            $table->foreign('to_point_id')->references('id')->on('points')->onDelete('cascade');
            $table->foreign('route_id')->references('id')->on('routes')->onDelete('cascade');
            $table->index(['number']);
        });
        Schema::table('trips', function (Blueprint $table) {
            $table->unsignedInteger('route_id')->nullable()->change();
            $table->unsignedInteger('scheme_id')->nullable()->default(null)->change();
            $table->unsignedInteger('bus_id')->nullable()->default(null)->change();
            $table->unsignedInteger('driver_id')->nullable()->default(null)->change();
        });
        \App\Models\Trip::withTrashed()->where('route_id', 0)->update(['route_id' => null]);
        \App\Models\Trip::withTrashed()->where('scheme_id', 0)->update(['scheme_id' => null]);
        \App\Models\Trip::withTrashed()->where('bus_id', 0)->update(['bus_id' => null]);
        \App\Models\Trip::withTrashed()->where('driver_id', 0)->update(['driver_id' => null]);
        Schema::table('trips', function (Blueprint $table) {
            $table->unsignedSmallInteger('status')->default(0)->change();
            $table->foreign('route_id')->references('id')->on('routes')->onDelete('cascade');
            $table->foreign('scheme_id')->references('id')->on('schemes')->onDelete('cascade');
            $table->foreign('bus_id')->references('id')->on('buses')->onDelete('cascade');
            $table->foreign('driver_id')->references('id')->on('drivers')->onDelete('cascade');
            $table->index(['status']);
        });
        Schema::table('places', function (Blueprint $table) {
            $table->unsignedInteger('scheme_id')->nullable()->default(null)->change();
            $table->unsignedSmallInteger('name')->change();
            $table->unsignedSmallInteger('x')->default(0)->change();
            $table->unsignedSmallInteger('y')->default(0)->change();
            $table->unsignedSmallInteger('status')->change();
            $table->foreign('scheme_id')->references('id')->on('schemes')->onDelete('cascade');
            $table->index(['status', 'name']);
        });
        Schema::table('tickets', function (Blueprint $table) {
            $table->unsignedInteger('client_id')->nullable()->default(null)->change();
        });
        \App\Models\Ticket::withTrashed()->where('client_id', 0)->update(['client_id' => null]);
        Schema::table('tickets', function (Blueprint $table) {
            $table->unsignedInteger('trip_id')->nullable()->change();
            $table->unsignedInteger('from_point_id')->nullable()->change();
            $table->unsignedInteger('to_point_id')->nullable()->change();
            $table->unsignedInteger('place_id')->nullable()->change();
            $table->unsignedInteger('agent_id')->nullable()->default(null)->change();
            $table->unsignedInteger('type_id')->nullable()->change();
            $table->unsignedSmallInteger('variant_id')->default(0)->change();
            $table->unsignedSmallInteger('currency_id')->change();
            $table->foreign('trip_id')->references('id')->on('trips')->onDelete('cascade');
            $table->foreign('from_point_id')->references('id')->on('points')->onDelete('cascade');
            $table->foreign('to_point_id')->references('id')->on('points')->onDelete('cascade');
            $table->foreign('place_id')->references('id')->on('places')->onDelete('cascade');
            $table->foreign('client_id')->references('id')->on('clients')->onDelete('cascade');
            $table->foreign('agent_id')->references('id')->on('agents')->onDelete('cascade');
            $table->foreign('type_id')->references('id')->on('ticket_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
