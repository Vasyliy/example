<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Models\Place;

class NewCoordinatesForScheme extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $places = [
            [34, 9],
            [34, 38],
            [34, 99],
            [34, 128],
            [69, 9],
            [69, 38],
            [69, 99],
            [69, 128],
            [103, 9],
            [103, 38],
            [103, 99],
            [103, 128],
            [138, 9],
            [138, 38],
            [138, 99],
            [138, 128],
            [172, 9],
            [172, 38],
            [172, 99],
            [172, 128],
            [216, 9],
            [216, 38],
            [250, 9],
            [250, 38],
            [250, 99],
            [250, 128],
            [285, 9],
            [285, 38],
            [285, 99],
            [285, 128],
            [319, 9],
            [319, 38],
            [319, 99],
            [319, 128],
            [354, 9],
            [354, 38],
            [354, 99],
            [354, 128],
            [388, 9],
            [388, 38],
            [388, 99],
            [388, 128],
            [423, 9],
            [423, 38],
            [423, 69],
            [423, 99],
            [423, 128],
            [0, 9],
            [0, 38],
            [0, 99],
            [0, 128],
            [216, 99],
            [216, 128],
        ];

        for($i=1;$i<=53;$i++){
            Place::where('id', $i)->update(['x' => $places[$i-1][0], 'y' => $places[$i-1][1]]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
