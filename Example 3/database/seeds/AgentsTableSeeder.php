<?php

use Illuminate\Database\Seeder;
use App\Models\Lang;

class AgentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('agents')->insert([
            'id' => 1,
            'name' => 'Gdamaler',
            'status' => 1,
        ]);
        DB::table('agents_names')->insert([ 'item_id' => 1, 'lang_id' => Lang::ID_LANG_EN, 'name' => 'Gdamaler', ]);
        DB::table('agents_names')->insert([ 'item_id' => 1, 'lang_id' => Lang::ID_LANG_RU, 'name' => 'Gdamaler', ]);
        DB::table('agents_names')->insert([ 'item_id' => 1, 'lang_id' => Lang::ID_LANG_UA, 'name' => 'Gdamaler', ]);
        DB::table('agents_names')->insert([ 'item_id' => 1, 'lang_id' => Lang::ID_LANG_PO, 'name' => 'Gdamaler', ]);
        DB::table('agents_names')->insert([ 'item_id' => 1, 'lang_id' => Lang::ID_LANG_CZ, 'name' => 'Gdamaler', ]);

        DB::table('agents')->insert([
            'id' => 2,
            'name' => 'InfoBus',
            'status' => 1,
        ]);

        DB::table('agents')->insert([
            'id' => 3,
            'name' => 'GillBus',
            'status' => 1,
        ]);

        DB::table('agents')->insert([
            'id' => 4,
            'name' => 'BusCom',
            'status' => 1,
        ]);

        DB::table('agents')->insert([
            'id' => 5,
            'name' => 'MusilTour',
            'status' => 1,
        ]);

        DB::table('agents')->insert([
            'id' => 6,
            'name' => 'DMD',
            'status' => 1,
        ]);

        DB::table('agents')->insert([
            'id' => 7,
            'name' => 'Polonus',
            'status' => 1,
        ]);
    }
}
