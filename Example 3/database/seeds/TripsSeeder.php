<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class TripsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('trips')->insert([
            'id' => 1,
            'route_id' => 1,
            'scheme_id' => 1,
            'bus_id' => 1,
            'driver_id' => 1,
            'phone' => "+380675432121",
            'date' => Carbon::now()->addDays(5),
        ]);
    }
}
