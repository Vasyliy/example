<?php

use Illuminate\Database\Seeder;

class DispatcherPhones extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('dispatcher_phones')->insert([
            'id' => 1,
            'phone' => '+38(095)402-8218',
        ]);
        DB::table('dispatcher_phones')->insert([
            'id' => 2,
            'phone' => '+38(095)402-8214',
        ]);
        DB::table('dispatcher_phones')->insert([
            'id' => 3,
            'phone' => '+38(095)402-8213',
        ]);
        DB::table('dispatcher_phones')->insert([
            'id' => 4,
            'phone' => '+38(095)402-8212',
        ]);
    }
}
