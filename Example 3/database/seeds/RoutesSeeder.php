<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class RoutesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('routes')->insert([
            'id' => 1,
            'scheme_id' => 1,
            'name' => 'Маршрут Винница Львов',
            'started' => Carbon::now()->setTime(0,0,0),
            'finished' => Carbon::now()->addMonth(2),
            'status' => \App\Models\Route::STATUS_ACTIVE,
        ]);
        // Заполнение маршрута
            DB::table('route_points')->insert([
                'id' => 1,
                'route_id' => 1,
                'from_point_id' => 1,
                'number' => 1,
                'to_point_id' => 2,
                'price' => 10000,
                'distance' => 10000,
                'from_time' =>  0, // 0h
                'to_time' => 2 * 60 * 60, // 2h0m,
            ]);
            DB::table('route_points')->insert([
                'id' => 2,
                'route_id' => 1,
                'from_point_id' => 2,
                'to_point_id' => 3,
                'number' => 2,
                'price' => 10000,
                'distance' => 10000,
                'from_time' =>  2 * 60 * 60 + 5 * 60, // 2h5m
                'to_time' => 4 * 60 * 60, // 4h,
            ]);
            DB::table('route_points')->insert([
                'id' => 3,
                'route_id' => 1,
                'from_point_id' => 3,
                'to_point_id' => 4,
                'number' => 3,
                'price' => 10000,
                'distance' => 10000,
                'from_time' =>  4 * 60 * 60 + 10 * 60, // 4h10m
                'to_time' => 6 * 60 * 60, // 6h,
            ]);
        // Заполнение прейскуранта
        DB::table('route_prices')->insert([
            'id' => 1,
            'route_id' => 1,
            'from_point_id' => 1,
            'to_point_id' => 2,
            'price' => 10000,
            'currency_id' => 1,
        ]);
        DB::table('route_prices')->insert([
            'id' => 2,
            'route_id' => 1,
            'from_point_id' => 1,
            'to_point_id' => 3,
            'price' => 10000,
            'currency_id' => 1,
        ]);
        DB::table('route_prices')->insert([
            'id' => 3,
            'route_id' => 1,
            'from_point_id' => 2,
            'to_point_id' => 3,
            'price' => 12300,
            'currency_id' => 1,
        ]);
    }
}
