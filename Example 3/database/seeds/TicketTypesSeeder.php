<?php

use Illuminate\Database\Seeder;
use App\Models\Lang;

class TicketTypesSeeder extends Seeder
{
    public function run()
    {
        DB::table('ticket_types')->insert([
            'id' => 1,
            'name' => 'Полный',
            'discount' => 0,
        ]);
        DB::table('ticket_types_names')->insert([ 'item_id' => 1, 'lang_id' => Lang::ID_LANG_EN, 'name' => 'Полный', ]);
        DB::table('ticket_types_names')->insert([ 'item_id' => 1, 'lang_id' => Lang::ID_LANG_RU, 'name' => 'Полный', ]);
        DB::table('ticket_types_names')->insert([ 'item_id' => 1, 'lang_id' => Lang::ID_LANG_UA, 'name' => 'Полный', ]);
        DB::table('ticket_types_names')->insert([ 'item_id' => 1, 'lang_id' => Lang::ID_LANG_PO, 'name' => 'Full', ]);
        DB::table('ticket_types_names')->insert([ 'item_id' => 1, 'lang_id' => Lang::ID_LANG_CZ, 'name' => 'Full', ]);

        DB::table('ticket_types')->insert([
            'id' => 2,
            'name' => 'Пенсионеры от 60 лет',
            'discount' => 5,
        ]);
        DB::table('ticket_types_names')->insert([ 'item_id' => 2, 'lang_id' => Lang::ID_LANG_EN, 'name' => 'Пенсионеры от 60 лет', ]);
        DB::table('ticket_types_names')->insert([ 'item_id' => 2, 'lang_id' => Lang::ID_LANG_RU, 'name' => 'Пенсионеры от 60 лет', ]);
        DB::table('ticket_types_names')->insert([ 'item_id' => 2, 'lang_id' => Lang::ID_LANG_UA, 'name' => 'Пенсионеры от 60 лет', ]);
        DB::table('ticket_types_names')->insert([ 'item_id' => 2, 'lang_id' => Lang::ID_LANG_PO, 'name' => 'Пенсионеры от 60 лет', ]);
        DB::table('ticket_types_names')->insert([ 'item_id' => 2, 'lang_id' => Lang::ID_LANG_CZ, 'name' => 'Пенсионеры от 60 лет', ]);

        DB::table('ticket_types')->insert([
            'id' => 3,
            'name' => 'Студенты до 26 лет',
            'discount' => 10,
        ]);
        DB::table('ticket_types_names')->insert([ 'item_id' => 3, 'lang_id' => Lang::ID_LANG_EN, 'name' => 'Студенты до 26 ле', ]);
        DB::table('ticket_types_names')->insert([ 'item_id' => 3, 'lang_id' => Lang::ID_LANG_RU, 'name' => 'Студенты до 26 ле', ]);
        DB::table('ticket_types_names')->insert([ 'item_id' => 3, 'lang_id' => Lang::ID_LANG_UA, 'name' => 'Студенты до 26 ле', ]);
        DB::table('ticket_types_names')->insert([ 'item_id' => 3, 'lang_id' => Lang::ID_LANG_PO, 'name' => 'Студенты до 26 ле', ]);
        DB::table('ticket_types_names')->insert([ 'item_id' => 3, 'lang_id' => Lang::ID_LANG_CZ, 'name' => 'Студенты до 26 ле', ]);
        
        DB::table('ticket_types')->insert([
            'id' => 4,
            'name' => 'Дети до 12 лет',
            'discount' => 50,
        ]);
        DB::table('ticket_types_names')->insert([ 'item_id' => 4, 'lang_id' => Lang::ID_LANG_EN, 'name' => 'Дети до 42 лет', ]);
        DB::table('ticket_types_names')->insert([ 'item_id' => 4, 'lang_id' => Lang::ID_LANG_RU, 'name' => 'Дети до 42 лет', ]);
        DB::table('ticket_types_names')->insert([ 'item_id' => 4, 'lang_id' => Lang::ID_LANG_UA, 'name' => 'Дети до 42 лет', ]);
        DB::table('ticket_types_names')->insert([ 'item_id' => 4, 'lang_id' => Lang::ID_LANG_PO, 'name' => 'Дети до 42 лет', ]);
        DB::table('ticket_types_names')->insert([ 'item_id' => 4, 'lang_id' => Lang::ID_LANG_CZ, 'name' => 'Дети до 42 лет', ]);
    }
}
