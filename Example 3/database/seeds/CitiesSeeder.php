<?php

use Illuminate\Database\Seeder;
use App\Models\Lang;

class CitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cities')->insert([
            'id' => 1,
            'country_id' => 1,
            'name' => 'Винница',
        ]);
        DB::table('cities_names')->insert([ 'item_id' => 1, 'lang_id' => Lang::ID_LANG_EN, 'name' => 'Vinnytca', ]);
        DB::table('cities_names')->insert([ 'item_id' => 1, 'lang_id' => Lang::ID_LANG_RU, 'name' => 'Винница', ]);
        DB::table('cities_names')->insert([ 'item_id' => 1, 'lang_id' => Lang::ID_LANG_UA, 'name' => 'Винница', ]);
        DB::table('cities_names')->insert([ 'item_id' => 1, 'lang_id' => Lang::ID_LANG_PO, 'name' => 'Винница', ]);
        DB::table('cities_names')->insert([ 'item_id' => 1, 'lang_id' => Lang::ID_LANG_CZ, 'name' => 'Винница', ]);

        DB::table('cities')->insert([
            'id' => 2,
            'country_id' => 1,
            'name' => 'Хмельницкий',
        ]);
        DB::table('cities_names')->insert([ 'item_id' => 2, 'lang_id' => Lang::ID_LANG_EN, 'name' => 'Khmelnitskiy', ]);
        DB::table('cities_names')->insert([ 'item_id' => 2, 'lang_id' => Lang::ID_LANG_RU, 'name' => 'Хмельницкий', ]);
        DB::table('cities_names')->insert([ 'item_id' => 2, 'lang_id' => Lang::ID_LANG_UA, 'name' => 'Хмельницкий', ]);
        DB::table('cities_names')->insert([ 'item_id' => 2, 'lang_id' => Lang::ID_LANG_PO, 'name' => 'Хмельницкий', ]);
        DB::table('cities_names')->insert([ 'item_id' => 2, 'lang_id' => Lang::ID_LANG_CZ, 'name' => 'Хмельницкий', ]);

        DB::table('cities')->insert([
            'id' => 3,
            'country_id' => 1,
            'name' => 'Тернополь',
        ]);
        DB::table('cities_names')->insert([ 'item_id' => 3, 'lang_id' => Lang::ID_LANG_EN, 'name' => 'Тернополь', ]);
        DB::table('cities_names')->insert([ 'item_id' => 3, 'lang_id' => Lang::ID_LANG_RU, 'name' => 'Тернополь', ]);
        DB::table('cities_names')->insert([ 'item_id' => 3, 'lang_id' => Lang::ID_LANG_UA, 'name' => 'Тернополь', ]);
        DB::table('cities_names')->insert([ 'item_id' => 3, 'lang_id' => Lang::ID_LANG_PO, 'name' => 'Тернополь', ]);
        DB::table('cities_names')->insert([ 'item_id' => 3, 'lang_id' => Lang::ID_LANG_CZ, 'name' => 'Тернополь', ]);

        DB::table('cities')->insert([
            'id' => 4,
            'country_id' => 1,
            'name' => 'Львов',
        ]);
        DB::table('cities_names')->insert([ 'item_id' => 3, 'lang_id' => Lang::ID_LANG_EN, 'name' => 'Львов', ]);
        DB::table('cities_names')->insert([ 'item_id' => 3, 'lang_id' => Lang::ID_LANG_RU, 'name' => 'Львов', ]);
        DB::table('cities_names')->insert([ 'item_id' => 3, 'lang_id' => Lang::ID_LANG_UA, 'name' => 'Львов', ]);
        DB::table('cities_names')->insert([ 'item_id' => 3, 'lang_id' => Lang::ID_LANG_PO, 'name' => 'Львов', ]);
        DB::table('cities_names')->insert([ 'item_id' => 3, 'lang_id' => Lang::ID_LANG_CZ, 'name' => 'Львов', ]);
    }
}
