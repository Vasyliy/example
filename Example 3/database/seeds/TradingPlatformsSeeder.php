<?php

use Illuminate\Database\Seeder;

class TradingPlatformsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('trading_platforms')->insert([
            'name' => 'bus.com.ua',
            'refresh_time_1' => '5',
            'refresh_time_2' => '15',
            'refresh_time_3' => '60'
        ]);


        DB::table('trading_platform_points')->insert([
            'trading_platform_id' => '1',
            'point_id' => '6',
            'name' => 'ВІННИЦЯ 1'
        ]);
        DB::table('trading_platform_points')->insert([
            'trading_platform_id' => '1',
            'point_id' => '1',
            'name' => 'ВІННИЦЯ 2'
        ]);
        DB::table('trading_platform_points')->insert([
            'trading_platform_id' => '1',
            'point_id' => '3',
            'name' => 'ХМЕЛЬНИЦЬКИЙ'
        ]);
        DB::table('trading_platform_points')->insert([
            'trading_platform_id' => '1',
            'point_id' => '26',
            'name' => 'РІВНЕ'
        ]);
        DB::table('trading_platform_points')->insert([
            'trading_platform_id' => '1',
            'point_id' => '35',
            'name' => 'ЛЮБЛІН'
        ]);
        DB::table('trading_platform_points')->insert([
            'trading_platform_id' => '1',
            'point_id' => '38',
            'name' => 'ВАРШАВА'
        ]);
        DB::table('trading_platform_points')->insert([
            'trading_platform_id' => '1',
            'point_id' => '39',
            'name' => 'ЛОДЗЬ'
        ]);
        DB::table('trading_platform_points')->insert([
            'trading_platform_id' => '1',
            'point_id' => '40',
            'name' => 'ПОЗНАНЬ'
        ]);
        DB::table('trading_platform_points')->insert([
            'trading_platform_id' => '1',
            'point_id' => '41',
            'name' => 'ЗЕЛЕНА-ГУРА'
        ]);


        DB::table('agent_trading_platforms')->insert([
            'agent_id' => '4',
            'trading_platform_id' => '1',
            'options' => '{"username":"\u0413\u0420\u0415\u0427\u0410\u041d\u042e\u041a","password":"\u0413\u0420\u0415\u0427\u0410\u041d\u042e\u041a","agent_number":"050100"}'
        ]);
        DB::table('agent_trading_platforms')->insert([
            'agent_id' => '5',
            'trading_platform_id' => '1',
            'options' => '{"username":"\u0413\u0420\u0415\u0427\u0410\u041d\u042e\u041a","password":"\u0413\u0420\u0415\u0427\u0410\u041d\u042e\u041a","agent_number":"050200"}'
        ]);
        DB::table('agent_trading_platforms')->insert([
            'agent_id' => '8',
            'trading_platform_id' => '1',
            'options' => '{"username":"\u0413\u0420\u0415\u0427\u0410\u041d\u042e\u041a","password":"\u0413\u0420\u0415\u0427\u0410\u041d\u042e\u041a","agent_number":"560100"}'
        ]);
        DB::table('agent_trading_platforms')->insert([
            'agent_id' => '9',
            'trading_platform_id' => '1',
            'options' => '{"username":"\u0413\u0420\u0415\u0427\u0410\u041d\u042e\u041a","password":"\u0413\u0420\u0415\u0427\u0410\u041d\u042e\u041a","agent_number":"610100"}'
        ]);
        DB::table('agent_trading_platforms')->insert([
            'agent_id' => '10',
            'trading_platform_id' => '1',
            'options' => '{"username":"\u0413\u0420\u0415\u0427\u0410\u041d\u042e\u041a","password":"\u0413\u0420\u0415\u0427\u0410\u041d\u042e\u041a","agent_number":"460100"}'
        ]);
        DB::table('agent_trading_platforms')->insert([
            'agent_id' => '6',
            'trading_platform_id' => '1',
            'options' => '{"username":"90679","password":"90679","agent_number":"680100"}'
        ]);


        DB::table('trading_platform_routes')->insert([
            'agent_trading_platform_id' => '1',
            'route_id' => '6',
            'name' => '51БН'
        ]);
        DB::table('trading_platform_routes')->insert([
            'agent_trading_platform_id' => '1',
            'route_id' => '2',
            'name' => '8БНВ'
        ]);
        DB::table('trading_platform_routes')->insert([
            'agent_trading_platform_id' => '2',
            'route_id' => '2',
            'name' => '0001'
        ]);
        DB::table('trading_platform_routes')->insert([
            'agent_trading_platform_id' => '2',
            'route_id' => '6',
            'name' => '0007'
        ]);
        DB::table('trading_platform_routes')->insert([
            'agent_trading_platform_id' => '3',
            'route_id' => '5',
            'name' => '232'
        ]);
        DB::table('trading_platform_routes')->insert([
            'agent_trading_platform_id' => '3',
            'route_id' => '6',
            'name' => '941'
        ]);
        DB::table('trading_platform_routes')->insert([
            'agent_trading_platform_id' => '3',
            'route_id' => '2',
            'name' => '231'
        ]);
        DB::table('trading_platform_routes')->insert([
            'agent_trading_platform_id' => '4',
            'route_id' => '9',
            'name' => '2МНК'
        ]);
        DB::table('trading_platform_routes')->insert([
            'agent_trading_platform_id' => '5',
            'route_id' => '9',
            'name' => '0479'
        ]);
        DB::table('trading_platform_routes')->insert([
            'agent_trading_platform_id' => '6',
            'route_id' => '6',
            'name' => '00031'
        ]);
        DB::table('trading_platform_routes')->insert([
            'agent_trading_platform_id' => '6',
            'route_id' => '2',
            'name' => '0103'
        ]);


        DB::table('trading_platform_ticket_variants')->insert([
            'trading_platform_id' => '1',
            'variant_id' => '0',
            'name' => 'ОБЩ'
        ]);
        DB::table('trading_platform_ticket_variants')->insert([
            'trading_platform_id' => '1',
            'variant_id' => '1',
            'name' => 'БРН'
        ]);
        DB::table('trading_platform_ticket_variants')->insert([
            'trading_platform_id' => '1',
            'variant_id' => '3',
            'name' => 'СВБ, СДН'
        ]);
    }
}
