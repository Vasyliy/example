<?php

use Illuminate\Database\Seeder;
use App\Models\Lang;

class CurrenciesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('currencies')->insert([
            'id' => 1,
            'name' => 'Украинская гривна',
            'short' => 'UAH',
            'symbol' => '₴',
            'exchange' => 100,
        ]);
        DB::table('currencies_names')->insert([ 'item_id' => 1, 'lang_id' => Lang::ID_LANG_EN, 'name' => 'Ukraine hrivna', ]);
        DB::table('currencies_names')->insert([ 'item_id' => 1, 'lang_id' => Lang::ID_LANG_RU, 'name' => 'Украинская гривна', ]);
        DB::table('currencies_names')->insert([ 'item_id' => 1, 'lang_id' => Lang::ID_LANG_UA, 'name' => 'Українська гривня', ]);
        DB::table('currencies_names')->insert([ 'item_id' => 1, 'lang_id' => Lang::ID_LANG_PO, 'name' => 'Ukraine po', ]);
        DB::table('currencies_names')->insert([ 'item_id' => 1, 'lang_id' => Lang::ID_LANG_CZ, 'name' => 'Ukraine cz', ]);

        DB::table('currencies')->insert([
            'id' => 2,
            'name' => 'Польский злотый',
            'short' => 'PLN',
            'symbol' => 'zł',
            'exchange' => 100,
        ]);
        DB::table('currencies_names')->insert([ 'item_id' => 2, 'lang_id' => Lang::ID_LANG_EN, 'name' => 'Польский злотый', ]);
        DB::table('currencies_names')->insert([ 'item_id' => 2, 'lang_id' => Lang::ID_LANG_RU, 'name' => 'Польский злотый', ]);
        DB::table('currencies_names')->insert([ 'item_id' => 2, 'lang_id' => Lang::ID_LANG_UA, 'name' => 'Польский злотий', ]);
        DB::table('currencies_names')->insert([ 'item_id' => 2, 'lang_id' => Lang::ID_LANG_PO, 'name' => 'Польский злотый', ]);
        DB::table('currencies_names')->insert([ 'item_id' => 2, 'lang_id' => Lang::ID_LANG_CZ, 'name' => 'Польский злотый', ]);

        DB::table('currencies')->insert([
            'id' => 3,
            'name' => 'Чешская крона',
            'short' => 'CZK',
            'symbol' => 'Kč',
            'exchange' => 100,
        ]);
        DB::table('currencies_names')->insert([ 'item_id' => 3, 'lang_id' => Lang::ID_LANG_EN, 'name' => 'Чешская крона', ]);
        DB::table('currencies_names')->insert([ 'item_id' => 3, 'lang_id' => Lang::ID_LANG_RU, 'name' => 'Чешская крона', ]);
        DB::table('currencies_names')->insert([ 'item_id' => 3, 'lang_id' => Lang::ID_LANG_UA, 'name' => 'Чешская крона', ]);
        DB::table('currencies_names')->insert([ 'item_id' => 3, 'lang_id' => Lang::ID_LANG_PO, 'name' => 'Чешская крона', ]);
        DB::table('currencies_names')->insert([ 'item_id' => 3, 'lang_id' => Lang::ID_LANG_CZ, 'name' => 'Чешская крона', ]);
    }
}
