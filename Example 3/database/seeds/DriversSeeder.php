<?php

use Illuminate\Database\Seeder;

class DriversSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('drivers')->insert([
            'id' => 1,
            'name' => 'Иванов Иван Иванович',
            'license' => 1,
            'note' => 'Один из лучших водителей',
            'status' => 1,
        ]);
    }
}
