<?php

use Illuminate\Database\Seeder;

class BusesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('buses')->insert([
            'id' => 1,
            'scheme_id' => 1,
            'name' => 'Икарус №1',
            'model' => 'TEMPLA',
            'number' => '12121212',
            'note' => 'Лучший автобус в мире',
        ]);
    }
}
