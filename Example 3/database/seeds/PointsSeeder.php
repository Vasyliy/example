<?php

use Illuminate\Database\Seeder;

class PointsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('points')->insert([
            'id' => 1,
            'city_id' => 1,
            'name' => 'Винница центральный автовокзал',
        ]);
        DB::table('points')->insert([
            'id' => 2,
            'city_id' => 2,
            'name' => 'Хмельницкий центральный автовокзал',
        ]);
        DB::table('points')->insert([
            'id' => 3,
            'city_id' => 3,
            'name' => 'Тернопольский Центральный автовокзал',
        ]);
        DB::table('points')->insert([
            'id' => 4,
            'city_id' => 4,
            'name' => 'Львовский Центральный автовокзал',
        ]);
    }
}
