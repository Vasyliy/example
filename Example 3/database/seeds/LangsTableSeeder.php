<?php

use Illuminate\Database\Seeder;
use App\Models\Lang;
use Carbon\Carbon;

class LangsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('langs')->insert([
            'id' => Lang::ID_LANG_EN,
            'name' => 'English',
            'short' => 'en',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('langs')->insert([
            'id' => Lang::ID_LANG_RU,
            'name' => 'Русский',
            'short' => 'ru',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('langs')->insert([
            'id' => Lang::ID_LANG_UA,
            'name' => 'Українська',
            'short' => 'ua',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('langs')->insert([
            'id' => Lang::ID_LANG_PO,
            'name' => 'Polish',
            'short' => 'po',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('langs')->insert([
            'id' => Lang::ID_LANG_CZ,
            'name' => 'Czech',
            'short' => 'cz',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }
}
