<?php

use Illuminate\Database\Seeder;

class ReturnsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('returns')->insert([
            'id' => 1,
            'interval' => 'более чем 48 ч.',
            'factor' => 100,
        ]);
        DB::table('returns')->insert([
            'id' => 2,
            'interval' => '48 ч. - 24 ч.',
            'factor' => 75,
        ]);
        DB::table('returns')->insert([
            'id' => 3,
            'interval' => '24 ч. - 12 ч.',
            'factor' => 50,
        ]);
        DB::table('returns')->insert([
            'id' => 4,
            'interval' => '12 ч. - 2 ч.',
            'factor' => 25,
        ]);
        DB::table('returns')->insert([
            'id' => 5,
            'interval' => 'менее 2х ч.',
            'factor' => 0,
        ]);
    }
}
