<?php

use Illuminate\Database\Seeder;
use App\Models\Lang;

class CountriesSeeder extends Seeder
{
    public function run()
    {
        DB::table('countries')->insert([
            'id' => 1,
            'name' => 'Ukraine',
        ]);
        DB::table('countries_names')->insert([ 'item_id' => 1, 'lang_id' => Lang::ID_LANG_EN, 'name' => 'Ukraine', ]);
        DB::table('countries_names')->insert([ 'item_id' => 1, 'lang_id' => Lang::ID_LANG_RU, 'name' => 'Украина', ]);
        DB::table('countries_names')->insert([ 'item_id' => 1, 'lang_id' => Lang::ID_LANG_UA, 'name' => 'Україна', ]);
        DB::table('countries_names')->insert([ 'item_id' => 1, 'lang_id' => Lang::ID_LANG_PO, 'name' => 'Ukraine', ]);
        DB::table('countries_names')->insert([ 'item_id' => 1, 'lang_id' => Lang::ID_LANG_CZ, 'name' => 'Ukraine', ]);

        DB::table('countries')->insert([
            'id' => 2,
            'name' => 'Poland',
        ]);
        DB::table('countries_names')->insert([ 'item_id' => 2, 'lang_id' => Lang::ID_LANG_EN, 'name' => 'Poland', ]);
        DB::table('countries_names')->insert([ 'item_id' => 2, 'lang_id' => Lang::ID_LANG_RU, 'name' => 'Польша', ]);
        DB::table('countries_names')->insert([ 'item_id' => 2, 'lang_id' => Lang::ID_LANG_UA, 'name' => 'Польща', ]);
        DB::table('countries_names')->insert([ 'item_id' => 2, 'lang_id' => Lang::ID_LANG_PO, 'name' => 'Poland', ]);
        DB::table('countries_names')->insert([ 'item_id' => 2, 'lang_id' => Lang::ID_LANG_CZ, 'name' => 'Poland', ]);

        DB::table('countries')->insert([
            'id' => 3,
            'name' => 'Czech Republic',
        ]);
        DB::table('countries_names')->insert([ 'item_id' => 3, 'lang_id' => Lang::ID_LANG_EN, 'name' => 'Czech Republic', ]);
        DB::table('countries_names')->insert([ 'item_id' => 3, 'lang_id' => Lang::ID_LANG_RU, 'name' => 'Чехия', ]);
        DB::table('countries_names')->insert([ 'item_id' => 3, 'lang_id' => Lang::ID_LANG_UA, 'name' => 'Чехія', ]);
        DB::table('countries_names')->insert([ 'item_id' => 3, 'lang_id' => Lang::ID_LANG_PO, 'name' => 'Czech Republic', ]);
        DB::table('countries_names')->insert([ 'item_id' => 3, 'lang_id' => Lang::ID_LANG_CZ, 'name' => 'Czech Republic', ]);
    }
}
