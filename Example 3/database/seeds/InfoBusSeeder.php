<?php

use Illuminate\Database\Seeder;

class InfoBusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $platform = \App\Models\TradingPlatform::create([
            'name' => 'InfoBus',
            'refresh_time_1' => '5',
            'refresh_time_2' => '15',
            'refresh_time_3' => '60',
            'options' => '{"login":"grechanyukdisp6443","password":"dNxBXz"}'
        ]);


        DB::table('trading_platform_points')->insert([
            'trading_platform_id' => $platform->id,
            'point_id' => '44',
            'name' => '3'
        ]);
        DB::table('trading_platform_points')->insert([
            'trading_platform_id' => $platform->id,
            'point_id' => '46',
            'name' => '46'
        ]);
        DB::table('trading_platform_points')->insert([
            'trading_platform_id' => $platform->id,
            'point_id' => '45',
            'name' => '44'
        ]);
        DB::table('trading_platform_points')->insert([
            'trading_platform_id' => $platform->id,
            'point_id' => '43',
            'name' => '103'
        ]);
        DB::table('trading_platform_points')->insert([
            'trading_platform_id' => $platform->id,
            'point_id' => '42',
            'name' => '411'
        ]);
        DB::table('trading_platform_points')->insert([
            'trading_platform_id' => $platform->id,
            'point_id' => '5',
            'name' => '7'
        ]);
        DB::table('trading_platform_points')->insert([
            'trading_platform_id' => $platform->id,
            'point_id' => '6',
            'name' => '14'
        ]);
        DB::table('trading_platform_points')->insert([
            'trading_platform_id' => $platform->id,
            'point_id' => '4',
            'name' => '13'
        ]);
        DB::table('trading_platform_points')->insert([
            'trading_platform_id' => $platform->id,
            'point_id' => '3',
            'name' => '32'
        ]);


        $agentOptions = \App\Models\AgentTradingPlatform::create([
            'agent_id' => '3',
            'trading_platform_id' => $platform->id
        ]);


        DB::table('trading_platform_routes')->insert([
            'agent_trading_platform_id' => $agentOptions->id,
            'route_id' => '6',
            'name' => '6870'
        ]);
        DB::table('trading_platform_routes')->insert([
            'agent_trading_platform_id' => $agentOptions->id,
            'route_id' => '2',
            'name' => '500'
        ]);
        DB::table('trading_platform_routes')->insert([
            'agent_trading_platform_id' => $agentOptions->id,
            'route_id' => '5',
            'name' => '501'
        ]);
        DB::table('trading_platform_routes')->insert([
            'agent_trading_platform_id' => $agentOptions->id,
            'route_id' => '8',
            'name' => '6871'
        ]);
        DB::table('trading_platform_routes')->insert([
            'agent_trading_platform_id' => $agentOptions->id,
            'route_id' => '10',
            'name' => '1985'
        ]);
        DB::table('trading_platform_routes')->insert([
            'agent_trading_platform_id' => $agentOptions->id,
            'route_id' => '9',
            'name' => '1986'
        ]);


        DB::table('trading_platform_ticket_variants')->insert([
            'trading_platform_id' => $platform->id,
            'variant_id' => '0',
            'name' => 'buy'
        ]);
    }
}
