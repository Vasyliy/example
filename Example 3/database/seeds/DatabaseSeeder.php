<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(AgentsTableSeeder::class);
        $this->call(LangsTableSeeder::class);
        $this->call(CurrenciesTableSeeder::class);
        $this->call(CountriesSeeder::class);
        $this->call(CitiesSeeder::class);
        $this->call(TicketTypesSeeder::class);
        $this->call(PointsSeeder::class);
        $this->call(RoutesSeeder::class);
        $this->call(SchemesSeeder::class);
        $this->call(BusesSeeder::class);
        $this->call(DriversSeeder::class);
        $this->call(TripsSeeder::class);
        $this->call(ClientsSeeder::class);
        $this->call(DispatcherPhones::class);
        $this->call(ReturnsSeeder::class);
        $this->call(FirstAdmin::class);
    }
}
