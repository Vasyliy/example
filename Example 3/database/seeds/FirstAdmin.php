<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use Carbon\Carbon;

class FirstAdmin extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table(User::TABLE_NAME)->insert([
            'name' => 'admin',
            'email' => 'gdamalerbus@gmail.com',
            'password' => bcrypt('catch2018'),
            'admin' => 1,
            'role' => User::ROLE_DISPATCHER_ID,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }
}
