<?php

use Illuminate\Database\Seeder;

class ClientsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('clients')->insert([
            'id' => 1,
            'user_id' => 0,
            'name1' => 'Воронцов',
            'name2' => 'Аркадий',
            'name3' => 'Иванович',
            'phone' => '+380671234567',
            'email' => 'test1@gmail.com',
            'comment' => 'Пробное примечание',
        ]);
        DB::table('clients')->insert([
            'id' => 2,
            'user_id' => 0,
            'name1' => 'Чорный',
            'name2' => 'Виталий',
            'name3' => 'Петрович',
            'phone' => '+380677654321',
            'email' => 'test2@gmail.com',
        ]);
    }
}
