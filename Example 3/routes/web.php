<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
  return redirect('home');
});

Route::get('/home', 'HomeController@index');

Auth::routes();

Route::get('/tickets/view/{viewer}', 'TicketController@view');

Route::middleware(['role:'.\App\Models\User::ROLE_SIMPLE_ID.','.\App\Models\User::ROLE_DISPATCHER_ID])->group(function () {
    Route::match(['GET', 'POST'], '/search', 'SearchController@index');
    Route::get('/tickets/checkclients', 'TicketController@checkclients');
    Route::get('/tickets/checkclient', 'TicketController@checkclient');
    Route::get('/tickets/checkplace', 'TicketController@checkPlace');
    Route::get('/tickets/cancel/{id}', 'TicketController@cancel');
    Route::resource('tickets', 'TicketController');
    Route::resource('trips', 'TripController');
    Route::get('/trips/tickets/{trip}', 'TripController@tickets');
    Route::get('/trips/waybill/{trip}', 'TripController@waybill');
    Route::get('/trips/generate/{route}', 'TripController@generate');
    Route::patch('/trips/blockedPlaces/{trip}', 'TripController@updateBlockedPlaces')
        ->name('trips.blockedPlaces.update');

    Route::get('/places/list', 'PlaceController@list');

    Route::get('/clients/list', 'ClientController@list');
    Route::get('/clients/clientsAutocomplite', 'ClientController@clientsAutocomplite');
    Route::resource('clients', 'ClientController');
});

Route::middleware(['role:'.\App\Models\User::ROLE_SIMPLE_ID])->group(function () {
    Route::get('/routes/clone/{route}', 'RouteController@clone');
    Route::get('/routes/pricelist/{route}', 'RouteController@pricelist');
    Route::resource('routes', 'RouteController');
    Route::resource('routeBlockedPlaces', 'RouteBlockedPlaceController');
    Route::resource('routePoints', 'RoutePointController');
    Route::get('points/restore/{id}', 'PointController@restore');
    Route::resource('points', 'PointController');
    Route::get('/routePrices/save', 'RoutePriceController@save');
    Route::get('/routePrices/saveCurrency', 'RoutePriceController@saveCurrency');
    Route::resource('routePrices', 'RoutePriceController');
    Route::resource('schedules', 'ScheduleController', [
        'parameters' => [
            'route' => 'route'
        ]
    ]);

    // Справочники
    Route::resource('agents', 'AgentController');
    Route::resource('langs', 'LangController');
    Route::resource('currencies', 'CurrencyController');
    Route::resource('ticketTypes', 'TicketTypeController', ['trip' => [
        'trip' => 'trip'
    ]]);
    Route::resource('drivers', 'DriverController');
    Route::resource('driverPhones', 'DriverPhoneController', ['parameters' => [
        'driverPhones' => 'driver'
    ]]);
    Route::resource('buses', 'BusController');
    Route::resource('countries', 'CountryController');
    Route::resource('cities', 'CityController');
    Route::resource('places', 'PlaceController');

    // Вспомогательные справочники (без UI)
    Route::resource('schemes', 'SchemeController');

    Route::get('/tripPointBlockeds/block/{trip}/{point}', 'TripPointBlockedController@block');
    Route::resource('tripPointBlockeds', 'TripPointBlockedController');

    Route::resource('dispatcherPhones', 'DispatcherPhoneController');

    Route::resource('settings', 'SettingsController');
    Route::get('/settings/general/{id}/edit', 'SettingsController@editGeneral')
        ->name('settings.general.edit');
    Route::patch('/settings/general/{id}/update', 'SettingsController@updateGeneral')
        ->name('settings.general.update');

    Route::resource('returns', 'ReturnsController');

    Route::resource('users', 'UserController');

    Route::resource('tradingPlatformPoints', 'TradingPlatformPointController', ['only' => [
        'store', 'destroy'
    ]]);
    Route::resource('tradingPlatformTicketVariants', 'TradingPlatformTicketVariantController', ['only' => [
        'store', 'update', 'destroy'
    ]]);
    Route::resource('tradingPlatformRoutes', 'TradingPlatformRoutesController', ['only' => [
        'store', 'destroy'
    ]]);
    Route::resource('tradingPlatforms', 'TradingPlatformController', ['only' => [
        'store', 'update', 'edit', 'destroy', 'create', 'index'
    ]]);
    Route::resource('agentTradingPlatforms', 'AgentTradingPlatformController');
    Route::resource('routeQuotas', 'RouteQuotaController', ['only' => [
        'store'
    ]]);
    Route::resource('tripQuotas', 'TripQuotaController', ['only' => [
        'store'
    ]]);
    Route::resource('activities', 'ActivityController', ['only' => [
        'index'
    ]]);
    Route::resource('reports', 'ReportController', ['only' => [
        'index'
    ]]);
});